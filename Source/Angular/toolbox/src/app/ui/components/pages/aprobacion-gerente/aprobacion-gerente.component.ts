import { Component, OnInit } from '@angular/core';
import { TablaBasicaDTO } from 'src/app/domain/model/dto/TablaBasicaDTO';
import {TablasBasicasService} from '../../../../infraestructure/services/tablasBasicas.service';
import {codigosParametros} from 'src/environments/environment.variables';

@Component({
  selector: 'app-aprobacion-gerente',
  templateUrl: './aprobacion-gerente.component.html',
  styleUrls: ['./aprobacion-gerente.component.css']
})
export class AprobacionGerenteComponent implements OnInit {
    siONo: TablaBasicaDTO[];
    classification:  TablaBasicaDTO[];
    selectedSiONo: TablaBasicaDTO;
    selectClassification: TablaBasicaDTO;

  constructor(private tb: TablasBasicasService) { }

  ngOnInit() {
      //this.siONo = this.tb.consultarTablaBasica(codigosParametros.siNo);
      //this.classification = this.tb.consultarTablaBasica(codigosParametros.clasificacionIncapacidad);
  }

}
