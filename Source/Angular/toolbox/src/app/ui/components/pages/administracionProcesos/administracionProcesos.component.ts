import { ChangeDetectorRef, Component, OnInit, ViewRef } from '@angular/core';
import { Router } from '@angular/router';
import { ConfirmationService } from 'primeng/primeng';
import { ProcesoDTO } from 'src/app/domain/model/dto/ProcesoDTO';
import { ContenedorDTO } from 'src/app/domain/model/dto/ContenedorDTO';
import { ProcessRequestDTO } from 'src/app/domain/model/dto/ProcessRequestDTO';
import { LoginService } from 'src/app/infraestructure/services/login.service';
import { ContainerService, ProcessService } from 'src/app/infraestructure/services/service.index';
import { AlertService } from 'src/app/infraestructure/services/alertService.service';
import { successResponse } from 'src/environments/environment.variables';
import { ROUTES_PATH } from '../../pages.routing.name';

@Component({
  selector: 'app-administracionProcesos',
  templateUrl: './administracionProcesos.component.html'
})
export class AdministracionProcesosComponent implements OnInit {

  procesos: ProcesoDTO[];
  listaContenedor: ContenedorDTO[];
  contenedorSelect: ContenedorDTO;
  request = new ProcessRequestDTO;

  constructor(
    private contenedorService: ContainerService,
    private procesoService: ProcessService,
    private cd: ChangeDetectorRef,
    private router: Router,
    private alertService: AlertService,
    private confirmationService: ConfirmationService,
    private loginService: LoginService
  ) {
    this.loginService.currentUser.subscribe(x => this.request.ownerUser = x);
  }

  ngOnInit() {
    this.cargaContenedores();
  }

  detectChanges() {
    setTimeout(() => {
      if (!(this.cd as ViewRef).destroyed) {
        this.cd.detectChanges();
      }
    }, 250);
  }

  cargaContenedores() {
    this.contenedorService.consultarContenedores(this.request)
      .subscribe(data => {
        if (!!data)
          this.listaContenedor = data.containers;
      }, error => this.alertService.error(error.message),
        () => this.detectChanges()
      );
  }

  buscarProcesos(contenedor: ContenedorDTO) {
    this.request.containerId = contenedor.containerId;
    this.procesoService.consultarProcesos(this.request)
      .subscribe(data => {
        if (!!data && !!data.containers) {
          data.containers.forEach(element => {
            if (element.containerId == contenedor.containerId)
              this.procesos = element.processes;
          });
        }
      }, error => this.alertService.error(error.message),
        () => this.detectChanges()
      );
  }

  iniciarProceso(proceso: ProcesoDTO) {
    this.request.processesId = proceso.processId;
    this.procesoService.iniciarProceso(this.request).subscribe(data => {
        if (!!data.response && data.response.statusCode == successResponse.statusCode) {
          const aux = data.containers[0].processes;
          for (let index = 0; index < aux.length; index++) {
            if (aux[index].processId == proceso.processId) {
              this.irListaTareas(aux[index]);;
              break;
            }
          }
        }
      }, error => this.alertService.error(error.message),
        () => this.detectChanges()
      );
  }

  irListaTareas(data: ProcesoDTO) {
    this.confirmationService.confirm({
      message: 'Numero de Instacia Creado: ' + data.instance.instanceId,
      header: 'Confirmar Solicitud',
      rejectVisible: false,
      accept: () => {
        this.router.navigate(['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.task]);
      }
    });
  }

}
