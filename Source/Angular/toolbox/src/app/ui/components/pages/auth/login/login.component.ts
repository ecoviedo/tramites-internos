import { Component, OnInit } from '@angular/core';
import { UserDTO } from '../../../../../domain/model/dto/UserDTO';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from '../../../../../infraestructure/services/login.service';
import { first } from 'rxjs/operators';
import { ROUTES_PATH } from '../../../pages.routing.name';
import { HttpHeaders } from '@angular/common/http';
import { successResponse } from 'src/environments/environment.variables';
import { MessageService } from 'primeng/components/common/messageservice';
import { Message } from 'primeng/components/common/api';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
    providers: [MessageService]
})
export class LoginComponent implements OnInit {

    form: FormGroup;
    msgs: Message[] = [];

    params = {};
    showPassword = false;
    classShowPassword = "glyphicon-eye-open";
    password: string;
    showProgressBar = false;

    constructor(private formBuilder: FormBuilder,
        private router: Router,
        private messageService: MessageService,
        private loginService: LoginService) {
        if (this.loginService.currentUserValue) {
            this.router.navigate(['/']);
        }
    }

    ngOnInit() {
        this.form = this.formBuilder.group({
            username: ['', Validators.required],
            fomrPassword: ['', Validators.required]
        });
    }

    submit() {
        this.showProgressBar = true;
        if (this.form.invalid) {
            return;
        }
        const user = new UserDTO();
        user.user = this.form.get('username').value;
        user.password = this.form.get('fomrPassword').value;

        let authorizationData = 'Basic ' + btoa(this.form.get('username').value + ':' + this.form.get('fomrPassword').value);
        let headerOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': authorizationData
            })
        };

        this.loginService.loginDos(headerOptions).pipe(first())
            .subscribe(data => {
                if (!!data.response && data.response.statusCode == successResponse.statusCode) {
                    const usuario = new UserDTO();
                    usuario.name = data.usuario;
                    usuario.user = user.user;
                    usuario.password = user.password;
                    usuario.email = data.mail;
                    this.consultarRol(usuario);
                    
                }else if (data == ""){
                    this.msgs = [];
                    this.msgs.push({ severity: 'error', summary: 'Error: ', detail: 'Ocurrió un problema técnico' });
                    this.showProgressBar = false;
                }else{
                    this.msgs = [];
                    this.msgs.push({ severity: 'error', summary: 'Error: ', detail: 'Usuario o contraseña incorrecta' });
                    this.showProgressBar = false;

                }
            }, error => console.log(error)
            );

    }

    private consultarRol(usuario: UserDTO){
        this.loginService.consultarRol(usuario.user).pipe(first())
            .subscribe(data => {
                if (!!data.response && data.response.statusCode == successResponse.statusCode) {
                    usuario.roleName = data.rolTramiteDTO.nombreRol;
                    usuario.roleId = data.rolTramiteDTO.idRol;
                    localStorage.setItem('currentUser', JSON.stringify(usuario));
                    this.loginService.currentUserSubject.next(usuario);
                    this.loginService.outFilingUser();
                    this.showProgressBar = false;
                    this.router.navigate(['/' + ROUTES_PATH.home]);
                }
            });
    }

    toggleShowPassword = function($event) {
        this.showPassword = !this.showPassword;
        this.classShowPassword = this.showPassword? "glyphicon-eye-close" : "glyphicon-eye-open";
      }
}
