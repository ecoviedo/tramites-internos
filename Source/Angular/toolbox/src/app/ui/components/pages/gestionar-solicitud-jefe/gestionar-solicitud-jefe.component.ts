import {Component, OnInit} from '@angular/core';
//import { SOLICITUD } from 'src/app/domain/model/mocks/SolicitudMock';
import {TramiteDTO} from 'src/app/domain/model/dto/TramiteDTO';
import {
    FILED_PROCEDURE, MOCK_COMPANY_AREA_TABLE, MOCK_PROCESS_MANAGERS_TABLE,
    MOCK_IDENTIFICATION_TYPE_TABLE, MOCK_PROCEDURE_TYPE_TABLE, MOCK_PERSONAL_REASON_TABLE
} from 'src/app/domain/model/mocks/SolicitudMock';

import {CatalogueDTO} from 'src/app/domain/model/dto/CatalogueDTO';
import {ProcessManagersDTO} from 'src/app/domain/model/dto/ProcessManagersDTO';
import {TipoTramiteDTO} from 'src/app/domain/model/dto/TipoTramiteDTO';
import {
    TablasBasicasService,
    LoginService,
    TaskService,
    ConfirmationService,
    TramiteService
} from 'src/app/infraestructure/services/service.index';
import {
    consPantalla,
    processContainer,
    statesTaskActions,
    successResponse
} from 'src/environments/environment.variables';
import {UserDTO} from 'src/app/domain/model/dto/UserDTO';
import {ProcessRequestDTO} from 'src/app/domain/model/dto/ProcessRequestDTO';
import {TareaDTO} from 'src/app/domain/model/dto/TareaDTO';
import {ParametrosDTO} from 'src/app/domain/model/dto/parametrosDTO';
import {RqGestionTramiteDTO} from 'src/app/domain/model/dto/RqGestionTramiteDTO';
import {Router} from '@angular/router';
import {ROUTES_PATH} from '../../pages.routing.name';
import {ProcesoDTO} from 'src/app/domain/model/dto/ProcesoDTO';

@Component({
    selector: 'app-gestionar-solicitud-jefe',
    templateUrl: './gestionar-solicitud-jefe.component.html'
})
export class gestionarSolicitudJefeComponent implements OnInit {
    solicitud: TramiteDTO;
    managementLegend: string;
    request = new ProcessRequestDTO;
    tarea: number;

    companyArea: CatalogueDTO[];
    immediateBosses: CatalogueDTO[];
    identificationTypes: CatalogueDTO[];
    requestType: TipoTramiteDTO[];
    processManagers: ProcessManagersDTO[];
    reasonForRequest: TipoTramiteDTO[];
    pantallaOrigen: string = consPantalla.gestionarSolicitudJefe;
    disabledForm: boolean = true;
    rqGestionTramite: RqGestionTramiteDTO;
    tareaId: number;
    instanciaId: string;

    tareaDTO: TareaDTO = new TareaDTO();

    constructor(
        private tb: TablasBasicasService,
        private loginService: LoginService,
        private tareaService: TaskService,
        private tramiteService: TramiteService,
        private confirmationService: ConfirmationService,
        private router: Router
    ) {
        this.requestType = MOCK_PROCEDURE_TYPE_TABLE;
        this.processManagers = MOCK_PROCESS_MANAGERS_TABLE;
        this.reasonForRequest = MOCK_PERSONAL_REASON_TABLE;

        this.loginService.currentUser.subscribe(x => this.request.ownerUser = x);
        this.tarea = JSON.parse(localStorage.getItem('task'));
    }

    ngOnInit() {
        console.log('************ngOnInit-gestionar-solicitud-jefe******************');
        this.consultarTramite();
        this.cargarCatalogos();
        this.managementLegend = 'Jefe inmediato';
    }

    private consultarTramite() {
        //this.solicitud = FILED_PROCEDURE;
        console.log('************consultarTramite - gestionar-solicitud-jefe******************');
        // Se reclama la info de la tarea q estaba en el store
        this.tareaDTO.taskId = JSON.parse(localStorage.getItem('task'));
        // Se reclama la info de la instacia q estaba en el store
        this.tareaDTO.instanceId = JSON.parse(localStorage.getItem('instanceId'));
        this.tramiteService.consultarTramite(this.tareaDTO.instanceId).subscribe(data => {
            if (!!data.response && data.response.statusCode == successResponse.statusCode) {
                this.solicitud = data.tramite;
            }
        });
    }

    cargarCatalogos() {
        //this.identificationTypes = MOCK_IDENTIFICATION_TYPE_TABLE;
        //this.companyArea = MOCK_COMPANY_AREA_TABLE;

        let listaCatalogos: string[] = ['AreaEmpresa', 'RolTramite', 'TipoIdentificacion'];
        this.tb.consultarCatalogos(listaCatalogos).subscribe(data => {
            if (!!data && data.length > 0) {
                data.forEach(element => {
                    if (!!element) {
                        if (element.catalogue == 'AreaEmpresa')
                            this.companyArea = element.data;
                        else if (element.catalogue == 'TipoIdentificacion')
                            this.identificationTypes = element.data;
                    }
                });
            }
        });
    }

    //************************************************************EVENTOS********************************************************** */

    recibirInformacionGestion(event: any) {
        //enviar a método que valida segun el tipo de tramite que formularios hacen que el botón se active
        if (!!event.gestionTramite) {
            this.rqGestionTramite = event.gestionTramite;
        }
        if (event.validForm) {
            this.disabledForm = false
        }
    }

    guardarEvent(tarea: TramiteDTO) {
        this.finalizarGestion(this.rqGestionTramite);
    }

    private finalizarGestion(rqGestionTramiteDTO: RqGestionTramiteDTO) {
        let processRequestAux = new ProcessRequestDTO();
        processRequestAux.containerId = processContainer.containerId;
        processRequestAux.taskId = this.tarea; //debe llegar por url o por storage
        processRequestAux.taskStatus = statesTaskActions.completed;

        let userDTO = new UserDTO;
        userDTO.user = this.request.ownerUser.user;
        userDTO.password = this.request.ownerUser.password;
        processRequestAux.ownerUser = userDTO;

        const valores = {
            Aprobacion: rqGestionTramiteDTO.respuestaTramite
        };

        //let parametrosDTO = new ParametrosDTO();
        //parametrosDTO.values = rqParametrosRadicarTramiteDTO;
        processRequestAux.parametros = new ParametrosDTO();
        processRequestAux.parametros.values = valores;
        this.completarTarea(processRequestAux);
    }

    private completarTarea(request: ProcessRequestDTO) {
        this.tareaService.cambiarEstadoTarea(request)
            .subscribe(data => {
                if (!!data && !!data.containers && !!data.containers[0].processes
                    && !!data.containers[0].processes[0].taskList) {
                    const task = data.containers[0].processes[0].taskList[0];
                    this.tareaId = data.containers[0].processes[0].taskList[0].taskId;
                    this.instanciaId = data.containers[0].processes[0].taskList[0].instanceId;
                    //localStorage.setItem('task', JSON.stringify(task));
                    //this.router.navigate(['/' + ROUTES_PATH.home + '/' + task.taskName.replace(/ /g, "")]);

                    this.irListaTareas(null);
                }
            });
    }

    private irListaTareas(data: ProcesoDTO) {
        this.confirmationService.confirm({
            message: 'Número de trámite:' + '__' + ' Número de proceso:' + this.instanciaId, // + data.instance.instanceId,
            header: 'Gestión Finalizada actividad ' + this.tareaId,
            rejectVisible: false,
            accept: () => {
                this.router.navigate(['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.task]);
            }
        });
    }
}
