import { Component, OnInit } from "@angular/core";
import { UserDTO } from "src/app/domain/model/dto/UserDTO";
import { FormGroup, FormBuilder } from "@angular/forms";
import { TablaBasicaDTO } from "src/app/domain/model/dto/TablaBasicaDTO";
import { UtilitiesFnService, ConfirmationService, LoginService, UserService } from "src/app/infraestructure/services/service.index";

@Component({
    selector: 'app-administracionUsuarios',
    templateUrl: './administracionUsuarios.component.html'
})
export class AdministracionUsuariosComponent implements OnInit {

    grupoUsuarios: UserDTO[];
    usuarioPadre: UserDTO;
    formUser: FormGroup;
    listaTipoIdent: TablaBasicaDTO[];
    listaRol: TablaBasicaDTO[];
    index: number = -1;
    currentUser: UserDTO;
    userAdmin: boolean;

    constructor(
        private _formBuilder: FormBuilder,
        private fn: UtilitiesFnService,
        private confirmationService: ConfirmationService,
        private loginService: LoginService,
        private usuarioService: UserService
    ) {
        this.loginService.currentUser.subscribe(x => this.currentUser = x);

        this.formUser = this._formBuilder.group({
            usuario: '',
            nombre: '',
            rol: ''
        });
    }

    ngOnInit() {
        //this.currentUser.roleId == "Administrador" ? this.userAdmin = true : this.userAdmin = false;

        this.cargarGrupos();

        // this.listaTipoIdent = [
        //     { name: 'Cédula Ciudadanía', value: '1' },
        //     { name: 'Tarjeta Identidad', value: '2' },
        //     { name: 'Tarjeta Pasaporte', value: '3' },
        //     { name: 'Registro Civil', value: '4' },
        //     { name: 'Cédula de Extranjería', value: '5' }
        // ];
    }

    cargarGrupos() {
        this.listaRol = new Array<TablaBasicaDTO>();

        this.usuarioService.consultarGrupos()
            .subscribe(data => {
                data.groups.forEach(element => {
                    var nvpGroup = new TablaBasicaDTO();
                    nvpGroup.nombre = element.name;
                    nvpGroup.codigoApp = element.name;
                    this.listaRol.push(nvpGroup);
                });
            }, error => console.log(error))
            ;
    }

    buscarGrupo() {
        let loginName = this.formUser.get("usuario").value;
        let userName = this.formUser.get("nombre").value;
        let rolName = this.formUser.get("rol").value;

        this.usuarioService.consultarUsuarios()
            .subscribe(data => {
                if (!!data && !!data.users) {
                    this.grupoUsuarios = new Array<UserDTO>();
                    if (!!loginName || !!userName || !!rolName) {
                        data.users.forEach(user => {
                            if (user.user == loginName || user.name == userName || user.role == rolName)
                                this.grupoUsuarios.push(user);
                        });
                    } else {
                        this.grupoUsuarios = data.users;
                    }
                }
            });

    }

    nuevoUsuario() {
        this.usuarioPadre = new UserDTO();
    }

    editarUsuario(user, index) {
        this.usuarioPadre = user;
        this.index = index;
    }

    guardarUsuario(newUser: UserDTO) {
        if (newUser != null) {
            const userCO = { user: newUser }
            if (this.index == -1) {
                this.usuarioService.crearUsuario(userCO).subscribe(data => { debugger });
            } else {
                this.usuarioService.editarUsuario(userCO).subscribe(data => { debugger });
                //newUser.roleId = newUser.newRole;
            }
            this.fn.updateItemByEntity(this.grupoUsuarios, newUser, this.index);
        }
        this.usuarioPadre = null;
        this.index = -1;
    }

    eliminarUsuario(usuario: UserDTO) {
        this.confirmationService.confirm({
            message: '¿Realmente desea eliminar el registro?',
            accept: () => {
                const userCO = { user: usuario }
                this.usuarioService.eliminarUsuario(userCO).subscribe(data => { debugger });
                this.grupoUsuarios = this.fn.deleteItemByEntity(this.grupoUsuarios, usuario);
            },
        });
    }

    valorLista(item: string, lista: any[]) {
        const itemNvp = this.fn.filterItemByListNvp(lista, item);
        return !!itemNvp ? itemNvp.name : '';
    }
}