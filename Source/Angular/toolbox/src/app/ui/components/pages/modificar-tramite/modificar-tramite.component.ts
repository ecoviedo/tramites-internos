
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ParametrosDTO } from 'src/app/domain/model/dto/parametrosDTO';
import { ProcessRequestDTO } from 'src/app/domain/model/dto/ProcessRequestDTO';
import { TramiteDTO } from 'src/app/domain/model/dto/TramiteDTO';
import { TareaDTO } from 'src/app/domain/model/dto/TareaDTO';

import { MOCK_COMPANY_AREA_TABLE } from 'src/app/domain/model/mocks/SolicitudMock';
import { MOCK_PROCESS_MANAGERS_TABLE } from 'src/app/domain/model/mocks/SolicitudMock';
import { MOCK_IDENTIFICATION_TYPE_TABLE } from 'src/app/domain/model/mocks/SolicitudMock';
import { MOCK_PROCEDURE_TYPE_TABLE } from 'src/app/domain/model/mocks/SolicitudMock';

import { LoginService } from 'src/app/infraestructure/services/login.service';
import { SolicitudService, TablasBasicasService, TramiteService } from 'src/app/infraestructure/services/service.index';
import { TaskService } from 'src/app/infraestructure/services/task.service';
import { UserDTO } from '../../../../domain/model/dto/UserDTO';
import { actividades, statesTaskActions, codigosParametros, constantCompanyAreaCode, constantProcessRoleCode, 
    consPantalla } from 'src/environments/environment.variables';
import { ROUTES_PATH } from '../../pages.routing.name';
import { SolicitudVacacionesDTO } from 'src/app/domain/model/dto/SolicitudVacacionesDTO';
import { TablaBasicaDTO } from 'src/app/domain/model/dto/TablaBasicaDTO';

import { EmpleadoDTO } from '../../../../domain/model/dto/EmpleadoDTO';
import { JefeInmediatoDTO } from '../../../../domain/model/dto/JefeInmediatoDTO';
import { JefeInmediatoService } from '../../../../infraestructure/services/jefe-inmediato.service';
import { JEFE } from '../../../../domain/model/mocks/JefeInmediatoMock';
import { GerenteDTO } from '../../../../domain/model/dto/GerenteDTO';
import { GERENTE } from '../../../../domain/model/mocks/gerenteMock';
import { GerenteService } from '../../../../infraestructure/services/gerente.service';
import { RecursosHumanosDTO } from '../../../../domain/model/dto/RecursosHumanosDTO';
import { RecursosHumanosService } from '../../../../infraestructure/services/recursos-humanos.service';
import { RECURSOS } from '../../../../domain/model/mocks/recursosHumanosMock';
import { MessageService } from 'primeng/api';
import { DatePipe } from '@angular/common';

import { ProcessManagersDTO } from 'src/app/domain/model/dto/ProcessManagersDTO';
import { CatalogueDTO } from 'src/app/domain/model/dto/CatalogueDTO';
import { MOCK_TRAMITE_CONSULTADO_VACACIONES, MOCK_TRAMITE_CONSULTADO_PERMISOS, MOCK_TRAMITE_CONSULTADO_INCAPACIDAD
    , MOCK_TRAMITE_CONSULTADO_LICENCIA, MOCK_TRAMITE_CONSULTADO_CERTIFICACION } from 'src/app/domain/model/mocks/TramiteMock';
import { TipoTramiteDTO } from 'src/app/domain/model/dto/TipoTramiteDTO';
import { successResponse } from 'src/environments/environment.variables';


interface Rol {
    name: string;
    code: string;
}

@Component({
    selector: 'app-modificar-tramite',
    templateUrl: './modificar-tramite.component.html',
    styles: [`
        :host ::ng-deep button {
            margin-right: .25em;
        }

        :host ::ng-deep .custom-toast .ui-toast-message {
            color: #ffffff;
            background: #FC466B;
            background: -webkit-linear-gradient(to right, #3F5EFB, #FC466B);
            background: linear-gradient(to right, #3F5EFB, #FC466B);
        }

        :host ::ng-deep .custom-toastError .ui-toast-message {
            color: #ffffff;
            background: #fb151e;
            background: -webkit-linear-gradient(to right, #e6effb, #faeefb);
            background: linear-gradient(to right, #fb151e, #850509);
        }

        :host ::ng-deep .custom-toast .custom-toastError .ui-toast-close-icon {
            color: #ffffff;
        }
    `],
    providers: [MessageService, DatePipe]
})
export class ModificarTramiteComponent implements OnInit {

    request = new ProcessRequestDTO;
    user = new UserDTO;
    activity = actividades;
    tarea: TareaDTO = new TareaDTO();
    empleado: EmpleadoDTO;
    //requestType: TablaBasicaDTO[];
    requestType: TipoTramiteDTO[];

    companyArea: CatalogueDTO[];
    immediateBosses: CatalogueDTO[];

    processManagers: ProcessManagersDTO[];

    identificationTypes: CatalogueDTO[];

    //tipoSolicitud: TablaBasicaDTO;
    tipoSolicitud: TipoTramiteDTO;
    solicitud: TramiteDTO;
    jefe: JefeInmediatoDTO;
    gerente: GerenteDTO;
    recursosHumanos: RecursosHumanosDTO;
    modificarTramite: boolean = true;
    disabledForm: boolean = true;
    pantallaOrigen: string = consPantalla.modificarTramite;

    constructor(
        private loginService: LoginService,
        private tareaService: TaskService,
        private solicitudService: SolicitudService,
        private tablasBasicasService: TablasBasicasService,
        private tramiteService: TramiteService,
        private jefeInmediato: JefeInmediatoService,
        private gerenteService: GerenteService,
        private recursosHumanosService: RecursosHumanosService,
        private router: Router,
        private messageService: MessageService,
        private datePipe: DatePipe
    ) {
        this.solicitud = new TramiteDTO;
        //this.solicitud = MOCK_TRAMITE_CONSULTADO_VACACIONES;
        //this.solicitud = MOCK_TRAMITE_CONSULTADO_PERMISOS;
        //this.solicitud = MOCK_TRAMITE_CONSULTADO_INCAPACIDAD;
        //this.solicitud = MOCK_TRAMITE_CONSULTADO_LICENCIA;
        this.solicitud = MOCK_TRAMITE_CONSULTADO_CERTIFICACION;
        
        this.tipoSolicitud = this.solicitud.idTipoTramite;

        // Se reclama la info del user q estaba en el store
        this.loginService.currentUser.subscribe(x => this.user = x);
        
    }

    ngOnInit() {
        console.log('************ngOnInit-modificar-tramite******************');
        this.consultarTramite();
        this.cargarCatalogos();
        this.jefe = JEFE;
        this.gerente = GERENTE;
        this.recursosHumanos = RECURSOS;

        
        // Cargue de la petición de negocio
        //this.request.containerId = this.tarea.containerId;
        //this.request.processInstance = this.tarea.instanceId;
        //this.request.taskId = this.tarea.taskId.toString();
        this.request.taskStatus = statesTaskActions.started;
        this.request.parametros = new ParametrosDTO();
        // Cambio de estado de la tarea a fin de q se inicie al reclamar tarea
        //this.tarea.taskStatus != states.inProgress ? this.cambiarEstadoTarea(this.request) : null;
        // Se carga el # de soilicitud y se procede a consultar la información
        /*this.solicitud.numeroRadicado = this.tarea.instanceId;
        this.jefe.numeroRadicado = this.tarea.instanceId;
        this.gerente.numeroRadicado = this.tarea.instanceId;
        this.recursosHumanos.numeroRadicado = this.tarea.instanceId;*/
        //this.consultarEmpleado(this.tarea.instanceId);
        // Has default value
    }

    private consultarTramite(){
        // Se reclama la info de la tarea q estaba en el store
        this.tarea.taskId = JSON.parse(localStorage.getItem('task'));
        // Se reclama la info de la instacia q estaba en el store
        this.tarea.instanceId = JSON.parse(localStorage.getItem('instanceId'));
        debugger;
        this.tramiteService.consultarTramite("170").subscribe(data => {
            if (!!data.response && data.response.statusCode == successResponse.statusCode) {
                debugger;
                //this.requestType = data.proceduresTypes;
            }
        });
    }

    cargarCatalogos(){
        //this.identificationTypes = MOCK_IDENTIFICATION_TYPE_TABLE;
        //this.companyArea = MOCK_COMPANY_AREA_TABLE;
        let listaCatalogos: string[] = ['AreaEmpresa', 'RolTramite', 'TipoIdentificacion'];
        this.tablasBasicasService.consultarCatalogos(listaCatalogos).subscribe(data => {
            if (!!data.response && data.response.statusCode == successResponse.statusCode) {
                data.catalogs.forEach(element => {
                    if (!!element) {
                        if (element.catalogo == 'AreaEmpresa')
                            this.companyArea = element.data;
                        else if (element.catalogo == 'TipoIdentificacion')
                            this.identificationTypes = element.data;
                    }
                });
            }
        });

        //this.requestType = MOCK_PROCEDURE_TYPE_TABLE;
        this.tablasBasicasService.consultarTipoTramite(null).subscribe(data => {
            if (!!data.response && data.response.statusCode == successResponse.statusCode) {
                this.requestType = data.proceduresTypes;
            }
        });
        //this.processManagers = MOCK_PROCESS_MANAGERS_TABLE;
        this.tramiteService.consultarGestorTramite().subscribe(data => {
            if (!!data.response && data.response.statusCode == successResponse.statusCode) {
                this.processManagers = data.processManagers;
                this.consultManagers(constantCompanyAreaCode.operationsManagementCode, constantProcessRoleCode.immediateBossCode);
            }
        });
      }

    consultManagers(idArea: string, idRole: number) {
        if (!!idArea && !!idRole) {
            this.immediateBosses = [];
            for (let index = 0; index < this.processManagers.length; index++) {
                if (this.processManagers[index].idArea == idArea && this.processManagers[index].idRol == idRole) {
                    this.immediateBosses.push(
                        {
                            id: this.processManagers[index].id,
                            nombre: this.processManagers[index].nombre + ' ' + this.processManagers[index].apellido,
                            descripcion: ''
                        }
                    );
                }
            }
        }
    }

    /**
     * Cambia el estado de la tarea entre los posibles existente a nivel de negocio
     * @param request
     */
    cambiarEstadoTarea(request: ProcessRequestDTO) {
        this.tareaService.cambiarEstadoTarea(request)
            .subscribe(data => {
                if (!!data && !!data.containers && !!data.containers[0].processes
                    && !!data.containers[0].processes[0].taskList) {
                    this.tarea = data.containers[0].processes[0].taskList[0];
                }
            });
    }

    /**
     * Consulta toda la información correspondinete a la empleado
     * @param request
     */
    // consultarEmpleado(request: string) {
    //   this.solicitudService.consultarEmpleado(request)
    //     .subscribe(data => {
    //       debugger;
    //       // Aca va la logica para cargar la info cuando se halla dado guardar
    //     });
    // }

    /**
     * Recibe los cambios sobre el tipo de solicitud selecionada
     * @param event
     */
    enviarInformacionPpal(event: TramiteDTO) {
        this.solicitud = event;
    }
    /*enviarNumeroDocumento(event: TramiteDTO){
        this.solicitud.solicitante = event;
        //debugger
    }*/

    /**
     * Recibe los cambios sobre el tipo de solicitud
     * @param event
     */
    enviarTipoSolicitud(event: TipoTramiteDTO) {
        this.tipoSolicitud = event;
    }

    /**
     * Recibe los cambios sobre la información de vacaciones
     * @param event
     */
    /*enviarInformacionVacaciones(event: SolicitudVacacionesDTO) {
      this.solicitud.solicitudVacacionesDto = event;
    }*/

    /**
     * Recibe los cambios sobre la información del detalle de la solicitud
     * @param event
     */
    /*enviarInformacionDetalle(event) {
      this.solicitud.detalleTramiteDTO = event;
      debugger
    }*/

    /**
     * Recibe los cambios de los documentos anexos
     * @param event
     */
    enviarDocumentos(event) {

    }
    /**
     * Recibe la información de los certificados
     * @param event
     */
    enviarInformacionJefe(event) {
        this.jefe = event;
    }
    /**
     * Recibe la información de los certificados
     * @param event
     */
    enviarInformacionGerente(event) {
        this.gerente = event;
    }
    /**
     * Recibe la información de los certificados
     * @param event
     */
    enviarInformacionRecursosHumanos(event) {
        this.recursosHumanos = event;
    }

    /**
     * Recibe la información de los certificados
     * @param event
     */
    /*enviarInformacionCertificado (event) {
      this.solicitud.certificado = event;
    }*/

    /**
     * Captura del evento de guardar solicitud
     * @param event
     */
    guardarEvent(tarea: TramiteDTO) {
        this.radicarSolicitud(this.solicitud);
        debugger;
    }
    /**
     * Captura del evento de guardar solicitud
     * @param event
     */
    guardarJefeEvent(tarea: TramiteDTO) {
        this.consumirJefeInmediato(this.jefe);
        debugger;
    }
    /**
     * Captura del evento de guardar solicitud
     * @param event
     */
    guardarGerenteEvent(tarea: TramiteDTO) {
        this.consumirGerente(this.gerente);
        debugger;
    }
    /**
     * Captura del evento de guardar solicitud
     * @param event
     */
    guardarRecursosEvent(tarea: TramiteDTO) {
        this.consumirRecursosHumanos(this.recursosHumanos);
        debugger;
    }


    /**
     * Ejecuta el servicio de crear el radicado o en su defecto almacenarlo
     * @param request
     */
    radicarSolicitud(request: TramiteDTO) {
        this.solicitudService.radicarSolicitud(this.solicitud)
            .subscribe(data => {
                debugger
                if (!data) {
                    this.messageService.add({ key: 'customError', severity: 'error', summary: 'Solicitud Rechazada', detail: 'Esta solicitud ya Existe' });
                }
                else {
                    this.messageService.add({ key: 'custom', severity: 'success', summary: 'Solicitud Guardada', detail: 'Su solicitud se ha guardado exitosamente' });
                }
            }, error1 => {
                if (error1) {
                    this.messageService.add({ key: 'custom', severity: 'success', summary: 'Solicitud Guardada', detail: 'Su solicitud se ha rechazado exitosamente' });
                }
            });
    }
    consumirJefeInmediato(request: JefeInmediatoDTO) {
        this.jefeInmediato.jefeInmediato(this.jefe).subscribe(data => {
            debugger
            if (!data) {
                this.messageService.add({ key: 'customError', severity: 'error', summary: 'Solicitud Rechazada', detail: 'Esta respuesta ya fue guardada' });
            }
            else {
                this.messageService.add({ key: 'custom', severity: 'success', summary: 'Respuesta Guardada', detail: 'Su respuesta se ha guardado y enviado exitosamente' });
            }
        }, error1 => {
            if (error1) {
                this.messageService.add({ key: 'custom', severity: 'success', summary: 'Solicitud Guardada', detail: 'Su solicitud se ha rechazado exitosamente' });
            }
        });
    }
    consumirGerente(request: GerenteDTO) {
        this.gerenteService.opGerente(this.gerente).subscribe(data => {
            debugger
            if (!data) {
                this.messageService.add({ key: 'customError', severity: 'error', summary: 'Solicitud Rechazada', detail: 'Esta respuesta ya fue guardada' });
            }
            else {
                this.messageService.add({ key: 'custom', severity: 'success', summary: 'Respuesta Guardada', detail: 'Su respuesta se ha guardado y enviado exitosamente' });
            }
        }, error1 => {
            if (error1) {
                this.messageService.add({ key: 'custom', severity: 'success', summary: 'Solicitud Guardada', detail: 'Su solicitud se ha rechazado exitosamente' });
            }

        });
    }
    consumirRecursosHumanos(request: RecursosHumanosDTO) {
        this.recursosHumanosService.recursosHumanos(this.recursosHumanos).subscribe(data => {
            if (!data) {
                this.messageService.add({ key: 'customError', severity: 'error', summary: 'Solicitud Rechazada', detail: 'Esta respuesta ya fue guardada' });
            }
            else {
                this.messageService.add({ key: 'custom', severity: 'success', summary: 'Respuesta Guardada', detail: 'Su respuesta se ha guardado y enviado exitosamente' });
            }
        }, error1 => {
            if (error1) {
                this.messageService.add({ key: 'custom', severity: 'success', summary: 'Solicitud Guardada', detail: 'Su solicitud se ha rechazado exitosamente' });
            }
        });
    }
}



