import { Component, OnInit } from '@angular/core';
import { TramiteDTO } from 'src/app/domain/model/dto/TramiteDTO';
import {
  FILED_PROCEDURE, MOCK_COMPANY_AREA_TABLE, MOCK_PROCESS_MANAGERS_TABLE, MOCK_IDENTIFICATION_TYPE_TABLE,
  MOCK_PROCEDURE_TYPE_TABLE, MOCK_PERSONAL_REASON_TABLE, MOCK_MANAGEMENT_PROCESS_MOST_RECENT_AREA_MANAGER,
  MOCK_MANAGEMENT_PROCESS_MOST_RECENT_IMMEDIATE_BOSS
} from 'src/app/domain/model/mocks/SolicitudMock';

import { CatalogueDTO } from 'src/app/domain/model/dto/CatalogueDTO';
import { ProcessManagersDTO } from 'src/app/domain/model/dto/ProcessManagersDTO';
import { GestionTramiteDTO } from 'src/app/domain/model/dto/GestionTramiteDTO';
import { TipoTramiteDTO } from 'src/app/domain/model/dto/TipoTramiteDTO';
import { consPantalla, processContainer, statesTaskActions, successResponse } from 'src/environments/environment.variables';
import { RqGestionTramiteDTO } from 'src/app/domain/model/dto/RqGestionTramiteDTO';
import { ProcessRequestDTO } from 'src/app/domain/model/dto/ProcessRequestDTO';
import { UserDTO } from 'src/app/domain/model/dto/UserDTO';
import { ParametrosDTO } from 'src/app/domain/model/dto/parametrosDTO';
import { ProcesoDTO } from 'src/app/domain/model/dto/ProcesoDTO';
import { LoginService } from 'src/app/infraestructure/services/login.service';
import { TaskService } from 'src/app/infraestructure/services/task.service';
import { ConfirmationService } from 'primeng/api';
import { Router } from '@angular/router';
import { ROUTES_PATH } from '../../pages.routing.name';
import { TablasBasicasService, TramiteService } from 'src/app/infraestructure/services/service.index';
import { TareaDTO } from 'src/app/domain/model/dto/TareaDTO';


@Component({
  selector: 'app-gestionar-solicitud-gerente-gh',
  templateUrl: './gestionar-solicitud-gerente-gh.component.html'
})
export class gestionarSolicitudGerenteGHComponent implements OnInit {
  solicitud: TramiteDTO;
  managementLegend: string;
  legendImmediateBossManagement: string;
  legendManagementAreaManager: string;
  companyArea: CatalogueDTO[];
  immediateBosses: CatalogueDTO[];
  identificationTypes: CatalogueDTO[];
  requestType: TipoTramiteDTO[];
  processManagers: ProcessManagersDTO[];
  reasonForRequest: TipoTramiteDTO[];
  managementImmediateBossProcedure: GestionTramiteDTO;
  managementProcessAreaManager: GestionTramiteDTO;
  disabledForm: boolean = true;
  pantallaOrigen: string = consPantalla.gestionarSolicitudGH;
  rqGestionTramite: RqGestionTramiteDTO;
  tareaId: number;
  instanciaId: string;
  tarea: number;
  request = new ProcessRequestDTO;
  tareaDTO: TareaDTO = new TareaDTO();

  constructor(
    private loginService: LoginService,
    private tareaService: TaskService,
    private confirmationService: ConfirmationService,
    private tablasBasicasService: TablasBasicasService,
    private tramiteService: TramiteService,
    private router: Router
  ) {
    this.requestType = MOCK_PROCEDURE_TYPE_TABLE;
    this.processManagers = MOCK_PROCESS_MANAGERS_TABLE;
    this.reasonForRequest = MOCK_PERSONAL_REASON_TABLE;
    this.managementImmediateBossProcedure = MOCK_MANAGEMENT_PROCESS_MOST_RECENT_IMMEDIATE_BOSS;
    this.managementProcessAreaManager = MOCK_MANAGEMENT_PROCESS_MOST_RECENT_AREA_MANAGER;

    this.loginService.currentUser.subscribe(x => this.request.ownerUser = x);
    this.tarea = JSON.parse(localStorage.getItem('task'));
  }

  ngOnInit() {
    this.managementLegend = 'Gestión humana';
    this.legendImmediateBossManagement = 'Jefe inmediato';
    this.legendManagementAreaManager = 'Gerente de área'

    this.consultarTramite();
    this.cargarCatalogos();
  }

  private consultarTramite(){
    //this.solicitud = FILED_PROCEDURE;
    console.log('************consultarTramite - gestionar-solicitud-jefe******************');
    // Se reclama la info de la tarea q estaba en el store
    this.tareaDTO.taskId = JSON.parse(localStorage.getItem('task'));
    // Se reclama la info de la instacia q estaba en el store
    this.tareaDTO.instanceId = JSON.parse(localStorage.getItem('instanceId'));
    this.tramiteService.consultarTramite(this.tareaDTO.instanceId).subscribe(data => {
        if (!!data.response && data.response.statusCode == successResponse.statusCode) {
            this.solicitud = data.tramite;
        }
    });
  }

  cargarCatalogos() {
    //this.identificationTypes = MOCK_IDENTIFICATION_TYPE_TABLE;
    //this.companyArea = MOCK_COMPANY_AREA_TABLE;

    let listaCatalogos: string[] = ['AreaEmpresa', 'RolTramite', 'TipoIdentificacion'];
    this.tablasBasicasService.consultarCatalogos(listaCatalogos).subscribe(data => {
      if (!!data && data.length > 0) {
        data.forEach(element => {
          if (!!element) {
            if (element.catalogue == 'AreaEmpresa')
              this.companyArea = element.data;
            else if (element.catalogue == 'TipoIdentificacion')
              this.identificationTypes = element.data;
          }
        });
      }
    });
  }
  //************************************************************EVENTOS********************************************************** */

  recibirInformacionGestion(event: any) {
    debugger
    //enviar a método que valida segun el tipo de tramite que formularios hacen que el botón se active
    if (!!event.gestionTramite) {
      this.rqGestionTramite = event.gestionTramite;
    }
    if (event.validForm) {
      this.disabledForm = false
    }
  }

  guardarEvent(tarea: TramiteDTO) {
    debugger;
    this.finalizarGestion(this.rqGestionTramite);
  }

  private finalizarGestion(rqGestionTramiteDTO: RqGestionTramiteDTO) {
    debugger;
    let processRequestAux = new ProcessRequestDTO();
    processRequestAux.containerId = processContainer.containerId;
    processRequestAux.taskId = this.tarea; //debe llegar por url o por storage
    processRequestAux.taskStatus = statesTaskActions.completed;

    let userDTO = new UserDTO;
    userDTO.user = this.request.ownerUser.user;
    userDTO.password = this.request.ownerUser.password;
    processRequestAux.ownerUser = userDTO;

    const valores = {
      Aprobacion: rqGestionTramiteDTO.respuestaTramite
    };

    //let parametrosDTO = new ParametrosDTO();
    //parametrosDTO.values = rqParametrosRadicarTramiteDTO;
    processRequestAux.parametros = new ParametrosDTO();
    processRequestAux.parametros.values = valores;
    debugger;
    this.completarTarea(processRequestAux);
  }

  private completarTarea(request: ProcessRequestDTO) {
    this.tareaService.cambiarEstadoTarea(request)
      .subscribe(data => {
        debugger
        if (!!data && !!data.containers && !!data.containers[0].processes
          && !!data.containers[0].processes[0].taskList) {
          const task = data.containers[0].processes[0].taskList[0];
          this.tareaId = data.containers[0].processes[0].taskList[0].taskId;
          this.instanciaId = data.containers[0].processes[0].taskList[0].instanceId;
          //localStorage.setItem('task', JSON.stringify(task));
          //this.router.navigate(['/' + ROUTES_PATH.home + '/' + task.taskName.replace(/ /g, "")]);

          this.irListaTareas(null);
        }
      });
  }

  private irListaTareas(data: ProcesoDTO) {
    this.confirmationService.confirm({
      message: 'Numero Tramite:' + '??' + ' Número proceso:' + this.instanciaId, // + data.instance.instanceId,
      header: 'Gestión Finalizada actividad ' + this.tareaId,
      rejectVisible: false,
      accept: () => {
        this.router.navigate(['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.task]);
      }
    });
  }
}