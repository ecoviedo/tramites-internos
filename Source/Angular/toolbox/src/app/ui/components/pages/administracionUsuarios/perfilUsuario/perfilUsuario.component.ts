import { Component, OnInit } from "@angular/core";
import { ROUTES_PATH } from "../../../pages.routing.name";
import { successResponse } from "src/environments/environment.variables";
import { TablaBasicaDTO } from "src/app/domain/model/dto/TablaBasicaDTO";
import { UserDTO } from "src/app/domain/model/dto/UserDTO";
import { UserService } from "src/app/infraestructure/services/user.service";
import { UtilitiesFnService, LoginService } from "src/app/infraestructure/services/service.index";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup } from "@angular/forms";

@Component({
    selector: 'app-perfilUsuario',
    templateUrl: './perfilUsuario.component.html'
})
export class PerfilUsuarioComponent implements OnInit {

    formUser: FormGroup;
    display: boolean = true;
    editarVsb: boolean = false;
    currentUser: UserDTO;
    gruposUsuario: TablaBasicaDTO[];
    tipoIdentList: TablaBasicaDTO[];
    identTypeSelect: TablaBasicaDTO;
    roleSelect: TablaBasicaDTO;

    constructor(
        private _formBuilder: FormBuilder,
        private router: Router,
        private loginService: LoginService,
        private fn: UtilitiesFnService,
        private usuarioService: UserService
    ) {
        this.loginService.currentUser.subscribe(x => this.currentUser = x);

        this.formUser = this._formBuilder.group({
            identType: '',
            identNumber: '',
            name: '',
            lastName: '',
            fullName: '',
            email: '',
            role: '',
            newRole: ''
        });

        // this.tipoIdentList = [
        //     {name: 'Cédula Ciudadanía', value: '1'},
        //     {name: 'Tarjeta Identidad', value: '2'},
        //     {name: 'Tarjeta Pasaporte', value: '3'},
        //     {name: 'Registro Civil', value: '4'},
        //     {name: 'Cédula de Extranjería', value: '5'}
        // ];
    }

    ngOnInit(): void {
        this.formUser.disable();

        this.onChargeEntity(this.currentUser);

        this.cargarGrupos();

        this.onChargeForm();
    }

    onChargeEntity (user: UserDTO) {
        if (!!user.identType) {
            this.formUser.controls['identType'].setValue(user.identType);
            this.identTypeSelect = this.fn.filterItemByListNvp(this.tipoIdentList, user.identType);
        }
        if (!!user.identNumber)
            this.formUser.controls['identNumber'].setValue(user.identNumber);    
        if (!!user.name)
            this.formUser.controls['name'].setValue(user.name);
        if (!!user.lastName)
            this.formUser.controls['lastName'].setValue(user.lastName);
        if (!!user.fullName)
            this.formUser.controls['fullName'].setValue(user.fullName);
        if (!!user.email)
            this.formUser.controls['email'].setValue(user.email);
        if (!!user.roleId) 
            this.formUser.controls['newRole'].setValue(user.roleId);
    }

    onChargeForm () {
        this.formUser.valueChanges
                     .subscribe( val => {
                        this.fn.formToDto(val, this.currentUser);
                     }, error => console.log(error)
                     );
    }

    cargarGrupos () {
        this.gruposUsuario = new Array<TablaBasicaDTO>();
        this.usuarioService.consultarGrupos()
                           .subscribe(data => {
                            data.groups.forEach(element => {
                                var nvpGroup = new TablaBasicaDTO();
                                    nvpGroup.nombre = element.name;
                                    nvpGroup.codigoApp = element.name;
                                this.gruposUsuario.push(nvpGroup);
                            });
                            if (!!this.currentUser.roleId)
                                this.roleSelect = this.fn.filterItemByName(this.gruposUsuario, this.currentUser.roleId);
                           }, error => console.log(error))
                           ;
    }

    selectForm = function (event, controlName) {
        if (!!event.value) 
          this.formUser.controls[controlName].setValue(event.value.value);
    }

    aceptarPerfil () {
        this.display = false;
        if (!!this.editarVsb) {
            const userCO = { user: this.currentUser }
            this.usuarioService.editarUsuario(userCO)
                               .subscribe(data => {
                                   if (!!data && !!data.response)
                                    data.response.statusCode == successResponse.statusCode
                                                             ? this.logout()
                                                             : null;                                  
                                }, error => console.log(error)
                                );
        } else {
            this.router.navigate(['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.dashboard]);
        }
    }

    editarPerfil () {
        this.formUser.enable();
        this.editarVsb = true;
    }

    logout() {
        this.loginService.logout();
        this.router.navigate(['/' + ROUTES_PATH.login]);
    } 
}