import { Component, OnInit } from "@angular/core";
import { Subscription } from "rxjs";
import { ProcessRequestDTO } from "src/app/domain/model/dto/ProcessRequestDTO";
import { LoginService } from "src/app/infraestructure/services/login.service";
import { states } from "src/environments/environment.variables";
import { TaskService } from "src/app/infraestructure/services/task.service";
import { Router } from "@angular/router";
import { ROUTES_PATH } from '../../pages.routing.name';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements OnInit {

    tasksStadistics: any[];
    subscriptions: Subscription[] = [];
    totalTask = 0;
    completedTasks = 0;
    progressTasks = 0;
    readyTasks = 0;
    completedTasksPercents = 0;
    progressTasksPercents = 0;
    readyTasksPercents = 0;
    data: any;
    request = new ProcessRequestDTO;

    constructor(
        private loginService: LoginService,
        private tareaService: TaskService,
        private router: Router
    ) {
        //debugger;
        this.loginService.currentUser.subscribe(x => this.request.ownerUser = x);

        /*if (this.request.ownerUser == null)
            this.router.navigate(['/' + ROUTES_PATH.login]);*/
    }

    ngOnInit() {
        //debugger;
        if (!!this.request.ownerUser)
            this.cargarDashboard();
        //else
        //this.router.navigate(['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.login]);
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(s => s.unsubscribe());
    }

    cargarDashboard() {
        //debugger;
        this.request.groups = new Array<string>();
        //this.request.groups.push(role);
        this.request.page = 0;
        this.request.pageSize = -1;
        //pendinete ordenamiento
        this.tareaService.consultarTareasPorPropietario(this.request)
            .subscribe(data => {
                if (!!data && !!data.containers && !!data.containers[0].processes) {
                    this.totalTask = data.containers[0].processes[0].taskList.length;
                    data.containers[0].processes[0].taskList.forEach(element => {
                        if (element.taskStatus == states.ready ||
                            element.taskStatus == states.create ||
                            element.taskStatus == states.reserved)
                            this.readyTasks++;
                        if (element.taskStatus == states.inProgress)
                            this.progressTasks++;
                        if (element.taskStatus == states.completed)
                            this.completedTasks++;
                    });
                    this.readyTasksPercents = this.readyTasks * 100 / this.totalTask;
                    this.readyTasksPercents = Number(this.readyTasksPercents.toFixed(2));

                    this.progressTasksPercents = this.progressTasks * 100 / this.totalTask;
                    this.progressTasksPercents = Number(this.progressTasksPercents.toFixed(2));

                    this.completedTasksPercents = this.completedTasks * 100 / this.totalTask;
                    this.completedTasksPercents = Number(this.completedTasksPercents.toFixed(2));

                    this.cargarGrafica();
                }
            });
    }

    cargarGrafica() {
        this.data = {
            labels: ['En proceso', 'Completadas', 'Pendiente por gestión'],
            datasets: [
                {
                    data: [this.progressTasks, this.completedTasks, this.readyTasks],
                    backgroundColor: [
                        "#4527A0",
                        "#2E7D32",
                        "#0277BD"
                    ],
                    hoverBackgroundColor: [
                        "#4527A0",
                        "#2E7D32",
                        "#0277BD"
                    ]
                }]
        };
    }
}