import { Component, OnInit, ChangeDetectorRef, ViewRef } from "@angular/core";
import { ProcesoDTO } from "src/app/domain/model/dto/ProcesoDTO";
import { TareaDTO } from "src/app/domain/model/dto/TareaDTO";
import { ProcessRequestDTO } from "src/app/domain/model/dto/ProcessRequestDTO";
import { TaskService } from "src/app/infraestructure/services/task.service";
import { Router } from "@angular/router";
import { AlertService, LoginService, ProcessService, ConfirmationService } from "src/app/infraestructure/services/service.index";
import { states, nombreTareas, failUserNotAuthorized, statesTaskActions, constantProcessRoleCode, 
    successResponse, processContainer, processes } from "src/environments/environment.variables";
import { UserDTO } from "src/app/domain/model/dto/UserDTO";
import { ROUTES_PATH } from "../../pages.routing.name";


@Component({
    selector: 'app-administracionTareas',
    templateUrl: './administracionTareas.component.html'
})

export class AdministracionTareasComponent implements OnInit {

    procesoPadre: ProcesoDTO;
    tareas: TareaDTO[];
    request = new ProcessRequestDTO;
    cols: any[];

    constructor(
        private cd: ChangeDetectorRef,
        private tareaService: TaskService,
        private router: Router,
        private alertService: AlertService,
        private loginService: LoginService,
        private procesoService: ProcessService,
        private confirmationService: ConfirmationService
    ) {
        this.loginService.currentUser.subscribe(x => this.request.ownerUser = x);
    }

    ngOnInit() {
        //debugger;
        this.cols = [
            { field: 'taskId', header: 'Id Tarea' },
            { field: 'taskName', header: 'Nombre' },
            { field: 'taskStatus', header: 'Estado' },
            { field: 'taskActualOwner', header: 'Propietario' },
            { field: 'instanceId', header: 'Radicado' },
            { field: 'containerId', header: 'Iniciar Tarea' }
        ];

        if (!!this.request.ownerUser)
            this.cargarTareasPorPropietario()
        //else
            //this.router.navigate(['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.login]);
    }

    cargarTareasPorPropietario() {
        this.request.groups = new Array<string>();
        //this.request.groups.push(role);
        //debugger;
        this.request.page = 0;
        this.request.pageSize = -1;
        this.tareaService.consultarTareasPorPropietario(this.request)
            .subscribe(data => {
                if (!!data && !!data.containers && !!data.containers[0].processes) {
                    this.tareas = new Array<TareaDTO>();
                    data.containers[0].processes[0].taskList.forEach(element => {
                        if (element.taskStatus == states.create ||
                            element.taskStatus == states.reserved ||
                            element.taskStatus == states.inProgress ||
                            element.taskStatus == states.ready)
                            this.tareas.push(element);
                    });
                }
            });
    }

    detectChanges() {
        setTimeout(() => {
            if (!(this.cd as ViewRef).destroyed) {
                this.cd.detectChanges();
            }
        }, 250);
    }

    enrutarIniciarTarea(tarea: TareaDTO) {
        switch (this.request.ownerUser.roleId) {
            case constantProcessRoleCode.employeeCode:
                debugger;
                localStorage.setItem('task', JSON.stringify(tarea.taskId));
                localStorage.setItem('instanceId', JSON.stringify(tarea.instanceId));
                this.router.navigate(['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.modificarTramite]);
                break;
            case constantProcessRoleCode.immediateBossCode:
                if(tarea.taskName == nombreTareas.gestionarTramite && tarea.taskStatus == states.reserved){
                    this.cargarDatosIniciarTarea(tarea);
                    localStorage.setItem('task', JSON.stringify(tarea.taskId));
                    localStorage.setItem('instanceId', JSON.stringify(tarea.instanceId));
                    this.router.navigate(['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.gestionJefe]);
                }else if(tarea.taskName == nombreTareas.gestionarTramite && tarea.taskStatus == states.inProgress){
                    localStorage.setItem('task', JSON.stringify(tarea.taskId));
                    localStorage.setItem('instanceId', JSON.stringify(tarea.instanceId));
                    this.router.navigate(['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.gestionJefe]);
                }
                break;
            case constantProcessRoleCode.areaManagerCode:
                if(tarea.taskName == nombreTareas.gestionarTramite && tarea.taskStatus == states.reserved){
                    this.cargarDatosIniciarTarea(tarea);
                    localStorage.setItem('task', JSON.stringify(tarea.taskId));
                    localStorage.setItem('instanceId', JSON.stringify(tarea.instanceId));
                    this.router.navigate(['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.gestionGerente]);
                }else if(tarea.taskName == nombreTareas.gestionarTramite && tarea.taskStatus == states.inProgress){
                    localStorage.setItem('task', JSON.stringify(tarea.taskId));
                    localStorage.setItem('instanceId', JSON.stringify(tarea.instanceId));
                    this.router.navigate(['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.gestionGerente]);
                }
                break;
            case constantProcessRoleCode.areaManagerGHCode:
                if(tarea.taskName == nombreTareas.gestionarTramite && tarea.taskStatus == states.reserved){
                    this.cargarDatosIniciarTarea(tarea);
                    localStorage.setItem('task', JSON.stringify(tarea.taskId));
                    localStorage.setItem('instanceId', JSON.stringify(tarea.instanceId));
                    this.router.navigate(['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.gestionGerenteGH]);
                }else if(tarea.taskName == nombreTareas.gestionarTramite && tarea.taskStatus == states.inProgress){
                    localStorage.setItem('task', JSON.stringify(tarea.taskId));
                    localStorage.setItem('instanceId', JSON.stringify(tarea.instanceId));
                    this.router.navigate(['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.gestionGerenteGH]);
                }
                break;
        }

        /*const userLogin = this.request.ownerUser.user;
        if (tarea.taskStatus != states.ready) {
            if (!!tarea.taskActualOwner && tarea.taskActualOwner == userLogin) {
                localStorage.setItem('task', JSON.stringify(tarea));
                this.router.navigate(['/' + ROUTES_PATH.home + '/' + tarea.taskName.replace(/ /g, "")]);
            } else {
                this.alertService.error(failUserNotAuthorized);
            }
        } else {
            this.request.containerId = tarea.containerId;
            this.request.taskId = tarea.taskId.toString();
            this.request.taskStatus = statesTaskActions.claimed;
            this.reclamarTarea(this.request);
        }*/
        
    }

    private cargarDatosIniciarTarea(tarea: TareaDTO){
        let processRequestAux = new ProcessRequestDTO();
        processRequestAux.containerId = processContainer.containerId;
        processRequestAux.taskId = tarea.taskId;
        localStorage.setItem('task', JSON.stringify(tarea.taskId));
        processRequestAux.taskStatus = statesTaskActions.started;
        processRequestAux.ownerUser = this.request.ownerUser;
        this.iniciarTarea(processRequestAux);
    }

    iniciarProceso(proceso: ProcesoDTO) {
        this.request.containerId = proceso.containerId;
        this.request.processesId = proceso.processId;
        debugger;
        this.procesoService.iniciarProceso(this.request).subscribe(data => {
            if (!!data.response && data.response.statusCode == successResponse.statusCode) {
              const aux = data.containers[0].processes;
              for (let index = 0; index < aux.length; index++) {
                if (aux[index].processId == proceso.processId) {
                  this.irListaTareas(aux[index]);;
                  break;
                }
              }
            }
          }, error => this.alertService.error(error.message),
            () => this.detectChanges()
          );
      }

      irListaTareas(data: ProcesoDTO) {
        this.confirmationService.confirm({
          message: 'Numero de Instacia Creado: ' + data.instance.instanceId,
          header: 'Confirmar Solicitud',
          rejectVisible: false,
          accept: () => {
            this.router.navigate(['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.task]);
          }
        });
      }

    private reclamarTarea(request: ProcessRequestDTO) {
        this.tareaService.cambiarEstadoTarea(request)
            .subscribe(data => {
                if (!!data && !!data.containers && !!data.containers[0].processes
                    && !!data.containers[0].processes[0].taskList) {
                    const task = data.containers[0].processes[0].taskList[0];
                    localStorage.setItem('task', JSON.stringify(task));
                    this.router.navigate(['/' + ROUTES_PATH.home + '/' + task.taskName.replace(/ /g, "")]);
                }
            });
    }

    private iniciarTarea(request: ProcessRequestDTO) {
        this.tareaService.cambiarEstadoTarea(request)
            .subscribe(data => {
                if (!!data && !!data.containers && !!data.containers[0].processes
                    && !!data.containers[0].processes[0].taskList) {
                    const task = data.containers[0].processes[0].taskList[0];
                    //this.tareaId = data.containers[0].processes[0].taskList[0].taskId;
                    //this.instanciaId = data.containers[0].processes[0].taskList[0].instanceId;
                    //localStorage.setItem('task', JSON.stringify(task));
                    //this.router.navigate(['/' + ROUTES_PATH.home + '/' + task.taskName.replace(/ /g, "")]);
                }
            });
    }

}