
import { Component, OnInit, ChangeDetectorRef, ViewRef } from '@angular/core';
import { Router } from '@angular/router';
import { ParametrosDTO } from 'src/app/domain/model/dto/parametrosDTO';
import { ProcessRequestDTO } from 'src/app/domain/model/dto/ProcessRequestDTO';
import { TramiteDTO } from 'src/app/domain/model/dto/TramiteDTO';
import { RqRadicarTramiteDTO } from 'src/app/domain/model/dto/RqRadicarTramiteDTO';
import { TareaDTO } from 'src/app/domain/model/dto/TareaDTO';

import { MOCK_COMPANY_AREA_TABLE } from 'src/app/domain/model/mocks/SolicitudMock';
import { MOCK_PROCESS_MANAGERS_TABLE } from 'src/app/domain/model/mocks/SolicitudMock';
import { MOCK_IDENTIFICATION_TYPE_TABLE } from 'src/app/domain/model/mocks/SolicitudMock';
import { MOCK_PROCEDURE_TYPE_TABLE } from 'src/app/domain/model/mocks/SolicitudMock';

import { LoginService } from 'src/app/infraestructure/services/login.service';
import { SolicitudService, TablasBasicasService, TramiteService, ProcessService, AlertService, UtilitiesFnService } from 'src/app/infraestructure/services/service.index';
import { TaskService } from 'src/app/infraestructure/services/task.service';
import { UserDTO } from '../../../../domain/model/dto/UserDTO';
import { actividades, statesTaskActions, codigosParametros, constantCompanyAreaCode, constantProcessRoleCode, consPantalla } from 'src/environments/environment.variables';
import { ROUTES_PATH } from '../../pages.routing.name';
import { SolicitudVacacionesDTO } from 'src/app/domain/model/dto/SolicitudVacacionesDTO';
import { TablaBasicaDTO } from 'src/app/domain/model/dto/TablaBasicaDTO';

import { EmpleadoDTO } from '../../../../domain/model/dto/EmpleadoDTO';
import { JefeInmediatoDTO } from '../../../../domain/model/dto/JefeInmediatoDTO';
import { JefeInmediatoService } from '../../../../infraestructure/services/jefe-inmediato.service';
import { JEFE } from '../../../../domain/model/mocks/JefeInmediatoMock';
import { GerenteDTO } from '../../../../domain/model/dto/GerenteDTO';
import { GERENTE } from '../../../../domain/model/mocks/gerenteMock';
import { GerenteService } from '../../../../infraestructure/services/gerente.service';
import { RecursosHumanosDTO } from '../../../../domain/model/dto/RecursosHumanosDTO';
import { RecursosHumanosService } from '../../../../infraestructure/services/recursos-humanos.service';
import { RECURSOS } from '../../../../domain/model/mocks/recursosHumanosMock';
import { MessageService, ConfirmationService } from 'primeng/api';
import { DatePipe } from '@angular/common';

import { ProcessManagersDTO } from 'src/app/domain/model/dto/ProcessManagersDTO';
import { CatalogueDTO } from 'src/app/domain/model/dto/CatalogueDTO';
import { TipoTramiteDTO } from 'src/app/domain/model/dto/TipoTramiteDTO';
import { successResponse, processContainer, processes, constantRequestType } from 'src/environments/environment.variables';
import { TipoIdentificacionDTO } from 'src/app/domain/model/dto/TipoIdentificacionDTO';
import { ProcesoDTO } from 'src/app/domain/model/dto/ProcesoDTO';
import { RqParametrosRadicarTramiteDTO } from 'src/app/domain/model/dto/RqParametrosRadicarTramiteDTO';
import { GestionTramiteDTO } from 'src/app/domain/model/dto/GestionTramiteDTO';
import { RolDTO } from 'src/app/domain/model/dto/RolDTO';


@Component({
    selector: 'app-crear-tramite',
    templateUrl: './crear-tramite.component.html',
    styles: [`
        :host ::ng-deep button {
            margin-right: .25em;
        }

        :host ::ng-deep .custom-toast .ui-toast-message {
            color: #ffffff;
            background: #FC466B;
            background: -webkit-linear-gradient(to right, #3F5EFB, #FC466B);
            background: linear-gradient(to right, #3F5EFB, #FC466B);
        }

        :host ::ng-deep .custom-toastError .ui-toast-message {
            color: #ffffff;
            background: #fb151e;
            background: -webkit-linear-gradient(to right, #e6effb, #faeefb);
            background: linear-gradient(to right, #fb151e, #850509);
        }

        :host ::ng-deep .custom-toast .custom-toastError .ui-toast-close-icon {
            color: #ffffff;
        }
    `],
    providers: [MessageService, DatePipe]
})
export class CrearTramiteComponent implements OnInit {

    request = new ProcessRequestDTO;
    //user = new UserDTO;
    activity = actividades;
    tarea: TareaDTO;
    empleado: EmpleadoDTO;
    //requestType: TablaBasicaDTO[];
    requestType: TipoTramiteDTO[] = new Array<TipoTramiteDTO>()

    companyArea: CatalogueDTO[] = new Array<CatalogueDTO>();
    identificationTypes: CatalogueDTO[] = new Array<CatalogueDTO>()
    immediateBosses: CatalogueDTO[];

    processManagers: ProcessManagersDTO[];

    //tipoSolicitud: TablaBasicaDTO;
    tipoSolicitud: TipoTramiteDTO;
    solicitud: TramiteDTO;
    rqRadicarTramiteDTO: RqRadicarTramiteDTO;
    jefe: JefeInmediatoDTO;
    gerente: GerenteDTO;
    recursosHumanos: RecursosHumanosDTO;

    modificarTramite: boolean = false;
    diasDisponibles: number;
    instanciaId: string;
    tareaId: number=0;
    disabledForm: boolean = true;

    formValidoInfoPrincipal: boolean = false;
    formValidoSolVacaciones: boolean = false;
    formValidoDetSolicitud: boolean = false;
    formValidoCertificados: boolean = false;
    diasSolicitados: number = 0;
    pantallaOrigen: string = consPantalla.crearTramite;

    constructor(
        private cd: ChangeDetectorRef,
        private loginService: LoginService,
        private tareaService: TaskService,
        private tb: TablasBasicasService,
        private fn: UtilitiesFnService,
        private tramiteService: TramiteService,
        private jefeInmediato: JefeInmediatoService,
        private gerenteService: GerenteService,
        private recursosHumanosService: RecursosHumanosService,
        private router: Router,
        private messageService: MessageService,
        private procesoService: ProcessService,
        private alertService: AlertService,
        private confirmationService: ConfirmationService,
        private datePipe: DatePipe
    ) {
        this.rqRadicarTramiteDTO = new RqRadicarTramiteDTO();
        this.solicitud = new TramiteDTO;
        // Se reclama la info del user q estaba en el store
        //this.loginService.currentUser.subscribe(x => this.user = x);
        this.loginService.currentUser.subscribe(x => this.request.ownerUser = x);
        // Se reclama la info de la tarea q estaba en el store
        this.tarea = JSON.parse(localStorage.getItem('task'));
    }

    ngOnInit() {
        if (!!this.request.ownerUser){
            let procesoDTO = new ProcesoDTO();
            procesoDTO.containerId = processContainer.containerId;
            procesoDTO.processId = processes.processId;

            this.iniciarProceso(procesoDTO);

            this.cargarCatalogos();
            this.jefe = JEFE;
            this.gerente = GERENTE;
            this.recursosHumanos = RECURSOS;

            // Cargue de la petición de negocio
            //this.request.containerId = this.tarea.containerId;
            //this.request.processInstance = this.tarea.instanceId;
            //this.request.taskId = this.tarea.taskId.toString();
            this.request.taskStatus = statesTaskActions.started;
            this.request.parametros = new ParametrosDTO();
            // Cambio de estado de la tarea a fin de q se inicie al reclamar tarea
            //this.tarea.taskStatus != states.inProgress ? this.cambiarEstadoTarea(this.request) : null;
            // Se carga el # de soilicitud y se procede a consultar la información
            /*this.solicitud.numeroRadicado = this.tarea.instanceId;
            this.jefe.numeroRadicado = this.tarea.instanceId;
            this.gerente.numeroRadicado = this.tarea.instanceId;
            this.recursosHumanos.numeroRadicado = this.tarea.instanceId;*/
            //this.consultarEmpleado(this.tarea.instanceId);
            let date: Date = new Date();
            this.solicitud.fecha = this.datePipe.transform(date, "dd/MM/yyyy");
        }else
            this.router.navigate(['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.login]);
    }

    private iniciarProceso(proceso: ProcesoDTO) {
        this.request.containerId = proceso.containerId;
        this.request.processesId = proceso.processId;
        this.procesoService.iniciarProceso(this.request).subscribe(data => {
            if (!!data.response && data.response.statusCode == successResponse.statusCode) {
              const aux = data.containers[0].processes;
              for (let index = 0; index < aux.length; index++) {
                if (aux[index].processId == proceso.processId) {
                    //this.irListaTareas(aux[index]);;
                    let processRequestAux = new ProcessRequestDTO();
                    processRequestAux.ownerUser = this.request.ownerUser;
                    processRequestAux.processInstance = aux[index].instance.instanceId;
                    debugger;
                
                    this.consultarTareasPorInstancia(processRequestAux);
                  break;
                }
              }
            }
          }, error => this.alertService.error(error.message),
            () => this.detectChanges()
          );
      }

      private consultarTareasPorInstancia(processRequest: ProcessRequestDTO){
          debugger;
        this.tareaService.consultarTareasPorInstancia(processRequest).subscribe(data => {
            if (!!data.response && data.response.statusCode == successResponse.statusCode) {
              const auxProcesses = data.containers[0].processes;
              for (let indexProcesses = 0; indexProcesses < auxProcesses.length; indexProcesses++) {
                const auxTaskList = auxProcesses[indexProcesses].taskList;
                for (let indexTaskList = 0; indexTaskList < auxTaskList.length; indexTaskList++) {
                    if (auxTaskList[indexTaskList].instanceId == processRequest.processInstance) {
                        let processRequestAux = new ProcessRequestDTO();
                        processRequestAux.containerId = processContainer.containerId;
                        processRequestAux.taskId = auxTaskList[indexTaskList].taskId;
                        processRequestAux.taskStatus = statesTaskActions.started;
                        processRequestAux.ownerUser = this.request.ownerUser;
                        debugger;
                        this.iniciarTarea(processRequestAux);
                        break;
                    }
                }
                
              }
            }
          }, error => this.alertService.error(error.message),
            () => this.detectChanges()
          );
      }

    private iniciarTarea(request: ProcessRequestDTO) {
        this.tareaService.cambiarEstadoTarea(request)
            .subscribe(data => {
                if (!!data && !!data.containers && !!data.containers[0].processes
                    && !!data.containers[0].processes[0].taskList) {
                    const task = data.containers[0].processes[0].taskList[0];
                    this.tareaId = data.containers[0].processes[0].taskList[0].taskId;
                    this.instanciaId = data.containers[0].processes[0].taskList[0].instanceId;
                    //localStorage.setItem('task', JSON.stringify(task));
                    //this.router.navigate(['/' + ROUTES_PATH.home + '/' + task.taskName.replace(/ /g, "")]);
                }
            });
    }

    

      detectChanges() {
        setTimeout(() => {
            if (!(this.cd as ViewRef).destroyed) {
                this.cd.detectChanges();
            }
        }, 250);
    }

    cargarCatalogos() {
        //this.identificationTypes = MOCK_IDENTIFICATION_TYPE_TABLE;
        //this.companyArea = MOCK_COMPANY_AREA_TABLE;
        let listaCatalogos: string[] = ['AreaEmpresa', 'RolTramite', 'TipoIdentificacion'];
        this.tb.consultarCatalogos(listaCatalogos).subscribe(data => {
            if (!!data.response && data.response.statusCode == successResponse.statusCode) {
                data.catalogs.forEach(element => {
                    if (!!element) {
                        if (element.catalogo == 'AreaEmpresa')
                            this.companyArea = element.data;
                        else if (element.catalogo == 'TipoIdentificacion')
                            this.identificationTypes = element.data;
                    }
                });
            }
        });

        //this.requestType = MOCK_PROCEDURE_TYPE_TABLE;
        this.tb.consultarTipoTramite(null).subscribe(data => {
            if (!!data.response && data.response.statusCode == successResponse.statusCode) {
                this.requestType = data.proceduresTypes;
            }
        });
        //this.processManagers = MOCK_PROCESS_MANAGERS_TABLE;
        this.tramiteService.consultarGestorTramite().subscribe(data => {
            if (!!data.response && data.response.statusCode == successResponse.statusCode) {
                this.processManagers = data.processManagers;
                this.consultManagers(constantCompanyAreaCode.operationsManagementCode, constantProcessRoleCode.immediateBossCode);
            }
        });
    }

    /**
     * 
     * @param idArea 
     * @param idRole 
     */
    consultManagers(idArea: string, idRole: number) {
        if (!!idArea && !!idRole && !!this.processManagers) {
            this.immediateBosses = new Array<CatalogueDTO>();
            for (let index = 0; index < this.processManagers.length; index++) {
                if (this.processManagers[index].idArea == idArea && this.processManagers[index].idRol == idRole) {
                    this.immediateBosses.push(
                        {
                            id: this.processManagers[index].id,
                            nombre: this.processManagers[index].nombre + ' ' + this.processManagers[index].apellido,
                            descripcion: '',
                            nombreBpm: this.processManagers[index].usuario
                        }
                    );
                }
            }
        }
    }



    /**
     * Cambia el estado de la tarea entre los posibles existente a nivel de negocio
     * @param request
     */
    cambiarEstadoTarea(request: ProcessRequestDTO) {
        this.tareaService.cambiarEstadoTarea(request)
            .subscribe(data => {
                if (!!data && !!data.containers && !!data.containers[0].processes
                    && !!data.containers[0].processes[0].taskList) {
                    this.tarea = data.containers[0].processes[0].taskList[0];
                }
            });
    }

    /**
     * Consulta toda la información correspondinete a la empleado
     * @param request
     */
    // consultarEmpleado(request: string) {
    //   this.solicitudService.consultarEmpleado(request)
    //     .subscribe(data => {
    //       debugger;
    //       // Aca va la logica para cargar la info cuando se halla dado guardar
    //     });
    // }

    //************************************************************EVENTOS********************************************************** */

    recibirInformacionPpal(event: any) {
        debugger
        this.rqRadicarTramiteDTO.idArea = event.infoPpalTramite != null? event.infoPpalTramite.idArea : '';
        this.rqRadicarTramiteDTO.idGestorTramite = event.infoPpalTramite != null? event.infoPpalTramite.idGestorTramite : '';
        this.rqRadicarTramiteDTO.idTipoIdentificacion = event.infoPpalTramite != null? event.infoPpalTramite.idTipoIdentificacion : '';
        this.rqRadicarTramiteDTO.idTipoTramite = event.infoPpalTramite != null? event.infoPpalTramite.idTipoTramite : '';
        this.rqRadicarTramiteDTO.tramite = event.infoPpalTramite != null? event.infoPpalTramite.tramite : '';
        this.activarEnvioFormulario(event.validForm, -1, -1, -1);
    }
    
    recibirInformacionCertificado (event: any) {
        debugger;
        if(event.length > 0){
            this.rqRadicarTramiteDTO.certificadoTramite = event.certificadoTramite;
        }
        this.activarEnvioFormulario(-1, -1, -1, event.validForm);
    }

    recibirInformacionVacaciones(event: any) {
        debugger;
        this.rqRadicarTramiteDTO.vacacionesTramite = event.vacacionesTramite;

        this.activarEnvioFormulario(-1, event.validForm, -1, -1);
    }

    recibirInformacionDetalle(event) {
        debugger;
        this.rqRadicarTramiteDTO.detalleTramite = event.detalleTramite != null? event.detalleTramite : null;
        
        this.activarEnvioFormulario(-1, -1, event.validForm, -1);
    }

    private activarEnvioFormulario(validoInfoPrincipal, validoSolVacaciones, validoDetSolicitud, validoCertificados) {
        debugger;
        this.formValidoInfoPrincipal = validoInfoPrincipal != -1? validoInfoPrincipal : this.formValidoInfoPrincipal;
        this.formValidoDetSolicitud = validoDetSolicitud != -1? validoDetSolicitud : this.formValidoDetSolicitud;
        this.formValidoSolVacaciones = validoSolVacaciones != -1? validoSolVacaciones : this.formValidoSolVacaciones;
        this.formValidoCertificados = validoCertificados != -1? validoCertificados : this.formValidoCertificados;

        if (!!this.tipoSolicitud && !!this.tipoSolicitud.id) {
            switch (this.tipoSolicitud.id) {
                case constantRequestType.holidayCode:
                    if (this.formValidoInfoPrincipal && this.formValidoSolVacaciones)
                        this.disabledForm = false;
                    else
                        this.disabledForm = true;
                    break;
                case constantRequestType.permissionsCode:
                    if (this.formValidoInfoPrincipal && this.formValidoDetSolicitud)
                        this.disabledForm = false;
                    else
                        this.disabledForm = true;
                    break;
                case constantRequestType.disabilityReportCode:
                    if (this.formValidoInfoPrincipal && this.formValidoDetSolicitud)
                        this.disabledForm = false;
                    else
                        this.disabledForm = true;
                    break;
                case constantRequestType.familyDayCode:
                    if (this.formValidoInfoPrincipal && this.formValidoDetSolicitud)
                        this.disabledForm = false;
                    else
                        this.disabledForm = true;
                    break;
                case constantRequestType.birthdayCode:
                    if (this.formValidoInfoPrincipal && this.formValidoDetSolicitud)
                        this.disabledForm = false;
                    break;
                case constantRequestType.domesticCalamityCode:
                    if (this.formValidoInfoPrincipal && this.formValidoDetSolicitud)
                        this.disabledForm = false;
                    else
                        this.disabledForm = true;
                    break;
                case constantRequestType.mourningLicenseCode:
                    if (this.formValidoInfoPrincipal && this.formValidoDetSolicitud)
                        this.disabledForm = false;
                    else
                        this.disabledForm = true;
                    break;
                case constantRequestType.compensatoryCode:
                    if (this.formValidoInfoPrincipal && this.formValidoDetSolicitud)
                        this.disabledForm = false;
                    else
                        this.disabledForm = true;
                    break;
                case constantRequestType.compensatoryVoteCode:
                    if (this.formValidoInfoPrincipal && this.formValidoDetSolicitud)
                        this.disabledForm = false;
                    else
                        this.disabledForm = true;
                    break;
                case constantRequestType.certificationCode:
                    if (this.formValidoInfoPrincipal && this.formValidoCertificados)
                        this.disabledForm = false;
                    else
                        this.disabledForm = true;
                    break;
                default:
                    console.log("Tipo de trámite no enviado");
                    this.disabledForm = true;
                    break;
            }
        }


    }

    

    /*enviarNumeroDocumento(event: TramiteDTO){
        this.solicitud.solicitante = event;
        //debugger
    }*/

    /**
     * Recibe los cambios sobre el tipo de solicitud
     * @param event
     */
    enviarTipoSolicitud(event: any) {
        this.tipoSolicitud = event;
    }

    /**
     * Método de comunicación de días disponibles a todas las pantallas dependientes
     * @param event 
     */
    enviarDiasDisponibles(event: number) {
        this.diasDisponibles = event;
    }


    

    /**
     * Recibe los cambios de los documentos anexos
     * @param event
     */
    enviarDocumentos(event) {

    }
    /**
     * Recibe la información de los certificados
     * @param event
     */
    /* enviarInformacionJefe(event) {
        this.jefe = event;
    } */
    /**
     * Recibe la información de los certificados
     * @param event
     */
    /* enviarInformacionGerente(event) {
        this.gerente = event;
    } */
    /**
     * Recibe la información de los certificados
     * @param event
     */
    enviarInformacionRecursosHumanos(event) {
        this.recursosHumanos = event;
    }

    

    /**
     * Captura del evento de guardar solicitud
     * @param event
     */
    guardarEvent(tarea: TramiteDTO) {
        debugger;
        this.radicarSolicitud(this.rqRadicarTramiteDTO);
    }
    /**
     * Captura del evento de guardar solicitud
     * @param event
     */
    guardarJefeEvent(tarea: TramiteDTO) {
        this.consumirJefeInmediato(this.jefe);
        debugger;
    }
    /**
     * Captura del evento de guardar solicitud
     * @param event
     */
    guardarGerenteEvent(tarea: TramiteDTO) {
        this.consumirGerente(this.gerente);
        debugger;
    }
    /**
     * Captura del evento de guardar solicitud
     * @param event
     */
    guardarRecursosEvent(tarea: TramiteDTO) {
        this.consumirRecursosHumanos(this.recursosHumanos);
        debugger;
    }

    private completarTarea(request: ProcessRequestDTO) {
        this.tareaService.cambiarEstadoTarea(request)
            .subscribe(data => {
                debugger
                if (!!data && !!data.containers && !!data.containers[0].processes
                    && !!data.containers[0].processes[0].taskList) {
                    const task = data.containers[0].processes[0].taskList[0];
                    this.tareaId = data.containers[0].processes[0].taskList[0].taskId;
                    this.instanciaId = data.containers[0].processes[0].taskList[0].instanceId;
                    //localStorage.setItem('task', JSON.stringify(task));
                    //this.router.navigate(['/' + ROUTES_PATH.home + '/' + task.taskName.replace(/ /g, "")]);

                    this.irListaTareas(null);
                } 
            });
    }

    private irListaTareas(data: ProcesoDTO) {
        this.confirmationService.confirm({
          message: 'Número de trámite:' + '__' + ' Número de proceso:' + this.instanciaId, // + data.instance.instanceId,
          header: 'Trámite Registrado',
          rejectVisible: false,
          accept: () => {
            this.router.navigate(['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.task]);
          }
        });
      }

    private validarCaracteresEspeciales(texto: string){
        return texto.normalize('NFD').replace(/[\u0300-\u036f]/g,"");
    }

    /**
     * Ejecuta el servicio de crear el radicado o en su defecto almacenarlo
     * @param request
     */
    private radicarSolicitud(rqRadicarTramiteDTO: RqRadicarTramiteDTO) {
        debugger;
        //<Carga de atributos faltantes para la radicación del tramite>
        //reformateo de fecha
        rqRadicarTramiteDTO.tramite.fecha = this.aplicarFormatoFecha(rqRadicarTramiteDTO.tramite.fecha); //2020-04-16
        rqRadicarTramiteDTO.tramite.idInstancia = this.instanciaId;
        let gestionTramiteDTO = new GestionTramiteDTO();
        gestionTramiteDTO.fecha = rqRadicarTramiteDTO.tramite.fecha //2020-04-16
        gestionTramiteDTO.revisadoPor = rqRadicarTramiteDTO.tramite.nombreEmpleado + ' ' + rqRadicarTramiteDTO.tramite.apellidoEmpleado;
        if(rqRadicarTramiteDTO.detalleTramite != null){
            gestionTramiteDTO.observaciones = rqRadicarTramiteDTO.detalleTramite.observaciones;
            this.diasSolicitados = rqRadicarTramiteDTO.detalleTramite.diasSolicitados;
        }
        gestionTramiteDTO.respuestaTramite = 'Radicar Trámite';
        let rolDTO = new RolDTO();
        rolDTO.id = this.request.ownerUser.roleId;
        gestionTramiteDTO.idRol = rolDTO;
        rqRadicarTramiteDTO.gestionTramite = new Array<GestionTramiteDTO>();
        rqRadicarTramiteDTO.gestionTramite.push(gestionTramiteDTO);
        //</Carga de atributos faltantes para la radicación del tramite>


        let rqParametrosRadicarTramiteDTO = new RqParametrosRadicarTramiteDTO();

        let selectedcompanyArea : CatalogueDTO;
        selectedcompanyArea = this.fn.findNameById(this.companyArea, rqRadicarTramiteDTO.idArea);

        rqParametrosRadicarTramiteDTO.Area = this.validarCaracteresEspeciales(selectedcompanyArea.nombre); //validar si info princ lo envía
        rqParametrosRadicarTramiteDTO.DiasSolicitados = this.diasSolicitados; //en algunos casos no llega validar que otros lo envien menos certificados
        rqParametrosRadicarTramiteDTO.Email = this.request.ownerUser.email;
        rqParametrosRadicarTramiteDTO.Rol = this.request.ownerUser.roleName;
        rqParametrosRadicarTramiteDTO.Tramite = this.validarCaracteresEspeciales(this.tipoSolicitud.nombre);
        debugger;
        let selectedImmediateBosses : CatalogueDTO;
        selectedImmediateBosses = this.fn.findNameById(this.immediateBosses, rqRadicarTramiteDTO.idGestorTramite);
        if(!!selectedImmediateBosses && !!selectedImmediateBosses.nombreBpm)
            rqParametrosRadicarTramiteDTO.Usuario = selectedImmediateBosses.nombreBpm;
        rqParametrosRadicarTramiteDTO.Fecha = this.aplicarFormatoFecha(rqRadicarTramiteDTO.tramite.fecha); //'2020-02-14'
        rqParametrosRadicarTramiteDTO.idTramite = 40; //de la ejecución del servicio de radicacion a BD

        //const valores = { values: JSON.stringify(rqParametrosRadicarTramiteDTO)};

        const valores = {
            Area: rqParametrosRadicarTramiteDTO.Area,
			DiasSolicitados: rqParametrosRadicarTramiteDTO.DiasSolicitados,
			Email: rqParametrosRadicarTramiteDTO.Email,
			Rol: rqParametrosRadicarTramiteDTO.Rol,
			Tramite: rqParametrosRadicarTramiteDTO.Tramite,
			Usuario: rqParametrosRadicarTramiteDTO.Usuario,
			Fecha: rqParametrosRadicarTramiteDTO.Fecha,
            idTramite: rqParametrosRadicarTramiteDTO.idTramite,
            Cancelar: false
        };

        let processRequestAux = new ProcessRequestDTO();
        processRequestAux.containerId = processContainer.containerId;
        processRequestAux.taskId = this.tareaId;
        processRequestAux.taskStatus = statesTaskActions.completed;

        let userDTO = new UserDTO;
        userDTO.user = this.request.ownerUser.user;
        userDTO.password = this.request.ownerUser.password;
        processRequestAux.ownerUser = userDTO;
        
        //let parametrosDTO = new ParametrosDTO();
        //parametrosDTO.values = rqParametrosRadicarTramiteDTO;
        processRequestAux.parametros = new ParametrosDTO();
        processRequestAux.parametros.values = valores;
        debugger;
        //this.completarTarea(processRequestAux);

        //prueba de radicacion
         this.tramiteService.radicarTramite(this.rqRadicarTramiteDTO)
            .subscribe(data => {
                debugger
                if (!data) {
                    this.messageService.add({ key: 'customError', severity: 'error', summary: 'Solicitud Rechazada', detail: 'Esta solicitud ya Existe' });
                }
                else {
                    this.messageService.add({ key: 'custom', severity: 'success', summary: 'Solicitud Guardada', detail: 'Su solicitud se ha guardado exitosamente' });
                    this.completarTarea(processRequestAux);
                }
            }, error1 => {
                if (error1) {
                    this.messageService.add({ key: 'custom', severity: 'success', summary: 'Solicitud Guardada', detail: 'Su solicitud se ha rechazado exitosamente' });
                }
            }); 
    }

    private aplicarFormatoFecha(fechaOrigen: string){
        let separador = '-';
        var splitted = fechaOrigen.split("/", 3);
        return splitted[2] + separador + splitted[1] + separador + splitted[0];
    }

    consumirJefeInmediato(request: JefeInmediatoDTO) {
        this.jefeInmediato.jefeInmediato(this.jefe).subscribe(data => {
            debugger
            if (!data) {
                this.messageService.add({ key: 'customError', severity: 'error', summary: 'Solicitud Rechazada', detail: 'Esta respuesta ya fue guardada' });
            }
            else {
                this.messageService.add({ key: 'custom', severity: 'success', summary: 'Respuesta Guardada', detail: 'Su respuesta se ha guardado y enviado exitosamente' });
            }
        }, error1 => {
            if (error1) {
                this.messageService.add({ key: 'custom', severity: 'success', summary: 'Solicitud Guardada', detail: 'Su solicitud se ha rechazado exitosamente' });
            }
        });
    }
    consumirGerente(request: GerenteDTO) {
        this.gerenteService.opGerente(this.gerente).subscribe(data => {
            debugger
            if (!data) {
                this.messageService.add({ key: 'customError', severity: 'error', summary: 'Solicitud Rechazada', detail: 'Esta respuesta ya fue guardada' });
            }
            else {
                this.messageService.add({ key: 'custom', severity: 'success', summary: 'Respuesta Guardada', detail: 'Su respuesta se ha guardado y enviado exitosamente' });
            }
        }, error1 => {
            if (error1) {
                this.messageService.add({ key: 'custom', severity: 'success', summary: 'Solicitud Guardada', detail: 'Su solicitud se ha rechazado exitosamente' });
            }

        });
    }
    consumirRecursosHumanos(request: RecursosHumanosDTO) {
        this.recursosHumanosService.recursosHumanos(this.recursosHumanos).subscribe(data => {
            if (!data) {
                this.messageService.add({ key: 'customError', severity: 'error', summary: 'Solicitud Rechazada', detail: 'Esta respuesta ya fue guardada' });
            }
            else {
                this.messageService.add({ key: 'custom', severity: 'success', summary: 'Respuesta Guardada', detail: 'Su respuesta se ha guardado y enviado exitosamente' });
            }
        }, error1 => {
            if (error1) {
                this.messageService.add({ key: 'custom', severity: 'success', summary: 'Solicitud Guardada', detail: 'Su solicitud se ha rechazado exitosamente' });
            }
        });
    }
}



