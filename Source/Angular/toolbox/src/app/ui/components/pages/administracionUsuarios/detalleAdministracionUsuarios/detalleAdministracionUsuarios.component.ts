import { Component, OnInit, EventEmitter, Input, Output } from "@angular/core";
import { UserDTO } from "src/app/domain/model/dto/UserDTO";
import { TablaBasicaDTO } from "src/app/domain/model/dto/TablaBasicaDTO";
import { states } from "src/environments/environment.variables";

@Component({
    selector: 'app-detalleAdministracionUsuarios',
    templateUrl: './detalleAdministracionUsuarios.component.html'
})
export class DetalleAdministracionUsuariosComponent implements OnInit {

    @Input() usuarioHijo: UserDTO;
    @Input() listaTipoIdent: TablaBasicaDTO[];
    @Input() listaRol: TablaBasicaDTO[];
    @Output() guardarUsuario = new EventEmitter<UserDTO>();
    display: boolean;
    rolSelect = new TablaBasicaDTO();
    tipoIdentSelect = new TablaBasicaDTO();
    newUser: boolean;

    constructor() {
        this.display = true;

        this.newUser = true;
    }

    ngOnInit() {
        if (!!this.usuarioHijo) {
            this.listaRol.forEach(element => {
                if (element.codigoApp == ''+this.usuarioHijo.roleId)
                    this.rolSelect = element;
            });

            this.listaTipoIdent.forEach(element => {
                if (element.codigoApp == this.usuarioHijo.identType)
                    this.tipoIdentSelect = element;
            });
            if (!!this.usuarioHijo.user)
                this.newUser = false;
        }
    }

    guardarUsuarioInfo() {
        this.display = false;
        this.usuarioHijo.identType = this.tipoIdentSelect.codigoApp;
        this.usuarioHijo.state = states.create;
        if (!!this.newUser) {
            this.usuarioHijo.roleId = Number(this.rolSelect.codigoApp);
        } else {
            this.usuarioHijo.newRole = this.rolSelect.codigoApp;
            this.usuarioHijo.password = null;
        }

        this.guardarUsuario.emit(this.usuarioHijo);
    }

    cerrar() {
        this.usuarioHijo = null;
        this.guardarUsuario.emit(this.usuarioHijo);
    }

}