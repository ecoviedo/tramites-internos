import { OnInit, Component, ChangeDetectorRef, ViewRef } from "@angular/core";
import { TareaDTO } from "src/app/domain/model/dto/TareaDTO";
import { ProcessRequestDTO } from "src/app/domain/model/dto/ProcessRequestDTO";
import { TaskService } from "src/app/infraestructure/services/task.service";
import { AlertService, LoginService } from "src/app/infraestructure/services/service.index";
import { Router } from "@angular/router";
import { UserDTO } from "src/app/domain/model/dto/UserDTO";
import { states, createdResponse } from "src/environments/environment.variables";
import { ROUTES_PATH } from "../../../pages.routing.name";

@Component({
    selector: 'app-reasignarTarea',
    templateUrl: './reasignarTarea.component.html'
})
export class ReasignarTareaComponent implements OnInit {

    tareas: TareaDTO[];
    request = new ProcessRequestDTO;

    constructor(
        private cd: ChangeDetectorRef,
        private tareaService: TaskService,
        private alertService: AlertService,
        private router: Router,
        private loginService: LoginService
    ) {
        this.loginService.currentUser.subscribe(x => this.request.ownerUser = x);
    }

    ngOnInit() {
        //if (!!this.request.ownerUser)
            //this.cargarTareasPorGrupo(this.request.ownerUser.role)

        this.request.assignment = new UserDTO();
    }

    cargarTareasPorGrupo(role: string) {
        this.request.groups = new Array<string>();
        this.request.groups.push(role);
        this.tareaService.consultarTareasPorGrupos(this.request)
            .subscribe(data => {
                if (!!data && !!data.containers && !!data.containers[0].processes) {
                    this.tareas = new Array<TareaDTO>();
                    data.containers[0].processes[0].taskList.forEach(element => {
                        if (element.taskStatus == states.create ||
                            element.taskStatus == states.inProgress ||
                            element.taskStatus == states.reserved)
                            this.tareas.push(element);
                    });
                }
            });
    }

    detectChanges() {
        setTimeout(() => {
            if (!(this.cd as ViewRef).destroyed) {
                this.cd.detectChanges();
            }
        }, 250);
    }

    reasignarTarea(task: TareaDTO) {
        if (!!task) {
            this.request.assignment.user = task.taskActualOwner;
            this.request.containerId = task.containerId;
            this.request.taskId = task.taskId;
        }
    }

    enviarAsignacion(valor: string) {
        if (!!valor) {
            this.request.assignment.targetUser = valor;
            this.tareaService.reasignarTarea(this.request)
                .subscribe(data => {
                    if (!!data.response && data.response.statusCode == createdResponse.statusCode)
                        this.router.navigate(['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.task]);
                });
        }
        this.request.assignment.targetUser = null;
        this.request.assignment.user = null;
    }
}