import { PAGES_ROUTES_MODULE } from './pages-routing.module';
import { PRIMENG_MODULES } from './../../shared/primeNg/primeng-elements';
import { PagesComponent } from './pages.component';
import { LayoutModule } from './layouts/layout.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './pages/auth/login/login.component';
import { RegisterComponent } from './pages/auth/register/register.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { InformacionPrincipalComponent } from './tools/informacion-principal/informacion-principal.component';
import { InformacionPrincipalLecturaComponent } from './tools/informacion-principal-lectura/informacion-principal-lectura.component';
import { CrearTramiteComponent } from './pages/crear-tramite/crear-tramite.component';
import { ModificarTramiteComponent } from './pages/modificar-tramite/modificar-tramite.component';
import {ReactiveFormsModule} from '@angular/forms';
import { SolicitudVacacionesComponent } from './tools/solicitud-vacaciones/solicitud-vacaciones.component';
import { SolicitudVacacionesComponentLectura } from './tools/solicitud-vacaciones-lectura/solicitud-vacaciones-lectura.component';
import { DetalleSolicitudComponent } from './tools/detalle-solicitud/detalle-solicitud.component';
import { DetalleSolicitudLecturaComponent } from './tools/detalle-solicitud-lectura/detalle-solicitud-lectura.component';
import { GestionCertificadosComponent } from './tools/gestion-certificados/gestion-certificados.component';
import { GestionCertificadosLecturaComponent } from './tools/gestion-certificados-lectura/gestion-certificados-lectura.component';
import { TrazabilidadTramiteComponent } from './tools/trazabilidad-tramite/trazabilidad-tramite.component';
import { TablaDocumentosComponent } from './tools/detalle-solicitud/tabla-documentos/tabla-documentos.component';
import {PdfViewerModule} from 'ng2-pdf-viewer';
import { AdministracionProcesosComponent } from './pages/administracionProcesos/administracionProcesos.component';
import { AdministracionServiciosComponent } from './pages/administracionServicios/administracionServicios.component';
import { AdministracionTareasComponent } from './pages/administracionTareas/administracionTareas.component';
import { ReasignarTareaComponent } from './pages/administracionTareas/reasignarTarea/reasignarTarea.component';
import { DetalleAdministracionUsuariosComponent } from './pages/administracionUsuarios/detalleAdministracionUsuarios/detalleAdministracionUsuarios.component';
import { PerfilUsuarioComponent } from './pages/administracionUsuarios/perfilUsuario/perfilUsuario.component';
import { AdministracionUsuariosComponent } from './pages/administracionUsuarios/administracionUsuarios.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ReasignarTareaDetalleComponent } from './pages/administracionTareas/reasignarTarea/reasignarTareaDetalle/reasignarTareaDetalle.component';
import { CabeceraProcesoComponent } from './tools/cabeceraProceso/cabeceraProceso.component';
import { BotoneraProcesoComponent } from './tools/botoneraProceso/botoneraProceso.component';
import { GestionTramiteVistoBuenoComponent } from './tools/gestion-tramite-visto-bueno/gestion-tramite-visto-bueno.component';
import { GestionTramiteVistoBuenoLecturaComponent } from './tools/gestion-tramite-visto-bueno-lectura/gestion-tramite-visto-bueno-lectura.component';
import { GestionTramiteVistoBuenoGHComponent } from './tools/gestion-tramite-visto-bueno-gh/gestion-tramite-visto-bueno-gh.component';
import { AprobacionGerenteComponent } from './pages/aprobacion-gerente/aprobacion-gerente.component';
import {BlockUIModule} from 'primeng/blockui';
import {QRCodeModule} from "angularx-qrcode";

import { gestionarSolicitudJefeComponent } from './pages/gestionar-solicitud-jefe/gestionar-solicitud-jefe.component';
import { gestionarSolicitudGerenteComponent } from './pages/gestionar-solicitud-gerente/gestionar-solicitud-gerente.component';
import { gestionarSolicitudGerenteGHComponent } from './pages/gestionar-solicitud-gerente-gh/gestionar-solicitud-gerente-gh.component';


@NgModule({
  declarations: [
      PagesComponent,
      LoginComponent,
      RegisterComponent,
      PageNotFoundComponent,
      AdministracionServiciosComponent,
      AdministracionTareasComponent,
      ReasignarTareaComponent,
      ReasignarTareaDetalleComponent,
      DetalleAdministracionUsuariosComponent,
      PerfilUsuarioComponent,
      AdministracionUsuariosComponent,
      DashboardComponent,
      InformacionPrincipalComponent,
      InformacionPrincipalLecturaComponent,
      AdministracionProcesosComponent,
      CrearTramiteComponent,
      ModificarTramiteComponent,
      SolicitudVacacionesComponent,
      SolicitudVacacionesComponentLectura,
      DetalleSolicitudComponent,
      DetalleSolicitudLecturaComponent,
      GestionCertificadosComponent,
      GestionCertificadosLecturaComponent,
      TrazabilidadTramiteComponent,
      TablaDocumentosComponent,
      CabeceraProcesoComponent,
      BotoneraProcesoComponent,
      GestionTramiteVistoBuenoComponent,
      GestionTramiteVistoBuenoLecturaComponent,
      GestionTramiteVistoBuenoGHComponent,
      AprobacionGerenteComponent,

      gestionarSolicitudJefeComponent,
      gestionarSolicitudGerenteComponent,
      gestionarSolicitudGerenteGHComponent
  ],
    exports: [
        PagesComponent,
        CabeceraProcesoComponent,
        InformacionPrincipalComponent,
        InformacionPrincipalLecturaComponent,
        GestionTramiteVistoBuenoComponent,
        GestionTramiteVistoBuenoLecturaComponent,
        GestionTramiteVistoBuenoGHComponent
    ],
    imports: [
        CommonModule,
        LayoutModule,
        ...PRIMENG_MODULES,
        PAGES_ROUTES_MODULE,
        ReactiveFormsModule,
        PdfViewerModule,
        BlockUIModule,
        QRCodeModule
    ]
})
export class PagesModule { }
