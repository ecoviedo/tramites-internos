import {ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import {DetalleSolicitudDTO} from 'src/app/domain/model/dto/DetalleSolicitudDTO';
import {DocumentDTO} from 'src/app/domain/model/dto/DocumentDTO';
import {TramiteDTO} from 'src/app/domain/model/dto/TramiteDTO';
import {TablaBasicaDTO} from 'src/app/domain/model/dto/TablaBasicaDTO';
import {ConfirmationService, UtilitiesFnService, TramiteService} from 'src/app/infraestructure/services/service.index';
import {TablasBasicasService} from '../../../../infraestructure/services/tablasBasicas.service';
import {codigosParametros} from 'src/environments/environment.variables';
import {MessageService} from 'primeng/components/common/messageservice';
import {Message} from 'primeng/components/common/api';

import { DocumentTypeDTO } from 'src/app/domain/model/dto/DocumentTypeDTO';
import { MOCK_PERSONAL_REASON_TABLE } from 'src/app/domain/model/mocks/SolicitudMock';
import { MOCK_DOCUMENT_TYPE_TABLE } from 'src/app/domain/model/mocks/SolicitudMock';
import { constantRequestType } from 'src/environments/environment.variables';
import { TipoTramiteDTO } from 'src/app/domain/model/dto/TipoTramiteDTO';
import { successResponse } from 'src/environments/environment.variables';
import { DatePipe } from '@angular/common';
import { RqRadicarTramiteDTO } from 'src/app/domain/model/dto/RqRadicarTramiteDTO';
import { DetalleTramiteDto } from 'src/app/domain/model/dto/DetalleTramiteDto';

@Component({
    selector: 'app-detalle-solicitud',
    templateUrl: './detalle-solicitud.component.html',
    providers: [MessageService]
})
export class DetalleSolicitudComponent implements OnInit {
    @Output() enviarInformacionDetalle = new EventEmitter<any>();
    @Output() enviarDocumentos = new EventEmitter<DocumentDTO[]>();

    @Input() solicitud: TramiteDTO;
    @Input() tipoSolicitud: TipoTramiteDTO;
    @Input() modificarTramite: boolean;

    rqRadicarTramiteDTO: RqRadicarTramiteDTO = new RqRadicarTramiteDTO();
    formDetalleSolicitud: FormGroup;
    documentParent: DocumentDTO;
    setEditable: boolean = true;
    disabledAddFile: boolean = false;
    documents: DocumentDTO[] = new Array<DocumentDTO>();
    index: number;
    es: any;
    aRequestedDay: boolean = true;
    enableReasonForRequest: boolean;
    currentDate: Date = new Date();
    minDate: Date = new Date();
    
    startDate: Date = new Date();
    endDate: Date = new Date();
    fechaReintegro: Date = new Date();
    startTime: Date = new Date();
    endTime: Date;
    invalidDates: Array<Date>;
    msgs: Message[] = [];
    requestedDays: number;
    reasonForRequest: TipoTramiteDTO[];
    motivoSolicitudSel: TipoTramiteDTO;
    enableOther: boolean;
    attachmentsSectionEnabled: boolean;
    attachmentsTextSectionEnabled: boolean;
    documentTypeAll: DocumentTypeDTO[];
    documentTypeSelected: DocumentTypeDTO;
    fechaDiaSolicitado: Date = new Date();
    otroMotivo: string;
    observacion: string;

    formFechaInicio = new FormControl('', [
        Validators.required,
      ]);
    
    formFechaFin = new FormControl('', [
        Validators.required,
      ]);

    formFechaDiaSolicitado = new FormControl('', [
        Validators.required,
      ]);

    formHoraInicio = new FormControl('', [
        Validators.required,
      ]);

    formHoraFin = new FormControl('', [
        Validators.required,
      ]);

    formMotivoSolicitud = new FormControl('', [
        Validators.required,
      ]);

    constructor(
        private fn: UtilitiesFnService,
        private confirmationService: ConfirmationService,
        private tb: TablasBasicasService,
        private t: TramiteService,
        private _formBuilder: FormBuilder,
        private cd: ChangeDetectorRef,
        private messageService: MessageService,
        private datePipe: DatePipe
    ) {
        this.formDetalleSolicitud = this._formBuilder.group({
            formDiasSolicitados: ['',Validators.required],
            formFechaDiaSolicitado: ['',Validators.required],
            formHoraInicio: ['',Validators.required],
            formHoraFin: ['',Validators.required],
            formFechaInicio: ['',Validators.required],
            formFechaFin: ['',Validators.required],
            formFechaReintegro: ['',Validators.required],
            formMotivoSolicitud: ['',Validators.required],
            formOtroMotivo: [''],
            formObservacion: [''] 
        });
    }

    private cargarCatalogos() {
       //this.reasonForRequest = MOCK_PERSONAL_REASON_TABLE;
        this.tb.consultarTipoTramite(constantRequestType.permissionsCode).subscribe(data => {
            if (!!data.response && data.response.statusCode == successResponse.statusCode) {
                this.reasonForRequest = data.proceduresTypes;
            }
        });
    }

    private cargarCatalogoTipoDocTramite(){
        //this.documentTypeAll = MOCK_DOCUMENT_TYPE_TABLE;
        this.t.consultarTipoDocTramite().subscribe(data => {
            if (!!data.response && data.response.statusCode == successResponse.statusCode) {
                this.documentTypeAll = data.tipoDocumentoTramite;
                this.cargarSeccionAdjuntos( this.tipoSolicitud ); 
            }
        });
    }

    ngOnChanges(changes: SimpleChanges) : void {
        this.cargarDiasSolicitados( this.tipoSolicitud );
        this.cargarMotivoSolicitud( this.tipoSolicitud );
        this.cargarCatalogoTipoDocTramite();

        if (changes.tipoSolicitud && changes.tipoSolicitud.currentValue) {
            var tipo = changes.tipoSolicitud.currentValue.codigoApp;
            //this.requestDayVsb = this.validateVisibility(tipo, ['@TS_DF', '@TS_DC', '@TS_CD', '@TS_PE']);
            this.aRequestedDay = true;

            //this.requestHourVsb = this.validateVisibility(tipo, ['@TS_CD', '@TS_PE']);
            //this.requestHourVsb = true;

            //this.reasonRequestVsb = this.validateVisibility(tipo, ['@TS_PE']);
            //this.requestDateVsb = this.validateVisibility(tipo, ['@TS_RI', '@TS_CV', '@TS_DF', '@TS_DC', '@TS_CO', '@TS_LL', '@TS_DC']);
            /*if (tipo == "@TS_LL") {
                this.formDetalleSolicitud.controls['requestedDays'].setValue(5);
                this.formDetalleSolicitud.controls['requestedDays'].disable();
            }*/
        }
    }
    
    ngOnInit() {
        if(this.modificarTramite)
            this.cargarData();
        this.cargarCatalogos();
        this.onChageForm();
    }

    cargarData(){
        this.requestedDays = this.solicitud.detalleTramite.diasSolicitados;
        this.fechaDiaSolicitado = new Date(this.solicitud.detalleTramite.fechaDiaSolicitado);
        this.startTime = new Date();
        this.endTime = new Date();
        this.startDate = null;//new Date();
        this.endDate = null;//new Date();
        this.startTime = this.cargarHora(this.startTime, this.solicitud.detalleTramite.horaInicio);
        this.endTime = this.cargarHora(this.endTime, this.solicitud.detalleTramite.horaFin);
        
        if(!!this.solicitud.detalleTramite.fechaInicio)
            this.startDate = new Date(this.solicitud.detalleTramite.fechaInicio);
        if(!!this.solicitud.detalleTramite.fechaFin)
            this.endDate = new Date(this.solicitud.detalleTramite.fechaFin);

        if(!!this.solicitud.detalleTramite.idSubtipoTramite)
            this.motivoSolicitudSel = this.fn.findNameById(this.reasonForRequest, this.solicitud.detalleTramite.idSubtipoTramite.id);
        if(!!this.solicitud.detalleTramite.otroMotivo)
            this.otroMotivo = this.solicitud.detalleTramite.otroMotivo;

        this.observacion = this.solicitud.detalleTramite.observaciones;
        this.validarHabilitarOtro();
    }

    cargarHora(horaOrigen: Date, horaACargar: string){
        let hora: Date = new Date();
        var splitted = horaACargar.split(":", 2); 
        hora.setHours( Number(splitted[0]),Number(splitted[1]) );
        return hora;
    }


    

    cargarSeccionAdjuntos(tipoSolicitud: TipoTramiteDTO){
        if(!!tipoSolicitud){
            this.attachmentsSectionEnabled = this.searchTypeDocumentProcess(tipoSolicitud.id, null);
            if(this.attachmentsSectionEnabled)
                this.cargarTextoSeccionAdjuntos(tipoSolicitud);
        }
    }

    cargarTextoSeccionAdjuntos(tipoSolicitud: TipoTramiteDTO){
        if(tipoSolicitud.id == constantRequestType.disabilityReportCode)
            this.attachmentsTextSectionEnabled = true;
        else
            this.attachmentsTextSectionEnabled = false;
    }

    searchTypeDocumentProcess(procedureTypeId, subprocedureTypeId){
        let flag = false;
        if(!!this.documentTypeAll){
            for (let index = 0; index < this.documentTypeAll.length; index++) {
                if (this.documentTypeAll[index].idTipoTramite.id == procedureTypeId){
                    if(this.documentTypeAll[index].idSubtipoTramite == null && subprocedureTypeId == null){
                        this.documentTypeSelected = this.documentTypeAll[index];
                        flag = true;
                        break;
                    }else if(this.documentTypeAll[index].idSubtipoTramite.id == subprocedureTypeId) {
                        this.documentTypeSelected = this.documentTypeAll[index];
                        flag = true;
                        break;
                    }
                }
            }
        }
        return flag;
    }
    

    cargarDiasSolicitados(tipoSolicitud: TipoTramiteDTO) {
        if(!!tipoSolicitud){
            if(tipoSolicitud.id === constantRequestType.mourningLicenseCode){
                this.requestedDays = 5;
                this.formDetalleSolicitud.controls['formDiasSolicitados'].disable();
            }else{
                this.requestedDays = null;
                this.formDetalleSolicitud.controls['formDiasSolicitados'].enable();
            }
        } 
    }

    
    cargarMotivoSolicitud(tipoSolicitud: TipoTramiteDTO) {
        if(!!tipoSolicitud){
            if(tipoSolicitud.id === constantRequestType.permissionsCode){
                this.enableReasonForRequest = true;
                this.cargarReglaMotivoSolicitud(tipoSolicitud.id);
            } else{
                this.enableReasonForRequest = false;
                this.enableOther = false;
                this.cargarReglaMotivoSolicitud(tipoSolicitud.id);
            }
        }
    }

    nuevoDocumento() {
        this.documentParent = new DocumentDTO();
    }

    editarDocumento(documentoSelect: DocumentDTO, index: number) {
        this.documentParent = documentoSelect;
        this.index = index;
    }

    guardarDocumentoInfo(nuevoDoc: DocumentDTO) {
        if (nuevoDoc != null) {
            this.fn.updateItemByEntity(this.documents, nuevoDoc, this.index);
            this.disabledAddFile = true;
        }
        this.enviarDocumentos.emit(this.documents);
        this.documentParent = null;
        this.index = -1;
    }

    eliminarDocumento(docEliminado: DocumentDTO) {
        this.confirmationService.confirm({
            message: '¿Realmente desea eliminar este documento?',
            accept: () => {
                this.documents = this.fn.deleteItemByEntity(this.documents, docEliminado);
                this.enviarDocumentos.emit(this.documents);
                this.disabledAddFile = false;
            },
        });
    }

    validarHabilitarOtro(){
        this.enableOther = false;
        if(!!this.motivoSolicitudSel){
            if(this.motivoSolicitudSel.id === constantRequestType.otherCodePermissions
                && this.tipoSolicitud.id === constantRequestType.permissionsCode)
                this.enableOther = true;
        }
        if(!!this.motivoSolicitudSel)
            this.attachmentsSectionEnabled = this.searchTypeDocumentProcess(this.tipoSolicitud.id, this.motivoSolicitudSel.id);
        else
            this.attachmentsSectionEnabled =  this.searchTypeDocumentProcess(this.tipoSolicitud.id, null);
    }

    valorLista(item: string, lista: any[]) {
        const itemNvp = this.fn.filterItemByListNvp(lista, item);
        return !!itemNvp ? itemNvp.name : '';
    }

    validateVisibility (argument: string, parameters: any[]) {
        for (let index = 0; index < parameters.length; index++) {
            if (parameters[index] == argument)
                return true;
        }
        return false;
    }

    /*onChangeStartDate (event) {
        if (this.formDetalleSolicitud.controls['formDiasSolicitados'].value) {
            var ct = parseInt(this.formDetalleSolicitud.controls['formDiasSolicitados'].value);
            this.formDetalleSolicitud.controls['fechaFin'].setValue(this.fn.addDayToDate(event, ct));
        }
    }*/

    /*onChangeRequestDay (event) {
        /*if (this.formDetalleSolicitud.controls['fechaInicio'].value) {
            var startDate = this.formDetalleSolicitud.controls['fechaInicio'].value;
            this.formDetalleSolicitud.controls['fechaFin'].setValue(this.fn.addDayToDate(startDate, parseInt(event)));
        }*
    }*/
    
    onChageForm() {
        console.log('******************onChageForm-DETALLE-SOLICITUD***********');
        this.formDetalleSolicitud.valueChanges.subscribe(val => {
            this.cargarFormularioParaEnvio(val);
            console.log('Estado Formulario DETALLE-SOLICITUD:' + this.formDetalleSolicitud.valid);
            this.enviarInformacionDetalle.emit({detalleTramite : this.rqRadicarTramiteDTO.detalleTramite, validForm : this.formDetalleSolicitud.valid});
            
            //this.enviarInformacionCertificado.emit(this.fn.formToDto(val, aux));
        }, error => console.log(error)
        );
    }

    private cargarFormularioParaEnvio(val:any){
        debugger
        this.rqRadicarTramiteDTO = new RqRadicarTramiteDTO();
        let detalleTramiteDto = new DetalleTramiteDto();
        //detalleTramiteDto.diasSolicitados = val.formDiasSolicitados;
        detalleTramiteDto.diasSolicitados = this.requestedDays;
        detalleTramiteDto.fechaDiaSolicitado = val.formFechaDiaSolicitado;
        detalleTramiteDto.fechaFin = val.formFechaFin;
        detalleTramiteDto.fechaInicio = val.formFechaInicio;
        detalleTramiteDto.horaFin = val.formHoraFin;
        detalleTramiteDto.horaInicio = val.formHoraInicio;
        //detalleTramiteDto.idSubtipoTramite = val.formMotivoSolicitud;
        if( val.formMotivoSolicitud != null ){
            let tipoTramiteDTO = new TipoTramiteDTO();
            tipoTramiteDTO.id = val.formMotivoSolicitud.id;
            detalleTramiteDto.idSubtipoTramite = tipoTramiteDTO;
        }    
        detalleTramiteDto.observaciones = val.formObservacion != null? val.formObservacion : '';
        detalleTramiteDto.otroMotivo = val.formOtroMotivo != null? val.formOtroMotivo: null;
        
        this.rqRadicarTramiteDTO.detalleTramite = detalleTramiteDto;
    }

    updateEndDate(){
        if(!this.modificarTramite){
            if(this.requestedDays > 1){
                console.log('Valor dias:' + this.requestedDays);
                this.endDate = new Date();
                this.endDate.setDate(this.startDate.getDate() + this.requestedDays);
                this.cargarReglasDias(this.requestedDays);
            }else if(this.requestedDays == 1){
                this.cargarReglasDias(this.requestedDays);
            }
            this.consultaFechaFin();
        }
    }

    private consultaFechaFin(){
        if(this.requestedDays > 1 && this.startDate != null){
            this.t.consultarFechaFin(this.datePipe.transform(this.startDate,"dd/MM/yyyy"), this.requestedDays).subscribe(data => {
                debugger;
                if (!!data.statusDTO && data.statusDTO.statusCode == successResponse.statusCode) {
                    var splitted = data.dateEnable.split("/", 3);
                    this.endDate = new Date(splitted[2] + "/" + splitted[1] + "/" + splitted[0]);
                    
                    var splittedAux =data.refundDate.split("/", 3);
                    this.fechaReintegro = new Date(splittedAux[2] + "/" + splittedAux[1] + "/" + splittedAux[0]);
                }
            });
        }
    }

    /* <********************************************************REGLAS DEL FORMULARIO**********************************> */
    private cargarReglaMotivoSolicitud(idTipoSolicitud: number){
        if(idTipoSolicitud === constantRequestType.permissionsCode){
            this.formDetalleSolicitud.addControl('formMotivoSolicitud', this.formMotivoSolicitud);
        }else{
            this.formDetalleSolicitud.removeControl('formMotivoSolicitud');
        }
    }

    private cargarReglasDias(dias: number){
        if(dias == 1){
            this.formDetalleSolicitud.removeControl('formFechaInicio');
            this.formDetalleSolicitud.removeControl('formFechaFin');

            this.formDetalleSolicitud.addControl('formFechaDiaSolicitado', this.formFechaDiaSolicitado);
            this.formDetalleSolicitud.addControl('formHoraInicio', this.formHoraInicio);
            this.formDetalleSolicitud.addControl('formHoraFin', this.formHoraFin);
        }else if(dias > 1){
            this.formDetalleSolicitud.removeControl('formFechaDiaSolicitado');
            this.formDetalleSolicitud.removeControl('formHoraInicio');
            this.formDetalleSolicitud.removeControl('formHoraFin');

            this.formDetalleSolicitud.addControl('formFechaInicio', this.formFechaInicio);
            this.formDetalleSolicitud.addControl('formFechaFin', this.formFechaFin);
        }
    }
    /* </********************************************************REGLAS DEL FORMULARIO**********************************> */


    show() {
        this.msgs.push({severity:'info', summary:'Info Message', detail:'PrimeNG rocks'});
    }
    
    clear() {
        this.msgs = [];
    }

    verifyNumber(){
        console.log('Ingresa');
        console.log(''+this.requestedDays);
        if(this.requestedDays < 1){
            this.msgs = [];
            this.msgs.push({severity:'error', summary:'Error Message', detail:'Validation failed'});
        }
    }
}
