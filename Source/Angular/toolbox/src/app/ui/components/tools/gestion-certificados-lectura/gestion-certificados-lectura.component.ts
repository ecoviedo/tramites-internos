import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TablaBasicaDTO } from 'src/app/domain/model/dto/TablaBasicaDTO';
import { TablasBasicasService } from '../../../../infraestructure/services/tablasBasicas.service';
import { codigosParametros } from 'src/environments/environment.variables';
import { TramiteDTO } from 'src/app/domain/model/dto/TramiteDTO';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UtilitiesFnService } from '../../../../infraestructure/services/UtilitiesFnService';
import { CertificadoDTO } from 'src/app/domain/model/dto/CertificadoDTO';

import { MOCK_CERTIFICATE_TYPE_TABLE } from 'src/app/domain/model/mocks/SolicitudMock';
import {MultiSelectModule} from 'primeng/multiselect';
import {SelectItem} from 'primeng/api';
import { TipoTramiteDTO } from 'src/app/domain/model/dto/TipoTramiteDTO';

@Component({
    selector: 'app-gestion-certificados-lectura',
    templateUrl: './gestion-certificados-lectura.component.html',
    styles: [`
        :host ::ng-deep .ui-multiselected-item-token,
        :host ::ng-deep .ui-multiselected-empty-token {
            padding: 2px 4px;
            margin: 0 0.286em 0 0;
            display: inline-block;
            vertical-align:middle;
            height: 1.857em;
        }

        :host ::ng-deep .ui-multiselected-item-token {
            background: #007ad9;
            color: #ffffff;
        }

        :host ::ng-deep .ui-multiselected-empty-token {
            background: #d95f00;
            color: #ffffff;
        }
    `]
})
export class GestionCertificadosLecturaComponent implements OnInit {

    @Output() enviarInformacionCertificado = new EventEmitter<CertificadoDTO>();
    @Input() solicitud: TramiteDTO;
    //certificateType: TablaBasicaDTO[];
    certificateTypeList: TipoTramiteDTO[];
    
    certificateTypeSelected: TipoTramiteDTO[] = [];
    //cars: SelectItem[];
    //items: SelectItem[];
    //item: string;

    constructor(
        private tb: TablasBasicasService,
        private _formBuilder: FormBuilder,
        private fn: UtilitiesFnService
    ) {

        /*this.cars = [
            {label: 'Certificado laboral estándar', value: '16'},
            {label: 'Certificado laboral con funciones', value: '17'},
            {label: 'Certificado ingresos y retenciones', value: '18'},
            {label: 'Certificado de cobertura ARL en el exterior', value: '19'},
            {label: 'Certificado medicina prepagada', value: '20'}
        ];*/

        /*this.items = [];
        for (let i = 0; i < 10000; i++) {
            this.items.push({label: 'Item ' + i, value: 'Item ' + i});
        }*/
    }

    ngOnInit() {
        //this.certificateType = this.tb.consultarTablaBasica(codigosParametros.tipoCertificado);

        //this.onChageForm();

        this.certificateTypeList = MOCK_CERTIFICATE_TYPE_TABLE;
        this.loadCertificatesSection();
    }

    loadCertificatesSection(){
        //verificacion de tipo de solicitud o tramite
        var procedureTypeDTO: TipoTramiteDTO;
        for (let index = 0; index < this.solicitud.certificadoTramiteList.length; index++) {
            procedureTypeDTO = this.fn.findNameById(this.certificateTypeList,this.solicitud.certificadoTramiteList[index].idSubtipoTramite);
            this.certificateTypeSelected.push(procedureTypeDTO);
        }
    }

    selectCertificateType(){
        console.log('*************FUNCIONA**************' + this.certificateTypeSelected);
        debugger;
    }
}
