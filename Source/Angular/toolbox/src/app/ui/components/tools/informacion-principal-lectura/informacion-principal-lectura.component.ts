import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { TramiteDTO } from 'src/app/domain/model/dto/TramiteDTO';
import { TablaBasicaDTO } from 'src/app/domain/model/dto/TablaBasicaDTO';
import { CatalogueDTO } from 'src/app/domain/model/dto/CatalogueDTO';
import { SolicitudService, UtilitiesFnService, TablasBasicasService } from 'src/app/infraestructure/services/service.index';
import { EmpleadoDTO } from '../../../../domain/model/dto/EmpleadoDTO';
import { BusinessResponseDTO } from '../../../../domain/model/dto/BusinessResponseDTO';
import { constantCompanyAreaCode } from 'src/environments/environment.variables';
import { ProcessManagersDTO } from 'src/app/domain/model/dto/ProcessManagersDTO';
import { TipoTramiteDTO } from 'src/app/domain/model/dto/TipoTramiteDTO';

@Component({
    selector: 'app-informacion-principal-lectura',
    templateUrl: './informacion-principal-lectura.component.html'
})

export class InformacionPrincipalLecturaComponent implements OnInit {

    @Input() solicitud: TramiteDTO;
    @Input() requestType: TipoTramiteDTO[];
    @Input() companyArea: CatalogueDTO[];
    //@Input() immediateBosses: CatalogueDTO[];
    @Input() processManagers: ProcessManagersDTO[];
    @Input() identificationTypes: CatalogueDTO[];


    @Output() enviarInformacionPpal = new EventEmitter<TramiteDTO>();
    @Output() enviarNumeroDocumento = new EventEmitter<TramiteDTO>();
    @Output() enviarTipoSolicitud = new EventEmitter<TablaBasicaDTO>();

    formaSolicitud: FormGroup;
    formDocumento: FormGroup;

    //Selected Type
    selectedRequestType: CatalogueDTO;
    selectedcompanyArea: CatalogueDTO;
    selectedIdentificationType: CatalogueDTO;
    selectedProcessManagers: ProcessManagersDTO;

    dateNow: string;
    visibilidad: boolean;
    showImmediateBoss: boolean = false;

    empleado: BusinessResponseDTO<EmpleadoDTO>;
    empleado2: EmpleadoDTO = {
        primerNombre: '',
        primerApellido: ''
    };

    constructor(
        private _formBuilder: FormBuilder,
        private fn: UtilitiesFnService,
        private empleados: SolicitudService,
        private tb: TablasBasicasService
    ) {
        //let date: Date = new Date();
        //this.dateNow = date.toISOString();
        // this.formaSolicitud = new FormGroup({
        //     tipoSolicitud: new FormControl('tipoSolicitud'[validators.required])
        // });

        this.formaSolicitud = this._formBuilder.group({
            fechaSolicitud: [''],
            tipoSolicitud: [''],
        });
        this.formDocumento = this._formBuilder.group({
            numeroDocumento: [''],
        });
    }

    ngOnInit() {
        this.visibilidad = false;
        //this.onChangeObject();
        this.onChageForm();
        //this.obtenerUsuario(event);
        //this.formDocument();
        this.cargarCatalogos();
    }

    cargarCatalogos() {
        //this.identificationTypes = MOCK_IDENTIFICATION_TYPE_TABLE;
        //this.companyArea = MOCK_COMPANY_AREA_TABLE;
        let listaCatalogos: string[] = ['AreaEmpresa', 'RolTramite', 'TipoIdentificacion'];
        this.tb.consultarCatalogos(listaCatalogos).subscribe(data => {
          if (!!data.response && data.response.statusCode == '200') {
            data.catalogs.forEach(element => {
              if (!!element) {
                if (element.catalogo == 'AreaEmpresa')
                  this.companyArea = element.data;
                else if (element.catalogo == 'TipoIdentificacion')
                  this.identificationTypes = element.data;
              }
            });

            this.loadView();
          }
        });

        //this.loadView();

      }

    loadView() {
        if(!!this.solicitud){
            if(!!this.requestType){
                if(!!this.solicitud.idTipoTramite.id){
                    this.selectedRequestType = this.fn.findNameById(this.requestType, this.solicitud.idTipoTramite.id);
                }
            }    
            if(!!this.identificationTypes){
                this.selectedIdentificationType = this.fn.findNameById(this.identificationTypes, this.solicitud.idTipoIdentificacion.idTipoIdentificacion);
            }
            if(!!this.companyArea){
                this.selectedcompanyArea = this.fn.findNameById(this.companyArea, this.solicitud.idArea.id);
            }
            if (this.solicitud.idGestorTramite != null) {
                this.selectedProcessManagers = this.fn.findNameById(this.processManagers, this.solicitud.idGestorTramite.id);
                this.showImmediateBoss = true;
            }
        }
    }

    /*obtenerUsuario(resquit) {
        this.empleados.consultarEmpleado(this.empleado2).subscribe(resultado => {
                this.empleado2 = resultado;
            //debugger
            },
            error => {

            });
    }*/

    immediateBossControl() {
        if (this.selectedcompanyArea != null && this.selectedcompanyArea.id === constantCompanyAreaCode.operationsManagementCode) {
            this.showImmediateBoss = true;
        } else {
            this.showImmediateBoss = false;
        }
    }

    /*onChangeObject() {
        if (!!this.solicitud) {
            this.fn.dtoToForm(this.formaSolicitud.controls, this.solicitud);
            //this.selectedRequestType = this.fn.filterItemByListNvp(this.requestType, this.solicitud.tipoSolicitud);
            //this.selectedDocumentType = this.fn.filterItemByListNvp(this.documentType, this.solicitud.solicitante.tipoDocumento);
        }
    }*/

    onChageForm() {
        let aux = this.solicitud;
        this.formaSolicitud.valueChanges.subscribe(val => {
            this.enviarInformacionPpal.emit(this.fn.formToDto(val, aux));
        }, error => console.log(error)
        );
    }

    /*formDocument() {
        let aux = this.solicitud.solicitante;
        this.formDocumento.valueChanges.subscribe(val => {
                this.enviarNumeroDocumento.emit(this.fn.formToDto(val, aux));
            }, error => console.log(error)
        );
    }*/

    selectForm = function (event, controlName) {
        if (!!event.value) {
            this.formaSolicitud.controls[controlName].setValue(event.value.id);
            this.enviarTipoSolicitud.emit(event.value);
        }
    };

}
