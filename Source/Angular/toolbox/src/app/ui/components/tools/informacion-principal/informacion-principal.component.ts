import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { TramiteDTO} from 'src/app/domain/model/dto/TramiteDTO';
import { RqRadicarTramiteDTO} from 'src/app/domain/model/dto/RqRadicarTramiteDTO';
import { TablaBasicaDTO} from 'src/app/domain/model/dto/TablaBasicaDTO';
import { CatalogueDTO} from 'src/app/domain/model/dto/CatalogueDTO';
import { SolicitudService, UtilitiesFnService, ServiciosExternosService} from 'src/app/infraestructure/services/service.index';
import { EmpleadoDTO} from '../../../../domain/model/dto/EmpleadoDTO';
import { BusinessResponseDTO} from '../../../../domain/model/dto/BusinessResponseDTO';
import { constantCompanyAreaCode, constantRequestType} from 'src/environments/environment.variables';
import { DatePipe } from '@angular/common';
import { TipoTramiteDTO } from 'src/app/domain/model/dto/TipoTramiteDTO';
import { MessageService } from 'primeng/api';
import { successResponse, failNoDataNofound } from 'src/environments/environment.variables';
import { RqTramiteDTO } from 'src/app/domain/model/dto/RqTramiteDTO';

@Component({
    selector: 'app-informacion-principal',
    templateUrl: './informacion-principal.component.html',
    providers: [DatePipe]
})

export class InformacionPrincipalComponent implements OnInit {

    @Output() enviarInformacionPpal = new EventEmitter<any>();
    @Output() enviarTipoSolicitud = new EventEmitter<TablaBasicaDTO>();
    @Output() enviarDiasDisponibles = new EventEmitter<number>();

    @Input() tramite: TramiteDTO;
    @Input() requestType: TipoTramiteDTO[];
    @Input() companyArea: CatalogueDTO[];
    @Input() modificarTramite: boolean;
    @Input() immediateBosses: CatalogueDTO[];
    @Input() identificationTypes: CatalogueDTO[];

    rqRadicarTramite: RqRadicarTramiteDTO;
    formInfoPrincipal: FormGroup;
    tipoTramiteSel: TipoTramiteDTO;
    //selectedDocumentType: TablaBasicaDTO;
    selectedcompanyArea: CatalogueDTO;
    selectedimmediateBoss: CatalogueDTO;
    selectedIdentificationType: CatalogueDTO;

    dateNow: string;
    showImmediateBoss: boolean = false;

    empleado: BusinessResponseDTO<EmpleadoDTO>;
    nombre: string;
    apellido: string;
    numeroDocumento: string;
    diasDisponibles: number;

    constructor(
        private _formBuilder: FormBuilder,
        private fn: UtilitiesFnService,
        private empleados: SolicitudService,
        private datePipe: DatePipe,
        private se: ServiciosExternosService,
        private messageService: MessageService
    ) {
        var date: Date = new Date();
        //this.dateNow = date.toISOString();
        this.dateNow = this.datePipe.transform(date,"dd/MM/yyyy");

        this.formInfoPrincipal = this._formBuilder.group({
            formTipoTramite: ['',Validators.required],
            formFechaSolicitud: [''],
            formNombre: ['',Validators.required],
            formApellido: ['',Validators.required],
            formTipoIdentificacion: ['',Validators.required],
            formNumeroDocumento: ['',Validators.required],
            formArea: ['',Validators.required],
            formJefeInmediato: ['']
        });

        this.formInfoPrincipal.controls['formFechaSolicitud'].setValue(this.dateNow);
    }

    ngOnInit() {
        if(this.modificarTramite)
            this.cargarData();

        this.onChageForm();


        //this.onChangeObject();
        //this.formDocument();
    }

    onChageForm() {
        console.log('******************onChageForm-INFO_PRINCIPAL***********');
        this.formInfoPrincipal.valueChanges.subscribe(val => {
            this.cargarFormularioParaEnvio(val);
            console.log('Estado Formulario INFO_PRINCIPAL:' + this.formInfoPrincipal.valid);
            this.enviarInformacionPpal.emit({infoPpalTramite : this.rqRadicarTramite, validForm : this.formInfoPrincipal.valid});

            //this.enviarInformacionCertificado.emit(this.fn.formToDto(val, aux));
        }, error => console.log(error)
        );
    }

    private cargarFormularioParaEnvio(val:any){
        debugger;
        this.rqRadicarTramite = new RqRadicarTramiteDTO();
        this.rqRadicarTramite.idArea = val.formArea != null? Number(val.formArea.id) : null;
        this.rqRadicarTramite.idGestorTramite = val.formJefeInmediato != null?val.formJefeInmediato.id : '';
        this.rqRadicarTramite.idTipoIdentificacion = val.formTipoIdentificacion != null? Number(val.formTipoIdentificacion.id) : null;
        this.rqRadicarTramite.idTipoTramite = val.formTipoTramite != null?val.formTipoTramite.id : '';

        let rqTramite = new RqTramiteDTO();
        rqTramite.idInstancia = '';
        rqTramite.identificacion = val.formNumeroDocumento;
        rqTramite.fecha = val.formFechaSolicitud;
        rqTramite.estado = 'Creado'
        rqTramite.nombreEmpleado = val.formNombre;
        rqTramite.apellidoEmpleado = val.formApellido;
        rqTramite.respuestaTramite = ''

        this.rqRadicarTramite.tramite = rqTramite;
    }

    cargarData(){
        if(!!this.requestType)
            this.tipoTramiteSel = this.fn.findNameById(this.requestType, this.tramite.idTipoTramite.id);
        this.formInfoPrincipal.controls['formFechaSolicitud'].setValue(this.tramite.fecha);
        this.nombre = this.tramite.nombreEmpleado;
        this.apellido = this.tramite.apellidoEmpleado;
        if(!!this.identificationTypes)
            this.selectedIdentificationType = this.fn.findNameById(this.identificationTypes, this.tramite.idTipoIdentificacion.idTipoIdentificacion);
        this.numeroDocumento = this.tramite.identificacion;
        if(!!this.companyArea)
            this.selectedcompanyArea = this.fn.findNameById(this.companyArea, this.tramite.idArea.id);
        var aux = this.immediateBossControl();
        if(aux)
            if(!!this.immediateBosses)
                this.selectedimmediateBoss = this.fn.findNameById(this.immediateBosses, this.tramite.idGestorTramite.id);
        
    }

    immediateBossControl() {
        if(this.selectedcompanyArea != null && this.selectedcompanyArea.id === constantCompanyAreaCode.operationsManagementCode)
            this.showImmediateBoss = true;
        else
            this.showImmediateBoss = false;
        
        return this.showImmediateBoss;
    }

    ingresaTipoDocumento = function (event, controlName) {
        debugger;
        if(!!this.tipoTramiteSel && (this.tipoTramiteSel.id == constantRequestType.holidayCode) ){
            if (!!event.value) 
                this.consultarDiasDisponibles(event.value, this.formInfoPrincipal.controls.formNumeroDocumento.value);
            else
                this.enviarDiasDisponibles.emit(null);
        }
    }

    ingresaNumDocumento = function (event, controlName) {
        if(!!this.tipoTramiteSel && (this.tipoTramiteSel.id == constantRequestType.holidayCode) ){
            let aux: string = this.formInfoPrincipal.controls[controlName].value;
            if(!!aux)
                this.consultarDiasDisponibles(this.selectedIdentificationType, aux);
            else
                this.enviarDiasDisponibles.emit(null);
        }
    }

    private consultarDiasDisponibles(tipoIdentificacion:CatalogueDTO, identificacion:string){
        if(!!tipoIdentificacion && !!identificacion){
            this.messageService.add({severity:'info', summary: 'Consultando días disponibles', 
                detail:'' + tipoIdentificacion.descripcion + ' ' + identificacion});
            //this.messageService.add({key: 'tc', severity:'warn', summary: 'Info Message', detail:'PrimeNG rocks'});

            this.se.consultarDiasDisponiblesEmpleado(tipoIdentificacion.id, identificacion).subscribe(data => {
                if (!!data.response && data.response.statusCode == successResponse.statusCode) {
                    this.diasDisponibles = data.diasDisponibles;
                    this.enviarDiasDisponibles.emit(this.diasDisponibles);
                }else if (!!data.response && data.response.statusCode == failNoDataNofound.statusCode){
                    this.messageService.add({severity:'warn', summary: 'No se encuentra información de días disponibles', 
                    detail:'' + tipoIdentificacion.descripcion + ' ' + identificacion});
                    this.enviarDiasDisponibles.emit(null);
                }
            });
            
        }
    }
    

    /*onChangeObject() {
        if (!!this.tramite) {
            this.fn.dtoToForm(this.formInfoPrincipal.controls, this.tramite);
            //this.tipoTramiteSel = this.fn.filterItemByListNvp(this.requestType, this.tramite.tipoSolicitud);
            //this.selectedDocumentType = this.fn.filterItemByListNvp(this.documentType, this.tramite.solicitante.tipoDocumento);
        }
    }*/

    

    /*formDocument() {
        let aux = this.tramite.solicitante;
        this.formInfoPrincipal.valueChanges.subscribe(val => {
                this.enviarNumeroDocumento.emit(this.fn.formToDto(val, aux));
            }, error => console.log(error)
        );
    }*/

    cambioTipoTramite = function (event, controlName) {
        debugger;
        if (!!event.value) {
            //this.formInfoPrincipal.controls[controlName].setValue(event.value.id);
            this.enviarTipoSolicitud.emit(event.value);
            if(this.tipoTramiteSel.id == constantRequestType.holidayCode)
                this.consultarDiasDisponibles(this.selectedIdentificationType, this.formInfoPrincipal.controls.formNumeroDocumento.value);
        }else
            this.enviarTipoSolicitud.emit(event.value);
    };

    
}
