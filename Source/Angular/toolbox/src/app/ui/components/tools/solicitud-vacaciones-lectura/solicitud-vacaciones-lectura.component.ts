import { Component, Input, OnInit } from '@angular/core';
import { TramiteDTO } from 'src/app/domain/model/dto/TramiteDTO';
import { UtilitiesFnService } from '../../../../infraestructure/services/UtilitiesFnService';

@Component({
    selector: 'app-solicitud-vacaciones-lectura',
    templateUrl: './solicitud-vacaciones-lectura.component.html'
})
export class SolicitudVacacionesComponentLectura implements OnInit {

    @Input() solicitud: TramiteDTO;
    checkedAvailableDays: boolean;

    constructor (
        private fn: UtilitiesFnService
    ) {
    }

    ngOnInit () {
        if(!!this.solicitud){
            if(!!this.solicitud.vacacionesTramite.totalDiasAnticipados && this.solicitud.vacacionesTramite.totalDiasAnticipados > 0)
                this.checkedAvailableDays = true;
            else
                this.checkedAvailableDays = false;
        }
        
    }
}