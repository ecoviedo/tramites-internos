import {Component, OnInit, Output, EventEmitter, Input} from '@angular/core';
//import {MessageService} from 'primeng/api';

@Component({
    selector: 'app-botoneraProceso',
    templateUrl:'./botoneraProceso.component.html',
})
export class BotoneraProcesoComponent implements OnInit {

    @Output() guardarEvent = new EventEmitter<any>();
    @Output() finalizarTareaEvent = new EventEmitter<any>();

    @Output() guardarJefeEvent = new EventEmitter<any>();
    @Output() guardarGerenteEvent = new EventEmitter<any>();
    @Output() guardarRecursosEvent = new EventEmitter<any>();

    @Input() selectedRol: any ;
    @Input() disabledForm: boolean;
    @Input() pantallaOrigen: string;

    constructor() { 
    }

    ngOnInit() {
        console.log('****************ngOnInit-Botonera***************');

        /*switch (this.pantallaOrigen) {
            case "JEFE_INMEDIATO":
                return ResourceType.PlainText;
            case "bmp", "jpg", "jpeg", "png", "tiff", "psd", "tga", "gif":
                return ResourceType.Image;
            case "mp3", "wav", "ogg", "flac", "wma", "m4a":
                return ResourceType.Audio;
            case "bin", "dat":
                return ResourceType.Binary;
            case "ico":
                return ResourceType.Icon;
            case "rc", "resx":
                return ResourceType.ResourceScript;
            default:
                return ResourceType.Unknown;
            }*/
    }

    guardarSolicitud (event) {
        this.guardarEvent.emit(event);
    }

    finalizarGestion (event) {
        this.finalizarTareaEvent.emit(event);
    }

    guardarJefe (event) {
        this.guardarJefeEvent.emit(event);
    }
    guardarGerente (event) {
        this.guardarGerenteEvent.emit(event);
    }
    guardarRecursos (event) {
        this.guardarRecursosEvent.emit(event);
    }
}
