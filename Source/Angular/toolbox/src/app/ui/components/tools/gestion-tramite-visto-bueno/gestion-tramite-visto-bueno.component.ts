import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TablaBasicaDTO} from 'src/app/domain/model/dto/TablaBasicaDTO';
import {TablasBasicasService} from '../../../../infraestructure/services/tablasBasicas.service';
import {codigosParametros, constantDecisionType} from 'src/environments/environment.variables';
import {BusinessResponseDTO} from '../../../../domain/model/dto/BusinessResponseDTO';
import {EmpleadoDTO} from '../../../../domain/model/dto/EmpleadoDTO';
import {SolicitudService} from '../../../../infraestructure/services/solicitud.service';
import {EmpleadoListService} from '../../../../infraestructure/services/empleado-list.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UtilitiesFnService} from '../../../../infraestructure/services/UtilitiesFnService';
import {CertificadoDTO} from '../../../../domain/model/dto/CertificadoDTO';
import {JefeInmediatoDTO} from '../../../../domain/model/dto/JefeInmediatoDTO';
import {TramiteDTO} from '../../../../domain/model/dto/TramiteDTO';
import {GerenteDTO} from '../../../../domain/model/dto/GerenteDTO';
import {logger} from 'codelyzer/util/logger';

import { MOCK_TRAMITE_CONSULTADO } from 'src/app/domain/model/mocks/TramiteMock';
import { MOCK_DECISION_TABLE, MOCK_PROCESSRESPONSE_TABLE } from 'src/app/domain/model/mocks/SolicitudMock';
import { CatalogueDTO } from 'src/app/domain/model/dto/CatalogueDTO';
import { RqRadicarTramiteDTO } from 'src/app/domain/model/dto/RqRadicarTramiteDTO';
import { GestionTramiteDTO } from 'src/app/domain/model/dto/GestionTramiteDTO';
import { RqGestionTramiteDTO } from 'src/app/domain/model/dto/RqGestionTramiteDTO';

@Component({
    selector: 'app-gestion-tramite-visto-bueno',
    templateUrl: './gestion-tramite-visto-bueno.component.html'
})
export class GestionTramiteVistoBuenoComponent implements OnInit {
    @Output() enviarInformacionGestion = new EventEmitter<any>();
    @Output() enviarInformacionGerente = new EventEmitter<GerenteDTO>();

    @Input() jefe: JefeInmediatoDTO;
    @Input() gerente: GerenteDTO;
    @Input() managementLegend: string;

    permisoRemunerado: CatalogueDTO[];
    requiereReemplazo: CatalogueDTO[];
    respTramite: CatalogueDTO[];
    showReplacementName: boolean = false;

    classification: TablaBasicaDTO[];
    selectedSiONo: TablaBasicaDTO;
    empleado: EmpleadoDTO[];
    emp: EmpleadoDTO;
    formJefeInmediato: FormGroup;
    nombreReemplazo: string;
    rqGestionTramite: RqGestionTramiteDTO;

    constructor(private tb: TablasBasicasService,
                private empleados: EmpleadoListService,
                private _formBuilder: FormBuilder,
                private fn: UtilitiesFnService) {

        this.formJefeInmediato = this._formBuilder.group({
            formPermisoRemunerado: ['', Validators.required],
            formRequiereReemplazo: ['', Validators.required],
            formNombreReemplazo: [''],
            formObservaciones: ['', Validators.required],
            formRespTramite: ['', Validators.required]
        });
    }

    ngOnInit() {
        this.permisoRemunerado = MOCK_DECISION_TABLE;
        this.requiereReemplazo = MOCK_DECISION_TABLE;
        this.respTramite = MOCK_PROCESSRESPONSE_TABLE;

        //this.classification = this.tb.consultarTablaBasica(codigosParametros.clasificacionIncapacidad);
        
        //this.listarEmpleados(event);
        this.onChageForm();
        
        /*this.onChageFormGerente();
        {
            var aux = this.jefe;
            this.formJefeInmediato.valueChanges.subscribe(val => {
                    this.enviarInformacionJefe.emit(this.fn.formToDto(val, aux));
                }, error => console.log(error)
            );
        }*/
    }

    /*listarEmpleados(resquit) {
        this.empleados.listarEmpleados(this.empleado).subscribe(resultado => {
                this.empleado = new Array<EmpleadoDTO>();
                for (var i in resultado.body) {
                    var nvpGroup = new EmpleadoDTO();
                    nvpGroup.primerNombre = resultado.body[i].primerNombre.concat(" "+resultado.body[i].segundoNombre+" "+resultado.body[i].primerApellido)+" "+resultado.body[i].segundoApellido;
                    nvpGroup.primerApellido = resultado.body[i].primerApellido;
                    nvpGroup.segundoNombre = resultado.body[i].segundoNombre;
                    nvpGroup.segundoApellido = resultado.body[i].segundoApellido;
                    this.empleado.push(nvpGroup);
                    this.empleado = [...this.empleado];
                }
            },
            error => {

            });
    }*/

    activarNombreReemplazo = function (event, controlName) {
        if (!!event.value) {
            //this.formJefeInmediato.controls[controlName].setValue(event.value.codigoApp.indexOf('@SN_S') == false);
            if(event.value.id == constantDecisionType.yes)
                this.showReplacementName = true;
            else
                this.showReplacementName = false;  
        }
    }

    /*selectEmpleado = function (event, controlName) {
        if (!!event.value) {
            this.formJefeInmediato.controls[controlName].setValue(event.value.numeroDocumento);
        }
    }*/

    /*selectedProcessResponse = function (event, controlName) {
        if (!!event.value) {
            this.formJefeInmediato.controls[controlName].setValue(event.value.codigoApp);
        }
    };*/

    onChageForm() {
        var aux = this.jefe;
        this.formJefeInmediato.valueChanges.subscribe(val => {
                this.cargarFormularioParaEnvio(val);
                console.log('Estado Formulario:' + this.formJefeInmediato.valid);
                this.enviarInformacionGestion.emit({gestionTramite : this.rqGestionTramite, validForm : this.formJefeInmediato.valid});
            }, error => console.log(error)
        );
    }

    private cargarFormularioParaEnvio(val:any){
        this.rqGestionTramite = new RqGestionTramiteDTO();
        this.rqGestionTramite.permisoRemunerado = val.formPermisoRemunerado.nombre;
        this.rqGestionTramite.requiereReemplazo = val.formRequiereReemplazo.nombre;
        this.rqGestionTramite.observaciones = val.formObservaciones;
        this.rqGestionTramite.respuestaTramite = val.formRespTramite.id;
    }

    /*onChageFormGerente() {
        var aux = this.gerente;
        this.formJefeInmediato.valueChanges.subscribe(val => {
                this.enviarInformacionGerente.emit(this.fn.formToDto(val, aux));
            }, error => console.log(error)
        );
    }*/

}
