import { Component, Input, OnInit } from '@angular/core';
import { GestionTramiteDTO } from 'src/app/domain/model/dto/GestionTramiteDTO';
import { UtilitiesFnService } from '../../../../infraestructure/services/UtilitiesFnService';

@Component({
    selector: 'app-gestion-tramite-visto-bueno-lectura',
    templateUrl: './gestion-tramite-visto-bueno-lectura.component.html'
})
export class GestionTramiteVistoBuenoLecturaComponent implements OnInit {

    @Input() managementProcess: GestionTramiteDTO;
    @Input() managementLegendPerformed: string;

    constructor (
        private fn: UtilitiesFnService
    ) { 
    }

    ngOnInit () {

    }
}