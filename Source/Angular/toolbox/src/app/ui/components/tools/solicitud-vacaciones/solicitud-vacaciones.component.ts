import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TramiteDTO } from 'src/app/domain/model/dto/TramiteDTO';
import { SolicitudVacacionesDTO } from 'src/app/domain/model/dto/SolicitudVacacionesDTO';
import { UtilitiesFnService } from '../../../../infraestructure/services/UtilitiesFnService';
import { DateEsEes } from 'src/environments/environment.variables';
import {EmpleadoDTO} from '../../../../domain/model/dto/EmpleadoDTO';
import {BusinessResponseDTO} from '../../../../domain/model/dto/BusinessResponseDTO';
import {SolicitudService} from '../../../../infraestructure/services/solicitud.service';
import { CatalogueDTO } from 'src/app/domain/model/dto/CatalogueDTO';
import { TramiteService } from 'src/app/infraestructure/services/service.index';
import { DatePipe } from '@angular/common';
import { successResponse } from 'src/environments/environment.variables';
import { RqRadicarTramiteDTO } from 'src/app/domain/model/dto/RqRadicarTramiteDTO';
import { VacacionesTramiteDto } from 'src/app/domain/model/dto/VacacionesTramiteDto';

@Component({
    selector: 'app-solicitud-vacaciones',
    templateUrl: './solicitud-vacaciones.component.html'
})
export class SolicitudVacacionesComponent implements OnInit {
    @Output() enviarInformacionVacaciones = new EventEmitter<any>();

    @Input() diasDisponibles: number;
    @Input() solicitud: TramiteDTO;
    @Input() modificarTramite: boolean;

    rqRadicarTramiteDTO: RqRadicarTramiteDTO = new RqRadicarTramiteDTO();
    formVacaciones: FormGroup;
    es: any = DateEsEes;
    startDate: Date = new Date();
    minDate: Date = new Date();
    endDate: Date;
    returnDate: Date;
    fechaActual: Date = new Date();
    earlyRequest: boolean = false;
    invalidDates: Array<Date>;
    fechaFin:any;
    empleado: BusinessResponseDTO<EmpleadoDTO>;
    
    checkDiasDisponibles: boolean;
    disabledCheckedAvailableDays: boolean;
    showErrorStartDate: boolean = false;
    totalDaysRequested: number = 0;
    totalDaysAnticipated: number = 0;
    observaciones: string;

    constructor (
        private _formBuilder: FormBuilder,
        private fn: UtilitiesFnService,
        private empleados : SolicitudService,
        private t: TramiteService,
        private datePipe: DatePipe
    ) {
        this.formVacaciones = this._formBuilder.group({
            formDiasDisponibles: [''],
            formCheckDiasAnticipados:[''],
            formFechaInicio: ['',Validators.required],
            formFechaFin: ['',Validators.required],
            formFechaReintegro: [''],
            formDiasSolicitados: [''],
            formDiasAnticipados: [''],
            formObservaciones: ['']
        });

        //consultar dias disponibles
        //this.consultarDiasDisponibles();
    }

    ngOnInit () {
        if(this.modificarTramite)
            this.cargarData();
        else{
            if(this.diasDisponibles == 0){
                this.checkDiasDisponibles = true;
                this.disabledCheckedAvailableDays = true;
            }else{
                this.checkDiasDisponibles = false;
                this.disabledCheckedAvailableDays = false;
            }
        }
        this.onChageForm();
    }

    ngOnChanges (){
    }

    

    cargarData(){
        this.diasDisponibles = this.solicitud.vacacionesTramite.diasDisponibles;
        this.checkDiasDisponibles = this.solicitud.vacacionesTramite.solicitaDiasAnticipados;
        this.startDate = new Date(this.solicitud.vacacionesTramite.fechaInicio);
        this.endDate = new Date(this.solicitud.vacacionesTramite.fechaFin);
        this.returnDate = new Date(this.solicitud.vacacionesTramite.fechaReintegro);
        this.totalDaysRequested = this.solicitud.vacacionesTramite.totalDiasSolicitados;
        this.totalDaysAnticipated = this.solicitud.vacacionesTramite.totalDiasAnticipados;
        this.observaciones = this.solicitud.vacacionesTramite.observaciones;
    }

    /*onChangeObject () {
        if (!!this.solicitud.solicitudVacacionesDto)
            this.fn.dtoToForm(this.formVacaciones.controls, this.solicitud.solicitudVacacionesDto);
    }*/

    onChageForm() {
        console.log('******************onChageForm-VACACIONES***********');
        this.formVacaciones.valueChanges.subscribe(val => {
            this.cargarFormularioParaEnvio(val);
            console.log('Estado Formulario VACACIONES:' + this.formVacaciones.valid);
            this.enviarInformacionVacaciones.emit({vacacionesTramite : this.rqRadicarTramiteDTO.vacacionesTramite, validForm : this.formVacaciones.valid});
            
            //this.enviarInformacionCertificado.emit(this.fn.formToDto(val, aux));
        }, error => console.log(error)
        );
    }

    private cargarFormularioParaEnvio(val:any){
        debugger
        this.rqRadicarTramiteDTO = new RqRadicarTramiteDTO();
        let vacacionesTramiteDto = new VacacionesTramiteDto();
        vacacionesTramiteDto.solicitaDiasAnticipados = val.formCheckDiasAnticipados;
        vacacionesTramiteDto.totalDiasAnticipados = val.formDiasAnticipados == ""? 0 : val.formDiasAnticipados;
        vacacionesTramiteDto.diasDisponibles = val.formDiasDisponibles;
        vacacionesTramiteDto.totalDiasSolicitados = val.formDiasSolicitados;
        vacacionesTramiteDto.fechaFin = val.formFechaFin;
        vacacionesTramiteDto.fechaInicio = val.formFechaInicio;
        vacacionesTramiteDto.fechaReintegro = val.formFechaReintegro;
        vacacionesTramiteDto.observaciones = val.formObservaciones != null? val.formObservaciones : '';
        
        this.rqRadicarTramiteDTO.vacacionesTramite = vacacionesTramiteDto;
    }

    onChangeStartDate (event) {
        //this.formVacaciones.controls["fechaFin"].setValue(null);
        //debugger;
        if(this.endDate != null){
            if(this.startDate.getTime() >= this.endDate.getTime() ){
                //this.startDate = new Date();
                this.startDate = null;
                this.showErrorStartDate = true;
            }else{
                this.showErrorStartDate = false;
            }
        }

        this.calcularDias();
    }

    onChangeEndDate (event) {
        //debugger;
        /*var diferencia = this.fn.calculateDiffDate(this.startDate, this.endDate);
        this.formVacaciones.controls["diasSolicitados"].setValue(diferencia);
        if (this.earlyRequest) {
            this.formVacaciones.controls["formDiasAnticipados"].setValue(diferencia);
        }
        this.fechaFin = this.formVacaciones.controls["fechaFin"].value;
        this.fechaFin.setDate(this.fechaFin.getDate() +1);
        this.formVacaciones.controls["fechaReintegro"].setValue(this.fechaFin);
        this.fechaFin.setDate(this.fechaFin.getDate() -1);*/
        //this.formVacaciones.controls["formFechaReintegro"].setValue(this.fechaFin.getDate() +1);
        if(this.startDate != null){
            if(this.endDate.getTime() <= this.startDate.getTime()){
                this.endDate = null;
                this.returnDate = null;
            }
        }

        this.calcularDias();
    }

    private calcularDias(){
        if(this.startDate != null && this.endDate != null){
            this.t.consultarFechaReintegro(this.datePipe.transform(this.startDate,"dd/MM/yyyy"), this.datePipe.transform(this.endDate,"dd/MM/yyyy")).subscribe(data => {
                if (!!data.statusDTO && data.statusDTO.statusCode == successResponse.statusCode) {
                    this.totalDaysRequested = data.daysEnable;
                    var splitted = data.dateEnable.split("/", 3);
                    this.returnDate = new Date(splitted[2] + "/" + splitted[1] + "/" + splitted[0]);

                    let aux = this.totalDaysRequested - this.diasDisponibles;
                    if(aux > 0)
                        this.totalDaysAnticipated = aux;
                    else
                        this.totalDaysAnticipated =  0;
                }
            }); 
        }else{
            this.totalDaysRequested = 0;
            this.totalDaysAnticipated = 0;
        }
    }

    calculateDay (event) {
        /*if (event) {
            if (!!this.startDate && !!this.endDate) {
                var diferencia = this.fn.calculateDiffDate(this.startDate, this.endDate);
                this.formVacaciones.controls["formDiasAnticipados"].setValue(diferencia);
            }
        } else {
            this.formVacaciones.controls["formDiasAnticipados"].setValue('');
        }*/
    }

    adelantarDia(event){
        this.fechaFin.setDate(this.fechaFin.getDate()+1);
    }

}
