import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TablaBasicaDTO} from 'src/app/domain/model/dto/TablaBasicaDTO';
import {TablasBasicasService} from '../../../../infraestructure/services/tablasBasicas.service';
import {constantDecisionType, constantRequestType} from 'src/environments/environment.variables';
import {EmpleadoDTO} from '../../../../domain/model/dto/EmpleadoDTO';
import {EmpleadoListService} from '../../../../infraestructure/services/empleado-list.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UtilitiesFnService} from '../../../../infraestructure/services/UtilitiesFnService';
import {ConfirmationService} from 'src/app/infraestructure/services/service.index';
import {JefeInmediatoDTO} from '../../../../domain/model/dto/JefeInmediatoDTO';
import {TramiteDTO} from '../../../../domain/model/dto/TramiteDTO';
import {GerenteDTO} from '../../../../domain/model/dto/GerenteDTO';

import {
    MOCK_CERTIFICATE_TYPE_TABLE,
    MOCK_DECISION_TABLE,
    MOCK_DISABILITY_CLASSIFICATION_TABLE,
    MOCK_PROCESSRESPONSE_TABLE
} from 'src/app/domain/model/mocks/SolicitudMock';
import {CatalogueDTO} from 'src/app/domain/model/dto/CatalogueDTO';
import {DocumentDTO} from 'src/app/domain/model/dto/DocumentDTO';
import {TipoTramiteDTO} from 'src/app/domain/model/dto/TipoTramiteDTO';
import {RqGestionTramiteDTO} from 'src/app/domain/model/dto/RqGestionTramiteDTO';


@Component({
    selector: 'app-gestion-tramite-visto-bueno-gh',
    templateUrl: './gestion-tramite-visto-bueno-gh.component.html'
})
export class GestionTramiteVistoBuenoGHComponent implements OnInit {
    @Output() enviarInformacionGestion = new EventEmitter<any>();
    @Output() enviarInformacionJefe = new EventEmitter<JefeInmediatoDTO>();
    @Output() enviarInformacionGerente = new EventEmitter<GerenteDTO>();
    @Output() enviarDocumentos = new EventEmitter<DocumentDTO[]>();

    @Input() solicitud: TramiteDTO;
    @Input() jefe: JefeInmediatoDTO;
    @Input() gerente: GerenteDTO;
    @Input() managementLegend: string;

    paidLeave: CatalogueDTO[];
    requiresReplacement: CatalogueDTO[];
    processResponse: CatalogueDTO[];
    disabilityClassificationList: TipoTramiteDTO[];
    seldisabilityClassification: TipoTramiteDTO;

    certificateTypeList: TipoTramiteDTO[];
    showReplacementName: boolean = false;

    documents: DocumentDTO[] = new Array<DocumentDTO>();
    documentParent: DocumentDTO;
    index: number;
    attachmentsSectionEnabled: boolean = true;
    setEditable: boolean = true;


    classification: TablaBasicaDTO[];
    selectedSiONo: TablaBasicaDTO;
    empleado: EmpleadoDTO[];
    emp: EmpleadoDTO;
    formGerenteGH: FormGroup;
    enableOther: boolean;
    enableClassification: boolean;
    nombreReemplazo: string;
    rqGestionTramite: RqGestionTramiteDTO;



    constructor(private tb: TablasBasicasService,
                private confirmationService: ConfirmationService,
                private empleados: EmpleadoListService,
                private _formBuilder: FormBuilder,
                private fn: UtilitiesFnService) {

        this.formGerenteGH = this._formBuilder.group({
            formPermisoRemunerado: ['', Validators.required],
            formRequiereReemplazo: ['', Validators.required],
            formNombreReemplazo: [''],
            formObservaciones: ['', Validators.required],
            formRespTramite: ['', Validators.required],
            formClasificacionDiscapacidad: [''],
            formOtraClasificacion: ['']
        });
    }

    ngOnInit() {
        this.paidLeave = MOCK_DECISION_TABLE;
        this.requiresReplacement = MOCK_DECISION_TABLE;
        this.processResponse = MOCK_PROCESSRESPONSE_TABLE;
        this.disabilityClassificationList = MOCK_DISABILITY_CLASSIFICATION_TABLE;
        this.certificateTypeList = MOCK_CERTIFICATE_TYPE_TABLE;

        //this.classification = this.tb.consultarTablaBasica(codigosParametros.clasificacionIncapacidad);
        
        //this.listarEmpleados(event);
        this.onChageForm();
        /*this.onChageFormGerente();
        {
            var aux = this.jefe;
            this.formGerenteGH.valueChanges.subscribe(val => {
                    this.enviarInformacionJefe.emit(this.fn.formToDto(val, aux));
                }, error => console.log(error)
            );
        }*/
        this.classificationLoad( this.solicitud.idTipoTramite );
        this.loadAttachmentsSection();
    }

    loadAttachmentsSection(){
        //verificacion de tipo de solicitud o tramite
        for (let index = 0; index < this.solicitud.certificadoTramiteList.length; index++) {
            var document = new DocumentDTO();
            document.tipoDocumental = this.fn.findNameById(this.certificateTypeList,this.solicitud.certificadoTramiteList[index].idSubtipoTramite.id).nombre;
            this.documents.push(document);
        }
    }

    nuevoDocumento(index: number) {
        this.documentParent = new DocumentDTO();
        this.index = index;
        this.documentParent.tipoDocumental = this.documents[index].tipoDocumental;
    }

    editarDocumento(documentoSelect: DocumentDTO, index: number) {
        this.documentParent = documentoSelect;
        this.index = index;
    }

    guardarDocumentoInfo(nuevoDoc: DocumentDTO) {
        if (nuevoDoc != null) {
            this.fn.updateItemByEntity(this.documents, nuevoDoc, this.index);
            //this.disabledAddFile = true;
        }
        this.enviarDocumentos.emit(this.documents);
        this.documentParent = null;
        this.index = -1;
    }

    eliminarDocumento(docEliminado: DocumentDTO) {
        this.confirmationService.confirm({
            message: '¿Realmente desea eliminar este documento?',
            accept: () => {
                this.documents = this.fn.deleteItemByEntity(this.documents, docEliminado);
                this.enviarDocumentos.emit(this.documents);
                //this.disabledAddFile = false;
            },
        });
    }

    classificationLoad(tipoSolicitud: TipoTramiteDTO) {
        if(tipoSolicitud.id === constantRequestType.disabilityReportCode)
            this.enableClassification = true;
        else{
            this.enableClassification = false;
            this.enableOther = false;
        }
            

    }

    validateEnableOther(){
        this.enableOther = false;
        if(!!this.seldisabilityClassification){
            if(this.seldisabilityClassification.id === constantRequestType.otherCodeInability)
                this.enableOther = true;
        }
    }

    /*listarEmpleados(resquit) {
        this.empleados.listarEmpleados(this.empleado).subscribe(resultado => {
                this.empleado = new Array<EmpleadoDTO>();
                for (var i in resultado.body) {
                    var nvpGroup = new EmpleadoDTO();
                    nvpGroup.primerNombre = resultado.body[i].primerNombre.concat(" "+resultado.body[i].segundoNombre+" "+resultado.body[i].primerApellido)+" "+resultado.body[i].segundoApellido;
                    nvpGroup.primerApellido = resultado.body[i].primerApellido;
                    nvpGroup.segundoNombre = resultado.body[i].segundoNombre;
                    nvpGroup.segundoApellido = resultado.body[i].segundoApellido;
                    this.empleado.push(nvpGroup);
                    this.empleado = [...this.empleado];
                }
            },
            error => {

            });
    }*/

    selectedPaidLeave = function (event, controlName) {
        if (!!event.value) {
            //this.formJefeInmediato.controls[controlName].setValue(event.value.codigoApp.indexOf('@SN_S') == false);
            if(event.value.id == constantDecisionType.yes)
                this.showReplacementName = true;
            else
                this.showReplacementName = false;  
        }
    }

    /*selectEmpleado = function (event, controlName) {
        if (!!event.value) {
            this.formJefeInmediato.controls[controlName].setValue(event.value.numeroDocumento);
        }
    }*/

    /*selectedProcessResponse = function (event, controlName) {
        if (!!event.value) {
            this.formJefeInmediato.controls[controlName].setValue(event.value.codigoApp);
        }
    };*/

    onChageForm() {
        debugger;
        var aux = this.jefe;
        this.formGerenteGH.valueChanges.subscribe(val => {
            debugger;
                this.cargarFormularioParaEnvio(val);
                console.log('Estado Formulario:' + this.formGerenteGH.valid);
                this.enviarInformacionGestion.emit({gestionTramite : this.rqGestionTramite, validForm : this.formGerenteGH.valid});
            }, error => console.log(error)
        );
    }

    private cargarFormularioParaEnvio(val:any){
        debugger;
        this.rqGestionTramite = new RqGestionTramiteDTO();
        this.rqGestionTramite.permisoRemunerado = val.formPermisoRemunerado.nombre;
        this.rqGestionTramite.requiereReemplazo = val.formRequiereReemplazo.nombre;
        this.rqGestionTramite.observaciones = val.formObservaciones;
        this.rqGestionTramite.respuestaTramite = val.formRespTramite.id;
    }

    /*onChageFormGerente() {
        var aux = this.gerente;
        this.formGerenteGH.valueChanges.subscribe(val => {
                this.enviarInformacionGerente.emit(this.fn.formToDto(val, aux));
            }, error => console.log(error)
        );
    }*/

}
