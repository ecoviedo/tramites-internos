import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TablasBasicasService } from '../../../../infraestructure/services/tablasBasicas.service';
import { codigosParametros } from 'src/environments/environment.variables';
import { TramiteDTO } from 'src/app/domain/model/dto/TramiteDTO';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UtilitiesFnService } from '../../../../infraestructure/services/UtilitiesFnService';
import { CertificadoDTO } from 'src/app/domain/model/dto/CertificadoDTO';

import { Car } from 'src/app/domain/model/dto/car';
import { SortEvent } from 'primeng/api';
//import { CarService } from '../../service/carservice';


@Component({
    selector: 'app-trazabilidad-tramite',
    templateUrl: './trazabilidad-tramite.component.html'
})
export class TrazabilidadTramiteComponent implements OnInit {

    @Output() enviarInformacionCertificado = new EventEmitter<CertificadoDTO>();
    @Input() solicitud: TramiteDTO;
    formTrazabilidad: FormGroup;

    cars1: Car[];

    cars2: Car[];

    cars3: Car[];

    cols: any[];

    constructor(
        private tb: TablasBasicasService,
        private _formBuilder: FormBuilder,
        private fn: UtilitiesFnService,
        //private carService: CarService
    ) {
        this.formTrazabilidad = this._formBuilder.group({
        });
    }

    ngOnInit() {
        //this.onChageForm();

        /*this.carService.getCarsSmall().then(cars => this.cars1 = cars);
        this.carService.getCarsSmall().then(cars => this.cars2 = cars);
        this.carService.getCarsSmall().then(cars => this.cars3 = cars);*/
        this.cars1 = [
            {"role": "Jefe Inmediato", "reviewedBy": "Fautino Hermo", "observations": "Se debe corregir la fecha de inicio del permiso", "date": "20/03/2020", "managementResult": "Solicitar cambios en la radicación"},
            {"role": "Jefe Inmediato", "reviewedBy": "Fautino Hermo", "observations": "Se debe corregir la fecha de inicio del permiso", "date": "15/03/2020", "managementResult": "Solicitar cambios en la radicación"},
            {"role": "Solicitante", "reviewedBy": "Ana Lucia Martinez Lopez", "observations": "Requiero mis vacaciones para tomar un curso en el extranjero", "date": "20/04/2020", "managementResult": "Radicar trámite"},
        ];



        this.cols = [
            { field: 'date', header: 'Fecha' },
            { field: 'reviewedBy', header: 'Revisado por' },
            { field: 'role', header: 'Rol' },
            { field: 'observations', header: 'Observaciones' },
            { field: 'managementResult', header: 'Resultado de la gestión' },
        ];
    }

    /*onChageForm() {
        var aux = this.solicitud.certificado;
        this.formTrazabilidad.valueChanges.subscribe(val => {
            this.enviarInformacionCertificado.emit(this.fn.formToDto(val, aux));
        }, error => console.log(error)
        );
    }*/

    customSort(event: SortEvent) {
        event.data.sort((data1, data2) => {
            let value1 = data1[event.field];
            let value2 = data2[event.field];
            let result = null;

            if (value1 == null && value2 != null)
                result = -1;
            else if (value1 != null && value2 == null)
                result = 1;
            else if (value1 == null && value2 == null)
                result = 0;
            else if (typeof value1 === 'string' && typeof value2 === 'string')
                result = value1.localeCompare(value2);
            else
                result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;

            return (event.order * result);
        });
    }

}
