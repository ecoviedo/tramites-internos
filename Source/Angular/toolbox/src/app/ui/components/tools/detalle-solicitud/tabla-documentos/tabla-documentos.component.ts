import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, ViewChild, Output, TestabilityRegistry } from '@angular/core';
import { Message } from 'primeng/api';
import { DocumentDTO } from 'src/app/domain/model/dto/DocumentDTO';
import { TablaBasicaDTO } from 'src/app/domain/model/dto/TablaBasicaDTO';

@Component({
    selector: 'app-tabla-documentos',
    templateUrl: './tabla-documentos.component.html'
})
export class TablaDocumentosComponent implements OnInit {

    @Input() documentoHijo: DocumentDTO;
    
    @Input() setEditable: boolean;
    @Output() guardarDocumentoInfo = new EventEmitter<DocumentDTO>();
    @ViewChild('uploader') uploader;
    //tipoDocSelect: TablaBasicaDTO;
    documentName: string;
    display: boolean;
    count: number = 0;
    url: any;
    previewToPdf = false;
    previewToImage = false;
    previewToDoc = false;
    documentRefreshed: boolean = false;
    msgs: Message[] = [];
    uploadDisabled: boolean;
    file: File;
    pdfData: any;
    documentValidate: boolean = true;

    constructor(
        private changeDetection: ChangeDetectorRef
    ) {
        this.display = true;
    }

    ngOnInit() {
        this.display = true;
        if (!!this.documentoHijo) {
            /*if (!!this.documentoHijo.tipoDocumental)
                this.tipoDocumental.forEach(element => {
                    if (element.codigoApp == this.documentoHijo.tipoDocumental)
                        this.tipoDocSelect = element;
                });*/

            if (!!this.documentoHijo.documento) {
                const base64 = this.documentoHijo.documento;
                const tipo = this.documentoHijo.tipoArchivo;
                this.pdfData = this.base64ToFilePdf(base64, tipo);
            }

            if (!!this.documentoHijo.nombreDocumento)
                this.documentName = this.documentoHijo.nombreDocumento;
        }
    }

    base64ToFilePdf = function (base64, tipo) {
        var decodedPdfContent = atob(base64);
        var byteArray = new Uint8Array(decodedPdfContent.length)
        for (var idx = 0; idx < decodedPdfContent.length; idx++) {
            byteArray[idx] = decodedPdfContent.charCodeAt(idx);
        }
        var blob = new Blob([byteArray], { type: tipo });
        return blob;
    }

    guardarDoc() {
        this.display = false;
        //this.documentoHijo.nombreDocumento = this.documentName;
        //this.documentoHijo.tipoDocumental = !!this.tipoDocSelect ? this.tipoDocSelect.nombre : "";
        this.guardarDocumentoInfo.emit(this.documentoHijo);
    }

    eliminarDoc() {
        this.documentoHijo = null;
        this.display = false;
        this.guardarDocumentoInfo.emit(this.documentoHijo);
    }

    close() {
        this.display = false;
        this.documentoHijo = null;
    }

    previewPdf(file) {
        const reader = new FileReader();
        this.count++;
        if (this.count % 2 == 0) {
            this.previewToPdf = false;
        } else {
            reader.addEventListener('load', () => {
                this.url = reader.result;
                this.previewToPdf = true;
                this.changeDetection.detectChanges();
            }, false);
            reader.readAsArrayBuffer(file);
        }
    }

    previewImage(file) {
        const reader = new FileReader();
        this.count++;
        if (this.count % 2 == 0) {
            this.previewToImage = false;
        } else {
            reader.readAsDataURL(file);
            reader.onload = (event) => {
                this.url = event.target;
                this.previewToImage = true;
                this.changeDetection.detectChanges();
            }
        }
    }

    previewDoc(file) {
        const reader = new FileReader();
        this.count++;
        if (this.count % 2 == 0) {
            this.previewToDoc = false;
        } else {
            reader.addEventListener('load', () => {
                this.url = reader.result;
                this.previewToDoc = true;
                this.changeDetection.detectChanges();
            }, false);
            reader.readAsArrayBuffer(file);
        }
    }

    onSelect(event) {
        var docAdjunto: any;
        const reader = new FileReader();
        if (this.uploader.files.length > 0) {
            docAdjunto = this.uploader.files[0];
        }
        if (!!docAdjunto) {
            reader.addEventListener('load', () => {
                if (reader.readyState == 2) {
                    this.cargarDocumentoAdjunto(reader, docAdjunto);
                    this.documentRefreshed = true;
                    this.msgs.push({ severity: "success", summary: "Carga Completa", detail: docAdjunto.name });
                }
                this.changeDetection.detectChanges();
            }, false);
            reader.readAsDataURL(docAdjunto);
        }
        this.previewToPdf = false;
        this.previewToImage = false;
        this.previewToDoc = false;
        this.changeDetection.detectChanges();
    }

    cargarDocumentoAdjunto(reader, adjunto) {
        this.documentoHijo.documento = !!reader.result ? reader.result.toString().split(",")[1] : "";
        this.documentoHijo.fechaCreacion = adjunto.lastModified;
        //this.documentoHijo.nombreDocumento = !!adjunto.name ? adjunto.name.split(".")[0] : "";
        this.documentoHijo.nombreDocumento = adjunto.name;
        this.documentoHijo.tamano = adjunto.size;
        this.documentoHijo.tipoArchivo = adjunto.type;
        console.log('**************************adjunto.type*****************' + adjunto.type);
        //if (!!this.documentName)
        this.documentValidate = false;
    }

    validarSeleccion() {
        debugger
        if (!!this.documentoHijo.documento) // && !!this.documentName)
            this.documentValidate = false;
    }

}

