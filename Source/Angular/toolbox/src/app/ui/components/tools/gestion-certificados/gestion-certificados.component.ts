import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TablaBasicaDTO } from 'src/app/domain/model/dto/TablaBasicaDTO';
import { TablasBasicasService } from '../../../../infraestructure/services/tablasBasicas.service';

import { TramiteDTO } from 'src/app/domain/model/dto/TramiteDTO';
import { RqRadicarTramiteDTO } from 'src/app/domain/model/dto/RqRadicarTramiteDTO';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UtilitiesFnService } from '../../../../infraestructure/services/UtilitiesFnService';
import { CertificadoDTO } from 'src/app/domain/model/dto/CertificadoDTO';

import { MOCK_CERTIFICATE_TYPE_TABLE } from 'src/app/domain/model/mocks/SolicitudMock';
import { MultiSelectModule } from 'primeng/multiselect';
import { SelectItem } from 'primeng/api';
import { TipoTramiteDTO } from 'src/app/domain/model/dto/TipoTramiteDTO';
import { constantRequestType, successResponse } from 'src/environments/environment.variables';
import { codigosParametros } from 'src/environments/environment.variables';
import { CertificadoTramiteDto } from 'src/app/domain/model/dto/CertificadoTramiteDto';

@Component({
    selector: 'app-gestion-certificados',
    templateUrl: './gestion-certificados.component.html',
    styles: [`
        :host ::ng-deep .ui-multiselected-item-token,
        :host ::ng-deep .ui-multiselected-empty-token {
            padding: 2px 4px;
            margin: 0 0.286em 0 0;
            display: inline-block;
            vertical-align:middle;
            height: 1.857em;
        }

        :host ::ng-deep .ui-multiselected-item-token {
            background: #007ad9;
            color: #ffffff;
        }

        :host ::ng-deep .ui-multiselected-empty-token {
            background: #d95f00;
            color: #ffffff;
        }
    `]
})
export class GestionCertificadosComponent implements OnInit {
    @Output() enviarInformacionCertificado = new EventEmitter<any>();

    @Input() solicitud: TramiteDTO;
    @Input() modificarTramite: boolean;

    rqRadicarTramiteDTO: RqRadicarTramiteDTO = new RqRadicarTramiteDTO();
    //certificateType: TablaBasicaDTO[];
    certificateType: TipoTramiteDTO[] = new Array<TipoTramiteDTO>();
    formCertificado: FormGroup;

    certificateTypeSelected: TipoTramiteDTO[] = [];
    cars: SelectItem[];
    items: SelectItem[];
    item: string;
    observaciones: string;

    constructor(
        private tb: TablasBasicasService,
        private _formBuilder: FormBuilder,
        private fn: UtilitiesFnService
    ) {
        console.log('**********************constructor-CERTIFICADOS*******************');

        this.formCertificado = this._formBuilder.group({
            formObservaciones: ['', Validators.required],
            formCertificateType: ['', Validators.required]
        });

        this.cars = [
            { label: 'Certificado laboral estándar', value: '16' },
            { label: 'Certificado laboral con funciones', value: '17' },
            { label: 'Certificado ingresos y retenciones', value: '18' },
            { label: 'Certificado de cobertura ARL en el exterior', value: '19' },
            { label: 'Certificado medicina prepagada', value: '20' }
        ];

        /*this.items = [];
        for (let i = 0; i < 10000; i++) {
            this.items.push({label: 'Item ' + i, value: 'Item ' + i});
        }*/
    }

    ngOnInit() {
        console.log('**********************ngOnInit-CERTIFICADOS*******************');
        this.cargarCatalogos();
        if (this.modificarTramite)
            this.cargarData();
        //else


        //this.certificateType = this.tb.consultarTablaBasica(codigosParametros.tipoCertificado);
        this.onChageForm();
    }

    ngOnChanges(){
        console.log('**********************ngOnChanges-CERTIFICADOS*******************');
    }

    private cargarCatalogos() {
        //this.certificateType = MOCK_CERTIFICATE_TYPE_TABLE;
        this.tb.consultarTipoTramite(constantRequestType.certificationCode).subscribe(data => {
            if (!!data.response && data.response.statusCode == successResponse.statusCode) {
                this.certificateType = data.proceduresTypes;
            }
        });
    }

    cargarData() {
        this.loadCertificatesSection();
        /*this.tipoTramiteSel = this.fn.findNameById(this.requestType, this.solicitud.idTipoTramite.id);
        this.formaSolicitud.controls['fechaSolicitud'].setValue(this.solicitud.fecha);
        this.nombre = this.solicitud.nombreEmpleado;
        this.apellido = this.solicitud.apellidoEmpleado;
        this.selectedIdentificationType = this.fn.findNameById(this.identificationTypes, this.solicitud.idTipoIdentificacion.idTipoIdentificacion);
        this.numeroDocumento = this.solicitud.identificacion;
        this.selectedcompanyArea = this.fn.findNameById(this.companyArea, this.solicitud.idArea.id);
        var aux = this.immediateBossControl();
        if(aux){
            this.selectedimmediateBoss = this.fn.findNameById(this.immediateBosses, this.solicitud.idGestorTramite.id);
        }*/
    }

    private loadCertificatesSection() {
        var procedureTypeDTO: TipoTramiteDTO;
        for (let index = 0; index < this.solicitud.certificadoTramiteList.length; index++) {
            this.observaciones = this.solicitud.certificadoTramiteList[index].observaciones;
            procedureTypeDTO = this.fn.findNameById(this.certificateType, this.solicitud.certificadoTramiteList[index].idSubtipoTramite.id);
            this.certificateTypeSelected.push(procedureTypeDTO);
        }
    }

    onChageForm() {
        console.log('******************onChageForm-CERTIFICADOS***********');
        this.formCertificado.valueChanges.subscribe(val => {
            this.cargarFormularioParaEnvio(val);
            console.log('Estado Formulario CERTIFICADOS:' + this.formCertificado.valid);
            this.enviarInformacionCertificado.emit({certificadoTramite : this.rqRadicarTramiteDTO.certificadoTramite, validForm : this.formCertificado.valid});
            
            //this.enviarInformacionCertificado.emit(this.fn.formToDto(val, aux));
        }, error => console.log(error)
        );
    }

    private cargarFormularioParaEnvio(val:any){
        //this.solicitud.certificadoTramiteList = new Array<CertificadoTramiteDto>();
        let auxCertificadoTramiteList = new Array<CertificadoTramiteDto>();
        for (let index = 0; index < val.formCertificateType.length; index++) {
            let auxCertificadoTramiteDto = new CertificadoTramiteDto();
            auxCertificadoTramiteDto.observaciones = val.formObservaciones;
            auxCertificadoTramiteDto.idSubtipoTramite = val.formCertificateType[index];
            auxCertificadoTramiteList.push(auxCertificadoTramiteDto);
        }
        this.rqRadicarTramiteDTO.certificadoTramite = auxCertificadoTramiteList;
    }

    selectCertificateType() {
        console.log('*************' + this.certificateTypeSelected);
        //debugger;
    }

    selectForm = function (event, controlName) {
        if (!!event.value)
            this.formCertificado.controls[controlName].setValue(event.value.codigoApp);
    }
}
