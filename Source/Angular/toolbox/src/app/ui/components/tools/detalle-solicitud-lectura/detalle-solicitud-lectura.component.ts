import {ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {DetalleSolicitudDTO} from 'src/app/domain/model/dto/DetalleSolicitudDTO';
import {DocumentDTO} from 'src/app/domain/model/dto/DocumentDTO';
import {TramiteDTO} from 'src/app/domain/model/dto/TramiteDTO';
import {TablaBasicaDTO} from 'src/app/domain/model/dto/TablaBasicaDTO';
import {ConfirmationService, UtilitiesFnService} from 'src/app/infraestructure/services/service.index';
import {TablasBasicasService} from '../../../../infraestructure/services/tablasBasicas.service';
import {codigosParametros, successResponse} from 'src/environments/environment.variables';
import {MessageService} from 'primeng/components/common/messageservice';
import {Message} from 'primeng/components/common/api';

import { DocumentTypeDTO } from 'src/app/domain/model/dto/DocumentTypeDTO';
import { MOCK_PERSONAL_REASON_TABLE } from 'src/app/domain/model/mocks/SolicitudMock';
import { MOCK_DOCUMENT_TYPE_TABLE } from 'src/app/domain/model/mocks/SolicitudMock';
import { constantRequestType } from 'src/environments/environment.variables';
import { TipoTramiteDTO } from 'src/app/domain/model/dto/TipoTramiteDTO';


@Component({
    selector: 'app-detalle-solicitud-lectura',
    templateUrl: './detalle-solicitud-lectura.component.html',
    providers: [MessageService]
})
export class DetalleSolicitudLecturaComponent implements OnInit {

    @Input() solicitud: TramiteDTO;
    //@Input() tipoSolicitud: TablaBasicaDTO;
    //@Input() tipoSolicitud: ProcedureTypeDTO;
    @Input() reasonForRequest: TipoTramiteDTO[];
    @Output() enviarInformacionDetalle = new EventEmitter<DetalleSolicitudDTO>();
    @Output() enviarDocumentos = new EventEmitter<DocumentDTO[]>();
    //formDetalleSolicitud: FormGroup;
    documentParent: DocumentDTO;
    //reasonRequest: TablaBasicaDTO[];
    //documentalType: TablaBasicaDTO[];
    documents: DocumentDTO[] = new Array<DocumentDTO>();
    index: number;
    es: any;
    aRequestedDay: boolean = true;
    //requestHourVsb: boolean = true;
    //reasonRequestVsb: boolean;
    enableReasonForRequest: boolean;
    currentDate: Date = new Date();
    requestedDay: Date = new Date();
    endDate: Date = new Date();
    msgs: Message[] = [];
    requestedDays: number;
    selectedReasonForRequest: TipoTramiteDTO;
    enableOther: boolean;
    attachmentsSectionEnabled: boolean;
    documentTypeAll: DocumentTypeDTO[];
    documentTypeSelected: DocumentTypeDTO;

    constructor(
        private fn: UtilitiesFnService,
        private confirmationService: ConfirmationService,
        private tb: TablasBasicasService,
        private _formBuilder: FormBuilder,
        private cd: ChangeDetectorRef,
        private messageService: MessageService
    ) {
        /*this.formDetalleSolicitud = this._formBuilder.group({
        });*/

        this.documentTypeAll = MOCK_DOCUMENT_TYPE_TABLE;
    }

    ngOnInit() {
        //this.documentalType = this.tb.consultarTablaBasica(codigosParametros.tipoDocumental);
        //this.reasonRequest = this.tb.consultarTablaBasica(codigosParametros.motivoSolciitud);
        //this.msgs = 'error jj';

        this.cargarCatalogos();
        //this.onChageForm();
        this.loadView();
    }

    private cargarCatalogos() {
        //this.reasonForRequest = MOCK_PERSONAL_REASON_TABLE;
         this.tb.consultarTipoTramite(constantRequestType.permissionsCode).subscribe(data => {
             if (!!data.response && data.response.statusCode == successResponse.statusCode) {
                 this.reasonForRequest = data.proceduresTypes;
                 this.loadView();
             }
         });
     }

    ngOnChanges(changes: SimpleChanges) : void {
        if(!!this.solicitud)
            this.loadReasonForRequest( this.solicitud.idTipoTramite );
        this.loadAttachmentsSection(); 



        if (changes.tipoSolicitud && changes.tipoSolicitud.currentValue) {
            var tipo = changes.tipoSolicitud.currentValue.codigoApp;
            //this.requestDayVsb = this.validateVisibility(tipo, ['@TS_DF', '@TS_DC', '@TS_CD', '@TS_PE']);
            this.aRequestedDay = true;

            //this.requestHourVsb = this.validateVisibility(tipo, ['@TS_CD', '@TS_PE']);
            //this.requestHourVsb = true;

            //this.reasonRequestVsb = this.validateVisibility(tipo, ['@TS_PE']);
            /*if (tipo == "@TS_LL") {
                this.formDetalleSolicitud.controls['requestedDays'].setValue(5);
                this.formDetalleSolicitud.controls['requestedDays'].disable();
            }*/
        }
    }

    loadView(){
        if(!!this.solicitud){
            this.selectedReasonForRequest = this.fn.findNameById(this.reasonForRequest, this.solicitud.detalleTramite.idSubtipoTramite.id);
        }
            
    }

    loadAttachmentsSection(){
        if(!!this.solicitud){
            if(!!this.solicitud.detalleTramite.idTipoDocumentoTramite)
                this.documentTypeSelected = this.fn.findNameById(this.documentTypeAll, this.solicitud.detalleTramite.idTipoDocumentoTramite.id);
            //this.attachmentsSectionEnabled = this.searchTypeDocumentProcess(tipoSolicitud, null);
            this.documentParent = new DocumentDTO();
            this.documentParent.nombreDocumento = this.solicitud.detalleTramite.nombreArchivo;
            this.documentParent.documento = this.solicitud.detalleTramite.contenidoArchivo;
            this.documentParent.tipoArchivo = this.solicitud.detalleTramite.tipoArchivo;
            this.guardarDocumentoInfo(this.documentParent);
        }
    }

    searchTypeDocumentProcess(procedureTypeId, subprocedureTypeId){
        let flag = false;
        if(!!this.documentTypeAll){
            for (let index = 0; index < this.documentTypeAll.length; index++) {
                if (this.documentTypeAll[index].idTipoTramite == procedureTypeId
                    && this.documentTypeAll[index].idSubtipoTramite == subprocedureTypeId) {
                    this.documentTypeSelected = this.documentTypeAll[index];
                    flag = true;
                    break;
                }
            }
        }
        return flag;
    }
    

    loadReasonForRequest(tipoSolicitud: TipoTramiteDTO) {
        if(tipoSolicitud.id === constantRequestType.permissionsCode)
            this.enableReasonForRequest = true;
        else{
            this.enableReasonForRequest = false;
            this.enableOther = false;
        }
            

    }

    editarDocumento(documentoSelect: DocumentDTO, index: number) {
        this.documentParent = documentoSelect;
        this.index = index;
    }
    
    guardarDocumentoInfo(nuevoDoc: DocumentDTO) {
        if (nuevoDoc != null) {
            this.fn.updateItemByEntity(this.documents, nuevoDoc, this.index);
        }
        this.enviarDocumentos.emit(this.documents);
        this.documentParent = null;
        this.index = -1;
    }

    eliminarDocumento(docEliminado: DocumentDTO) {
        this.confirmationService.confirm({
            message: '¿Realmente desea eliminar este documento?',
            accept: () => {
                this.documents = this.fn.deleteItemByEntity(this.documents, docEliminado);
                this.enviarDocumentos.emit(this.documents);
            },
        });
    }

    

    valorLista(item: string, lista: any[]) {
        const itemNvp = this.fn.filterItemByListNvp(lista, item);
        return !!itemNvp ? itemNvp.name : '';
    }

    validateVisibility (argument: string, parameters: any[]) {
        for (let index = 0; index < parameters.length; index++) {
            if (parameters[index] == argument)
                return true;
        }
        return false;
    }



    /*onChageForm() {
        var aux = this.solicitud.detalleSolicitudDto;
        /*this.formDetalleSolicitud.valueChanges.subscribe(val => {
                this.enviarInformacionDetalle.emit(this.fn.formToDto(val, aux));
            }, error => console.log(error)
        );*
    }*/


    show() {
        this.msgs.push({severity:'info', summary:'Info Message', detail:'PrimeNG rocks'});
    }
    
    clear() {
        this.msgs = [];
    }

    verifyNumber(){
        console.log('Ingresa');
        console.log(''+this.requestedDays);
        if(this.requestedDays < 1){
            this.msgs = [];
            this.msgs.push({severity:'error', summary:'Error Message', detail:'Validation failed'});
        }
    }
}
