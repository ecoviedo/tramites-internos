import { RouterModule, Routes } from '@angular/router';
import { PagesComponent } from './pages.component';
import { ROUTES_PATH } from './pages.routing.name';
import { AprobacionGerenteComponent } from './pages/aprobacion-gerente/aprobacion-gerente.component';
import { CrearTramiteComponent } from './pages/crear-tramite/crear-tramite.component';
import { ModificarTramiteComponent } from './pages/modificar-tramite/modificar-tramite.component';
import { AdministracionProcesosComponent } from './pages/administracionProcesos/administracionProcesos.component';
import { AdministracionServiciosComponent } from './pages/administracionServicios/administracionServicios.component';
import { AdministracionTareasComponent } from './pages/administracionTareas/administracionTareas.component';
import { ReasignarTareaComponent } from './pages/administracionTareas/reasignarTarea/reasignarTarea.component';
import { PerfilUsuarioComponent } from './pages/administracionUsuarios/perfilUsuario/perfilUsuario.component';
import { AdministracionUsuariosComponent } from './pages/administracionUsuarios/administracionUsuarios.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';

import { gestionarSolicitudJefeComponent } from './pages/gestionar-solicitud-jefe/gestionar-solicitud-jefe.component';
import { gestionarSolicitudGerenteComponent } from './pages/gestionar-solicitud-gerente/gestionar-solicitud-gerente.component';
import { gestionarSolicitudGerenteGHComponent } from './pages/gestionar-solicitud-gerente-gh/gestionar-solicitud-gerente-gh.component';

// Array of routes
const PAGES_ROUTES: Routes = [
    //{path: '', redirectTo: ROUTES_PATH.home, pathMatch: 'full'},
    {
        path: ROUTES_PATH.home,
        component: PagesComponent,
        children: [
            {
                path: ROUTES_PATH.dashboard,
                component: DashboardComponent
            },
            {
                path: ROUTES_PATH.adminUser,
                component: AdministracionUsuariosComponent
            },
            {
                path: ROUTES_PATH.trackServices,
                component: AdministracionServiciosComponent
            },
            {
                path: ROUTES_PATH.userProfile,
                component: PerfilUsuarioComponent
            },
            {
                path: ROUTES_PATH.processes,
                component: AdministracionProcesosComponent
            },
            {
                path: ROUTES_PATH.task,
                component: AdministracionTareasComponent
            },
            {
                path: ROUTES_PATH.reassignTask,
                component: ReasignarTareaComponent
            },
            {
                path: ROUTES_PATH.crearTramite,
                component: CrearTramiteComponent
            },
            {
                path: ROUTES_PATH.modificarTramite,
                component: ModificarTramiteComponent
            },
            {
                path: ROUTES_PATH.aprobacionGerente,
                component: AprobacionGerenteComponent
            },

            {
                path: ROUTES_PATH.gestionJefe,
                component: gestionarSolicitudJefeComponent
            },
            {
                path: ROUTES_PATH.gestionGerente,
                component: gestionarSolicitudGerenteComponent
            },
            {
                path: ROUTES_PATH.gestionGerenteGH,
                component: gestionarSolicitudGerenteGHComponent
            }
        ]
    }
];

export const PAGES_ROUTES_MODULE = RouterModule.forChild(PAGES_ROUTES);
