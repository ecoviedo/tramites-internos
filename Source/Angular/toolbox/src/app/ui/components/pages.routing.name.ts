export const ROUTES_PATH = {
    home: 'home',
    login: 'login',
    dashboard: 'dashboard',
    processes: 'processes',
    task: 'tareas',
    reassignTask: 'reassignTask',
    adminUser: 'adminUser',
    trackServices: 'trackServices',
    userProfile: 'userProfile',
    crearTramite: 'crear-tramite',
    aprobacionGerente: 'AprobacionGerente',
    modificarTramite: 'modificar-tramite',
    
    gestionJefe: 'gestion-jefe_inmediato',
    gestionGerente: 'gestion-gerente',
    gestionGerenteGH: 'gestion-gerente-gh'
}

export const GESTION_JEFE = {
    label: 'Gestionar Trámite Jefe Inmediato',
    icon: 'warning',
    routerLink: ['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.gestionJefe]
}

export const GESTION_GERENTE = {
    label: 'Gestionar Trámite Gerente',
    icon: 'warning',
    routerLink: ['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.gestionGerente]
}

export const GESTION_GERENTE_GH = {
    label: 'Gestionar Trámite Gerente GH',
    icon: 'warning',
    routerLink: ['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.gestionGerenteGH]
}

export const DASHBOARD = {
    label: 'Tablero de Proceso',
    icon: 'dashboard',
    routerLink: ['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.dashboard]
}

export const PROCESS_VIEW = {
    label: 'Procesos',
    icon: 'work',
    routerLink: ['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.processes]
}

export const TASK_VIEW = {
    label: 'Tareas',
    icon: 'list',
    routerLink: ['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.task]
}

export const REASIGN_TASK = {
    label: 'Reasignar Tareas',
    icon: 'subject',
    routerLink: ['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.reassignTask]
}
export const CREAR_TRAMITE = {
    label: 'Crear Trámite',
    icon: 'assignment',
    routerLink: ['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.crearTramite]
}

export const MODIFICAR_TRAMITE = {
    label: 'Modificar Trámite',
    icon: 'assignment',
    routerLink: ['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.modificarTramite]
}

export const APROBAR_GERENTE = {
    label: 'Aprovacion Gerente',
    icon: 'assignment_late',
    routerLink: ['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.aprobacionGerente]
}

