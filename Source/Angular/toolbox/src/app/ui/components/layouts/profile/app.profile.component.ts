import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { PagesComponent } from '../../pages.component';
import { UserDTO } from 'src/app/domain/model/dto/UserDTO';
import { LoginService } from 'src/app/infraestructure/services/login.service';
import { ROUTES_PATH } from '../../pages.routing.name';

@Component({
    selector: 'app-inline-profile',
    templateUrl: './app.profile.component.html',
    animations: [
        trigger('menu', [
            state('hidden', style({
                height: '0px'
            })),
            state('visible', style({
                height: '*'
            })),
            transition('visible => hidden', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)')),
            transition('hidden => visible', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
        ])
    ]
})

export class AppInlineProfileComponent {

    active: boolean;
    currentUser: UserDTO;
    @Output() onSecurityRole: EventEmitter<any> = new EventEmitter();
    @Output() onProfileRole: EventEmitter<any> = new EventEmitter();
    @Output() onTrackServices: EventEmitter<any> = new EventEmitter();

    constructor(
        public app: PagesComponent,
        private router: Router,
        private loginService: LoginService
    ) {
        this.loginService.currentUser.subscribe(x => this.currentUser = x);
    }

    onClick(event) {
        this.active = !this.active;
        setTimeout(() => {
          this.app.layoutMenuScrollerViewChild.moveBar();
        }, 450);
        event.preventDefault();
    }

    logout() {
        this.loginService.logout();
        this.router.navigate(['/' + ROUTES_PATH.login]);
    } 

    _onSecurityRole (event): void {
        this.onSecurityRole.emit();
        event.preventDefault();
    }

    _onProfile (event): void {
        this.onProfileRole.emit();
        event.preventDefault();
    }

    _onTrackServices  (event): void {
        this.onTrackServices.emit();
        event.preventDefault();
    }
}
