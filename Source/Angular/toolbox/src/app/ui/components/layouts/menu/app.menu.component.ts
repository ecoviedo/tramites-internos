import { Component, Input, OnInit } from '@angular/core';
import {APROBAR_GERENTE, DASHBOARD, PROCESS_VIEW, CREAR_TRAMITE, MODIFICAR_TRAMITE, REASIGN_TASK, TASK_VIEW, 
    GESTION_JEFE, GESTION_GERENTE, GESTION_GERENTE_GH} from '../../pages.routing.name';
import { PagesComponent } from './../../pages.component';

@Component({
    selector: 'app-menu',
    templateUrl: './app.menu.component.html'
})
export class AppMenuComponent implements OnInit {

    @Input() reset: boolean;
    model: any[] = [];

    constructor(
        public app: PagesComponent
    ) {}

    ngOnInit() {
        this.model.push(DASHBOARD);
        this.model.push(TASK_VIEW);

        this.model.push(CREAR_TRAMITE);
        //this.model.push(MODIFICAR_TRAMITE);

        /*this.model.push(GESTION_JEFE);
        this.model.push(GESTION_GERENTE);
        this.model.push(GESTION_GERENTE_GH);*/
    }
}


