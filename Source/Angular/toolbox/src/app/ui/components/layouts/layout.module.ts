
import { PRIMENG_MODULES } from './../../../shared/primeNg/primeng-elements';
import { AppInlineProfileComponent } from './profile/app.profile.component';
import { AppRightpanelComponent } from './right-panel/app.rightpanel.component';
import { AppMenuComponent } from './menu/app.menu.component';
import { AppBreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppTopbarComponent } from './top-bar/app.topbar.component';
import { AppSubMenuComponent } from './menu/subMenu/app.SubMenu.Component';
import { AppFooterComponent } from './footer/app.footer.component';
import { AlertComponent } from './alert/alert.component';

@NgModule({
  declarations: [
      AppSubMenuComponent,
      AppMenuComponent,
      AppTopbarComponent,
      AppFooterComponent,
      AppBreadcrumbComponent,
      AppRightpanelComponent,
      AppInlineProfileComponent,
      AlertComponent
    ],
  exports: [
    AppSubMenuComponent,
    AppMenuComponent,
    AppTopbarComponent,
    AppFooterComponent,
    AppBreadcrumbComponent,
    AppRightpanelComponent,
    AppInlineProfileComponent,
    AlertComponent
  ],
  imports: [
    CommonModule,
    ...PRIMENG_MODULES
  ]
})
export class LayoutModule { }
