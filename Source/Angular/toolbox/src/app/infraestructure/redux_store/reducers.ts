import * as fromRouter from '@ngrx/router-store';
import * as Auth from './states/authentication/reducers';
import * as Notification from './states/notification/reducers';
import {ActionReducer, ActionReducerMap, MetaReducer} from '@ngrx/store';


export interface  State {
    router: fromRouter.RouterReducerState;
    auth: Auth.State;
    notification: Notification.State ;

    // include here yours reducers: Incluye aqui tus reducers
}

export const reducers: ActionReducerMap<State> = {
    router: fromRouter.routerReducer,
    auth: Auth.loginReducer,
    notification: Notification.reducer
};

export function appReducers(reducer: ActionReducer<State>): ActionReducer<State> {

    return function(state: State, action: any): State {

        return reducer(state, action);
    };
}

export const metaReducers: MetaReducer<State>[] = [appReducers];
