import {Action} from '@ngrx/store';

export const ActionTypes = {
    LOGIN: '[Login] Login Dispatch',
    LOGIN_SUCCESS: '[Login] Login SUCCESS',
    LOGIN_FAIL: '[Login] Login FAIL',
    LOGOUT: '[Logout] Logout Dispatch',
    LOGOUT_SUCCESS: '[Logout] Logout SUCCESS',
    LOGOUT_FAIL: '[Logout] Logout FAIL',
};

export class LoginAction implements Action {
    type = ActionTypes.LOGIN;

    constructor(public payload?: any) { }
}

export class LoginSuccessAction implements Action {
    type = ActionTypes.LOGIN_SUCCESS;

    constructor(public payload?: any) { }
}

export class LoginFailAction implements Action {
    type = ActionTypes.LOGIN_FAIL;

    constructor(public payload?: any) { }
}

export class LogoutAction implements Action {
    type = ActionTypes.LOGOUT;

    constructor(public payload?: any) { }
}

export class LogoutSuccessAction implements Action {
    type = ActionTypes.LOGOUT_SUCCESS;

    constructor(public payload?: any) { }
}

export class LogoutFailAction implements Action {
    type = ActionTypes.LOGOUT_FAIL;

    constructor(public payload?: any) { }
}


export type Actions =
    LoginAction |
    LoginSuccessAction |
    LoginFailAction |
    LogoutAction |
    LogoutSuccessAction |
    LogoutFailAction;
