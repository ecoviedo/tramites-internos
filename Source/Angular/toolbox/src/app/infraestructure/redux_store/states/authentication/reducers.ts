import { UserProfileDTO } from 'src/app/domain/model/dto/userProfileDTO';
import {Actions, ActionTypes} from './actions';
import {tassign} from 'tassign';


export interface State {
    token: string;
    profile: UserProfileDTO;
    isAuthenticated: boolean;
    isLoading: boolean;
    error: string;
}

const initialState: State = {
    token: null,
    profile: null,
    isLoading: false,
    error: null,
    isAuthenticated: false
};

export function loginReducer(state = initialState, action: Actions): State {
    switch (action.type) {

        case ActionTypes.LOGIN_SUCCESS:

            return tassign(state, {
                token: action.payload.token,
                profile: action.payload.credentials,
                isLoading: false,
                error: null,
                isAuthenticated: true
            });

        case ActionTypes.LOGIN_FAIL:
            return tassign(state, {
                token: null,
                profile: null,
                error: action.payload.error,
                isLoading: false
            });

        case ActionTypes.LOGOUT:
            return tassign(state, {
                token: null,
                profile: null,
                error: null,
                isLoading: false,
                isAuthenticated: false
            });

        default:
          return tassign(state, {
                token: null,
                profile: null,
                error: null,
                isLoading: false,
                isAuthenticated: true
            });;
    }
}



