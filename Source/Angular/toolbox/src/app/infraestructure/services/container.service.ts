import { Injectable } from "@angular/core";
import { ApiManagerService } from "./api/api-manager";
import { ProcessRequestDTO } from "src/app/domain/model/dto/ProcessRequestDTO";
import { ProcessResponseDTO } from "src/app/domain/model/dto/ProcessResponseDTO";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";

@Injectable()
export class ContainerService {

    constructor(
        private _api: ApiManagerService
    ) {}

    consultarContenedores (request: ProcessRequestDTO) : Observable<ProcessResponseDTO> {
        const endpoint = environment.contenedor_endPoint + '/containers';
        return this._api.post(endpoint, request);
    }
}
