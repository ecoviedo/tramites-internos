import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { successResponse } from 'src/environments/environment.variables';
import { ApiManagerService } from './api/api-manager';
import { CatalogueDTO } from 'src/app/domain/model/dto/CatalogueDTO';
import { Observable } from "rxjs";
import { stringify } from 'querystring';
import { TipoTramiteDTO } from 'src/app/domain/model/dto/TipoTramiteDTO';
import { CatalogoRqDTO } from 'src/app/domain/model/dto/CatalogoRQDTO';
import { CatalogosRqDTO } from 'src/app/domain/model/dto/CatalogosRqDTO';

@Injectable()
export class TablasBasicasService {

    constructor(
        private _api: ApiManagerService
    ) { }

    consultarCatalogos (catalogos: string[]): Observable<any> {
        var catalogosRQ = this.cargarCatalogos(catalogos);
        const endpoint = `${environment.catalogos_endPoint}`;
        return this._api.post(endpoint, catalogosRQ);
    }
    
    consultarTipoTramite (padreId: number): Observable<any> {
        var endpoint = ``;
        if(!!padreId){
            endpoint = `${environment.tipoTramite_endPoint}?parentCode=` + padreId;
        }else
            endpoint = `${environment.tipoTramite_endPoint}`;

        var listaTipoTramiteDTO = new Array<TipoTramiteDTO>();
        return this._api.get(endpoint);
    }

    /**
     * Metodo privado para crear objeto con catalogos a buscar
     * @param catalogos 
     */
    private cargarCatalogos(catalogos: string[]): CatalogosRqDTO{
        var catalogosRqDTO = new CatalogosRqDTO;
        var listaCatalogos = new Array<CatalogoRqDTO>();
        catalogos.forEach(element => {
            var catalogo1 = new CatalogoRqDTO;
            catalogo1.catalogo = element;
            listaCatalogos.push(catalogo1);
        });
        catalogosRqDTO.catalogs = listaCatalogos;
        return catalogosRqDTO;
    }

    /**
     * Método de prueba
     */
    consultarTipoTramiteDos (padreId: number): TipoTramiteDTO[] {
        var endpoint = ``;
        if(!!padreId){
            endpoint = `${environment.tipoTramite_endPoint}?parentCode=` + padreId;
        }else
            endpoint = `${environment.tipoTramite_endPoint}`;

        var listaTipoTramiteDTO = new Array<TipoTramiteDTO>();
        this._api.get(endpoint).subscribe(data => {
            debugger;
            if (!!data.response && data.response.statusCode == successResponse.statusCode) {
                listaTipoTramiteDTO = data.proceduresTypes;
                

                /*data.proceduresTypes.forEach(element => {
                    if (!!element){
                        debugger;
                        listaTipoTramiteDTO.push(element);
                    }
                });*/

                return listaTipoTramiteDTO;
            }
        });
        debugger;
        return listaTipoTramiteDTO;
    }

    /**
     * Método de prueba
     */
    consultarCatalogosDos (): CatalogueDTO[] {
            const endpoint = `${environment.catalogos_endPoint}`;
            var listaParametros = new Array<CatalogueDTO>();
            var request = '{"catalogs":[{"catalogo":"AreaEmpresa"},{"catalogo":"RolTramite"},{"catalogo":"TipoIdentificacion"}]}';
            this._api.post(endpoint, request).subscribe(data => {
                if (!!data.response && data.response.statusCode == successResponse.statusCode) {
                    debugger;
                    data.catalogs.forEach(element => {
                        debugger;
                        if (!!element) {
                          if (element.catalogo == 'AreaEmpresa'){
                            listaParametros = element.data;
                          }
                        }
                        return listaParametros;
                      });
                }
            });
            debugger;
            return listaParametros;
        }
}
