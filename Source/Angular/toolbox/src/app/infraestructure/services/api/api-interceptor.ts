import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { catchError, map } from "rxjs/operators";
import { MessageDTO } from "src/app/domain/model/dto/MessageDTO";
import { AlertService } from "../alertService.service";
import { failUserNotFound, failUserNotAuthorized, failDemandNotFound, ticketAlreadyExists } from "src/environments/environment.variables";

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {

    constructor (
        private alertService: AlertService
    ) { }

    intercept (request: HttpRequest<any>, next: HttpHandler): Observable <HttpEvent<any>> {
        return next.handle(request).pipe(
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    //debugger
                    if (event.body && event.body.response) {
                        switch (event.body.response.statusCode) {
                            case failUserNotFound.statusCode:
                                this.alertService.info(event.body.response);
                                break;
                            case failUserNotAuthorized.statusCode:
                                this.alertService.info(event.body.response);
                                break;
                            default:
                                break;
                        }                        
                    } 
                    if (event.body && event.body.codigo){
                        switch (event.body.codigo) {
                            case failDemandNotFound.statusCode:
                                let message = new MessageDTO();
                                message.type = failDemandNotFound.type;
                                message.message = failDemandNotFound.msg;
                                message.statusCode = failDemandNotFound.statusCode;
                                this.alertService.info(message);
                                break;
                            case ticketAlreadyExists.statusCode:
                                let msgTicket = new MessageDTO();
                                msgTicket.type = ticketAlreadyExists.type;
                                if(event.body.detalles)
                                    msgTicket.message = event.body.detalles[0];
                                else
                                    msgTicket.message = ticketAlreadyExists.msg;
                                msgTicket.statusCode = ticketAlreadyExists.statusCode;
                                this.alertService.error(msgTicket);
                                break;
                            default:
                                break;
                        }
                    }

                }
                return event;
            }),
            catchError((error: HttpErrorResponse) => {
                debugger
                this.alertService.error(error);
                return throwError(error);
            }));
    }
}
