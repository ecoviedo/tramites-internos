import { Injectable } from '@angular/core';
import {ApiManagerService} from './api/api-manager';
import {BusinessResponseDTO} from '../../domain/model/dto/BusinessResponseDTO';
import {EmpleadoDTO} from '../../domain/model/dto/EmpleadoDTO';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EmpleadoListService {

  constructor(private _api: ApiManagerService) { }

    listarEmpleados (request: EmpleadoDTO[]) : Observable<any> {
        const endpoint = environment.consultarEmpleado + '/';
        return this._api.get(endpoint);
    }
}
