import { Injectable } from '@angular/core';
import {TramiteDTO} from '../../domain/model/dto/TramiteDTO';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {ApiManagerService} from './api/api-manager';
import {JefeInmediatoDTO} from '../../domain/model/dto/JefeInmediatoDTO';

@Injectable({
  providedIn: 'root'
})
export class JefeInmediatoService {

  constructor(private _api: ApiManagerService) { }

    jefeInmediato (request: JefeInmediatoDTO) : Observable<any> {
        const endpoint = environment.jefeImediato + '/aprobjefeinmediato/';
        return this._api.post(endpoint, request);
    }
}
