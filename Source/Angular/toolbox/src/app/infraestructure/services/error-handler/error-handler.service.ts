import {Injectable} from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {switchMap, take} from 'rxjs/internal/operators';
import {LogoutAction} from '../../redux_store/states/authentication/actions';
import {select, Store} from '@ngrx/store';
import {State} from '../../redux_store/reducers';
import {PushNotificationAction} from '../../redux_store/states/notification/actions';
import {BAD_AUTHENTICATION} from '../../config/constantes';



@Injectable()
export  class  ErrorHandlerService {
    private token$: Observable<string>;

    constructor(private _store: Store<State>) {
        this.token$ = _store.pipe(select(s => s.auth.token));
    }

    handleAuthExpiredError(error): Observable<any> {
        return this.token$.pipe(take(1), switchMap(token => {
            if (token !== null) {
                this._store.dispatch(new LogoutAction());
            } else {
                this._store.dispatch(new PushNotificationAction({
                    severity: 'info',
                    summary: BAD_AUTHENTICATION
                }));
            }
            return throwError(error);
        }));
    }

    handleError(error) {
        this._store.dispatch(new PushNotificationAction({
            summary: error.status
        }));
        return throwError(error);
    }
}
