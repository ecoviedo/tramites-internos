import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { successResponse } from 'src/environments/environment.variables';
import { ApiManagerService } from './api/api-manager';
import { CatalogueDTO } from 'src/app/domain/model/dto/CatalogueDTO';
import { Observable } from "rxjs";
import { stringify } from 'querystring';
import { TipoTramiteDTO } from 'src/app/domain/model/dto/TipoTramiteDTO';
import { CatalogoRqDTO } from 'src/app/domain/model/dto/CatalogoRQDTO';
import { CatalogosRqDTO } from 'src/app/domain/model/dto/CatalogosRqDTO';
import { RqRadicarTramiteDTO } from 'src/app/domain/model/dto/RqRadicarTramiteDTO';

@Injectable()
export class TramiteService {

    constructor(
        private _api: ApiManagerService
    ) { }

    radicarTramite (request: RqRadicarTramiteDTO) : Observable<any> {
        const endpoint = environment.radicarSolicitud;
        return this._api.post(endpoint, request);
    }

    consultarTramite (idInstancia: string) : Observable<any> {
        const endpoint = environment.radicarSolicitud + '/' + idInstancia;
        return this._api.get(endpoint);
    }

    consultarTipoDocTramite (): Observable<any> {
        const endpoint = `${environment.tipoDocTramite_endPoint}`;
        return this._api.get(endpoint);
    }

    consultarGestorTramite (): Observable<any> {
        const endpoint = `${environment.gestorTramite_endPoint}`;
        return this._api.get(endpoint);
    }

    /**
     * Servicios que consulta la fecha de reintegro del empleado y los días hábiles que se encuentran entre las dos fechas
     * @param fechaIni fecha inicial formato dd/mm/aaaa
     * @param fechaFin fecha final formato dd/mm/aaaa
     */
    consultarFechaReintegro (fechaIni: string, fechaFin: string): Observable<any> {
        const endpoint = `${environment.fechaReintegro_endPoint}?dateIni=` + fechaIni + `&dateFin=` + fechaFin;
        return this._api.get(endpoint);
    }

    /**
     * Servicios que consulta la fecha final del permiso solicitado por el empleado dado una fecha inicio y los días hábiles a solicitar
     * @param fechaIni fecha inicial formato dd/mm/aaaa
     * @param dias días hábiles a solicitar
     */
    consultarFechaFin (fechaIni: string, dias: number): Observable<any> {
        const endpoint = `${environment.fechaFin_endPoint}?date=` + fechaIni + `&days=` + dias;
        return this._api.get(endpoint);
    }
}
