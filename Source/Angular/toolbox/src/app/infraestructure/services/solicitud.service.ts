import { Injectable } from "@angular/core";
import { ApiManagerService } from "./api/api-manager";
import { TramiteDTO } from "src/app/domain/model/dto/TramiteDTO";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import {EmpleadoDTO} from '../../domain/model/dto/EmpleadoDTO';
import {ResponseDTO} from '../../domain/model/dto/ResponseDTO';
import {BusinessResponseDTO} from '../../domain/model/dto/BusinessResponseDTO';

@Injectable()
export class SolicitudService {

    constructor(
        private _api: ApiManagerService
    ) {}

    radicarSolicitud (request: TramiteDTO) : Observable<any> {
        const endpoint = environment.radicarSolicitud + '/solicitud';
        return this._api.post(endpoint, request);
    }

    consultarEmpleado (request: EmpleadoDTO) : Observable<any> {
        const endpoint = environment.consultarEmpleado + '/numeroDocumento/123456789';
        return this._api.get(endpoint);
    }

}
