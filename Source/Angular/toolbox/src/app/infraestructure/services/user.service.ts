import { Injectable } from "@angular/core";
import { ApiManagerService } from "./api/api-manager";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";


@Injectable()
export class UserService {

    constructor(
        private _api: ApiManagerService
    ) {}

    consultarGrupos () : Observable<any> {
        const endpoint = environment.user_endPoint + '/groups';
        return this._api.post(endpoint);
    }

    consultarUsuarios () : Observable<any> {
        const endpoint = environment.user_endPoint + '/users';
        return this._api.post(endpoint);
    }

    crearUsuario (user: any) : Observable<any> {
        const endpoint = environment.user_endPoint + '/user';
        return this._api.put(endpoint, user);
    }

    editarUsuario (user: any) : Observable<any> {
        const endpoint = environment.user_endPoint + '/modifyUser';
        return this._api.post(endpoint, user);
    }

    eliminarUsuario (user: any) : Observable<any> {
        const endpoint = environment.user_endPoint + '/user';
        return this._api.delete(endpoint, user);
    }

    consultarUsuarioCargaGrupo (user: any) : Observable<any> {
        const endpoint = environment.user_endPoint + '/lawyerLessBurden';
        return this._api.post(endpoint, user);
    }
}