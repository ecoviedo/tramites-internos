import { Injectable } from "@angular/core";
import { ApiManagerService } from "./api/api-manager";
import { ProcessRequestDTO } from "src/app/domain/model/dto/ProcessRequestDTO";
import { ProcessResponseDTO } from "src/app/domain/model/dto/ProcessResponseDTO";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";

@Injectable()
export class TaskService {

    constructor(
        private _api: ApiManagerService
    ) {}

    consultarTareasPorInstancia (request: ProcessRequestDTO) : Observable<ProcessResponseDTO> {
        const endpoint = environment.tarea_endPoint + '/tasksByProcessInstance';
        return this._api.post(endpoint, request);
    }

    consultarTareasPorGrupos (request: ProcessRequestDTO) : Observable<ProcessResponseDTO> {
        const endpoint = environment.tarea_endPoint + '/tasksByGroups';
        return this._api.post(endpoint, request);
    }

    consultarTareasPorPropietario (request: ProcessRequestDTO) : Observable<ProcessResponseDTO> {
        const endpoint = environment.tarea_endPoint + '/tasksByOwner';
        return this._api.post(endpoint, request);
    }

    cambiarEstadoTarea (request: ProcessRequestDTO) : Observable<ProcessResponseDTO> {
        const endpoint = environment.tarea_endPoint + '/taskStatus';
        return this._api.put(endpoint, request);
    }

    reasignarTarea (request: ProcessRequestDTO) : Observable<ProcessResponseDTO> {
        const endpoint = environment.tarea_endPoint + '/reassignTask';
        return this._api.put(endpoint, request);
    }
}