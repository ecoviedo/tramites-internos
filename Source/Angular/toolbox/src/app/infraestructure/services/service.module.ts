import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {
    EventService,
    LoadingService,
    ApiBase,
    BreadcrumbService,
    ErrorHandlerService,
    ConfirmationService,
    UtilitiesFnService,
    LoginService,
    ApiManagerService,
    HttpHandlerService,
    UserService,
    TablasBasicasService,
    TramiteService,
    ServiciosExternosService,
    TaskService,
    ContainerService,
    ProcessService,
    AlertService,
    SolicitudService
} from './service.index';
import { HttpConfigInterceptor } from './api/api-interceptor';


@NgModule({
    imports: [
        CommonModule,
        HttpClientModule
    ],
    providers: [
        {provide: HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi: true },
        EventService,
        LoadingService,
        ApiBase,
        BreadcrumbService,
        ErrorHandlerService,
        ConfirmationService,
        UtilitiesFnService,
        LoginService,
        ApiManagerService,
        HttpHandlerService,
        UserService,
        TablasBasicasService,
        TramiteService,
        ServiciosExternosService,
        TaskService,
        ContainerService,
        ProcessService,
        AlertService,
        SolicitudService
    ],
    declarations: []
})
export class ServiceModule {
}
