import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { UserDTO } from '../../domain/model/dto/UserDTO';
import { ApiManagerService } from './api/api-manager';
import { HttpHeaders } from '@angular/common/http';
import { first } from 'rxjs/operators';
import { successResponse } from 'src/environments/environment.variables';

@Injectable()
export class LoginService {

    public currentUserSubject: BehaviorSubject<UserDTO>;

    public currentUser: Observable<UserDTO>;

    private filingUserSubject: BehaviorSubject<UserDTO>;

    public filingUser: Observable<UserDTO>;

    constructor(
        private _api: ApiManagerService
    ) {
        this.currentUserSubject = new BehaviorSubject<UserDTO>(JSON.parse(localStorage.getItem('currentUser')));

        this.currentUser = this.currentUserSubject.asObservable();

        this.filingUserSubject = new BehaviorSubject<UserDTO>(JSON.parse(localStorage.getItem('filingUser')));

        this.filingUser = this.filingUserSubject.asObservable();
    }

    public get currentUserValue(): UserDTO {
        return this.currentUserSubject.value;
    }

    public get filingUserValue(): UserDTO {
        return this.filingUserSubject.value;
    }

    login(userDto: UserDTO) {
        /*const usuario = new UserDTO();
        usuario.user = 'Empleado01'; //wbadmin
        usuario.password = 'Colombia2020'; //wbadmin
        usuario.role = "Empleado";
        localStorage.setItem('currentUser', JSON.stringify(usuario));
        this.currentUserSubject.next(usuario);
        return usuario;*/

        let authorizationData = 'Basic ' + btoa(userDto.user + ':' + userDto.password);
        let headerOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': authorizationData
            })
        };

        debugger;
        const endpoint = environment.user_endPoint + '/authentication';
        return this._api.get(endpoint, null, headerOptions).pipe(first())
            .subscribe(data => {
                debugger;
                if (!!data.response && data.response.statusCode == successResponse.statusCode) {
                    const usuario = new UserDTO();
                    usuario.user = data.usuario;
                    usuario.password = userDto.password;
                    usuario.name = 'Prueba';
                    usuario.lastName = 'Prueba'
                    localStorage.setItem('currentUser', JSON.stringify(usuario));
                    this.currentUserSubject.next(usuario);
                    return usuario;
                }
            });
    }

    loginDos(options: {}): Observable<any> {
        const endpoint = environment.user_endPoint + '/authentication';
        return this._api.get(endpoint, null, options);
    }

    consultarRol(usuario: string){ // 
        const endpoint = environment.rolTramite_endPoint + '?usuario=' + usuario;
        return this._api.get(endpoint);
    }

    logout() {
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }

    setFilingUser(userDto: UserDTO) {
        if (userDto) {
            localStorage.setItem('filingUser', JSON.stringify(userDto));
            this.filingUserSubject.next(userDto);
        }
    }

    outFilingUser() {
        localStorage.removeItem('filingUser');
        this.filingUserSubject.next(null);
    }

}
