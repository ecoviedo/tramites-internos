import { Injectable } from '@angular/core';
import {ApiManagerService} from './api/api-manager';
import {JefeInmediatoDTO} from '../../domain/model/dto/JefeInmediatoDTO';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GerenteService {

  constructor(private _api: ApiManagerService) { }
    opGerente (request: JefeInmediatoDTO) : Observable<any> {
        const endpoint = environment.gerente + '/aprobgerente/';
        return this._api.post(endpoint, request);
    }
}
