import { Injectable } from "@angular/core";
import { ApiManagerService } from "./api/api-manager";
import { ProcessRequestDTO } from "src/app/domain/model/dto/ProcessRequestDTO";
import { ProcessResponseDTO } from "src/app/domain/model/dto/ProcessResponseDTO";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";

@Injectable()
export class ProcessService {
    
    constructor(
        private _api: ApiManagerService
    ) {}

    consultarProcesos (request: ProcessRequestDTO) : Observable<ProcessResponseDTO> {
        const endpoint = environment.proceso_endPoint + '/processesByContainer';
        return this._api.post(endpoint, request);
    }

    iniciarProceso (request: ProcessRequestDTO) : Observable<ProcessResponseDTO> {
        const endpoint = environment.proceso_endPoint + '/startProcessInstance';
        return this._api.post(endpoint, request);
    }
}