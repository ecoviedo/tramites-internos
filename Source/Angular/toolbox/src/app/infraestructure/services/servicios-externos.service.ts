import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { successResponse } from 'src/environments/environment.variables';
import { ApiManagerService } from './api/api-manager';
import { CatalogueDTO } from 'src/app/domain/model/dto/CatalogueDTO';
import { Observable } from "rxjs";
import { stringify } from 'querystring';
import { TipoTramiteDTO } from 'src/app/domain/model/dto/TipoTramiteDTO';
import { CatalogoRqDTO } from 'src/app/domain/model/dto/CatalogoRQDTO';
import { CatalogosRqDTO } from 'src/app/domain/model/dto/CatalogosRqDTO';

@Injectable()
export class ServiciosExternosService {

    constructor(
        private _api: ApiManagerService
    ) { }

    consultarDiasDisponiblesEmpleado (idTipoIdentificacion:string, identificacion:string): Observable<any> {
        const endpoint = `${environment.diasDisponibles_endPoint}/` + idTipoIdentificacion + `/` + identificacion;
        return this._api.get(endpoint);
    }
}
