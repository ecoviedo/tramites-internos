import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {ApiManagerService} from './api/api-manager';
import {RecursosHumanosDTO} from '../../domain/model/dto/RecursosHumanosDTO';

@Injectable({
  providedIn: 'root'
})
export class RecursosHumanosService {

  constructor(private _api: ApiManagerService) { }

    recursosHumanos(request: RecursosHumanosDTO) : Observable<any> {
        const endpoint = environment.recursosHumanos + '/aprobrrhh/';
        return this._api.post(endpoint, request);
    }
}
