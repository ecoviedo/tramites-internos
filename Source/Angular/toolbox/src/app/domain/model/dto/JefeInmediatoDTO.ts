import {TablaBasicaDTO} from './TablaBasicaDTO';
import {EmpleadoDTO} from './EmpleadoDTO';

export class JefeInmediatoDTO {
    numeroRadicado: string;
    empleadoRemplazo: any = EmpleadoDTO;
    reemplazo:boolean;
    remunerado:boolean;
    vistoBueno:string;
    observaciones: string;
    fechaAprobacion:string;
}
