export class VacacionesDTO {
    fechaInicio?: string;
    fechaFin?: string;
    diasDisponibles?: number;
    diasAnticipados?: number;
    diasSolicitados?: number
}
