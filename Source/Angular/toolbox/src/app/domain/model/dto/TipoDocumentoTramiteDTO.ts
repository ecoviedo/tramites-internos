import { TipoTramiteDTO } from "./TipoTramiteDTO";

export class TipoDocumentoTramite {
    id: number;
    nombre?: string;
    obligatorio: boolean;
    descripcion: string;
    idTipoTramite: TipoTramiteDTO;
    idSubtipoTramite: TipoTramiteDTO;
}
