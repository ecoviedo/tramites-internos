import { RolDTO } from "./RolDTO";

export class GestionTramiteDTO {
    id: number;
    idTramite: number;
    fecha: string;
    revisadoPor: string;
    observaciones: string;
    respuestaTramite: string;
    permisoRemunerado: string;
    requiereReemplazo: string;
    nombreReemplazo: string;
    idIncapacidad: number;
    idRol: RolDTO;
}