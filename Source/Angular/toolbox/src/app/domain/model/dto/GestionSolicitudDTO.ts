export class GestionSolicitudDTO {
    idGestionSolicitud?: number;
    aprobJefe?: boolean;
    aprobGerente?: boolean;
    aprobRrhh?: boolean;
    fechaAprobJefe?: string;
    fechaAprobGerente?: string;
    fechaAprobRrhh?: string;
}
