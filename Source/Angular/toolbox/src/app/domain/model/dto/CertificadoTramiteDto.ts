import { TipoTramiteDTO } from "./TipoTramiteDTO";

export class CertificadoTramiteDto {
    id: number;
    idTramite: string;
    idSubtipoTramite: TipoTramiteDTO;
    nombreArchivo: string;
    tipoArchivo: string;
    contenidoArchivo: string;
    ubicacionArchivo: string;
    observaciones: string;
}
