import { AreaDTO } from "./AreaDTO";
import { TipoIdentificacionDTO } from "./TipoIdentificacionDTO";
import { CatalogueDTO } from "./CatalogueDTO";

export class GestorTramiteDTO {
    id: number;
    identificacion: string;
    usuario: string
    nombre: string;
    apellido: string
    idArea: AreaDTO;
    idTipoIdentificacion: TipoIdentificacionDTO;
    idRol: CatalogueDTO;
}
