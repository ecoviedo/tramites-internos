export class VacacionesTramiteDto {
    id: string;
    idTramite: string;
    diasDisponibles: number;
    solicitaDiasAnticipados: boolean;
    totalDiasSolicitados: number;
    totalDiasAnticipados: number;
    fechaInicio: string;
    fechaFin: string;
    fechaReintegro: string;
    observaciones: string;
}
