export class MessageDTO {
    type: string;
    message: string;
    statusCode: string;
}