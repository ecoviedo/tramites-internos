export class InformacionVacacionesDTO {
    idInformacionVacaciones: number;
    diasDisponibles: number;
    diasSolicitados: number;
    fechaIngreso: string;
}
