export class TipoTramiteDTO {
    id: number;
    nombre?: string;
    descripcion?: string;
    padreId?: number;
}
