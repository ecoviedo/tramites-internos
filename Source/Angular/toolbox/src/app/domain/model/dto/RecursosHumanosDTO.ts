export class RecursosHumanosDTO{
    numeroRadicado?: string;
    aprobRrhh?: boolean;
    clasificacion?: string;
    fechaAprob?: string;
    observaciones?: string;
}
