export class RolDTO {
    id: number;
    nombre: string;
    descripcion: string;
    nombreBpm: string;
}