export class DocumentDTO {
    nombreDocumento: string;
    tipoDocumental: string;
    nombreArchivo: string;
    documento: string;
    fechaCreacion?: Date;
    tamano?: string;
    tipoArchivo?: string;
}