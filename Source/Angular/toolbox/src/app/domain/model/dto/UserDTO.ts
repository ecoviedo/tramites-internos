export class UserDTO {
    user?: string;
    identType?: string;
    identNumber?: string;
    name?: string;
    lastName?: string;
    fullName?: string;
    email?: string;
    state?: string;
    roleId?: number;
    roleName?: string;
    newRole?: string;
    password?: string;
    targetUser?: string;
}
