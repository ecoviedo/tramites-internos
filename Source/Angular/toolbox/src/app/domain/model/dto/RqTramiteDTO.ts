export class RqTramiteDTO {
    idInstancia?: string;
    identificacion?: string;
    fecha?: string;
    estado?: string;
    nombreEmpleado?: string;
    apellidoEmpleado?: string;
    respuestaTramite?: string;
}