import { TipoTramiteDTO } from "./TipoTramiteDTO";

export class DocumentTypeDTO {
    id?: string;
    idTipoTramite?: TipoTramiteDTO;
    idSubtipoTramite?: TipoTramiteDTO;
    nombre?: string;
    obligatorio?: boolean;
    descripcion?: string;
}