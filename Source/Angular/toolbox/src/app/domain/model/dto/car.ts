export interface Car {
    date?;
    reviewedBy?;
    role?;
    observations?;
    managementResult?;
}