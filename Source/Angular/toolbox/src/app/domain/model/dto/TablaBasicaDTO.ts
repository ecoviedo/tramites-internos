export class TablaBasicaDTO {
    idTablaBasica?: string;
    nombre?: string;
    descripcion?: string;
    codigoApp?: string;
    tipoTablaBasica?: any;
}