export class ProcessManagersDTO {
    id?: string;
    idTipoIdentificacion?: string;
    identificacion?: string;
    usuario?: string;
    nombre?: string;
    apellido?: string;
    idArea?: string;
    idRol?: number;
}
