import { CertificadoTramiteDto } from './CertificadoTramiteDto';
import { VacacionesTramiteDto } from './VacacionesTramiteDto';
import { DetalleTramiteDto } from './DetalleTramiteDto';
import { RqTramiteDTO } from './RqTramiteDTO';
import { GestionTramiteDTO } from './GestionTramiteDTO';

export class RqParametrosRadicarTramiteDTO {
    Area: string;
    DiasSolicitados?: number;
    Email?: string;
    Rol: string;
    Tramite: string;
    Usuario: string;
    Fecha: string;
    idTramite: number;
}