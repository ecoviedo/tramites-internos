import { CertificadoTramiteDto } from './CertificadoTramiteDto';
import { VacacionesTramiteDto } from './VacacionesTramiteDto';
import { DetalleTramiteDto } from './DetalleTramiteDto';
import { AreaDTO } from './AreaDTO';
import { TipoIdentificacionDTO } from './TipoIdentificacionDTO';
import { TipoTramiteDTO } from './TipoTramiteDTO';
import { GestionTramiteDTO } from './GestionTramiteDTO';
import { GestorTramiteDTO } from './GestorTramiteDTO';

export class TramiteDTO {
    id?: number;
    idInstancia?: number;
    idTipoIdentificacion?: TipoIdentificacionDTO;
    identificacion?: string;
    fecha?: string;
    idTipoTramite?: TipoTramiteDTO;
    estado?: string;
    nombreEmpleado?: string;
    apellidoEmpleado?: string;
    idArea?: AreaDTO;
    idGestorTramite?: GestorTramiteDTO;
    respuestaTramite?: string;
    detalleTramite?: DetalleTramiteDto;
    vacacionesTramite?: VacacionesTramiteDto;
    certificadoTramiteList?: CertificadoTramiteDto[];
    gestionTramiteList?: GestionTramiteDTO[];
}
