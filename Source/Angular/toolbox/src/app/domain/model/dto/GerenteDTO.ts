export class GerenteDTO {
    numeroRadicado: string;
    empleadoRemplazo: string;
    reemplazo:boolean;
    remunerado:boolean;
    vistoBueno:string;
    observaciones: string;
    fechaAprobacion:string;
}
