import { UserDTO } from "./UserDTO";
import { ParametrosDTO } from "./parametrosDTO";

export class ProcessRequestDTO {
	containerId?: string;
	processesId?: string;
	processInstance?: string;
	taskId?: number;
	taskStatus?: string;
	groups?: string[];
	ownerUser?: UserDTO;
	assignment?: UserDTO;
	parametros?: ParametrosDTO;
	signal?: string;
	page?: number;
	pageSize?: number;
	sort?: number;
}
