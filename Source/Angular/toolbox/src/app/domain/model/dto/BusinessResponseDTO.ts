export class BusinessResponseDTO<T> {
    body?: T;
    status: string;
    timeResponse: string;
    message: string;
    path: string;
    transactionState: string;

}
