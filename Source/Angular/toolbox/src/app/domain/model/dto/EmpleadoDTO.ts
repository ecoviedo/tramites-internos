import { InformacionVacacionesDTO } from './InformacionVacacionesDTO';

export class EmpleadoDTO {
    idEmpleado?: number;
    numeroDocumento?: string;
    tipoDocumento?: string;
    primerNombre?: string;
    segundoNombre?: string;
    primerApellido?: string;
    segundoApellido?: string;
    genero?: string;
    correoElectronico?: string;
    telefono?: string;
    imagen?: string;
    fechaIngreso?: string;
    estado?: boolean;
    fechaCreacion?: string;
    fechaModificacion?: string;
    jefeInmediato?: EmpleadoDTO;
    informacionVacacionesDto?: InformacionVacacionesDTO;
    cargo?: string;
    estadoEmpleado?: string;
    tipoContrato?: string;
}
