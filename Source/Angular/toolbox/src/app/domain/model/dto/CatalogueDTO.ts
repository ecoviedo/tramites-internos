export class CatalogueDTO {
    id: string;
    nombre: string;
    descripcion?: string;
    nombreBpm?: string;
}
