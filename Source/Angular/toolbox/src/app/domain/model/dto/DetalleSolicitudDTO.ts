
export class DetalleSolicitudDTO {
    diaSolicitado?: string;
    horaInicio?: string;
    horaFin?: string;
    descripcionMotivo?: string;
    diasSolicitados?: number;
    fechaInicio?: string;
    fechaFin?: string;
    motivoSolicitud: string
}
