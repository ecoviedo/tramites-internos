export class TipoIdentificacionDTO {
    idTipoIdentificacion: number;
    nombreTipoIdentificacion?: string;
    descripcionTipoIdentificacion?: string;
}
