import { CertificadoTramiteDto } from './CertificadoTramiteDto';
import { VacacionesTramiteDto } from './VacacionesTramiteDto';
import { DetalleTramiteDto } from './DetalleTramiteDto';
import { RqTramiteDTO } from './RqTramiteDTO';
import { GestionTramiteDTO } from './GestionTramiteDTO';

export class RqRadicarTramiteDTO {
    id?: number;
    idArea?: number;
    idGestorTramite?: number;
    idTipoIdentificacion?: number;
    idTipoTramite?: number;
    tramite?: RqTramiteDTO;
    gestionTramite?: GestionTramiteDTO[];
    vacacionesTramite?: VacacionesTramiteDto;
    certificadoTramite?: CertificadoTramiteDto[];
    detalleTramite?: DetalleTramiteDto;
}