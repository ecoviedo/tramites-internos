export class SolicitudVacacionesDTO {
    fechaInicio: string;
    fechaFin: string;
    diasDisponibles: number;
    diasAnticipados: number;
    diasSolicitados: number;
    fechaReintegro: string;
}
