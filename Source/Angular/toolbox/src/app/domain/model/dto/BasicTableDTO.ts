export class BasicTableDTO {
    idTablaBasica?: string;
    nombre?: string;
    descripcion?: string;
    codigoApp?: string;
    tipoTablaBasica?: string;
}
