import { TipoDocumentoTramite } from "./TipoDocumentoTramiteDTO";
import { TipoTramiteDTO } from "./TipoTramiteDTO";

export class DetalleTramiteDto {
    id: number;
    idTramite?: string;
    diasSolicitados: number;
    fechaDiaSolicitado: string;
    horaInicio: string;
    horaFin: string;
    fechaInicio: string;
    fechaFin: string;
    idSubtipoTramite: TipoTramiteDTO;
    otroMotivo: string;
    observaciones: string;
    idTipoDocumentoTramite: TipoDocumentoTramite;
    nombreArchivo: string;
    tipoArchivo: string;
    contenidoArchivo: string;
    ubicacionArchivo: string;
}
