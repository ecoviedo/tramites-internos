import { TramiteDTO } from "../dto/TramiteDTO";

export const MOCK_TRAMITE_CONSULTADO_CERTIFICACION: TramiteDTO =
{
  id: 45,
  idInstancia: 177,
  identificacion: '1010142720',
  fecha: '2020-04-21',
  estado: 'creada',
  nombreEmpleado: 'Cristian Alberto',
  apellidoEmpleado: 'Quintero Gaitans',
  respuestaTramite: '',
  idArea: {
    id: 3,
    nombre: 'Gerencia de operaciones',
    descripcion: '',
    nombreBpm: 'Gerencia de operaciones'
  },
  idGestorTramite: {
    id: 9,
    identificacion: "80101089",
    usuario: "JefeInmediato02",
    nombre: "JefeInmediato",
    apellido: "02",
    idArea: null,
    idTipoIdentificacion: null,
    idRol: null
  },
  idTipoIdentificacion: {
    idTipoIdentificacion: 1,
    nombreTipoIdentificacion: 'CC',
    descripcionTipoIdentificacion: 'Cédula de Ciudadanía'
  },
  idTipoTramite: {
    id: 10,
    nombre: 'Certificación',
    descripcion: 'Certificación',
    padreId: null
  },
  vacacionesTramite: {
    id: '2',
    diasDisponibles: 10,
    solicitaDiasAnticipados: true,
    totalDiasSolicitados: 12,
    totalDiasAnticipados: 2,
    fechaInicio: '2020-04-20',
    fechaFin: '2020-05-06',
    fechaReintegro: '2020-05-07',
    observaciones: 'Pedido de días disponibles',
    idTramite: null
  },
  certificadoTramiteList: [
    {
      id: 2,
      nombreArchivo: 'example',
      tipoArchivo: 'example',
      contenidoArchivo: null,
      ubicacionArchivo: 'example',
      observaciones: 'example',
      idSubtipoTramite: {
        id: 16,
        nombre: 'Certificado laboral estándar',
        descripcion: 'Certificado laboral estándar',
        padreId: 10
      },
      idTramite: null
    },
    {
      id: 3,
      nombreArchivo: 'example1',
      tipoArchivo: 'example1',
      contenidoArchivo: null,
      ubicacionArchivo: 'example1',
      observaciones: 'example1',
      idSubtipoTramite: {
        id: 17,
        nombre: 'Certificado laboral con funciones',
        descripcion: 'Certificado laboral con funciones',
        padreId: 10
      },
      idTramite: null
    },
    {
      id: 4,
      nombreArchivo: 'example2',
      tipoArchivo: 'example2',
      contenidoArchivo: null,
      ubicacionArchivo: 'example2',
      observaciones: 'example2',
      idSubtipoTramite: {
        id: 18,
        nombre: 'Certificado ingresos y retenciones',
        descripcion: 'Certificado ingresos y retenciones',
        padreId: 10
      },
      idTramite: null
    },
    {
      id: 5,
      nombreArchivo: 'example3',
      tipoArchivo: 'example3',
      contenidoArchivo: null,
      ubicacionArchivo: 'example3',
      observaciones: 'example3',
      idSubtipoTramite: {
        id: 19,
        nombre: 'Certificado de cobertura ARL en el exterior',
        descripcion: 'Certificado de cobertura ARL en el exterior',
        padreId: 10
      },
      idTramite: null
    },
    {
      id: 6,
      nombreArchivo: 'example4',
      tipoArchivo: 'example4',
      contenidoArchivo: null,
      ubicacionArchivo: 'example4',
      observaciones: 'example4',
      idSubtipoTramite: {
        id: 20,
        nombre: 'Certificado medicina prepagada',
        descripcion: 'Certificado medicina prepagada',
        padreId: 10
      },
      idTramite: null
    }
  ],
  detalleTramite: {
    id: 35,
    diasSolicitados: 5,
    fechaDiaSolicitado: '2020-04-21',
    horaInicio: '10:00:00',
    horaFin: '15:00:00',
    fechaInicio: '07-04-2020',
    fechaFin: '01-03-2020',
    otroMotivo: 'Consulta de exámenes',
    observaciones: 'Las actividades se encuentran al día',
    nombreArchivo: 'Incapacidad.pdf',
    tipoArchivo: 'application/pdf',
    contenidoArchivo: null,
    ubicacionArchivo: 'string',
    idTipoDocumentoTramite: {
      id: 2,
      nombre: 'Cita médica',
      obligatorio: false,
      descripcion: '',
      idTipoTramite: {
        id: 2,
        nombre: 'Permisos',
        descripcion: 'Permisos',
        padreId: null
      },
      idSubtipoTramite: null
    },
    idSubtipoTramite: null,
    idTramite: null
  },
  gestionTramiteList: [
    {
      id: 19,
      fecha: '2020-04-21',
      revisadoPor: 'cristian Quintero',
      observaciones: 'ninguna',
      respuestaTramite: 'Radicar Tramite',
      permisoRemunerado: 'si',
      requiereReemplazo: 'no',
      nombreReemplazo: 'na',
      idIncapacidad: null,
      idTramite: null,
      idRol: {
        id: 1,
        nombre: 'Empleado',
        descripcion: '',
        nombreBpm: 'Empleado'
      }
    }
  ]
};

export const MOCK_TRAMITE_CONSULTADO_LICENCIA: TramiteDTO =
{
  id: 45,
  idInstancia: 177,
  identificacion: '1010142720',
  fecha: '2020-04-21',
  estado: 'creada',
  nombreEmpleado: 'Cristian Alberto',
  apellidoEmpleado: 'Quintero Gaitans',
  respuestaTramite: '',
  idArea: {
    id: 3,
    nombre: 'Gerencia de operaciones',
    descripcion: '',
    nombreBpm: 'Gerencia de operaciones'
  },
  idGestorTramite: {
    id: 9,
    identificacion: "80101089",
    usuario: "JefeInmediato02",
    nombre: "JefeInmediato",
    apellido: "02",
    idArea: null,
    idTipoIdentificacion: null,
    idRol: null
  },
  idTipoIdentificacion: {
    idTipoIdentificacion: 1,
    nombreTipoIdentificacion: 'CC',
    descripcionTipoIdentificacion: 'Cédula de Ciudadanía'
  },
  idTipoTramite: {
    id: 7,
    nombre: 'Licencia por luto',
    descripcion: 'Licencia por luto',
    padreId: null
  },
  vacacionesTramite: {
    id: '2',
    diasDisponibles: 10,
    solicitaDiasAnticipados: true,
    totalDiasSolicitados: 12,
    totalDiasAnticipados: 2,
    fechaInicio: '2020-04-20',
    fechaFin: '2020-05-06',
    fechaReintegro: '2020-05-07',
    observaciones: 'Pedido de días disponibles',
    idTramite: null
  },
  certificadoTramiteList: [
    {
      id: 2,
      nombreArchivo: 'example',
      tipoArchivo: 'example',
      contenidoArchivo: null,
      ubicacionArchivo: 'example',
      observaciones: 'example',
      idSubtipoTramite: {
        id: 16,
        nombre: 'Certificado laboral estándar',
        descripcion: 'Certificado laboral estándar',
        padreId: 10
      },
      idTramite: null
    },
    {
      id: 3,
      nombreArchivo: 'example1',
      tipoArchivo: 'example1',
      contenidoArchivo: null,
      ubicacionArchivo: 'example1',
      observaciones: 'example1',
      idSubtipoTramite: {
        id: 17,
        nombre: 'Certificado laboral con funciones',
        descripcion: 'Certificado laboral con funciones',
        padreId: 10
      },
      idTramite: null
    },
    {
      id: 4,
      nombreArchivo: 'example2',
      tipoArchivo: 'example2',
      contenidoArchivo: null,
      ubicacionArchivo: 'example2',
      observaciones: 'example2',
      idSubtipoTramite: {
        id: 18,
        nombre: 'Certificado ingresos y retenciones',
        descripcion: 'Certificado ingresos y retenciones',
        padreId: 10
      },
      idTramite: null
    },
    {
      id: 5,
      nombreArchivo: 'example3',
      tipoArchivo: 'example3',
      contenidoArchivo: null,
      ubicacionArchivo: 'example3',
      observaciones: 'example3',
      idSubtipoTramite: {
        id: 19,
        nombre: 'Certificado de cobertura ARL en el exterior',
        descripcion: 'Certificado de cobertura ARL en el exterior',
        padreId: 10
      },
      idTramite: null
    },
    {
      id: 6,
      nombreArchivo: 'example4',
      tipoArchivo: 'example4',
      contenidoArchivo: null,
      ubicacionArchivo: 'example4',
      observaciones: 'example4',
      idSubtipoTramite: {
        id: 20,
        nombre: 'Certificado medicina prepagada',
        descripcion: 'Certificado medicina prepagada',
        padreId: 10
      },
      idTramite: null
    }
  ],
  detalleTramite: {
    id: 35,
    diasSolicitados: 5,
    fechaDiaSolicitado: '2020-04-21',
    horaInicio: '10:00:00',
    horaFin: '15:00:00',
    fechaInicio: '07-04-2020',
    fechaFin: '01-03-2020',
    otroMotivo: 'Consulta de exámenes',
    observaciones: 'Las actividades se encuentran al día',
    nombreArchivo: 'Incapacidad.pdf',
    tipoArchivo: 'application/pdf',
    contenidoArchivo: null,
    ubicacionArchivo: 'string',
    idTipoDocumentoTramite: {
      id: 2,
      nombre: 'Cita médica',
      obligatorio: false,
      descripcion: '',
      idTipoTramite: {
        id: 2,
        nombre: 'Permisos',
        descripcion: 'Permisos',
        padreId: null
      },
      idSubtipoTramite: null
    },
    idSubtipoTramite: null,
    idTramite: null
  },
  gestionTramiteList: [
    {
      id: 19,
      fecha: '2020-04-21',
      revisadoPor: 'cristian Quintero',
      observaciones: 'ninguna',
      respuestaTramite: 'Radicar Tramite',
      permisoRemunerado: 'si',
      requiereReemplazo: 'no',
      nombreReemplazo: 'na',
      idIncapacidad: null,
      idTramite: null,
      idRol: {
        id: 1,
        nombre: 'Empleado',
        descripcion: '',
        nombreBpm: 'Empleado'
      }
    }
  ]
};

export const MOCK_TRAMITE_CONSULTADO_INCAPACIDAD: TramiteDTO =
{
  id: 45,
  idInstancia: 177,
  identificacion: '1010142720',
  fecha: '2020-04-21',
  estado: 'creada',
  nombreEmpleado: 'Cristian Alberto',
  apellidoEmpleado: 'Quintero Gaitans',
  respuestaTramite: '',
  idArea: {
    id: 3,
    nombre: 'Gerencia de operaciones',
    descripcion: '',
    nombreBpm: 'Gerencia de operaciones'
  },
  idGestorTramite: {
    id: 9,
    identificacion: "80101089",
    usuario: "JefeInmediato02",
    nombre: "JefeInmediato",
    apellido: "02",
    idArea: null,
    idTipoIdentificacion: null,
    idRol: null
  },
  idTipoIdentificacion: {
    idTipoIdentificacion: 1,
    nombreTipoIdentificacion: 'CC',
    descripcionTipoIdentificacion: 'Cédula de Ciudadanía'
  },
  idTipoTramite: {
    id: 3,
    nombre: 'Reporte de incapacidad',
    descripcion: 'Reporte de incapacidad',
    padreId: null
  },
  vacacionesTramite: {
    id: '2',
    diasDisponibles: 10,
    solicitaDiasAnticipados: true,
    totalDiasSolicitados: 12,
    totalDiasAnticipados: 2,
    fechaInicio: '2020-04-20',
    fechaFin: '2020-05-06',
    fechaReintegro: '2020-05-07',
    observaciones: 'Pedido de días disponibles',
    idTramite: null
  },
  certificadoTramiteList: [
    {
      id: 2,
      nombreArchivo: 'example',
      tipoArchivo: 'example',
      contenidoArchivo: null,
      ubicacionArchivo: 'example',
      observaciones: 'example',
      idSubtipoTramite: {
        id: 16,
        nombre: 'Certificado laboral estándar',
        descripcion: 'Certificado laboral estándar',
        padreId: 10
      },
      idTramite: null
    },
    {
      id: 3,
      nombreArchivo: 'example1',
      tipoArchivo: 'example1',
      contenidoArchivo: null,
      ubicacionArchivo: 'example1',
      observaciones: 'example1',
      idSubtipoTramite: {
        id: 17,
        nombre: 'Certificado laboral con funciones',
        descripcion: 'Certificado laboral con funciones',
        padreId: 10
      },
      idTramite: null
    },
    {
      id: 4,
      nombreArchivo: 'example2',
      tipoArchivo: 'example2',
      contenidoArchivo: null,
      ubicacionArchivo: 'example2',
      observaciones: 'example2',
      idSubtipoTramite: {
        id: 18,
        nombre: 'Certificado ingresos y retenciones',
        descripcion: 'Certificado ingresos y retenciones',
        padreId: 10
      },
      idTramite: null
    },
    {
      id: 5,
      nombreArchivo: 'example3',
      tipoArchivo: 'example3',
      contenidoArchivo: null,
      ubicacionArchivo: 'example3',
      observaciones: 'example3',
      idSubtipoTramite: {
        id: 19,
        nombre: 'Certificado de cobertura ARL en el exterior',
        descripcion: 'Certificado de cobertura ARL en el exterior',
        padreId: 10
      },
      idTramite: null
    },
    {
      id: 6,
      nombreArchivo: 'example4',
      tipoArchivo: 'example4',
      contenidoArchivo: null,
      ubicacionArchivo: 'example4',
      observaciones: 'example4',
      idSubtipoTramite: {
        id: 20,
        nombre: 'Certificado medicina prepagada',
        descripcion: 'Certificado medicina prepagada',
        padreId: 10
      },
      idTramite: null
    }
  ],
  detalleTramite: {
    id: 35,
    diasSolicitados: 3,
    fechaDiaSolicitado: '2020-04-21',
    horaInicio: '10:00:00',
    horaFin: '15:00:00',
    fechaInicio: '07-04-2020',
    fechaFin: '01-03-2020',
    otroMotivo: 'Consulta de exámenes',
    observaciones: 'Las actividades se encuentran al día',
    nombreArchivo: 'Incapacidad.pdf',
    tipoArchivo: 'application/pdf',
    contenidoArchivo: null,
    ubicacionArchivo: 'string',
    idTipoDocumentoTramite: {
      id: 2,
      nombre: 'Cita médica',
      obligatorio: false,
      descripcion: '',
      idTipoTramite: {
        id: 2,
        nombre: 'Permisos',
        descripcion: 'Permisos',
        padreId: null
      },
      idSubtipoTramite: {
        id: 12,
        nombre: 'Cita médica',
        descripcion: 'Cita médica',
        padreId: 2
      }
    },
    idSubtipoTramite: null,
    idTramite: null
  },
  gestionTramiteList: [
    {
      id: 19,
      fecha: '2020-04-21',
      revisadoPor: 'cristian Quintero',
      observaciones: 'ninguna',
      respuestaTramite: 'Radicar Tramite',
      permisoRemunerado: 'si',
      requiereReemplazo: 'no',
      nombreReemplazo: 'na',
      idIncapacidad: null,
      idTramite: null,
      idRol: {
        id: 1,
        nombre: 'Empleado',
        descripcion: '',
        nombreBpm: 'Empleado'
      }
    }
  ]
};

export const MOCK_TRAMITE_CONSULTADO_PERMISOS: TramiteDTO =
{
  id: 45,
  idInstancia: 177,
  identificacion: '1010142720',
  fecha: '2020-04-21',
  estado: 'creada',
  nombreEmpleado: 'Cristian Alberto',
  apellidoEmpleado: 'Quintero Gaitans',
  respuestaTramite: '',
  idArea: {
    id: 3,
    nombre: 'Gerencia de operaciones',
    descripcion: '',
    nombreBpm: 'Gerencia de operaciones'
  },
  idGestorTramite: {
    id: 9,
    identificacion: "80101089",
    usuario: "JefeInmediato02",
    nombre: "JefeInmediato",
    apellido: "02",
    idArea: null,
    idTipoIdentificacion: null,
    idRol: null
  },
  idTipoIdentificacion: {
    idTipoIdentificacion: 1,
    nombreTipoIdentificacion: 'CC',
    descripcionTipoIdentificacion: 'Cédula de Ciudadanía'
  },
  idTipoTramite: {
    id: 2,
    nombre: 'Permisos',
    descripcion: 'Permisos',
    padreId: null
  },
  vacacionesTramite: {
    id: '2',
    diasDisponibles: 10,
    solicitaDiasAnticipados: true,
    totalDiasSolicitados: 12,
    totalDiasAnticipados: 2,
    fechaInicio: '2020-04-20',
    fechaFin: '2020-05-06',
    fechaReintegro: '2020-05-07',
    observaciones: 'Pedido de días disponibles',
    idTramite: null
  },
  certificadoTramiteList: [
    {
      id: 2,
      nombreArchivo: 'example',
      tipoArchivo: 'example',
      contenidoArchivo: null,
      ubicacionArchivo: 'example',
      observaciones: 'example',
      idSubtipoTramite: {
        id: 16,
        nombre: 'Certificado laboral estándar',
        descripcion: 'Certificado laboral estándar',
        padreId: 10
      },
      idTramite: null
    },
    {
      id: 3,
      nombreArchivo: 'example1',
      tipoArchivo: 'example1',
      contenidoArchivo: null,
      ubicacionArchivo: 'example1',
      observaciones: 'example1',
      idSubtipoTramite: {
        id: 17,
        nombre: 'Certificado laboral con funciones',
        descripcion: 'Certificado laboral con funciones',
        padreId: 10
      },
      idTramite: null
    },
    {
      id: 4,
      nombreArchivo: 'example2',
      tipoArchivo: 'example2',
      contenidoArchivo: null,
      ubicacionArchivo: 'example2',
      observaciones: 'example2',
      idSubtipoTramite: {
        id: 18,
        nombre: 'Certificado ingresos y retenciones',
        descripcion: 'Certificado ingresos y retenciones',
        padreId: 10
      },
      idTramite: null
    },
    {
      id: 5,
      nombreArchivo: 'example3',
      tipoArchivo: 'example3',
      contenidoArchivo: null,
      ubicacionArchivo: 'example3',
      observaciones: 'example3',
      idSubtipoTramite: {
        id: 19,
        nombre: 'Certificado de cobertura ARL en el exterior',
        descripcion: 'Certificado de cobertura ARL en el exterior',
        padreId: 10
      },
      idTramite: null
    },
    {
      id: 6,
      nombreArchivo: 'example4',
      tipoArchivo: 'example4',
      contenidoArchivo: null,
      ubicacionArchivo: 'example4',
      observaciones: 'example4',
      idSubtipoTramite: {
        id: 20,
        nombre: 'Certificado medicina prepagada',
        descripcion: 'Certificado medicina prepagada',
        padreId: 10
      },
      idTramite: null
    }
  ],
  detalleTramite: {
    id: 35,
    diasSolicitados: 3,
    fechaDiaSolicitado: '2020-04-21',
    horaInicio: '10:00:00',
    horaFin: '15:00:00',
    fechaInicio: '07-04-2020',
    fechaFin: '01-03-2020',
    otroMotivo: 'Consulta de exámenes',
    observaciones: 'Las actividades se encuentran al día',
    nombreArchivo: 'Incapacidad.pdf',
    tipoArchivo: 'application/pdf',
    contenidoArchivo: null,
    ubicacionArchivo: 'string',
    idTipoDocumentoTramite: {
      id: 2,
      nombre: 'Cita médica',
      obligatorio: false,
      descripcion: '',
      idTipoTramite: {
        id: 2,
        nombre: 'Permisos',
        descripcion: 'Permisos',
        padreId: null
      },
      idSubtipoTramite: {
        id: 12,
        nombre: 'Cita médica',
        descripcion: 'Cita médica',
        padreId: 2
      }
    },
    idSubtipoTramite: {
      id: 13,
      nombre: 'Trámite académico',
      descripcion: 'Trámite académico',
      padreId: 2
    },
    idTramite: null
  },
  gestionTramiteList: [
    {
      id: 19,
      fecha: '2020-04-21',
      revisadoPor: 'cristian Quintero',
      observaciones: 'ninguna',
      respuestaTramite: 'Radicar Tramite',
      permisoRemunerado: 'si',
      requiereReemplazo: 'no',
      nombreReemplazo: 'na',
      idIncapacidad: null,
      idTramite: null,
      idRol: {
        id: 1,
        nombre: 'Empleado',
        descripcion: '',
        nombreBpm: 'Empleado'
      }
    }
  ]
};

export const MOCK_TRAMITE_CONSULTADO_VACACIONES: TramiteDTO =
{
  id: 45,
  idInstancia: 177,
  identificacion: '1010142720',
  fecha: '2020-04-21',
  estado: 'creada',
  nombreEmpleado: 'Cristian Alberto',
  apellidoEmpleado: 'Quintero Gaitans',
  respuestaTramite: '',
  idArea: {
    id: 3,
    nombre: 'Gerencia de operaciones',
    descripcion: '',
    nombreBpm: 'Gerencia de operaciones'
  },
  idGestorTramite: {
    id: 9,
    identificacion: "80101089",
    usuario: "JefeInmediato02",
    nombre: "JefeInmediato",
    apellido: "02",
    idArea: null,
    idTipoIdentificacion: null,
    idRol: null
  },
  idTipoIdentificacion: {
    idTipoIdentificacion: 1,
    nombreTipoIdentificacion: 'CC',
    descripcionTipoIdentificacion: 'Cédula de Ciudadanía'
  },
  idTipoTramite: {
    id: 1,
    nombre: 'Vacaciones',
    descripcion: 'Vacaciones',
    padreId: null
  },
  vacacionesTramite: {
    id: '2',
    diasDisponibles: 10,
    solicitaDiasAnticipados: true,
    totalDiasSolicitados: 12,
    totalDiasAnticipados: 2,
    fechaInicio: '2020-04-20',
    fechaFin: '2020-05-06',
    fechaReintegro: '2020-05-07',
    observaciones: 'Pedido de días disponibles',
    idTramite: null
  },
  certificadoTramiteList: [
    {
      id: 2,
      nombreArchivo: 'example',
      tipoArchivo: 'example',
      contenidoArchivo: null,
      ubicacionArchivo: 'example',
      observaciones: 'example',
      idSubtipoTramite: {
        id: 16,
        nombre: 'Certificado laboral estándar',
        descripcion: 'Certificado laboral estándar',
        padreId: 10
      },
      idTramite: null
    },
    {
      id: 3,
      nombreArchivo: 'example1',
      tipoArchivo: 'example1',
      contenidoArchivo: null,
      ubicacionArchivo: 'example1',
      observaciones: 'example1',
      idSubtipoTramite: {
        id: 17,
        nombre: 'Certificado laboral con funciones',
        descripcion: 'Certificado laboral con funciones',
        padreId: 10
      },
      idTramite: null
    },
    {
      id: 4,
      nombreArchivo: 'example2',
      tipoArchivo: 'example2',
      contenidoArchivo: null,
      ubicacionArchivo: 'example2',
      observaciones: 'example2',
      idSubtipoTramite: {
        id: 18,
        nombre: 'Certificado ingresos y retenciones',
        descripcion: 'Certificado ingresos y retenciones',
        padreId: 10
      },
      idTramite: null
    },
    {
      id: 5,
      nombreArchivo: 'example3',
      tipoArchivo: 'example3',
      contenidoArchivo: null,
      ubicacionArchivo: 'example3',
      observaciones: 'example3',
      idSubtipoTramite: {
        id: 19,
        nombre: 'Certificado de cobertura ARL en el exterior',
        descripcion: 'Certificado de cobertura ARL en el exterior',
        padreId: 10
      },
      idTramite: null
    },
    {
      id: 6,
      nombreArchivo: 'example4',
      tipoArchivo: 'example4',
      contenidoArchivo: null,
      ubicacionArchivo: 'example4',
      observaciones: 'example4',
      idSubtipoTramite: {
        id: 20,
        nombre: 'Certificado medicina prepagada',
        descripcion: 'Certificado medicina prepagada',
        padreId: 10
      },
      idTramite: null
    }
  ],
  detalleTramite: {
    id: 35,
    diasSolicitados: 1,
    fechaDiaSolicitado: '2020-04-21',
    horaInicio: '05:00:00',
    horaFin: '10:00:00',
    fechaInicio: null,
    fechaFin: null,
    otroMotivo: 'Consulta de exámenes',
    observaciones: 'Las actividades se encuentran al día',
    nombreArchivo: 'Incapacidad.pdf',
    tipoArchivo: 'application/pdf',
    contenidoArchivo: null,
    ubicacionArchivo: 'string',
    idTipoDocumentoTramite: {
      id: 2,
      nombre: 'Cita médica',
      obligatorio: false,
      descripcion: '',
      idTipoTramite: {
        id: 2,
        nombre: 'Permisos',
        descripcion: 'Permisos',
        padreId: null
      },
      idSubtipoTramite: {
        id: 12,
        nombre: 'Cita médica',
        descripcion: 'Cita médica',
        padreId: 2
      }
    },
    idSubtipoTramite: {
      id: 20,
      nombre: 'Certificado medicina prepagada',
      descripcion: 'Certificado medicina prepagada',
      padreId: 10
    },
    idTramite: null
  },
  gestionTramiteList: [
    {
      id: 19,
      fecha: '2020-04-21',
      revisadoPor: 'cristian Quintero',
      observaciones: 'ninguna',
      respuestaTramite: 'Radicar Tramite',
      permisoRemunerado: 'si',
      requiereReemplazo: 'no',
      nombreReemplazo: 'na',
      idIncapacidad: null,
      idTramite: null,
      idRol: {
        id: 1,
        nombre: 'Empleado',
        descripcion: '',
        nombreBpm: 'Empleado'
      }
    }
  ]
};

export const MOCK_TRAMITE_CONSULTADO: TramiteDTO =
{
  id: 45,
  idInstancia: 177,
  identificacion: '1010142720',
  fecha: '2020-04-21',
  estado: 'creada',
  nombreEmpleado: 'Cristian Alberto',
  apellidoEmpleado: 'Quintero Gaitans',
  respuestaTramite: '',
  idArea: {
    id: 3,
    nombre: 'Gerencia de operaciones',
    descripcion: '',
    nombreBpm: 'Gerencia de operaciones'
  },
  idGestorTramite: {
    id: 9,
    identificacion: "80101089",
    usuario: "JefeInmediato02",
    nombre: "JefeInmediato",
    apellido: "02",
    idArea: null,
    idTipoIdentificacion: null,
    idRol: null
  },
  idTipoIdentificacion: {
    idTipoIdentificacion: 1,
    nombreTipoIdentificacion: 'CC',
    descripcionTipoIdentificacion: 'Cédula de Ciudadanía'
  },
  idTipoTramite: {
    id: 1,
    nombre: 'Vacaciones',
    descripcion: 'Vacaciones',
    padreId: null
  },
  vacacionesTramite: {
    id: '2',
    diasDisponibles: 10,
    solicitaDiasAnticipados: true,
    totalDiasSolicitados: 12,
    totalDiasAnticipados: 2,
    fechaInicio: '2020-04-20',
    fechaFin: '2020-05-06',
    fechaReintegro: '2020-05-07',
    observaciones: 'Pedido de días disponibles',
    idTramite: null
  },
  certificadoTramiteList: [
    {
      id: 2,
      nombreArchivo: 'example',
      tipoArchivo: 'example',
      contenidoArchivo: null,
      ubicacionArchivo: 'example',
      observaciones: 'example',
      idSubtipoTramite: {
        id: 16,
        nombre: 'Certificado laboral estándar',
        descripcion: 'Certificado laboral estándar',
        padreId: 10
      },
      idTramite: null
    },
    {
      id: 3,
      nombreArchivo: 'example1',
      tipoArchivo: 'example1',
      contenidoArchivo: null,
      ubicacionArchivo: 'example1',
      observaciones: 'example1',
      idSubtipoTramite: {
        id: 17,
        nombre: 'Certificado laboral con funciones',
        descripcion: 'Certificado laboral con funciones',
        padreId: 10
      },
      idTramite: null
    },
    {
      id: 4,
      nombreArchivo: 'example2',
      tipoArchivo: 'example2',
      contenidoArchivo: null,
      ubicacionArchivo: 'example2',
      observaciones: 'example2',
      idSubtipoTramite: {
        id: 18,
        nombre: 'Certificado ingresos y retenciones',
        descripcion: 'Certificado ingresos y retenciones',
        padreId: 10
      },
      idTramite: null
    },
    {
      id: 5,
      nombreArchivo: 'example3',
      tipoArchivo: 'example3',
      contenidoArchivo: null,
      ubicacionArchivo: 'example3',
      observaciones: 'example3',
      idSubtipoTramite: {
        id: 19,
        nombre: 'Certificado de cobertura ARL en el exterior',
        descripcion: 'Certificado de cobertura ARL en el exterior',
        padreId: 10
      },
      idTramite: null
    },
    {
      id: 6,
      nombreArchivo: 'example4',
      tipoArchivo: 'example4',
      contenidoArchivo: null,
      ubicacionArchivo: 'example4',
      observaciones: 'example4',
      idSubtipoTramite: {
        id: 20,
        nombre: 'Certificado medicina prepagada',
        descripcion: 'Certificado medicina prepagada',
        padreId: 10
      },
      idTramite: null
    }
  ],
  detalleTramite: {
    id: 35,
    diasSolicitados: 1,
    fechaDiaSolicitado: '2020-04-21',
    horaInicio: '05:00:00',
    horaFin: '10:00:00',
    fechaInicio: null,
    fechaFin: null,
    otroMotivo: 'Consulta de exámenes',
    observaciones: 'Las actividades se encuentran al día',
    nombreArchivo: 'Incapacidad.pdf',
    tipoArchivo: 'application/pdf',
    contenidoArchivo: null,
    ubicacionArchivo: 'string',
    idTipoDocumentoTramite: {
      id: 2,
      nombre: 'Cita médica',
      obligatorio: false,
      descripcion: '',
      idTipoTramite: {
        id: 2,
        nombre: 'Permisos',
        descripcion: 'Permisos',
        padreId: null
      },
      idSubtipoTramite: {
        id: 12,
        nombre: 'Cita médica',
        descripcion: 'Cita médica',
        padreId: 2
      }
    },
    idSubtipoTramite: {
      id: 20,
      nombre: 'Certificado medicina prepagada',
      descripcion: 'Certificado medicina prepagada',
      padreId: 10
    },
    idTramite: null
  },
  gestionTramiteList: [
    {
      id: 19,
      fecha: '2020-04-21',
      revisadoPor: 'cristian Quintero',
      observaciones: 'ninguna',
      respuestaTramite: 'Radicar Tramite',
      permisoRemunerado: 'si',
      requiereReemplazo: 'no',
      nombreReemplazo: 'na',
      idIncapacidad: null,
      idTramite: null,
      idRol: {
        id: 1,
        nombre: 'Empleado',
        descripcion: '',
        nombreBpm: 'Empleado'
      }
    }
  ]
};