import { TramiteDTO } from "../dto/TramiteDTO";
import { TablaBasicaDTO } from "../dto/TablaBasicaDTO";
import { CatalogueDTO } from "../dto/CatalogueDTO";
import { ProcessManagersDTO } from "../dto/ProcessManagersDTO";
import { DocumentTypeDTO } from "../dto/DocumentTypeDTO";
import { GestionTramiteDTO } from "../dto/GestionTramiteDTO";
import { TipoTramiteDTO } from "../dto/TipoTramiteDTO";

export const MOCK_DISABILITY_CLASSIFICATION_TABLE: TipoTramiteDTO[] = [
    { id: 21, nombre: "Afectaciones gastrointestinales", descripcion: "", padreId: 3 },
    { id: 22, nombre: "Afecciones renales y vías urinarias", descripcion: "", padreId: 3 },
    { id: 23, nombre: "Apendicitis", descripcion: "", padreId: 3 },
    { id: 24, nombre: "Embarazo o licencia de maternidad / paternidad", descripcion: "", padreId: 3 },
    { id: 25, nombre: "Enfermedades cutánias", descripcion: "", padreId: 3 },
    { id: 26, nombre: "Hernias, fisuras o fistulas", descripcion: "", padreId: 3 },
    { id: 27, nombre: "Infecciones ojos y/o oidos", descripcion: "", padreId: 3 },
    { id: 28, nombre: "Infecciones VAS", descripcion: "", padreId: 3 },
    { id: 29, nombre: "Lesiones osteomusculares", descripcion: "", padreId: 3 },
    { id: 30, nombre: "Migraña cefalea, encefalitis", descripcion: "", padreId: 3 },
    { id: 31, nombre: "Otros", descripcion: "", padreId: 3 }
];

export const MOCK_PROCESSRESPONSE_TABLE: CatalogueDTO[] = [
    {
        id: "Cambios",
        nombre: "Solicitar cambios en la radicación",
        descripcion: ""
    },
    {
        id: "Si",
        nombre: "Aprobar",
        descripcion: ""
    },
    {
        id: "No",
        nombre: "Rechazar",
        descripcion: ""
    }
];

export const MOCK_DECISION_TABLE: CatalogueDTO[] = [
    {
        id: "1",
        nombre: "Si",
        descripcion: ""
    },
    {
        id: "2",
        nombre: "No",
        descripcion: ""
    }
];

export const MOCK_DOCUMENT_TYPE_TABLE: DocumentTypeDTO[] = [
    {
        id: "1",
        idTipoTramite: {
            id: 2,
            nombre: "Permisos",
            descripcion: "Permisos",
            padreId: null
        },
        idSubtipoTramite: {
            id: 11,
            nombre: "Motivo personal",
            descripcion: "Motivo personal",
            padreId: 2
        },
        nombre: "Permiso motivo personal",
        obligatorio: false,
        descripcion: "string"
    },
    {
        id: "2",
        idTipoTramite: {
            id: 2,
            nombre: "Permisos",
            descripcion: "Permisos",
            padreId: null
        },
        idSubtipoTramite: {
            id: 12,
            nombre: "Cita médica",
            descripcion: "Cita médica",
            padreId: 2
        },
        nombre: "Cita médica",
        obligatorio: false,
        descripcion: "string"
    },
    {
        id: "3",
        idTipoTramite: {
            id: 2,
            nombre: "Permisos",
            descripcion: "Permisos",
            padreId: null
        },
        idSubtipoTramite: {
            id: 13,
            nombre: "Trámite académico",
            descripcion: "Trámite académico",
            padreId: 2
        },
        nombre: "Permiso trámite académico",
        obligatorio: false,
        descripcion: "string"
    },
    {
        id: "4",
        idTipoTramite: {
            id: 2,
            nombre: "Permisos",
            descripcion: "Permisos",
            padreId: null
        },
        idSubtipoTramite: {
            id: 14,
            nombre: "Otro",
            descripcion: "Otro",
            padreId: 2
        },
        nombre: "Permiso por otro motivo",
        obligatorio: false,
        descripcion: "string"
    },
    {
        id: "5",
        idTipoTramite: {
            id: 3,
            nombre: "Reporte de incapacidad",
            descripcion: "Reporte de incapacidad",
            padreId: null
        },
        idSubtipoTramite: null,
        nombre: "Incapacidad",
        obligatorio: true,
        descripcion: "string"
    },
    {
        id: "6",
        idTipoTramite: {
            id: 3,
            nombre: "Reporte de incapacidad",
            descripcion: "Reporte de incapacidad",
            padreId: null
        },
        idSubtipoTramite: null,
        nombre: "Incapacidad, historia clínica y copia del carnet eps (o copia del carnet de medicina prepagada).",
        obligatorio: true,
        descripcion: "string"
    },
    {
        id: "7",
        idTipoTramite: {
            id: 6,
            nombre: "Calamidad doméstica",
            descripcion: "Calamidad doméstica",
            padreId: null
        },
        idSubtipoTramite: null,
        nombre: "Calamidad doméstica",
        obligatorio: false,
        descripcion: "string"
    },
    {
        id: "8",
        idTipoTramite: {
            id: 7,
            nombre: "Licencia por luto",
            descripcion: "Licencia por luto",
            padreId: null
        },
        idSubtipoTramite: null,
        nombre: "Certificado de defunción",
        obligatorio: true,
        descripcion: "string"
    },
    {
        id: "9",
        idTipoTramite: {
            id: 9,
            nombre: "Compensatorio por votación",
            descripcion: "Compensatorio por votación",
            padreId: null
        },
        idSubtipoTramite: null,
        nombre: "Certificado electoral",
        obligatorio: true,
        descripcion: "string"
    }
];

export const MOCK_PROCESS_MANAGERS_TABLE: ProcessManagersDTO[] = [
    {
        id: "1",
        idTipoIdentificacion: "1",
        identificacion: "80101080",
        usuario: "GerenteArea01",
        nombre: "GerenteArea",
        apellido: "01",
        idArea: "1",
        idRol: 3
    },
    {
        id: "2",
        idTipoIdentificacion: "1",
        identificacion: "80101081",
        usuario: "GerenteArea02",
        nombre: "GerenteArea",
        apellido: "02",
        idArea: "2",
        idRol: 3
    },
    {
        id: "3",
        idTipoIdentificacion: "1",
        identificacion: "80101082",
        usuario: "GerenteArea03",
        nombre: "GerenteArea",
        apellido: "03",
        idArea: "3",
        idRol: 3
    },
    {
        id: "4",
        idTipoIdentificacion: "1",
        identificacion: "80101083",
        usuario: "GerenteArea04",
        nombre: "GerenteArea",
        apellido: "04",
        idArea: "4",
        idRol: 3
    },
    {
        id: "5",
        idTipoIdentificacion: "1",
        identificacion: "80101084",
        usuario: "GerenteGH01",
        nombre: "GerenteGH",
        apellido: "01",
        idArea: "5",
        idRol: 4
    },
    {
        id: "6",
        idTipoIdentificacion: "1",
        identificacion: "80101085",
        usuario: "GerenteArea05",
        nombre: "GerenteArea",
        apellido: "05",
        idArea: "6",
        idRol: 3
    },
    {
        id: "7",
        idTipoIdentificacion: "1",
        identificacion: "80101086",
        usuario: "GerenteArea06",
        nombre: "GerenteArea",
        apellido: "06",
        idArea: "7",
        idRol: 3
    },
    {
        id: "8",
        idTipoIdentificacion: "1",
        identificacion: "80101087",
        usuario: "GerenteArea07",
        nombre: "GerenteArea",
        apellido: "07",
        idArea: "8",
        idRol: 3
    },
    {
        id: "9",
        idTipoIdentificacion: "2",
        identificacion: "80101088",
        usuario: "JefeInmediato02",
        nombre: "JefeInmediato",
        apellido: "02",
        idArea: "3",
        idRol: 2
    },
    {
        id: "10",
        idTipoIdentificacion: "1",
        identificacion: "80101089",
        usuario: "JefeInmediato03",
        nombre: "JefeInmediato",
        apellido: "03",
        idArea: "3",
        idRol: 2
    },
    {
        id: "11",
        idTipoIdentificacion: "1",
        identificacion: "80101090",
        usuario: "JefeInmediato04",
        nombre: "JefeInmediato",
        apellido: "04",
        idArea: "3",
        idRol: 2
    },
    {
        id: "12",
        idTipoIdentificacion: "1",
        identificacion: "80101091",
        usuario: "JefeInmediato01",
        nombre: "JefeInmediato",
        apellido: "01",
        idArea: "3",
        idRol: 2
    }
];

export const MOCK_PROCEDURE_TYPE_TABLE: TipoTramiteDTO[] = [
    { id: 1, nombre: "Vacaciones", descripcion: "", padreId: null },
    { id: 2, nombre: "Permisos", descripcion: "", padreId: null },
    { id: 3, nombre: "Reporte de incapacidad", descripcion: "", padreId: null },
    { id: 4, nombre: "Día de la familia", descripcion: "", padreId: null },
    { id: 5, nombre: "Día cumpleaños", descripcion: "", padreId: null },
    { id: 6, nombre: "Calamidad doméstica", descripcion: "", padreId: null },
    { id: 7, nombre: "Licencia por luto", descripcion: "", padreId: null },
    { id: 8, nombre: "Compensatorios", descripcion: "", padreId: null },
    { id: 9, nombre: "Compensatorio por votación", descripcion: "", padreId: null },
    { id: 10, nombre: "Certificación", descripcion: "", padreId: null }
];

export const MOCK_CERTIFICATE_TYPE_TABLE: TipoTramiteDTO[] = [
    { id: 16, nombre: "Certificado laboral estándar", descripcion: "", padreId: 10 },
    { id: 17, nombre: "Certificado laboral con funciones", descripcion: "", padreId: 10 },
    { id: 18, nombre: "Certificado ingresos y retenciones", descripcion: "", padreId: 10 },
    { id: 19, nombre: "Certificado de cobertura ARL en el exterior", descripcion: "", padreId: 10 },
    { id: 20, nombre: "Certificado medicina prepagada", descripcion: "", padreId: 10 }
];

export const MOCK_MEDICAL_DISABILITY_TABLE: TipoTramiteDTO[] = [
    {
        id: 30,
        nombre: "Afecciones gastrointestinales",
        descripcion: "",
        padreId: 3
    },
    {
        id: 31,
        nombre: "Afecciones renales y vías urinarias",
        descripcion: "",
        padreId: 3
    },
    {
        id: 32,
        nombre: "Apendicitis",
        descripcion: "",
        padreId: 3
    },
    {
        id: 33,
        nombre: "Embarazo o licencia de maternidad / paternidad",
        descripcion: "",
        padreId: 3
    },
    {
        id: 34,
        nombre: "Enfermedades cutánias",
        descripcion: "",
        padreId: 3
    },
    {
        id: 35,
        nombre: "Hernias, fisuras o fistulas",
        descripcion: "",
        padreId: 3
    },
    {
        id: 36,
        nombre: "Infecciones ojos y/o oidos",
        descripcion: "",
        padreId: 3
    },
    {
        id: 37,
        nombre: "Infecciones VAS",
        descripcion: "",
        padreId: 3
    },
    {
        id: 38,
        nombre: "Lesiones osteomusculares",
        descripcion: "",
        padreId: 3
    },
    {
        id: 39,
        nombre: "Migraña cefalea, encefalitis",
        descripcion: "",
        padreId: 3
    },
    {
        id: 40,
        nombre: "Otros",
        descripcion: "",
        padreId: 3
    }
];

export const MOCK_PERSONAL_REASON_TABLE: TipoTramiteDTO[] = [
    {
        id: 11,
        nombre: "Motivo personal",
        descripcion: "",
        padreId: 2
    },
    {
        id: 12,
        nombre: "Cita médica",
        descripcion: "",
        padreId: 2
    },
    {
        id: 13,
        nombre: "Trámite académico",
        descripcion: "",
        padreId: 2
    },
    {
        id: 14,
        nombre: "Otro",
        descripcion: "",
        padreId: 2
    }
];

export const MOCK_IDENTIFICATION_TYPE_TABLE: CatalogueDTO[] = [
    { id: "1", nombre: "CC", descripcion: "Cédula de Ciudadanía" },
    { id: "2", nombre: "CE", descripcion: "Cédula de Extranjería" }
];

export const MOCK_PROCESS_MANAGER_TABLE: CatalogueDTO[] = [
    {
        id: "1",
        nombre: "Faustino Hermo Gonzalez",
        descripcion: ""
    },
    {
        id: "2",
        nombre: "Luz Stella Forero Reyes",
        descripcion: ""
    },
    {
        id: "3",
        nombre: "Angel Lubiel Simbaqueva Ruiz",
        descripcion: ""
    },
    {
        id: "4",
        nombre: "Monica Yulieth Suarez Roldan",
        descripcion: ""
    }
];

export const MOCK_COMPANY_AREA_TABLE: CatalogueDTO[] = [
    {
        id: "1",
        nombre: "Gerencia financiera",
        descripcion: ""
    },
    {
        id: "2",
        nombre: "Coordinación general y administrativa",
        descripcion: ""
    },
    {
        id: "3",
        nombre: "Gerencia de operaciones",
        descripcion: ""
    },
    {
        id: "4",
        nombre: "Dirección general",
        descripcion: ""
    },
    {
        id: "5",
        nombre: "Gerencia gestión humana",
        descripcion: ""
    },
    {
        id: "6",
        nombre: "Gerencia comercial y de mercadeo",
        descripcion: ""
    },
    {
        id: "7",
        nombre: "Gerencia de producto",
        descripcion: ""
    },
    {
        id: "8",
        nombre: "Gerencia de calidad",
        descripcion: ""
    }
];



/**************************************************************** */




export const TABLABASICA: TablaBasicaDTO[] = [
    {
        idTablaBasica: "9",
        nombre: "Vacaciones",
        descripcion: "Tipo solicitud - Vacaciones",
        codigoApp: "@TS_VA",
        tipoTablaBasica: "1",
    },
    {
        idTablaBasica: "11",
        nombre: "Reporte de incapacidad",
        descripcion: "Tipo solicitud - Reporte de incapacidad",
        codigoApp: "@TS_RI",
        tipoTablaBasica: "1",
    },
    {
        idTablaBasica: "10",
        nombre: "Permisos",
        descripcion: "Tipo solicitud - Permisos",
        codigoApp: "@TS_PE",
        tipoTablaBasica: "1",
    },
    {
        idTablaBasica: "15",
        nombre: "Licencia por luto",
        descripcion: "Tipo solicitud - Licencia por luto",
        codigoApp: "@TS_LL",
        tipoTablaBasica: "1",
    },
    {
        idTablaBasica: "12",
        nombre: "Día de la familia",
        descripcion: "Tipo solicitud - Dia de la familia",
        codigoApp: "@TS_DF",
        tipoTablaBasica: "1",
    },
    {
        idTablaBasica: "13",
        nombre: "Día cumpleaños",
        descripcion: "Tipo solicitud - Dia cumpleaños",
        codigoApp: "@TS_DC",
        tipoTablaBasica: "1",
    },
    {
        idTablaBasica: "17",
        nombre: "Compensatorio por votación",
        descripcion: "Tipo solicitud - Compensatorio por votacion",
        codigoApp: "@TS_CV",
        tipoTablaBasica: "1",
    },
    {
        idTablaBasica: "16",
        nombre: "Compensatorios",
        descripcion: "Tipo solicitud - Compensatorio",
        codigoApp: "@TS_CO",
        tipoTablaBasica: "1",
    },
    {
        idTablaBasica: "18",
        nombre: "Certificación",
        descripcion: "Tipo solicitud - Certificación",
        codigoApp: "@TS_CE",
        tipoTablaBasica: "1",
    },
    {
        idTablaBasica: "14",
        nombre: "Calamidad doméstica",
        descripcion: "Tipo solicitud - Calamidad domestica",
        codigoApp: "@TS_CD",
        tipoTablaBasica: "1",
    }
];

/*detalleTramiteDTO:{
        diaSolicitado: "2019-09-23T02:25:35.000+0000",
        horaInicio: "2019-09-23T02:25:35.000+0000",
        horaFin: "2019-09-23T02:25:35.000+0000",
        descripcionMotivo: "Homologacion Materia",
        diasSolicitados: 3,
        fechaInicio: "2019-09-23T02:25:35.000+0000",
        fechaFin: "2019-09-23T02:25:35.000+0000",
        motivoSolicitud: "@MS_TA"
    }*/

/*fechaSolicitud: "2019-10-16T03:29:25.000+0000",
    estado: true,
    numeroRadicado: "192",
    solicitante: {numeroDocumento: "987654321"},
    certificado:{
        observaciones: "SinComentarios",
        tipoCertificado: "@TC_LS",
    },
    tipoSolicitud: null,
    solicitudVacacionesDto: {
        fechaInicio: null,
        fechaFin: null,
        diasDisponibles: 15,
        diasAnticipados: 0,
        diasSolicitados: 0,
        fechaReintegro: "2019-10-18T02:25:35.000+0000"
    },*/

export const FILED_PROCEDURE: TramiteDTO = {
    id: 1,
    idInstancia: 170,
    idTipoIdentificacion: {
        idTipoIdentificacion: 1,
        nombreTipoIdentificacion: 'CC',
        descripcionTipoIdentificacion: 'Cédula de Ciudadanía'
    },
    identificacion: '80101082',
    fecha: '01/04/2020',
    idTipoTramite: {
        id: 3,
        nombre: 'Reporte de incapacidad',
        descripcion: 'Reporte de incapacidad',
        padreId: null
    },
    estado: 'string',
    nombreEmpleado: 'Jhon Jairo',
    apellidoEmpleado: 'Morales Vargas',
    idArea: {
        id: 3,
        nombre: 'Gerencia de operaciones',
        descripcion: '',
        nombreBpm: 'Gerencia de operaciones'
    },
    idGestorTramite: {
        id: 9,
        identificacion: "80101089",
        usuario: "JefeInmediato02",
        nombre: "JefeInmediato",
        apellido: "02",
        idArea: {
            id: 3,
            nombre: 'Gerencia de operaciones',
            descripcion: '',
            nombreBpm: 'Gerencia de operaciones'
        },
        idTipoIdentificacion: {
            idTipoIdentificacion: 1,
            nombreTipoIdentificacion: 'CC',
            descripcionTipoIdentificacion: 'Cédula de Ciudadanía'
        },
        idRol: null
    },
    respuestaTramite: '',
    vacacionesTramite: {
        id: '1',
        idTramite: '12',
        diasDisponibles: 10,
        solicitaDiasAnticipados: true,
        totalDiasSolicitados: 12,
        totalDiasAnticipados: 2,
        fechaInicio: '01/04/2020',
        fechaFin: '13/04/2020',
        fechaReintegro: '14/04/2020',
        observaciones: 'Pedido de días disponibles',
    },
    detalleTramite: {
        id: 1,
        idTramite: '12',
        diasSolicitados: 12,
        fechaDiaSolicitado: '13/04/2020',
        horaInicio: '14:00',
        horaFin: '18:00',
        fechaInicio: null,
        fechaFin: null,
        idSubtipoTramite: {
            id: 12,
            nombre: 'Cita médica',
            descripcion: 'Cita médica',
            padreId: 2
        },
        otroMotivo: 'Consulta de exámenes',
        observaciones: 'Las actividades se encuentran al día',
        idTipoDocumentoTramite: {
            id: 2,
            nombre: 'Cita médica',
            obligatorio: false,
            descripcion: '',
            idTipoTramite: {
                id: 2,
                nombre: 'Permisos',
                descripcion: 'Permisos',
                padreId: null
            },
            idSubtipoTramite: {
                id: 12,
                nombre: 'Cita médica',
                descripcion: 'Cita médica',
                padreId: 2
            }
        },
        nombreArchivo: "Incapacidad.pdf",
        tipoArchivo: 'application/pdf',
        contenidoArchivo: 'string',
        ubicacionArchivo: 'string',
    },
    certificadoTramiteList: [
        {
            id: 2,
            nombreArchivo: 'example',
            tipoArchivo: 'example',
            contenidoArchivo: null,
            ubicacionArchivo: 'example',
            observaciones: 'example',
            idSubtipoTramite: {
              id: 16,
              nombre: 'Certificado laboral estándar',
              descripcion: 'Certificado laboral estándar',
              padreId: 10
            },
            idTramite: null
          },
          {
            id: 3,
            nombreArchivo: 'example1',
            tipoArchivo: 'example1',
            contenidoArchivo: null,
            ubicacionArchivo: 'example1',
            observaciones: 'example1',
            idSubtipoTramite: {
              id: 17,
              nombre: 'Certificado laboral con funciones',
              descripcion: 'Certificado laboral con funciones',
              padreId: 10
            },
            idTramite: null
          },
          {
            id: 4,
            nombreArchivo: 'example2',
            tipoArchivo: 'example2',
            contenidoArchivo: null,
            ubicacionArchivo: 'example2',
            observaciones: 'example2',
            idSubtipoTramite: {
              id: 18,
              nombre: 'Certificado ingresos y retenciones',
              descripcion: 'Certificado ingresos y retenciones',
              padreId: 10
            },
            idTramite: null
          },
          {
            id: 5,
            nombreArchivo: 'example3',
            tipoArchivo: 'example3',
            contenidoArchivo: null,
            ubicacionArchivo: 'example3',
            observaciones: 'example3',
            idSubtipoTramite: {
              id: 19,
              nombre: 'Certificado de cobertura ARL en el exterior',
              descripcion: 'Certificado de cobertura ARL en el exterior',
              padreId: 10
            },
            idTramite: null
          },
          {
            id: 6,
            nombreArchivo: 'example4',
            tipoArchivo: 'example4',
            contenidoArchivo: null,
            ubicacionArchivo: 'example4',
            observaciones: 'example4',
            idSubtipoTramite: {
              id: 20,
              nombre: 'Certificado medicina prepagada',
              descripcion: 'Certificado medicina prepagada',
              padreId: 10
            },
            idTramite: null
          }
    ]
};

export const MOCK_TRAMITE_CONSULTADO: TramiteDTO = {
    id: 1,
    idInstancia: 170,
    idTipoIdentificacion: {
        idTipoIdentificacion: 1,
        nombreTipoIdentificacion: 'CC',
        descripcionTipoIdentificacion: 'Cédula de Ciudadanía'
    },
    identificacion: '80101082',
    fecha: '01/04/2020',
    idTipoTramite: {
        id: 3,
        nombre: 'Reporte de incapacidad',
        descripcion: 'Reporte de incapacidad',
        padreId: null
    },
    estado: 'string',
    nombreEmpleado: 'Jhon Jairo',
    apellidoEmpleado: 'Morales Vargas',
    idArea: {
        id: 3,
        nombre: 'Gerencia de operaciones',
        descripcion: '',
        nombreBpm: 'Gerencia de operaciones'
    },
    idGestorTramite: {
        id: 9,
        identificacion: "80101089",
        usuario: "JefeInmediato02",
        nombre: "JefeInmediato",
        apellido: "02",
        idArea: {
            id: 3,
            nombre: 'Gerencia de operaciones',
            descripcion: '',
            nombreBpm: 'Gerencia de operaciones'
        },
        idTipoIdentificacion: {
            idTipoIdentificacion: 1,
            nombreTipoIdentificacion: 'CC',
            descripcionTipoIdentificacion: 'Cédula de Ciudadanía'
        },
        idRol: null
    },
    respuestaTramite: '',
    vacacionesTramite: {
        id: '1',
        idTramite: '12',
        diasDisponibles: 10,
        solicitaDiasAnticipados: true,
        totalDiasSolicitados: 12,
        totalDiasAnticipados: 2,
        fechaInicio: '01/04/2020',
        fechaFin: '13/04/2020',
        fechaReintegro: '14/04/2020',
        observaciones: 'Pedido de días disponibles',
    },
    detalleTramite: {
        id: 1,
        idTramite: '12',
        diasSolicitados: 12,
        fechaDiaSolicitado: '13/04/2020',
        horaInicio: '14:00',
        horaFin: '18:00',
        fechaInicio: null,
        fechaFin: null,
        idSubtipoTramite: {
            id: 12,
            nombre: 'Cita médica',
            descripcion: 'Cita médica',
            padreId: 2
        },
        otroMotivo: 'Consulta de exámenes',
        observaciones: 'Las actividades se encuentran al día',
        idTipoDocumentoTramite: {
            id: 2,
            nombre: 'Cita médica',
            obligatorio: false,
            descripcion: '',
            idTipoTramite: {
                id: 2,
                nombre: 'Permisos',
                descripcion: 'Permisos',
                padreId: null
            },
            idSubtipoTramite: {
                id: 12,
                nombre: 'Cita médica',
                descripcion: 'Cita médica',
                padreId: 2
            }
        },
        nombreArchivo: "Incapacidad.pdf",
        tipoArchivo: 'application/pdf',
        contenidoArchivo: 'string',
        ubicacionArchivo: 'string',
    },
    certificadoTramiteList: [
        {
            id: 2,
            nombreArchivo: 'example',
            tipoArchivo: 'example',
            contenidoArchivo: null,
            ubicacionArchivo: 'example',
            observaciones: 'example',
            idSubtipoTramite: {
              id: 16,
              nombre: 'Certificado laboral estándar',
              descripcion: 'Certificado laboral estándar',
              padreId: 10
            },
            idTramite: null
          },
          {
            id: 3,
            nombreArchivo: 'example1',
            tipoArchivo: 'example1',
            contenidoArchivo: null,
            ubicacionArchivo: 'example1',
            observaciones: 'example1',
            idSubtipoTramite: {
              id: 17,
              nombre: 'Certificado laboral con funciones',
              descripcion: 'Certificado laboral con funciones',
              padreId: 10
            },
            idTramite: null
          },
          {
            id: 4,
            nombreArchivo: 'example2',
            tipoArchivo: 'example2',
            contenidoArchivo: null,
            ubicacionArchivo: 'example2',
            observaciones: 'example2',
            idSubtipoTramite: {
              id: 18,
              nombre: 'Certificado ingresos y retenciones',
              descripcion: 'Certificado ingresos y retenciones',
              padreId: 10
            },
            idTramite: null
          },
          {
            id: 5,
            nombreArchivo: 'example3',
            tipoArchivo: 'example3',
            contenidoArchivo: null,
            ubicacionArchivo: 'example3',
            observaciones: 'example3',
            idSubtipoTramite: {
              id: 19,
              nombre: 'Certificado de cobertura ARL en el exterior',
              descripcion: 'Certificado de cobertura ARL en el exterior',
              padreId: 10
            },
            idTramite: null
          },
          {
            id: 6,
            nombreArchivo: 'example4',
            tipoArchivo: 'example4',
            contenidoArchivo: null,
            ubicacionArchivo: 'example4',
            observaciones: 'example4',
            idSubtipoTramite: {
              id: 20,
              nombre: 'Certificado medicina prepagada',
              descripcion: 'Certificado medicina prepagada',
              padreId: 10
            },
            idTramite: null
          }
    ]
};

export const MOCK_MANAGEMENT_PROCESS_MOST_RECENT_IMMEDIATE_BOSS: GestionTramiteDTO = {
    id: 1,
    idTramite: 101,
    fecha: '13/04/2020',
    revisadoPor: 'Diana Buitrago',
    idRol: {
        id: 2,
        nombre: 'Jefe inmediato',
        descripcion: '',
        nombreBpm: 'JefeInmediato'
      },
    observaciones: 'Por favor adjuntar soporte',
    respuestaTramite: 'Aprobar',
    permisoRemunerado: 'Si',
    requiereReemplazo: 'Si',
    nombreReemplazo: 'Jhon Morales',
    idIncapacidad: null
};

export const MOCK_MANAGEMENT_PROCESS_MOST_RECENT_AREA_MANAGER: GestionTramiteDTO = {
    id: 10,
    idTramite: 101,
    fecha: '20/04/2020',
    revisadoPor: 'Carolina Gomez',
    idRol: {
        id: 3,
        nombre: 'Gerente de área',
        descripcion: '',
        nombreBpm: 'gerenteArea'
      },
    observaciones: 'Por favor aplicar',
    respuestaTramite: 'Aprobar',
    permisoRemunerado: 'Si',
    requiereReemplazo: 'Si',
    nombreReemplazo: 'Juan Torres',
    idIncapacidad: null
};