import {Component,  OnInit} from '@angular/core';
import {LoginService} from './infraestructure/services/login.service';
import {UserDTO} from './domain/model/dto/UserDTO';




@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit  {

    currentUser: UserDTO;

    constructor( private loginService: LoginService){
        this.loginService.currentUser.subscribe(x => this.currentUser = x);
    }

    ngOnInit(){

    }


}
