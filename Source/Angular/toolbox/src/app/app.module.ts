import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
// NGRX
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from 'src/environments/environment';
// Componenst
import { AppComponent } from './app.component';
import { APP_ROUTES_MODULE } from './app.routes';
import { metaReducers, reducers } from './infraestructure/redux_store/reducers';
// Services
import { ServiceModule } from './infraestructure/services/service.module';
// PrimeNG Modules
import { PRIMENG_MODULES } from './shared/primeNg/primeng-elements';
import { PagesModule } from './ui/components/pages.module';
import {QRCodeModule} from 'angularx-qrcode';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        QRCodeModule,
        BrowserModule,
        FormsModule,
        ServiceModule,
        HttpClientModule,
        PagesModule,
        BrowserAnimationsModule,
        ...PRIMENG_MODULES,
        StoreModule.forRoot(reducers, { metaReducers }),
        APP_ROUTES_MODULE,
        StoreRouterConnectingModule,
        !environment.production
            ? StoreDevtoolsModule.instrument({ maxAge: 50 }) : []
    ],
    providers: [
        { provide: LocationStrategy, useClass: HashLocationStrategy }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
