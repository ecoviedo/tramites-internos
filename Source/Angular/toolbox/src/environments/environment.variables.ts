export const deploydVersion = {
    version: '0.0.3'
}

export const processContainer = {
    containerId: 'TramitesInternos_2.0.0-SNAPSHOT',
    version: '2.0.0-SNAPSHOT'
}

export const processes = {
    processId: 'process.TramitesInternos',
    processName: 'Tramites Internos',
    processVersion: '1.0'
}

export const constantCompanyAreaCode = {
    operationsManagementCode: '3'
}

export const constantProcessRoleCode = {
    employeeCode: 1,
    immediateBossCode: 2,
    areaManagerCode: 3,
    areaManagerGHCode: 4
}

export const constantRequestType = {
    holidayCode: 1,
    permissionsCode: 2,
    disabilityReportCode: 3,
    familyDayCode: 4,
    birthdayCode: 5,
    domesticCalamityCode: 6,
    mourningLicenseCode: 7,
    compensatoryCode: 8,
    compensatoryVoteCode: 9,
    certificationCode: 10,
    otherCodePermissions: 14,
    otherCodeInability: 31
}

export const constantDecisionType = {
    yes: '1',
    no: '2'
}

export const states = {
    create: 'Created',
    ready: 'Ready',
    reserved: 'Reserved',
    inProgress: 'InProgress',
    suspended: 'Suspended',
    completed: 'Completed',
    failed: 'Failed',
    error: 'Error',
    exited: 'Exited',
    obsolete: 'Obsolete'
};

export const statesTaskActions = {
    claimed: 'claimed',
    released: 'released',
    delegated: 'delegated',
    completed: 'completed',
    activated: 'activated',
    forwarded: 'forwarded',
    nominated: 'nominated',
    exited: 'exited',
    started: 'started',
    stopped: 'stopped',
    suspended: 'suspended',
    resumed: 'resumed',
    failed: 'failed',
    skipped: 'skipped'
};

export const nombreTareas = {
    radicarTramite: 'Radicar Tramite',
    gestionarTramite: 'Gestionar Tramite'
}

export const codigosParametros = {
    tipoSolicitud: '@TS',
    tipoDocumento: '@TD',
    motivoSolciitud: '@MS',
    tipoCertificado: '@TC',
    clasificacionIncapacidad: '@CI',
    siNo: '@SN',
    tipoDocumental: '@TC',
    accionSeguir: '@VB'
};
export const documento = {
    documento1: '123456789',

};

export const consPantalla = {
    crearTramite: 'CREAR_TRAMITE',
    modificarTramite: 'MODIFICAR_TRAMITE',
    gestionarSolicitudJefe: 'GESTIONAR_SOLICITUD_JEFE',
    gestionarSolicitudGerente: 'GESTIONAR_SOLICITUD_GERENTE',
    gestionarSolicitudGH: 'GESTIONAR_SOLICITUD_GH'
}

export const actividades = {
    radicarSolicitud: 'RADICAR_SOLICITUD',
    aprobacionGerente: 'APROBACION_GERENTE'
};

export const DateEsEes = {
    firstDayOfWeek: 1,
    dayNames: ['domingo', 'lunes', 'martes', 'miércoles', 'jueves', 'viernes', 'sábado'],
    dayNamesShort: ['dom', 'lun', 'mar', 'mié', 'jue', 'vie', 'sáb'],
    dayNamesMin: ['D', 'L', 'M', 'X', 'J', 'V', 'S'],
    monthNames: ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre',
        'octubre', 'noviembre', 'diciembre'],
    monthNamesShort: ['ene', 'feb', 'mar', 'abr', 'may', 'jun', 'jul', 'ago', 'sep', 'oct', 'nov', 'dic'],
    today: 'Hoy',
    clear: 'Borrar'
};

export const successResponse = {
    type: 'SUCCESS',
    statusCode: '200'
};

export const createdResponse = {
    type: 'SUCCESS',
    statusCode: '201'
};

export const userAuthenticationSuccessful = {
    type: 'SUCCESS',
    msg: 'Authentication successful',
    statusCode: '200'
};

export const failUserNotFound = {
    type: 'FAIL',
    msg: 'User not found',
    statusCode: '404'
};

export const ticketAlreadyExists = {
    type: 'ERROR',
    msg: 'Este número de radicado ya existe',
    statusCode: '500'
};

export const failureType = {
    error: 'ERROR',
    fail: 'FAIL'
};

export const failDemandNotFound = {
    type: 'ERROR',
    msg: 'Demand not found',
    statusCode: '404'
};

export const filingUser = {
    user: 'Ciudadano1',
    password: 'Ciudadano1'
}

export const roles = {
    ciudadano: 'ciudadano',
    abogado: 'abogado',
    supervisor: 'supervisor',
    administrador: 'Administrador'
}

export const estadoDemanda = {
    radicado: 'radicado',
    revision: 'revision',
    aprovado: 'aprovado',
    rechazada: 'rechazada'
}

export const estadosDemanda = {
    radicacion: 'radicación',
    evaluacion: 'evaluacion',
    acepatada: 'acepatada',
    cancelada: 'cancelada'
}

export const canalNotificacion = {
    email: 'EMAIL',
    mensaje: 'SMS'
}

export const failUserNotAuthorized = {
    type: 'FAIL',
    msg: 'User Not Authorized',
    statusCode: '204'
}

export const failNoDataNofound = {
    type: 'FAIL',
    msg: 'NO_CONTENT',
    statusCode: '204'
}