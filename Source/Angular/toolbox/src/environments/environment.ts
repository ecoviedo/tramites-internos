// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.

import { deprecate } from "util";


const hostZuulGateway = '192.168.1.59';
const port = ':8089';
const api = '/api';
const version = '/v1';
const ambiente = '/Dev';

//obsoleto
const hostTablasBasicas = `http://${hostZuulGateway}:8089/srv-tablas-basicas`;

const hostTipoTramite = `http://192.168.1.59:8089/srv-consult-types-procedures`;
const hostCatalogos = `http://192.168.1.59:8089/srv-consult-catalogs`;
const hostTipoDocTramite = `http://192.168.1.59:8089/srv-consult-types-document-procedure`;
const hostGestorTramite = `http://192.168.1.59:8089/srv-consult-managers-procedures`;
const hostFechaReintegro = `http://192.168.1.59:8089/srv-dias-habiles`;
const hostFechaFin = `http://192.168.1.59:8089/srv-dias-habiles`;
//Servicios Externos
const hostDiasDisponibles = `http://192.168.1.59:8089/srv-consult-days-availables`;

const hostRolTramite = `http://192.168.1.59:8089/srv-consult-role-procedure`;
//const hostApiBpm = `http://localhost:8090/api`;
//const hostApiBpm = `http://192.168.1.59:8090/ApiJbpm/api`;
const hostApiBpm = `http://192.168.1.59:8090/api`;
//const userAdm = `http://192.168.1.59:8089/srv-api-autentication/api`;
const userAdm = `http://192.168.1.59:8012/api`;
//Radicar Tramite
const hostRadicacion = `http://192.168.1.59:8089/srv-driving-procedure`;
const hostEmpleado = `http://192.168.1.59:8089/srv-empleado`;
const hostJefeInmediato = `http://192.168.1.59:8089/srv-aprob-jefe-inmediato`;
const hostGerente = `http://192.168.1.59:8089/srv-aprob-gerente`;
const hostRecursosHumanos = `http://192.168.1.59:8089/srv-aprob-rrhh`;

export const environment = {
    production: false,
    baseUrl: '',
    // Ldap
    user_endPoint: `${userAdm}/user`,
    
    // Parametros
    tablasBasicas_endPoint: `${hostTablasBasicas}/api/v1`,
    // Catalogos
    catalogos_endPoint: `${hostCatalogos}${ambiente}/api/core/catalogue/catalogs${version}`,
    // TipoTramite
    tipoTramite_endPoint: `${hostTipoTramite}${ambiente}/api/core/procedure/types${version}/`,
    // TipoDocTramite
    tipoDocTramite_endPoint: `${hostTipoDocTramite}${ambiente}/api/core/procedure/document-types${version}`,
    // FechaReintegro
    fechaReintegro_endPoint: `${hostFechaReintegro}${ambiente}/api/core/common/refund-date/date-range${version}`,
    // FechaFin
    fechaFin_endPoint: `${hostFechaFin}${ambiente}/api/core/common/refund-date/requested-days${version}`,
    // GestorTramite
    gestorTramite_endPoint: `${hostGestorTramite}${ambiente}/api/core/managers/procedures${version}`,
    // Dias Disponibles
    diasDisponibles_endPoint: `${hostDiasDisponibles}${ambiente}/api/core/days/available/employee${version}`,
    // RolTramite
    rolTramite_endPoint: `${hostRolTramite}${ambiente}/api/core/role/user${version}`,
    // Solicitud
    radicarSolicitud: `${hostRadicacion}${ambiente}/api/core/procedure/`,
    // Jefe Inmediato
    jefeImediato: `${hostJefeInmediato}/api/v1`,
    // Gerente
    gerente: `${hostGerente}/api/v1`,
    // Recursos Humanos
    recursosHumanos: `${hostRecursosHumanos}/api/v1`,
    // Empleado
    consultarEmpleado: `${hostEmpleado}/api/v1`,
    // Api BPM
    contenedor_endPoint: `${hostApiBpm}/container`,
    proceso_endPoint: `${hostApiBpm}/process`,
    tarea_endPoint: `${hostApiBpm}/task`,
    regla_endPoint: `${hostApiBpm}/rule`
};
