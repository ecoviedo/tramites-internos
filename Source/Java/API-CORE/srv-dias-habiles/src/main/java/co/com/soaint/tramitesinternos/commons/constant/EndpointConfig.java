package co.com.soaint.tramitesinternos.commons.constant;

public interface EndpointConfig {

    String BASE_ENDPOINT = "/Dev/api/core/common/refund-date";
    String GET_DAYENABLE = "/requested-days/v1";
    String GET_DAYENABLE_BETWEENDATES = "/date-range/v1";
}
