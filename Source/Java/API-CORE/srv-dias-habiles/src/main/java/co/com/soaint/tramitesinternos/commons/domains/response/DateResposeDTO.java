package co.com.soaint.tramitesinternos.commons.domains.response;

import co.com.soaint.tramitesinternos.commons.dto.StatusDTO;
import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class DateResposeDTO {
    private String dateEnable;
    private String refundDate;
    private int daysEnable;
    private StatusDTO statusDTO;
}
