package co.com.soaint.tramitesinternos.services.impl;

import co.com.soaint.tramitesinternos.commons.domains.util.DateUtil;
import co.com.soaint.tramitesinternos.commons.domains.response.DateResposeDTO;
import co.com.soaint.tramitesinternos.commons.dto.StatusDTO;
import co.com.soaint.tramitesinternos.commons.exception.system.SystemException;
import co.com.soaint.tramitesinternos.services.IServiceDateManagement;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Service
public class ServiceDateManagement implements IServiceDateManagement {

    @Override
    public DateResposeDTO findNextDateEnable(String date, int days) {
        try {
            Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(date);
            Date response = DateUtil.getNextDayEnabled(date1,days);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(response);
            calendar.add(Calendar.DATE, -1);
            String DateResposeString = new SimpleDateFormat("dd/MM/yyyy").format(calendar.getTime());
            String refundDate = new SimpleDateFormat("dd/MM/yyyy").format(response);
            StatusDTO statusDTO = StatusDTO.builder()
                    .status(HttpStatus.OK.name())
                    .statusCode(String.valueOf(HttpStatus.OK.value()))
                    .msg("Consult successful")
                    .build();
            DateResposeDTO resposeDTO = DateResposeDTO.builder()
                    .daysEnable(days).dateEnable(DateResposeString)
                    .refundDate(refundDate)
                    .statusDTO(statusDTO)
                    .build();
            return resposeDTO;
        }catch (ParseException e) {
            e.printStackTrace();
            throw new SystemException();
        }catch (SystemException ex){
            throw new SystemException();
        }
    }

    @Override
    public DateResposeDTO numberDaysBetweenDates(String dateIni, String dateEnd) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date dateI = formatter.parse(dateIni);
            Date dateE = formatter.parse(dateEnd);
            Date dateOriginal = formatter.parse(dateIni);
            int daysResponse = DateUtil.countDateEnable(dateI,dateE);
            Date dateResponse = DateUtil.getNextDayEnabled(dateOriginal, daysResponse);
            StatusDTO statusDTO = StatusDTO.builder()
                    .status(HttpStatus.OK.name())
                    .statusCode(String.valueOf(HttpStatus.OK.value()))
                    .msg("Consult successful")
                    .build();
            DateResposeDTO resposeDTO = DateResposeDTO.builder()
                    .dateEnable(new SimpleDateFormat("dd/MM/yyyy").format(dateResponse))
                    .daysEnable(daysResponse)
                    .statusDTO(statusDTO)
                    .build();
            return resposeDTO;
        }catch (ParseException e){
            e.printStackTrace();
            throw new SystemException();
        }catch (SystemException ex){
            throw new SystemException();
        }
    }
}
