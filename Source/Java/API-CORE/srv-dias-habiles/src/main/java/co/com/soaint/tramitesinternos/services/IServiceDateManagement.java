package co.com.soaint.tramitesinternos.services;

import co.com.soaint.tramitesinternos.commons.domains.response.DateResposeDTO;

public interface IServiceDateManagement {

    DateResposeDTO findNextDateEnable(String date, int days);

    DateResposeDTO numberDaysBetweenDates(String dateIni, String dateEnd);
}
