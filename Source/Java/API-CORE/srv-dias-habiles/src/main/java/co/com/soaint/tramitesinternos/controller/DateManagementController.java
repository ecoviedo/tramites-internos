package co.com.soaint.tramitesinternos.controller;

import co.com.soaint.tramitesinternos.commons.constant.EndpointConfig;
import co.com.soaint.tramitesinternos.services.IServiceDateManagement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.QueryParam;

@RestController
@RequestMapping(EndpointConfig.BASE_ENDPOINT)
@Component
public class DateManagementController {

    private final IServiceDateManagement serviceDateManagement;

    @Autowired
    public DateManagementController(IServiceDateManagement serviceDateManagement) {
        this.serviceDateManagement = serviceDateManagement;
    }

    @GetMapping(EndpointConfig.GET_DAYENABLE)
    public ResponseEntity<?> findNextDayEnable(@QueryParam("date") String date,
                                               @QueryParam("days") int days) {

        return new ResponseEntity<>(serviceDateManagement.findNextDateEnable(date, days), HttpStatus.OK);
    }

    @GetMapping(EndpointConfig.GET_DAYENABLE_BETWEENDATES)
    public ResponseEntity<?> DayEnableBetweenDates(@QueryParam("dateIni") String dateIni,
                                                   @QueryParam("dateFin") String dateFin) {

        return new ResponseEntity<>(serviceDateManagement.numberDaysBetweenDates(dateIni, dateFin), HttpStatus.OK);
    }
}
