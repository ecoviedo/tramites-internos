package co.com.soaint.tramitesinternos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class SrvDiasHabilesApplication {

    public static void main(String[] args) {
        SpringApplication.run(SrvDiasHabilesApplication.class, args);
    }

}
