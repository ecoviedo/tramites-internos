package co.com.soaint.tramitesinternos.services.impl;

import co.com.soaint.tramitesinternos.commons.dto.ResponseDTO;
import co.com.soaint.tramitesinternos.commons.dto.StatusDTO;
import co.com.soaint.tramitesinternos.commons.exception.business.ResponseNotFoundException;
import co.com.soaint.tramitesinternos.commons.exception.system.SystemException;
import co.com.soaint.tramitesinternos.commonscomponents.dtos.GestorTramiteDTO;
import co.com.soaint.tramitesinternos.commonscomponents.entities.GestorTramite;
import co.com.soaint.tramitesinternos.repository.IGestorTramiteRepository;
import co.com.soaint.tramitesinternos.services.IServicesManagersProcedures;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Component
public class ServicesManagersProcedures implements IServicesManagersProcedures{

    private final IGestorTramiteRepository gestorTramiteRepository;

    @Autowired
    public ServicesManagersProcedures(IGestorTramiteRepository gestorTramiteRepository) {
        this.gestorTramiteRepository = gestorTramiteRepository;
    }

    public ResponseDTO findAllUser(){
        try {
            List<GestorTramite> gestorTramites = gestorTramiteRepository.findAll();
            List<GestorTramiteDTO> tramiteDTOS = new ArrayList<>();
            for (GestorTramite dto : gestorTramites) {
                GestorTramiteDTO gestorTramiteDTO = GestorTramiteDTO.builder()
                        .id(dto.getId())
                        .identificacion(dto.getIdentificacion())
                        .idTipoIdentificacion(dto.getIdTipoIdentificacion().getId())
                        .usuario(dto.getUsuario())
                        .nombre(dto.getNombre())
                        .apellido(dto.getApellido())
                        .idArea(dto.getIdArea().getId())
                        .idRol(dto.getIdRol().getId()).build();
                tramiteDTOS.add(gestorTramiteDTO);
            }
            if (tramiteDTOS.size() == 0) {
                throw new ResponseNotFoundException();
            } else {
                StatusDTO statusDTO = StatusDTO.builder()
                        .status(HttpStatus.OK.name())
                        .statusCode(String.valueOf(HttpStatus.OK.value()))
                        .msg("Consult successful").build();
                return new ResponseDTO(tramiteDTOS, statusDTO);
            }
        }catch (SystemException ex){
            throw new SystemException();
        }
    }
}
