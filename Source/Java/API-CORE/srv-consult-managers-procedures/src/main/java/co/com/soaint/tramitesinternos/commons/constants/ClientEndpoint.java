package co.com.soaint.tramitesinternos.commons.constants;

public interface ClientEndpoint {
    public final String BASE_ENDPOINT = "/Dev/api/core/managers/procedures/v1";
}
