package co.com.soaint.tramitesinternos.controller;

import co.com.soaint.tramitesinternos.commons.constants.ClientEndpoint;
import co.com.soaint.tramitesinternos.commons.dto.ResponseDTO;
import co.com.soaint.tramitesinternos.services.IServicesManagersProcedures;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(ClientEndpoint.BASE_ENDPOINT)
@Component
public class ManagersProcedures {

    private final IServicesManagersProcedures iServicesManagersProcedures;

    @Autowired
    public ManagersProcedures(IServicesManagersProcedures iServicesManagersProcedures) {
        this.iServicesManagersProcedures = iServicesManagersProcedures;
    }

    @GetMapping
    public ResponseEntity<?> findAllUsers(){
        return new ResponseEntity<ResponseDTO>(iServicesManagersProcedures.findAllUser(), HttpStatus.OK);
    }
}
