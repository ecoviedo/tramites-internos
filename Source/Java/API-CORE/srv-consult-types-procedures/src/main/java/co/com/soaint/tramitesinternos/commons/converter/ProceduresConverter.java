package co.com.soaint.tramitesinternos.commons.converter;

import co.com.soaint.tramitesinternos.commons.dto.ResponseDTO;
import co.com.soaint.tramitesinternos.commonscomponents.dtos.TipoTramiteDTO;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;

public class ProceduresConverter{

    public static void Converte(ModelMapper modelMapper, List<?> objects, ResponseDTO responseDTO){
        List<TipoTramiteDTO> tramiteDTOS = new ArrayList<>();
        for(int i=0;i<objects.size();i++){
            tramiteDTOS.add(modelMapper.map(objects.get(i), TipoTramiteDTO.class));
        }
        responseDTO.setProceduresTypes(tramiteDTOS);
    }
}
