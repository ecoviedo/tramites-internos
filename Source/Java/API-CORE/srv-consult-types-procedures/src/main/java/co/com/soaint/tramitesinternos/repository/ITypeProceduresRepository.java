package co.com.soaint.tramitesinternos.repository;

import co.com.soaint.tramitesinternos.commonscomponents.entities.TipoTramite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ITypeProceduresRepository extends JpaRepository<TipoTramite, Integer> {

    @Query("select t FROM TipoTramite t where t.padreId = :parentId")
    List<TipoTramite> findByPadreId(@Param("parentId") Integer parentId);

    @Query("select tt from TipoTramite tt where tt.padreId is null")
    List<TipoTramite> findAllByPadreIdIsNull();
}
