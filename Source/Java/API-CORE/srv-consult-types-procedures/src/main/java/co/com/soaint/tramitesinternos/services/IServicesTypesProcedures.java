package co.com.soaint.tramitesinternos.services;

import co.com.soaint.tramitesinternos.commons.dto.ResponseDTO;

public interface IServicesTypesProcedures {

    public ResponseDTO findTypesProcedures(Integer parentCode);
}
