package co.com.soaint.tramitesinternos.commons.dto;

import co.com.soaint.tramitesinternos.commonscomponents.dtos.TipoTramiteDTO;
import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class ResponseDTO {
    private List<TipoTramiteDTO> proceduresTypes;
    private StatusDTO response;
}
