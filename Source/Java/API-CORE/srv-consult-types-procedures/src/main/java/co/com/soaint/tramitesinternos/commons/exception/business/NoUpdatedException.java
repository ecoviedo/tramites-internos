package co.com.soaint.tramitesinternos.commons.exception.business;


import co.com.soaint.tramitesinternos.commons.exception.generic.BaseRuntimeException;

public class NoUpdatedException extends BaseRuntimeException {

    public NoUpdatedException() {
    }
}
