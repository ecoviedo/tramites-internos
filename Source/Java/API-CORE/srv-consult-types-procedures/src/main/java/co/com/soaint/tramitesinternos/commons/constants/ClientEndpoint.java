package co.com.soaint.tramitesinternos.commons.constants;

public interface ClientEndpoint {
    public final String BASE_ENDPOINT = "Dev/api/core/procedure/types/v1";
    public final String FIND_TYPES_PROCEDURES = "/{ParentCode}";
}
