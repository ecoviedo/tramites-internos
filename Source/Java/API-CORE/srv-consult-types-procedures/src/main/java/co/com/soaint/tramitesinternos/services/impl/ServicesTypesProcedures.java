package co.com.soaint.tramitesinternos.services.impl;

import co.com.soaint.tramitesinternos.commons.converter.ProceduresConverter;
import co.com.soaint.tramitesinternos.commons.dto.ResponseDTO;
import co.com.soaint.tramitesinternos.commons.dto.StatusDTO;
import co.com.soaint.tramitesinternos.commons.exception.business.ResponseNotFoundException;
import co.com.soaint.tramitesinternos.commons.exception.system.SystemException;
import co.com.soaint.tramitesinternos.commonscomponents.entities.TipoTramite;
import co.com.soaint.tramitesinternos.repository.ITypeProceduresRepository;
import co.com.soaint.tramitesinternos.services.IServicesTypesProcedures;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ServicesTypesProcedures implements IServicesTypesProcedures {

    @Autowired
    private ModelMapper modelMapper;

    private final ITypeProceduresRepository iTypeProceduresRepository;

    @Autowired
    public ServicesTypesProcedures(ITypeProceduresRepository iTypeProceduresRepository) {
        this.iTypeProceduresRepository = iTypeProceduresRepository;
    }

    @Override
    public ResponseDTO findTypesProcedures(Integer parentCode) {
        try {
            ResponseDTO responseDTO = new ResponseDTO();
            List<TipoTramite> tipoTramites;
            if(parentCode != null) {
                tipoTramites = iTypeProceduresRepository.findByPadreId(parentCode);
            }else {
                tipoTramites = iTypeProceduresRepository.findAllByPadreIdIsNull();
            }
            ProceduresConverter.Converte(modelMapper, tipoTramites, responseDTO);
            if (responseDTO.getProceduresTypes().size() == 0) {
                throw new ResponseNotFoundException();
            }
            StatusDTO statusDTO = StatusDTO.builder()
                    .status(HttpStatus.OK.name())
                    .statusCode(String.valueOf(HttpStatus.OK.value()))
                    .msg("Consult successful").build();
            responseDTO.setResponse(statusDTO);
            return responseDTO;
        } catch (SystemException ex) {
            throw new SystemException();
        }
    }
}
