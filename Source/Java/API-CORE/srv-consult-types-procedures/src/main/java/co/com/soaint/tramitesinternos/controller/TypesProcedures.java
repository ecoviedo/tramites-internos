package co.com.soaint.tramitesinternos.controller;

import co.com.soaint.tramitesinternos.commons.constants.ClientEndpoint;
import co.com.soaint.tramitesinternos.commons.dto.ResponseDTO;
import co.com.soaint.tramitesinternos.services.IServicesTypesProcedures;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.QueryParam;

@RestController
@RequestMapping(ClientEndpoint.BASE_ENDPOINT)
@Component
public class TypesProcedures {
    private final IServicesTypesProcedures iServicesTypesProcedures;

    @Autowired
    public TypesProcedures(IServicesTypesProcedures iServicesTypesProcedures) {
        this.iServicesTypesProcedures = iServicesTypesProcedures;
    }

    @GetMapping("/")
    public ResponseEntity<?> findTypesProcedures(@QueryParam("parentCode") Integer parentCode){
        return new ResponseEntity<ResponseDTO>(iServicesTypesProcedures.findTypesProcedures(parentCode), HttpStatus.OK);
    }
}
