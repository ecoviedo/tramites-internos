package co.com.soaint.tramitesinternos.commons.dto;

import lombok.*;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class RequestValidaDTO {
    private String identificacion;
    private Integer idTipoIdentificaion;
    private String tipoTramite;
    private Date diaSolicitado;
    private Boolean tramiteHabilitado;
    private StatusDTO response;
}
