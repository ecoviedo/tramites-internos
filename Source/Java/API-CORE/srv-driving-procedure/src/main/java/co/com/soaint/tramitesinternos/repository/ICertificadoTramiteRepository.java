package co.com.soaint.tramitesinternos.repository;

import co.com.soaint.tramitesinternos.commonscomponents.entities.CertificadoTramite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ICertificadoTramiteRepository extends JpaRepository<CertificadoTramite, Integer> {

    @Query("SELECT ct FROM CertificadoTramite  ct where ct.idTramite.id = :idTramite")
    List<CertificadoTramite> findAllByIdTramite(@Param("idTramite") Integer idTramite);
}
