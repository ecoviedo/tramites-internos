package co.com.soaint.tramitesinternos.services;

import co.com.soaint.tramitesinternos.commons.dto.RequestDTO;
import co.com.soaint.tramitesinternos.commons.dto.RequestValidaDTO;
import co.com.soaint.tramitesinternos.commons.dto.ResponseDTO;
import co.com.soaint.tramitesinternos.commons.dto.ResponseTramiteDTO;

public interface ISerivicesManagementProcedure {

    ResponseDTO createProcedure(RequestDTO requestDTO);

    ResponseDTO updateProcedure(RequestDTO requestDTO);

    ResponseTramiteDTO findProcedure(Integer idInstancia);

    public RequestValidaDTO findProcedureBySemester(RequestValidaDTO requestValidaDTO);

}
