package co.com.soaint.tramitesinternos.repository;

import co.com.soaint.tramitesinternos.commonscomponents.entities.Tramite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import javax.xml.crypto.Data;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface ITramiteRepository extends JpaRepository<Tramite, Integer> {

    @Query("SELECT t FROM Tramite t where t.idInstancia = :idInstancia")
    Optional<Tramite> findTramiteByIdInstancia(@Param("idInstancia") Integer idInstancia);

    @Query("SELECT t FROM Tramite t " +
            "inner JOIN DetalleTramite dt ON t.id = dt.idTramite.id " +
            "where t.identificacion=:identificacion " +
            "and t.idTipoIdentificacion.id=:idTipoIdentificacion " +
            "and dt.fechaDiaSolicitado BETWEEN :fechaInicio AND :fechaFin")
    Optional<Tramite> findtramitebyidentificacion(@Param("identificacion") String identificacion,
                                                  @Param("idTipoIdentificacion") Integer idTipoIdentificacion,
                                                  @Param("fechaInicio") Date fechaInicio,
                                                  @Param("fechaFin")Date fechaFin);
}
