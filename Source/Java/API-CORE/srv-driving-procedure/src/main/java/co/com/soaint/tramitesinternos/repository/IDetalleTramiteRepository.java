package co.com.soaint.tramitesinternos.repository;

import co.com.soaint.tramitesinternos.commonscomponents.entities.DetalleTramite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface IDetalleTramiteRepository extends JpaRepository<DetalleTramite, Integer> {

    @Query("SELECT dt FROM DetalleTramite dt WHERE dt.idTramite.id = :idTramite")
    DetalleTramite findAllByIdTramite(@Param("idTramite") Integer idTramite);
}
