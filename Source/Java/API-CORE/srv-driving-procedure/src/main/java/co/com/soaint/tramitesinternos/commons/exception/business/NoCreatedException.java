package co.com.soaint.tramitesinternos.commons.exception.business;


import co.com.soaint.tramitesinternos.commons.exception.generic.BaseRuntimeException;

public class NoCreatedException extends BaseRuntimeException {

    public NoCreatedException() {
    }
}
