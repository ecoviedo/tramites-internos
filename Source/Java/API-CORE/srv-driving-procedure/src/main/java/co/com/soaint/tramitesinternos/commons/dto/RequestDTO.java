package co.com.soaint.tramitesinternos.commons.dto;

import co.com.soaint.tramitesinternos.commonscomponents.dtos.*;
import lombok.*;

import java.util.List;
import java.util.Optional;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class RequestDTO {
    private Integer idArea;
    private Integer idGestorTramite;
    private Integer idTipoIdentificacion;
    private Integer idTipoTramite;
    private TramiteDTO tramite;
    private List<GestionTramiteDTO> gestionTramite;
    Optional<VacacionesTramiteDTO> vacacionesTramite;
    Optional<List<CertificadoTramiteDTO>> certificadoTramite;
    Optional<DetalleTramiteDTO> detalleTramite;
}
