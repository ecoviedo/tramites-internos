package co.com.soaint.tramitesinternos.commons.converter;

import co.com.soaint.tramitesinternos.commonscomponents.dtos.TramiteDTO;
import co.com.soaint.tramitesinternos.commonscomponents.entities.Tramite;
import org.modelmapper.ModelMapper;

import java.util.Calendar;

public class ProceduresConverter {

    public static void tramiteConverte(ModelMapper modelMapper, TramiteDTO dto, Tramite tramite) {
        //mapeo el objeto tramiteDTO a la entidad
        modelMapper.map(dto, tramite);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(tramite.getFecha()); // Configuramos la fecha que se recibe
        calendar.add(Calendar.DAY_OF_YEAR, 1);  // numero de días a añadir, o restar en caso de días<0
        tramite.setFecha(calendar.getTime());
        modelMapper.map(dto.getIdArea(), tramite.getIdArea());
        modelMapper.map(dto.getIdTipoIdentificacion(), tramite.getIdTipoIdentificacion());
        if (tramite.getIdGestorTramite() != null) {
            modelMapper.map(dto.getIdGestorTramite(), tramite.getIdGestorTramite());
        }
        modelMapper.map(dto.getIdTipoTramite(), tramite.getIdTipoTramite());

    }

    public static void tramiteDTOConverte(ModelMapper modelMapper, Tramite tramite, TramiteDTO tramiteDTO) {

        modelMapper.map(tramite, tramiteDTO);
        if (tramiteDTO.getIdGestorTramite() != null) {
            tramiteDTO.getIdGestorTramite().setIdRol(null);
            tramiteDTO.getIdGestorTramite().setIdArea(null);
            tramiteDTO.getIdGestorTramite().setIdTipoIdentificacion(null);
        }
        if (tramiteDTO.getDetalleTramite() != null) {
            tramiteDTO.getDetalleTramite().setIdTramite(null);
        }
        if (tramiteDTO.getVacacionesTramite() != null) {
            tramiteDTO.getVacacionesTramite().setIdTramite(null);
        }
        if (tramiteDTO.getCertificadoTramiteList() != null) {
            for (int i = 0; i < tramiteDTO.getCertificadoTramiteList().size(); i++) {
                tramiteDTO.getCertificadoTramiteList().get(i).setIdTramite(null);
            }
        }

        for (int i = 0; i < tramiteDTO.getGestionTramiteList().size(); i++) {
            tramiteDTO.getGestionTramiteList().get(i).setIdTramite(null);
        }
    }


}
