package co.com.soaint.tramitesinternos.services.impl;

import co.com.soaint.tramitesinternos.commons.converter.ProceduresConverter;
import co.com.soaint.tramitesinternos.commons.dto.*;
import co.com.soaint.tramitesinternos.commons.exception.business.NoCreatedException;
import co.com.soaint.tramitesinternos.commons.exception.system.SystemException;
import co.com.soaint.tramitesinternos.commonscomponents.dtos.*;
import co.com.soaint.tramitesinternos.commonscomponents.entities.*;
import co.com.soaint.tramitesinternos.repository.*;
import co.com.soaint.tramitesinternos.services.ISerivicesManagementProcedure;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Component
public class SerivicesManagementProcedure implements ISerivicesManagementProcedure {

    @Autowired
    private ModelMapper modelMapper;

    @PersistenceContext
    EntityManager em;

    private final ITramiteRepository tramiteRepository;
    private final ITipoTramiteRepository tipoTramiteRepository;
    private final IAreaEmpresaRepository areaEmpresaRepository;
    private final IGestorTramiteRepository gestorTramiteRepository;
    private final ITipoIdentificacionRepository tipoIdentificacionRepository;
    private final IDetalleTramiteRepository detalleTramiteRepository;
    private final IVacacionesTramiteRepository vacacionesTramiteRepository;
    private final ICertificadoTramiteRepository certificadoTramiteRepository;
    private final IGestionTramiteRepository gestionTramiteRepository;

    @Autowired
    public SerivicesManagementProcedure(ITramiteRepository tramiteRepository, ITipoTramiteRepository tipoTramiteRepository, IAreaEmpresaRepository areaEmpresaRepository, IGestorTramiteRepository gestorTramiteRepository, ITipoIdentificacionRepository tipoIdentificacionRepository, IDetalleTramiteRepository detalleTramiteRepository, IVacacionesTramiteRepository vacacionesTramiteRepository, ICertificadoTramiteRepository certificadoTramiteRepository, IGestionTramiteRepository gestionTramiteRepository) {
        this.tramiteRepository = tramiteRepository;
        this.tipoTramiteRepository = tipoTramiteRepository;
        this.areaEmpresaRepository = areaEmpresaRepository;
        this.gestorTramiteRepository = gestorTramiteRepository;
        this.tipoIdentificacionRepository = tipoIdentificacionRepository;
        this.detalleTramiteRepository = detalleTramiteRepository;
        this.vacacionesTramiteRepository = vacacionesTramiteRepository;
        this.certificadoTramiteRepository = certificadoTramiteRepository;
        this.gestionTramiteRepository = gestionTramiteRepository;
    }

    @Override
    @Transactional
    public ResponseDTO createProcedure(RequestDTO requestDTO) {
        try {
            Tramite tramiteSave = tramite(requestDTO);
            //creo las otras entidades a partir del tramite que ya se creo y estas se las seteo al tramite
            //para luego persitir los datos a la base de datos
            //para el detalle del tramite primero se valida si viene en el request
            if (requestDTO.getDetalleTramite() != null) {
                requestDTO.getDetalleTramite().get().setIdTramite(modelMapper.map(tramiteSave, TramiteDTO.class));
                DetalleTramite detalleTramite = new DetalleTramite();
                modelMapper.map(requestDTO.getDetalleTramite().get(), detalleTramite);
                detalleTramiteRepository.save(detalleTramite);
            }
            //se realiaza el mismo proceso para las vacaciones del tramite
            if (requestDTO.getVacacionesTramite() != null) {
                requestDTO.getVacacionesTramite().get().setIdTramite(modelMapper.map(tramiteSave, TramiteDTO.class));
                VacacionesTramite vacacionesTramite = new VacacionesTramite();
                modelMapper.map(requestDTO.getVacacionesTramite().get(), vacacionesTramite);
                vacacionesTramiteRepository.save(vacacionesTramite);
            }
            //se valida que la lista a insertar venga en el request
            if (requestDTO.getCertificadoTramite() != null) {
                certificadoTramite(requestDTO.getCertificadoTramite().get(), tramiteSave);
            }
            StatusDTO statusDTO = StatusDTO.builder()
                    .status(HttpStatus.CREATED.name())
                    .statusCode(String.valueOf(HttpStatus.CREATED.value()))
                    .msg("Create successful").build();
            ResponseDTO responseDTO = ResponseDTO.builder()
                    .idTramite(tramiteSave.getId())
                    .response(statusDTO).build();
            return responseDTO;
        } catch (SystemException ex) {
            throw new SystemException();
        }
    }

    @Override
    @Transactional
    public ResponseDTO updateProcedure(RequestDTO requestDTO) {
        try {
            Tramite tramiteSave = tramite(requestDTO);
            //creo las otras entidades a partir del tramite que ya se creo y estas se las seteo al tramite
            //para luego persitir los datos a la base de datos
            //para el detalle del tramite primero se valida si viene en el request
            if (requestDTO.getDetalleTramite() != null) {
                DetalleTramite detalleTramite = detalleTramiteRepository.findAllByIdTramite(tramiteSave.getId());
                requestDTO.getDetalleTramite().get().setIdTramite(modelMapper.map(tramiteSave, TramiteDTO.class));
                DetalleTramite detalleTramiteUpdate = new DetalleTramite();
                modelMapper.map(requestDTO.getDetalleTramite().get(), detalleTramiteUpdate);
                detalleTramiteUpdate.setId(detalleTramite.getId());
                detalleTramiteRepository.save(detalleTramiteUpdate);
            }
            //se realiaza el mismo proceso para las vacaciones del tramite
            if (requestDTO.getVacacionesTramite() != null) {
                VacacionesTramite vacacionesTramite = vacacionesTramiteRepository.findAllByIdTramite(tramiteSave.getId());
                requestDTO.getVacacionesTramite().get().setIdTramite(modelMapper.map(tramiteSave, TramiteDTO.class));
                VacacionesTramite vacacionesTramiteUpdate = new VacacionesTramite();
                modelMapper.map(requestDTO.getVacacionesTramite().get(), vacacionesTramiteUpdate);
                vacacionesTramiteUpdate.setId(vacacionesTramite.getId());
                vacacionesTramiteRepository.save(vacacionesTramiteUpdate);
            }
            //se valida que la lista a insertar venga en el request
            if (requestDTO.getCertificadoTramite() != null) {
                List<CertificadoTramite> certificadoTramites = certificadoTramiteRepository.findAllByIdTramite(tramiteSave.getId());
                certificadoTramiteRepository.deleteAll(certificadoTramites);
                certificadoTramite(requestDTO.getCertificadoTramite().get(), tramiteSave);
            }
            StatusDTO statusDTO = StatusDTO.builder()
                    .status(HttpStatus.OK.name())
                    .statusCode(String.valueOf(HttpStatus.OK.value()))
                    .msg("Update successful").build();
            ResponseDTO responseDTO = ResponseDTO.builder()
                    .idTramite(tramiteSave.getId())
                    .response(statusDTO).build();
            return responseDTO;
        } catch (SystemException ex) {
            throw new SystemException();
        }
    }

    @Override
    public ResponseTramiteDTO findProcedure(Integer idInstancia) {
        try {
            TramiteDTO tramiteDTO = new TramiteDTO();
            ResponseTramiteDTO responseTramiteDTO = new ResponseTramiteDTO();
            Optional<Tramite> tramite = tramiteRepository.findTramiteByIdInstancia(idInstancia);
            StatusDTO statusDTO;
            if (!tramite.isPresent()) {
                statusDTO = StatusDTO.builder()
                        .status(HttpStatus.NO_CONTENT.name())
                        .statusCode(String.valueOf(HttpStatus.NO_CONTENT.value()))
                        .msg("Not Content").build();
            } else {
                statusDTO = StatusDTO.builder()
                        .status(HttpStatus.OK.name())
                        .statusCode(String.valueOf(HttpStatus.OK.value()))
                        .msg("Consult Successful").build();
                ProceduresConverter.tramiteDTOConverte(modelMapper, tramite.get(), tramiteDTO);
                responseTramiteDTO.setTramite(tramiteDTO);
            }
            responseTramiteDTO.setResponse(statusDTO);
            return responseTramiteDTO;
        } catch (SystemException ex) {
            throw new SystemException();
        }
    }

    @Override
    public RequestValidaDTO findProcedureBySemester(RequestValidaDTO requestValidaDTO) {
        try {
            String tipoTramite = requestValidaDTO.getTipoTramite();
            if (tipoTramite.equalsIgnoreCase("Dia de la familia") || tipoTramite.equalsIgnoreCase("Dia cumpleanos")) {
                int mesAnnon = requestValidaDTO.getDiaSolicitado().getMonth();
                String fechaIni = null;
                String fechaFin = null;
                Date inicio = null;
                Date fin = null;
                if (tipoTramite.equalsIgnoreCase("Dia de la familia")) {
                    if (mesAnnon <= 5) {
                        fechaIni = new SimpleDateFormat("yyyy").format(requestValidaDTO.getDiaSolicitado()) + "-01-01";
                        fechaFin = new SimpleDateFormat("yyyy").format(requestValidaDTO.getDiaSolicitado()) + "-06-30";
                    } else {
                        fechaIni = new SimpleDateFormat("yyyy").format(requestValidaDTO.getDiaSolicitado()) + "-07-01";
                        fechaFin = new SimpleDateFormat("yyyy").format(requestValidaDTO.getDiaSolicitado()) + "-12-31";
                    }
                } else if (tipoTramite.equalsIgnoreCase("Dia cumpleanos")) {
                    fechaIni = new SimpleDateFormat("yyyy").format(requestValidaDTO.getDiaSolicitado()) + "-01-01";
                    fechaFin = new SimpleDateFormat("yyyy").format(requestValidaDTO.getDiaSolicitado()) + "-12-31";
                }
                inicio = new SimpleDateFormat("yyyy-MM-dd").parse(fechaIni);
                fin = new SimpleDateFormat("yyyy-MM-dd").parse(fechaFin);
                Optional<Tramite> tramite = tramiteRepository.findtramitebyidentificacion(
                        requestValidaDTO.getIdentificacion(),
                        requestValidaDTO.getIdTipoIdentificaion(),
                        inicio,
                        fin);
               StatusDTO statusDTO = StatusDTO.builder()
                        .status(HttpStatus.OK.name())
                        .statusCode(String.valueOf(HttpStatus.OK.value()))
                        .msg("Consult Successful").build();
                requestValidaDTO.setResponse(statusDTO);
                if (!tramite.isPresent()) {
                    //armo la respuesta que si se puede hacer
                    requestValidaDTO.setTramiteHabilitado(true);
                    return requestValidaDTO;
                } else {
                    //armo la respuesta que ya se hizo
                    requestValidaDTO.setTramiteHabilitado(false);
                    return requestValidaDTO;
                }
            }else {
                StatusDTO statusDTO = StatusDTO.builder()
                        .status(HttpStatus.OK.name())
                        .statusCode(String.valueOf(HttpStatus.OK.value()))
                        .msg("Consult Successful").build();
                requestValidaDTO.setResponse(statusDTO);
                requestValidaDTO.setTramiteHabilitado(true);
                return requestValidaDTO;
            }
        } catch (ParseException e) {
            throw new SystemException();
        } catch (SystemException ex) {
            throw new SystemException();
        }
    }

    private Tramite tramite(RequestDTO requestDTO) {
        try {
            //se buscan y se settean los objetos que tienen relacion con el tramite
            requestDTO.getTramite().setIdArea(modelMapper.map(
                    areaEmpresaRepository.findById(requestDTO.getIdArea()).get(),
                    AreaEmpresaDTO.class));
            if (requestDTO.getIdGestorTramite() != null) {
                requestDTO.getTramite().setIdGestorTramite(modelMapper.map(
                        gestorTramiteRepository.findById(requestDTO.getIdGestorTramite()).get(),
                        GestorTramiteDTO.class));
            }
            requestDTO.getTramite().setIdTipoIdentificacion(modelMapper.map(
                    tipoIdentificacionRepository.findById(requestDTO.getIdTipoIdentificacion()).get(),
                    TipoIdentificacionDTO.class));
            requestDTO.getTramite().setIdTipoTramite(modelMapper.map(
                    tipoTramiteRepository.findById(requestDTO.getIdTipoTramite()).get(),
                    TipoTramiteDTO.class));
            //se convierte el dto a entity
            Tramite tramiteEntity = new Tramite();
            ProceduresConverter.tramiteConverte(modelMapper, requestDTO.getTramite(), tramiteEntity);
            Tramite tramiteSave = tramiteRepository.save(tramiteEntity);
            //se realiza el mismo procedimiento anterior para gestion Tramite
            List<GestionTramite> gestionTramites = new ArrayList<GestionTramite>();
            for (GestionTramiteDTO dto : requestDTO.getGestionTramite()) {
                dto.setIdTramite(modelMapper.map(tramiteSave, TramiteDTO.class));
                GestionTramite gestionCreate = new GestionTramite();
                modelMapper.map(dto, gestionCreate);
                if (dto.getIdIncapacidad() != null) {
                    modelMapper.map(dto.getIdIncapacidad(), gestionCreate.getIdIncapacidad());
                }
                gestionTramites.add(gestionCreate);
            }
            gestionTramiteRepository.saveAll(gestionTramites);
            return tramiteSave;
        } catch (SystemException ex) {
            throw new NoCreatedException();
        }
    }

    private void certificadoTramite(List<CertificadoTramiteDTO> certificadoTramiteDTOS, Tramite tramiteSave) {
        try {
            List<CertificadoTramite> certificadoTramitesUpdate = new ArrayList<CertificadoTramite>();
            for (CertificadoTramiteDTO dto : certificadoTramiteDTOS) {
                //mapeo el objeto a la entidad le agrego el tramite y lo guardo en la lista
                dto.setIdTramite(modelMapper.map(tramiteSave, TramiteDTO.class));
                CertificadoTramite certificadocreate = new CertificadoTramite();
                modelMapper.map(dto, certificadocreate);
                modelMapper.map(dto.getIdSubtipoTramite(), certificadocreate.getIdSubtipoTramite());
                certificadoTramitesUpdate.add(certificadocreate);
            }
            certificadoTramiteRepository.saveAll(certificadoTramitesUpdate);
        } catch (SystemException ex) {
            throw new NoCreatedException();
        }
    }

}
