package co.com.soaint.tramitesinternos.commons.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class ResponseDTO {
    private Integer idTramite;
    private StatusDTO response;
}
