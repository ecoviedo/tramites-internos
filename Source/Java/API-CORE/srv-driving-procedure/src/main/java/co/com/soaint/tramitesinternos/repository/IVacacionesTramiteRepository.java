package co.com.soaint.tramitesinternos.repository;

import co.com.soaint.tramitesinternos.commonscomponents.entities.VacacionesTramite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface IVacacionesTramiteRepository extends JpaRepository<VacacionesTramite, Integer> {

    @Query("SELECT vt FROM VacacionesTramite vt WHERE vt.idTramite.id = :idTramite")
    VacacionesTramite findAllByIdTramite(@Param("idTramite") Integer idTramite);
}
