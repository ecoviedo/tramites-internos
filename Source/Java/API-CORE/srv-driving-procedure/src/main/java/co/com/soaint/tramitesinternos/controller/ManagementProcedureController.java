package co.com.soaint.tramitesinternos.controller;

import co.com.soaint.tramitesinternos.commons.constants.ClientEndpoint;
import co.com.soaint.tramitesinternos.commons.dto.RequestDTO;
import co.com.soaint.tramitesinternos.commons.dto.RequestValidaDTO;
import co.com.soaint.tramitesinternos.services.ISerivicesManagementProcedure;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(ClientEndpoint.BASE_ENDPOINT)
@Component
public class ManagementProcedureController {

    private final ISerivicesManagementProcedure serivicesManagementProcedure;

    @Autowired
    public ManagementProcedureController(ISerivicesManagementProcedure serivicesManagementProcedure) {
        this.serivicesManagementProcedure = serivicesManagementProcedure;
    }

    @PostMapping
    public ResponseEntity<?> createProcedure(@RequestBody RequestDTO requestDTO) {
        return new ResponseEntity<>(serivicesManagementProcedure.createProcedure(requestDTO), HttpStatus.CREATED);
    }

    @PutMapping("update")
    public ResponseEntity<?> updateProcedure(@RequestBody RequestDTO requestDTO) {
        return new ResponseEntity<>(serivicesManagementProcedure.updateProcedure(requestDTO), HttpStatus.OK);
    }

    @GetMapping("{idInstancia}")
    public ResponseEntity<?> findProcedureByIdInstancia(@PathVariable("idInstancia") Integer idTramite){
        return new ResponseEntity<>(serivicesManagementProcedure.findProcedure(idTramite), HttpStatus.OK);
    }

    @PostMapping("validate")
    public ResponseEntity<?> validateProcedure(@RequestBody RequestValidaDTO requestValidaDTO){
        return new ResponseEntity<>(serivicesManagementProcedure.findProcedureBySemester(requestValidaDTO), HttpStatus.OK);
    }

}
