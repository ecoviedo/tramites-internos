package co.com.soaint.tramitesinternos.repository;

import co.com.soaint.tramitesinternos.commonscomponents.entities.TipoTramite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ITipoTramiteRepository extends JpaRepository<TipoTramite, Integer> {
}
