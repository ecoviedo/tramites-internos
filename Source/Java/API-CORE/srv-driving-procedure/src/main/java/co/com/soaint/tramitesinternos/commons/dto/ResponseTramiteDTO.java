package co.com.soaint.tramitesinternos.commons.dto;

import co.com.soaint.tramitesinternos.commonscomponents.dtos.TramiteDTO;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class ResponseTramiteDTO {
    private TramiteDTO tramite;
    private StatusDTO response;
}
