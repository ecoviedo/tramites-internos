package co.com.soaint.tramitesinternos.services.impl;

import co.com.soaint.tramitesinternos.commons.dto.ResponseDTO;
import co.com.soaint.tramitesinternos.commons.dto.StatusDTO;
import co.com.soaint.tramitesinternos.commons.exception.system.SystemException;
import co.com.soaint.tramitesinternos.commonscomponents.dtos.RolTramiteDTO;
import co.com.soaint.tramitesinternos.commonscomponents.entities.GestorTramite;
import co.com.soaint.tramitesinternos.repository.IGestorTramiteRepository;
import co.com.soaint.tramitesinternos.repository.IRoleUserRepository;
import co.com.soaint.tramitesinternos.services.IServicesRoleUser;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Component
public class ServicesRoleUser implements IServicesRoleUser {

    @Autowired
    private ModelMapper modelMapper;

    private final IRoleUserRepository RoleUserRepository;
    private final IGestorTramiteRepository gestorTramiteRepository;

    @Autowired
    public ServicesRoleUser(IRoleUserRepository roleUserRepository, IGestorTramiteRepository gestorTramiteRepository) {
        RoleUserRepository = roleUserRepository;
        this.gestorTramiteRepository = gestorTramiteRepository;
    }

    @Override
    public ResponseDTO findRolByUser(String usuario) {
        try {
            ResponseDTO responseDTO = new ResponseDTO();
            responseDTO.setUsuario(usuario);
            Optional<GestorTramite> gestorTramite = gestorTramiteRepository.countGestorTramiteByUsuario(usuario);
            if (gestorTramite.isPresent()){
                responseDTO.setRolTramiteDTO(modelMapper.map(gestorTramite.get().getIdRol(), RolTramiteDTO.class));
            }else {
                responseDTO.setRolTramiteDTO(modelMapper.map(
                        RoleUserRepository.findRolTramiteEmpelado("Empleado").get(), RolTramiteDTO.class));
            }
            StatusDTO statusDTO = new StatusDTO();
            if (responseDTO.getRolTramiteDTO() == null){
                statusDTO = StatusDTO.builder()
                        .status(HttpStatus.NO_CONTENT.name())
                        .statusCode(String.valueOf(HttpStatus.NO_CONTENT.value()))
                        .msg("No Encontrado").build();
            }else {
                statusDTO = StatusDTO.builder()
                        .status(HttpStatus.OK.name())
                        .statusCode(String.valueOf(HttpStatus.OK.value()))
                        .msg("Consult successful").build();
            }
            responseDTO.setResponse(statusDTO);
            return responseDTO;
        }catch (SystemException ex){
            throw new SystemException();
        }
    }
}
