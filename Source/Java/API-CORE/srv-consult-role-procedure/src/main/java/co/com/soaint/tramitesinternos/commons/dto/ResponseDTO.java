package co.com.soaint.tramitesinternos.commons.dto;

import co.com.soaint.tramitesinternos.commonscomponents.dtos.GestorTramiteDTO;
import co.com.soaint.tramitesinternos.commonscomponents.dtos.RolTramiteDTO;
import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class ResponseDTO {
    private String usuario;
    private RolTramiteDTO rolTramiteDTO;
    private StatusDTO response;
}
