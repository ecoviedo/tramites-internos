package co.com.soaint.tramitesinternos.repository;

import co.com.soaint.tramitesinternos.commonscomponents.entities.RolTramite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IRoleUserRepository extends JpaRepository<RolTramite, Integer> {

    @Query("select rt FROM RolTramite rt where rt.nombre =:nombre")
    Optional<RolTramite> findRolTramiteEmpelado(@Param("nombre") String nombre);
}
