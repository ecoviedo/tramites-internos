package co.com.soaint.tramitesinternos.commons.exception.system;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class SystemException extends RuntimeException {

    private Exception ex;

    public SystemException() {
    }
}

