package co.com.soaint.tramitesinternos.repository;

import co.com.soaint.tramitesinternos.commonscomponents.entities.GestorTramite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IGestorTramiteRepository extends JpaRepository<GestorTramite, Integer>{

    @Query("select gt from GestorTramite gt where gt.usuario =:usuario")
    Optional<GestorTramite> countGestorTramiteByUsuario(@Param("usuario") String usuario);
}
