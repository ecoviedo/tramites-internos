package co.com.soaint.tramitesinternos.services;

import co.com.soaint.tramitesinternos.commons.dto.ResponseDTO;

public interface IServicesRoleUser {

    public ResponseDTO findRolByUser(String usuario);
}
