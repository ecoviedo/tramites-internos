package co.com.soaint.tramitesinternos.controller;

import co.com.soaint.tramitesinternos.commons.constants.ClientEndpoint;
import co.com.soaint.tramitesinternos.services.IServicesRoleUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.QueryParam;

@RestController
@RequestMapping(ClientEndpoint.BASE_ENDPOINT)
@Component
public class RoleApiController {

    private final IServicesRoleUser servicesRoleUser;

    @Autowired
    public RoleApiController(IServicesRoleUser servicesRoleUser) {
        this.servicesRoleUser = servicesRoleUser;
    }

    @GetMapping
    public ResponseEntity<?> findRoleByUser(@QueryParam("usuario") String usuario){
        return new ResponseEntity<>(servicesRoleUser.findRolByUser(usuario), HttpStatus.OK);
    }
}
