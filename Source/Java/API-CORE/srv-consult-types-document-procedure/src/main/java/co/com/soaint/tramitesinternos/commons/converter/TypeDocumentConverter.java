package co.com.soaint.tramitesinternos.commons.converter;

import co.com.soaint.tramitesinternos.commons.dto.ResponseDTO;
import co.com.soaint.tramitesinternos.commonscomponents.dtos.TipoDocumentoTramiteDTO;
import co.com.soaint.tramitesinternos.commonscomponents.entities.TipoDocumentoTramite;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;

public class TypeDocumentConverter {

    public static void Converte(ModelMapper modelMapper, List<TipoDocumentoTramite> objects, ResponseDTO responseDTO){
        List<TipoDocumentoTramiteDTO> tramiteDTOS = new ArrayList<>();
        for(TipoDocumentoTramite dto: objects){
            TipoDocumentoTramiteDTO tramiteDTO = new TipoDocumentoTramiteDTO();
            modelMapper.map(dto, tramiteDTO);
            tramiteDTOS.add(tramiteDTO);
        }
        responseDTO.setTipoDocumentoTramite(tramiteDTOS);
    }
}
