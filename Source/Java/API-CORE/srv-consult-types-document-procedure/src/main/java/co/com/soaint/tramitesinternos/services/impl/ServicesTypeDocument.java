package co.com.soaint.tramitesinternos.services.impl;

import co.com.soaint.tramitesinternos.commons.converter.TypeDocumentConverter;
import co.com.soaint.tramitesinternos.commons.dto.ResponseDTO;
import co.com.soaint.tramitesinternos.commons.dto.StatusDTO;
import co.com.soaint.tramitesinternos.commons.exception.business.ResponseNotFoundException;
import co.com.soaint.tramitesinternos.commons.exception.system.SystemException;
import co.com.soaint.tramitesinternos.commonscomponents.entities.TipoDocumentoTramite;
import co.com.soaint.tramitesinternos.repository.ITypeDocumentRepository;
import co.com.soaint.tramitesinternos.services.IServicesTypeDocument;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServicesTypeDocument implements IServicesTypeDocument {

    @Autowired
    private ModelMapper modelMapper;

    private final ITypeDocumentRepository typeDocumentRepository;

    @Autowired
    public ServicesTypeDocument(ITypeDocumentRepository typeDocumentRepository) {
        this.typeDocumentRepository = typeDocumentRepository;
    }

    @Override
    public ResponseDTO findAllTypesDocuments(){
        try {
            List<TipoDocumentoTramite> tipoDocumentoTramites = typeDocumentRepository.findAll();
            ResponseDTO responseDTO = new ResponseDTO();
            TypeDocumentConverter.Converte(modelMapper, tipoDocumentoTramites, responseDTO);
            if (responseDTO.getTipoDocumentoTramite().size()==0){
                throw new ResponseNotFoundException();
            }
            StatusDTO statusDTO = StatusDTO.builder()
                    .status(HttpStatus.OK.name())
                    .statusCode(String.valueOf(HttpStatus.OK.value()))
                    .msg("Consult successful").build();
            responseDTO.setResponse(statusDTO);
            return responseDTO;
        }catch (SystemException ex){
            throw new SystemException();
        }
    }


}
