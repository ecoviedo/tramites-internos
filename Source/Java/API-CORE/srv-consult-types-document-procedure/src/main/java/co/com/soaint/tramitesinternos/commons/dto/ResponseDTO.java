package co.com.soaint.tramitesinternos.commons.dto;

import co.com.soaint.tramitesinternos.commonscomponents.dtos.TipoDocumentoTramiteDTO;
import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class ResponseDTO {
    private List<TipoDocumentoTramiteDTO> tipoDocumentoTramite;
    private StatusDTO response;
}
