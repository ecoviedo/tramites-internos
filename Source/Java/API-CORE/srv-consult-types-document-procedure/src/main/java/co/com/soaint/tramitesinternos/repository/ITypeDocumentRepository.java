package co.com.soaint.tramitesinternos.repository;

import co.com.soaint.tramitesinternos.commonscomponents.entities.TipoDocumentoTramite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ITypeDocumentRepository extends JpaRepository<TipoDocumentoTramite, Integer> {
}
