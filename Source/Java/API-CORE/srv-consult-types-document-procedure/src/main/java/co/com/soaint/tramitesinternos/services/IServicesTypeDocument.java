package co.com.soaint.tramitesinternos.services;

import co.com.soaint.tramitesinternos.commons.dto.ResponseDTO;

public interface IServicesTypeDocument {

    ResponseDTO findAllTypesDocuments();
}
