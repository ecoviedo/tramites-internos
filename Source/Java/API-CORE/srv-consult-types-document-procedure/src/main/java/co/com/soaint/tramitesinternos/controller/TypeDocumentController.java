package co.com.soaint.tramitesinternos.controller;

import co.com.soaint.tramitesinternos.commons.constants.ClientEndpoint;
import co.com.soaint.tramitesinternos.services.IServicesTypeDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(ClientEndpoint.BASE_ENDPOINT)
@Component
public class TypeDocumentController {

    private final IServicesTypeDocument servicesTypeDocument;

    @Autowired
    public TypeDocumentController(IServicesTypeDocument servicesTypeDocument) {
        this.servicesTypeDocument = servicesTypeDocument;
    }

    @GetMapping
    public ResponseEntity<?> findAllTypeDocument(){
        return new ResponseEntity<>(servicesTypeDocument.findAllTypesDocuments(), HttpStatus.OK);
    }
}
