package co.com.soaint.tramitesinternos.controller;

import co.com.soaint.tramitesinternos.commons.constants.ClientEndpoint;
import co.com.soaint.tramitesinternos.commonscomponents.dtos.GestionTramiteDTO;
import co.com.soaint.tramitesinternos.services.IServicesGestionTramite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(ClientEndpoint.BASE_ENDPOINT)
@Component
public class GestionTramiteController {

    private final IServicesGestionTramite servicesGestionTramite;

    @Autowired
    public GestionTramiteController(IServicesGestionTramite servicesGestionTramite) {
        this.servicesGestionTramite = servicesGestionTramite;
    }

    @GetMapping(ClientEndpoint.GET_GESTION_TRAMITE)
    public ResponseEntity<?> findGestionTrramite(@PathVariable("idTramite") Integer idTramite,
                                                 @PathVariable("idRol") Integer idRol){

        return new ResponseEntity<>(servicesGestionTramite.FindGestionTramite(idTramite, idRol), HttpStatus.OK);
    }

    @PostMapping(ClientEndpoint.SAVE_GESTION_TRAMITE)
    public ResponseEntity<?> saveGestionTramite(@RequestBody GestionTramiteDTO gestionTramiteDTO){
        return new ResponseEntity<>(servicesGestionTramite.SaveGestionTramite(gestionTramiteDTO), HttpStatus.CREATED);
    }
}
