package co.com.soaint.tramitesinternos.services.impl;

import co.com.soaint.tramitesinternos.commons.dto.ResponseDTO;
import co.com.soaint.tramitesinternos.commons.dto.StatusDTO;
import co.com.soaint.tramitesinternos.commons.exception.business.ResponseNotFoundException;
import co.com.soaint.tramitesinternos.commons.exception.system.SystemException;
import co.com.soaint.tramitesinternos.commonscomponents.dtos.GestionTramiteDTO;
import co.com.soaint.tramitesinternos.commonscomponents.entities.GestionTramite;
import co.com.soaint.tramitesinternos.repository.IGestionTramiteRepository;
import co.com.soaint.tramitesinternos.services.IServicesGestionTramite;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServicesGestionTramiteImpl implements IServicesGestionTramite {

    @Autowired
    private ModelMapper modelMapper;

    private final IGestionTramiteRepository gestionTramiteRepository;

    public ServicesGestionTramiteImpl(IGestionTramiteRepository gestionTramiteRepository) {
        this.gestionTramiteRepository = gestionTramiteRepository;
    }

    @Override
    public ResponseDTO FindGestionTramite(Integer idTramite, Integer idRol) {
        try {
            List<GestionTramite> gestionTramites = gestionTramiteRepository.findGestionTramiteByIdTramiteAndIdRol(idTramite, idRol);
            if (gestionTramites.size() == 0) {
                throw new ResponseNotFoundException();
            }
            GestionTramiteDTO gestionTramiteDTO = new GestionTramiteDTO();
            modelMapper.map(gestionTramites.get(0), gestionTramiteDTO);
            gestionTramiteDTO.setIdTramite(null);
            StatusDTO statusDTO = StatusDTO.builder()
                    .status(HttpStatus.OK.name())
                    .statusCode(String.valueOf(HttpStatus.OK.value()))
                    .msg("Consult successful").build();
            ResponseDTO responseDTO = ResponseDTO.builder()
                    .gestionTramite(gestionTramiteDTO)
                    .response(statusDTO)
                    .build();
            return responseDTO;
        } catch (SystemException ex) {
            throw new SystemException();
        }
    }

    @Override
    public ResponseDTO SaveGestionTramite(GestionTramiteDTO gestionTramiteDTO) {
        try {
            GestionTramite gestionTramite = new GestionTramite();
            modelMapper.map(gestionTramiteDTO, gestionTramite);
            GestionTramiteDTO gestionsave = modelMapper.map(gestionTramiteRepository.save(gestionTramite), GestionTramiteDTO.class);
            StatusDTO statusDTO = StatusDTO.builder()
                    .status(HttpStatus.CREATED.name())
                    .statusCode(String.valueOf(HttpStatus.CREATED.value()))
                    .msg("Create successful").build();
            ResponseDTO responseDTO = ResponseDTO.builder()
                    .gestionTramite(gestionsave)
                    .response(statusDTO)
                    .build();
            return responseDTO;
        } catch (SystemException e) {
            throw new SystemException();
        }
    }
}
