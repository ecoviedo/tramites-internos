package co.com.soaint.tramitesinternos.commons.dto;

import co.com.soaint.tramitesinternos.commonscomponents.dtos.GestionTramiteDTO;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class ResponseDTO {
    private GestionTramiteDTO gestionTramite;
    private StatusDTO response;
}
