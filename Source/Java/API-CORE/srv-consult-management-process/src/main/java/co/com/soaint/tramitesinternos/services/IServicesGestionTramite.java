package co.com.soaint.tramitesinternos.services;

import co.com.soaint.tramitesinternos.commons.dto.ResponseDTO;
import co.com.soaint.tramitesinternos.commonscomponents.dtos.GestionTramiteDTO;

public interface IServicesGestionTramite {

    ResponseDTO FindGestionTramite(Integer idTramite, Integer idRol);

    ResponseDTO SaveGestionTramite(GestionTramiteDTO gestionTramiteDTO);
}
