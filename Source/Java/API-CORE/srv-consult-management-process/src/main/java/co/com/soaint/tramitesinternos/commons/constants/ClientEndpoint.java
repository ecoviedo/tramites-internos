package co.com.soaint.tramitesinternos.commons.constants;

public interface ClientEndpoint {
    String BASE_ENDPOINT = "Dev/api/core/procedure/management/";
    String GET_GESTION_TRAMITE = "{idTramite}/{idRol}";
    String SAVE_GESTION_TRAMITE = "";
}
