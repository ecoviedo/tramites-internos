package co.com.soaint.tramitesinternos.repository;

import co.com.soaint.tramitesinternos.commonscomponents.entities.GestionTramite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IGestionTramiteRepository extends JpaRepository<GestionTramite, Integer> {

    @Query("select gt FROM GestionTramite gt " +
            "where gt.idTramite.id = :idTramite " +
            "and gt.idRol.id = :idRol")
    List<GestionTramite> findGestionTramiteByIdTramiteAndIdRol(@Param("idTramite") Integer idTramite,
                                                               @Param("idRol") Integer idRol);
}
