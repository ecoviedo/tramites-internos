package co.com.soaint.tramitesinternos.commonscomponents.entities;

import lombok.*;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Entity
@Table(name = "dias_disponibles_empleado")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "DiasDisponiblesEmpleado.findAll", query = "SELECT d FROM DiasDisponiblesEmpleado d")})
public class DiasDisponiblesEmpleado implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;

    @Basic(optional = false)
    @Column(name = "apellido")
    private String apellido;

    @Column(name = "dias_disponibles")
    private Integer diasDisponibles;

    @Basic(optional = false)
    @Column(name = "identificacion")
    private String identificacion;

    @JoinColumn(name = "id_tipo_identificacion", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private TipoIdentificacion idTipoIdentificacion;
}
