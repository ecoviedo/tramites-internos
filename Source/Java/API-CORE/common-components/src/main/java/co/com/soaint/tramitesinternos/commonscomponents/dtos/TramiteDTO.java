package co.com.soaint.tramitesinternos.commonscomponents.dtos;

import lombok.*;

import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class TramiteDTO {

    private Integer id;

    private int idInstancia;

    private String identificacion;

    private Date fecha;

    private String estado;

    private String nombreEmpleado;

    private String apellidoEmpleado;

    private String respuestaTramite;

    private AreaEmpresaDTO idArea;

    private GestorTramiteDTO idGestorTramite;

    private TipoIdentificacionDTO idTipoIdentificacion;

    private TipoTramiteDTO idTipoTramite;

    private VacacionesTramiteDTO vacacionesTramite;

    private List<CertificadoTramiteDTO> certificadoTramiteList;

    private DetalleTramiteDTO detalleTramite;

    private List<GestionTramiteDTO> gestionTramiteList;
}
