package co.com.soaint.tramitesinternos.commonscomponents.dtos;

import lombok.*;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class TipoIdentificacionDTO {
    private int idTipoIdentificacion;
    private String nombreTipoIdentificacion;
    private String descripcionTipoIdentificacion;
}
