package co.com.soaint.tramitesinternos.commonscomponents.dtos;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class DiasDisponiblesEmpleadoDTO {
    private Integer id;
    private String nombre;
    private String apellido;
    private Integer diasDisponibles;
    private String identificacion;
    private TipoIdentificacionDTO idTipoIdentificacion;
}
