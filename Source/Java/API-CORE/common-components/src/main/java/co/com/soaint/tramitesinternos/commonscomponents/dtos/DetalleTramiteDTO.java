package co.com.soaint.tramitesinternos.commonscomponents.dtos;

import lombok.*;

import java.sql.Time;
import java.util.Date;
import java.util.Timer;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class DetalleTramiteDTO {

    private Integer id;

    private int diasSolicitados;

    private Date fechaDiaSolicitado;

    private Date horaInicio;

    private Date horaFin;

    private Date fechaInicio;

    private Date fechaFin;

    private String otroMotivo;

    private String observaciones;

    private String nombreArchivo;

    private String tipoArchivo;

    private String contenidoArchivo;

    private String ubicacionArchivo;

    private TipoDocumentoTramiteDTO idTipoDocumentoTramite;

    private TipoTramiteDTO idSubtipoTramite;

    private TramiteDTO idTramite;
}
