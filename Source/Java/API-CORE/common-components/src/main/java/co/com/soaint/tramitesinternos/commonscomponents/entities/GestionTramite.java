/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.soaint.tramitesinternos.commonscomponents.entities;

import lombok.*;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Entity
@Table(name = "gestion_tramite")
@NamedQueries({
    @NamedQuery(name = "GestionTramite.findAll", query = "SELECT g FROM GestionTramite g")})
public class GestionTramite implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;

    @Column(name = "revisado_por")
    private String revisadoPor;

    @Column(name = "observaciones")
    private String observaciones;

    @Column(name = "respuesta_tramite")
    private String respuestaTramite;

    @Column(name = "permiso_remunerado")
    private String permisoRemunerado;

    @Column(name = "requiere_reemplazo")
    private String requiereReemplazo;

    @Column(name = "nombre_reemplazo")
    private String nombreReemplazo;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @JoinColumn(name = "id_incapacidad", referencedColumnName = "id")
    @ManyToOne
    private IncapacidadTramite idIncapacidad;

    @JoinColumn(name = "id_tramite", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Tramite idTramite;

    @JoinColumn(name = "id_rol", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private RolTramite idRol;
}
