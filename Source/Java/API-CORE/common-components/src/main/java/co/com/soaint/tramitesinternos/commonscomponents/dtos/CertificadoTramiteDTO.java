package co.com.soaint.tramitesinternos.commonscomponents.dtos;

import co.com.soaint.tramitesinternos.commonscomponents.entities.TipoTramite;
import co.com.soaint.tramitesinternos.commonscomponents.entities.Tramite;
import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class CertificadoTramiteDTO {

    private Integer id;

    private String nombreArchivo;

    private String tipoArchivo;

    private String contenidoArchivo;

    private String ubicacionArchivo;

    private String observaciones;

    private TipoTramiteDTO idSubtipoTramite;

    private TramiteDTO idTramite;
}
