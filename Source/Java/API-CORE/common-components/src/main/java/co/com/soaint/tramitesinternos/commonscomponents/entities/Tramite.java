/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.soaint.tramitesinternos.commonscomponents.entities;

import lombok.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "tramite")
@NamedQueries({
    @NamedQuery(name = "Tramite.findAll", query = "SELECT t FROM Tramite t")})
public class Tramite implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @Column(name = "id_instancia")
    private int idInstancia;

    @Basic(optional = false)
    @Column(name = "identificacion")
    private String identificacion;

    @Basic(optional = false)
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;

    @Basic(optional = false)
    @Column(name = "estado")
    private String estado;

    @Basic(optional = false)
    @Column(name = "nombre_empleado")
    private String nombreEmpleado;

    @Basic(optional = false)
    @Column(name = "apellido_empleado")
    private String apellidoEmpleado;

    @Column(name = "respuesta_tramite")
    private String respuestaTramite;


    @JoinColumn(name = "id_area", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private AreaEmpresa idArea;

    @JoinColumn(name = "id_gestor_tramite", referencedColumnName = "id")
    @ManyToOne
    private GestorTramite idGestorTramite;

    @JoinColumn(name = "id_tipo_identificacion", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TipoIdentificacion idTipoIdentificacion;

    @JoinColumn(name = "id_tipo_tramite", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TipoTramite idTipoTramite;

    @OneToOne(mappedBy = "idTramite",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private VacacionesTramite vacacionesTramite;

    @OneToMany(mappedBy = "idTramite",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<CertificadoTramite> certificadoTramiteList;

    @OneToOne(mappedBy = "idTramite",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private DetalleTramite detalleTramite;

    @OneToMany(mappedBy = "idTramite",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<GestionTramite> gestionTramiteList;

}
