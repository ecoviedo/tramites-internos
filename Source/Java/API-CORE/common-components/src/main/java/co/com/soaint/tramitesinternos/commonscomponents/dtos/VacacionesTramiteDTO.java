package co.com.soaint.tramitesinternos.commonscomponents.dtos;

import lombok.*;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class VacacionesTramiteDTO {

    private Integer id;

    private int diasDisponibles;

    private Boolean solicitaDiasAnticipados;

    private int totalDiasSolicitados;

    private int totalDiasAnticipados;

    private Date fechaInicio;

    private Date fechaFin;

    private Date fechaReintegro;

    private String observaciones;

    private TramiteDTO idTramite;
}
