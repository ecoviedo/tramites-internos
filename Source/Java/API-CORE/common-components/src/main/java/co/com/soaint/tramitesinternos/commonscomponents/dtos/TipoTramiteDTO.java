package co.com.soaint.tramitesinternos.commonscomponents.dtos;

import lombok.*;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class TipoTramiteDTO {
    private Integer id;
    private String nombre;
    private String descripcion;
    private Integer padreId;
}
