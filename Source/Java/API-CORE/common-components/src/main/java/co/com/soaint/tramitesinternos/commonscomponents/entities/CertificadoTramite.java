/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.soaint.tramitesinternos.commonscomponents.entities;

import lombok.*;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Entity
@Table(name = "certificado_tramite")
@NamedQueries({
    @NamedQuery(name = "CertificadoTramite.findAll", query = "SELECT c FROM CertificadoTramite c")})
public class CertificadoTramite implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "nombre_archivo")
    private String nombreArchivo;
    @Column(name = "tipo_archivo")
    private String tipoArchivo;
    @Column(name = "contenido_archivo")
    private String contenidoArchivo;
    @Column(name = "ubicacion_archivo")
    private String ubicacionArchivo;
    @Column(name = "observaciones")
    private String observaciones;
    @JoinColumn(name = "id_subtipo_tramite", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TipoTramite idSubtipoTramite;
    @JoinColumn(name = "id_tramite", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Tramite idTramite;
    
}
