package co.com.soaint.tramitesinternos.commonscomponents.dtos;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class IncapacidadTramiteDTO {


    private Integer id;

    private String otro;

    private TipoTramiteDTO idSubtipoTramite;

}
