/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.soaint.tramitesinternos.commonscomponents.entities;

import lombok.*;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Entity
@Table(name = "tipo_tramite")
@NamedQueries({
    @NamedQuery(name = "TipoTramite.findAll", query = "SELECT t FROM TipoTramite t")})
public class TipoTramite implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "padre_id")
    private Integer padreId;

    /*@OneToMany(mappedBy = "idSubtipoTramite")
    private List<IncapacidadTramite> incapacidadTramiteList;

    @OneToMany(mappedBy = "idSubtipoTramite")
    private List<CertificadoTramite> certificadoTramiteList;

    @OneToMany(mappedBy = "idTipoTramite")
    private List<Tramite> tramiteList;

    @OneToMany(mappedBy = "idSubtipoTramite")
    private List<DetalleTramite> detalleTramiteList;

    @OneToMany(mappedBy = "idTipoTramite")
    private List<TipoDocumentoTramite> tipoDocumentoTramiteList;

    @OneToMany(mappedBy = "idSubtipoTramite")
    private List<TipoDocumentoTramite> tipoDocumentoTramiteList1;*/

}
