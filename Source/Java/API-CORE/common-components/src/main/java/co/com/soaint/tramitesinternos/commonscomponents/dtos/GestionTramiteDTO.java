package co.com.soaint.tramitesinternos.commonscomponents.dtos;

import lombok.*;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class GestionTramiteDTO {

    private Integer id;

    private Date fecha;

    private String revisadoPor;

    private String observaciones;

    private String respuestaTramite;

    private String permisoRemunerado;

    private String requiereReemplazo;

    private String nombreReemplazo;

    private IncapacidadTramiteDTO idIncapacidad;

    private TramiteDTO idTramite;

    private RolTramiteDTO idRol;
}
