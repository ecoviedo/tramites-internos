/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.soaint.tramitesinternos.commonscomponents.entities;

import lombok.*;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Entity
@Table(name = "incapacidad_tramite")
@NamedQueries({
    @NamedQuery(name = "IncapacidadTramite.findAll", query = "SELECT i FROM IncapacidadTramite i")})
public class IncapacidadTramite implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "otro")
    private String otro;
    @JoinColumn(name = "id_subtipo_tramite", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TipoTramite idSubtipoTramite;
    @OneToMany(mappedBy = "idIncapacidad")
    private List<GestionTramite> gestionTramiteList;

}
