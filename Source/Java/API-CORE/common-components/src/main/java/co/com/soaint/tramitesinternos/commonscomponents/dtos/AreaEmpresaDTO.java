package co.com.soaint.tramitesinternos.commonscomponents.dtos;

import lombok.*;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class AreaEmpresaDTO {

    private int id;
    private String nombre;
    private String descripcion;
    private String nombreBpm;
}
