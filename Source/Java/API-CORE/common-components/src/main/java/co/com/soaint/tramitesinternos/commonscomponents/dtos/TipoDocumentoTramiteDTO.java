package co.com.soaint.tramitesinternos.commonscomponents.dtos;

import co.com.soaint.tramitesinternos.commonscomponents.entities.TipoTramite;
import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class TipoDocumentoTramiteDTO {

    private Integer id;

    private String nombre;

    private boolean obligatorio;

    private String descripcion;

    private TipoTramiteDTO idTipoTramite;

    private TipoTramiteDTO idSubtipoTramite;
}
