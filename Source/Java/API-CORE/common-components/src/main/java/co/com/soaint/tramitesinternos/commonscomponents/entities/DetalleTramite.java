/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.soaint.tramitesinternos.commonscomponents.entities;

import lombok.*;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Entity
@Table(name = "detalle_tramite")
@NamedQueries({
    @NamedQuery(name = "DetalleTramite.findAll", query = "SELECT d FROM DetalleTramite d")})
public class DetalleTramite implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "dias_solicitados")
    private int diasSolicitados;
    @Column(name = "fecha_dia_solicitado")
    @Temporal(TemporalType.DATE)
    private Date fechaDiaSolicitado;
    @Column(name = "hora_inicio")
    @Temporal(TemporalType.TIME)
    private Date horaInicio;
    @Column(name = "hora_fin")
    @Temporal(TemporalType.TIME)
    private Date horaFin;
    @Column(name = "fecha_inicio")
    @Temporal(TemporalType.DATE)
    private Date fechaInicio;
    @Column(name = "fecha_fin")
    @Temporal(TemporalType.DATE)
    private Date fechaFin;
    @Column(name = "otro_motivo")
    private String otroMotivo;
    @Column(name = "observaciones")
    private String observaciones;
    @Column(name = "nombre_archivo")
    private String nombreArchivo;
    @Column(name = "tipo_archivo")
    private String tipoArchivo;
    @Column(name = "contenido_archivo")
    private String contenidoArchivo;
    @Column(name = "ubicacion_archivo")
    private String ubicacionArchivo;

    @JoinColumn(name = "id_tipo_documento_tramite", referencedColumnName = "id")
    @ManyToOne
    private TipoDocumentoTramite idTipoDocumentoTramite;

    @JoinColumn(name = "id_subtipo_tramite", referencedColumnName = "id")
    @ManyToOne()
    private TipoTramite idSubtipoTramite;

    @JoinColumn(name = "id_tramite", referencedColumnName = "id")
    @OneToOne(optional = false, fetch = FetchType.LAZY)
    private Tramite idTramite;

}
