package co.com.soaint.tramitesinternos.commonscomponents.dtos;

import lombok.*;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class GestorTramiteDTO {
    private Integer id;
    private String identificacion;
    private String usuario;
    private String nombre;
    private String apellido;
    private AreaEmpresaDTO idArea;
    private TipoIdentificacionDTO idTipoIdentificacion;
    private RolTramiteDTO idRol;
}
