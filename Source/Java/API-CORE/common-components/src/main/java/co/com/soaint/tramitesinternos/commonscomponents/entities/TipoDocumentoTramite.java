/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.soaint.tramitesinternos.commonscomponents.entities;

import lombok.*;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Entity
@Table(name = "tipo_documento_tramite")
@NamedQueries({
    @NamedQuery(name = "TipoDocumentoTramite.findAll", query = "SELECT t FROM TipoDocumentoTramite t")})
public class TipoDocumentoTramite implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "obligatorio")
    private boolean obligatorio;
    @Column(name = "descripcion")
    private String descripcion;
    /*@OneToMany(mappedBy = "idTipoDocumentoTramite")
    private List<DetalleTramite> detalleTramiteList;*/
    @JoinColumn(name = "id_tipo_tramite", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private TipoTramite idTipoTramite;

    @JoinColumn(name = "id_subtipo_tramite", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private TipoTramite idSubtipoTramite;

}
