/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.soaint.tramitesinternos.commonscomponents.entities;

import lombok.*;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Entity
@Table(name = "vacaciones_tramite")
@NamedQueries({
    @NamedQuery(name = "VacacionesTramite.findAll", query = "SELECT v FROM VacacionesTramite v")})
public class VacacionesTramite implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "dias_disponibles")
    private int diasDisponibles;
    @Column(name = "solicita_dias_anticipados")
    private Boolean solicitaDiasAnticipados;
    @Basic(optional = false)
    @Column(name = "total_dias_solicitados")
    private int totalDiasSolicitados;
    @Basic(optional = false)
    @Column(name = "total_d\u00edas_anticipados")
    private int totalDíasAnticipados;
    @Basic(optional = false)
    @Column(name = "fecha_inicio")
    @Temporal(TemporalType.DATE)
    private Date fechaInicio;
    @Basic(optional = false)
    @Column(name = "fecha_fin")
    @Temporal(TemporalType.DATE)
    private Date fechaFin;
    @Basic(optional = false)
    @Column(name = "fecha_reintegro")
    @Temporal(TemporalType.DATE)
    private Date fechaReintegro;
    @Column(name = "observaciones")
    private String observaciones;

    @JoinColumn(name = "id_tramite", referencedColumnName = "id")
    @OneToOne(optional = false)
    private Tramite idTramite;

}
