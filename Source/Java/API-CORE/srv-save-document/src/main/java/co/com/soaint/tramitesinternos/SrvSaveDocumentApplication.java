package co.com.soaint.tramitesinternos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EntityScan(basePackages = {"co.com.soaint.tramitesinternos.commonscomponents.entities"})
@EnableEurekaClient
@EnableJpaRepositories
@SpringBootApplication
public class SrvSaveDocumentApplication {

	public static void main(String[] args) {
		SpringApplication.run(SrvSaveDocumentApplication.class, args);
	}

}
