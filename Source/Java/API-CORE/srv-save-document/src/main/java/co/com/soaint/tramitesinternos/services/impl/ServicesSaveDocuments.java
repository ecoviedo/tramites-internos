package co.com.soaint.tramitesinternos.services.impl;

import co.com.soaint.tramitesinternos.commons.constants.Path;
import co.com.soaint.tramitesinternos.commons.exception.system.SystemException;
import co.com.soaint.tramitesinternos.services.IServicesSaveDocuments;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
@Component
public class ServicesSaveDocuments implements IServicesSaveDocuments {

    public List<String> saveDocuements(MultipartFile[] multipartFile) throws IOException {
        List<String> response = new ArrayList<>();

        for (MultipartFile file: multipartFile){
            File archivo = new File(Path.Path_Document.concat(file.getOriginalFilename()));
            byte[] originalFile = file.getBytes();
            try (FileOutputStream fos = new FileOutputStream(archivo)) {
                fos.write(originalFile);
            } catch (Exception e) {
                e.printStackTrace();
                throw new SystemException();
            }
            String namefile  = file.getOriginalFilename();
            response.add(namefile);
        }
        return response;
    }
}
