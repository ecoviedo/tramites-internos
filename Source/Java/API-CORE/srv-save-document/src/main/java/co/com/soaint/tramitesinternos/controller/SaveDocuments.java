package co.com.soaint.tramitesinternos.controller;

import co.com.soaint.tramitesinternos.commons.constants.ClientEndpoint;
import co.com.soaint.tramitesinternos.services.IServicesSaveDocuments;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping(ClientEndpoint.BASE_ENDPOINT)
@Component
public class SaveDocuments {
    private final IServicesSaveDocuments iServicesSaveDocuments;

    @Autowired
    public SaveDocuments(IServicesSaveDocuments iServicesSaveDocuments) {
        this.iServicesSaveDocuments = iServicesSaveDocuments;
    }

    @PostMapping
    public ResponseEntity<?> saveDocuments(@RequestParam ("file") MultipartFile[] file) throws IOException {
        List<String> response = iServicesSaveDocuments.saveDocuements(file);
        return new ResponseEntity<List<String>>(response, HttpStatus.OK);
    }

}
