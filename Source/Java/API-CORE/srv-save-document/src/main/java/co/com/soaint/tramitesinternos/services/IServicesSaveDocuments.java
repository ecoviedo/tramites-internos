package co.com.soaint.tramitesinternos.services;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface IServicesSaveDocuments {

    public List<String> saveDocuements(MultipartFile[] multipartFile) throws IOException;
}
