package co.com.soaint.tramitesinternos.repository;

import co.com.soaint.tramitesinternos.commonscomponents.entities.DiasDisponiblesEmpleado;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface IDiasDisponiblesRepository extends JpaRepository<DiasDisponiblesEmpleado, Integer> {

    @Query("SELECT dd from DiasDisponiblesEmpleado dd " +
            "where dd.identificacion = :identificacion")
    List<Optional<DiasDisponiblesEmpleado>> findDiasDisponiblesEmpleado(@Param("identificacion") String identificacion);

}
