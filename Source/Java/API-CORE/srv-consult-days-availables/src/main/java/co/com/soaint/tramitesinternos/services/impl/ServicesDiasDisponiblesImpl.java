package co.com.soaint.tramitesinternos.services.impl;

import co.com.soaint.tramitesinternos.commons.dto.ResponseDTO;
import co.com.soaint.tramitesinternos.commons.dto.StatusDTO;
import co.com.soaint.tramitesinternos.commons.exception.business.ResponseNotFoundException;
import co.com.soaint.tramitesinternos.commons.exception.system.SystemException;
import co.com.soaint.tramitesinternos.commonscomponents.entities.DiasDisponiblesEmpleado;
import co.com.soaint.tramitesinternos.repository.IDiasDisponiblesRepository;
import co.com.soaint.tramitesinternos.services.IServicesDiasDisponibles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Component
public class ServicesDiasDisponiblesImpl implements IServicesDiasDisponibles {

    private final IDiasDisponiblesRepository diasDisponiblesRepository;

    @Autowired
    public ServicesDiasDisponiblesImpl(IDiasDisponiblesRepository diasDisponiblesRepository) {
        this.diasDisponiblesRepository = diasDisponiblesRepository;
    }

    @Override
    public ResponseDTO getDiasDisponiblesEmpleado(Integer idTipoIdentificaion, String identificacion) {
        try {
            List<Optional<DiasDisponiblesEmpleado>> diasDisponibles = diasDisponiblesRepository.
                    findDiasDisponiblesEmpleado(identificacion);
            if (diasDisponibles == null) {
                throw new ResponseNotFoundException();
            }
            Integer diasResponse = null;
            for (Optional<DiasDisponiblesEmpleado> dto: diasDisponibles){
                if (dto.get().getIdTipoIdentificacion().getId()==idTipoIdentificaion){
                    diasResponse = dto.get().getDiasDisponibles();
                }
            }
            StatusDTO statusDTO;
            if(diasResponse==null){
                statusDTO = StatusDTO.builder()
                        .status(HttpStatus.NO_CONTENT.name())
                        .statusCode(String.valueOf(HttpStatus.NO_CONTENT.value()))
                        .msg("Not Found").build();
            }else {
                statusDTO = StatusDTO.builder()
                        .status(HttpStatus.OK.name())
                        .statusCode(String.valueOf(HttpStatus.OK.value()))
                        .msg("Consult successful").build();
            }
            ResponseDTO responseDTO = ResponseDTO.builder()
                    .diasDisponibles(diasResponse)
                    .idTipoIdentificacion(idTipoIdentificaion)
                    .identificaion(identificacion)
                    .response(statusDTO)
                    .build();
            return responseDTO;
        }catch (SystemException ex){
            throw new SystemException();
        }
    }
}
