package co.com.soaint.tramitesinternos.commons.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class ResponseDTO {
    private Integer idTipoIdentificacion;
    private String identificaion;
    private Integer diasDisponibles;
    private StatusDTO response;
}
