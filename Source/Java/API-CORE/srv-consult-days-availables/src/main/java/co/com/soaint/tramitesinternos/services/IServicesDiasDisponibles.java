package co.com.soaint.tramitesinternos.services;

import co.com.soaint.tramitesinternos.commons.dto.ResponseDTO;

public interface IServicesDiasDisponibles {

    ResponseDTO getDiasDisponiblesEmpleado(Integer idTipoIdentificaion, String Identificacion);

}
