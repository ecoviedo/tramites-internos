package co.com.soaint.tramitesinternos.controller;

import co.com.soaint.tramitesinternos.commons.constants.ClientEndpoint;
import co.com.soaint.tramitesinternos.services.IServicesDiasDisponibles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(ClientEndpoint.BASE_ENDPOINT)
@Component
public class DiasDispobleController {

    private final IServicesDiasDisponibles servicesDiasDispobles;

    @Autowired
    public DiasDispobleController(IServicesDiasDisponibles servicesDiasDispobles) {
        this.servicesDiasDispobles = servicesDiasDispobles;
    }

    @GetMapping(ClientEndpoint.GET_DAYS_AVAILABLE)
    public ResponseEntity<?> FindDaysAvailableByEmployee(@PathVariable("idTipoIntificacion") Integer idTipoIntificacion,
                                                         @PathVariable("identificacion") String identificacion){
        return new ResponseEntity<>(
                servicesDiasDispobles.getDiasDisponiblesEmpleado(idTipoIntificacion,identificacion),
                HttpStatus.OK);
    }
}
