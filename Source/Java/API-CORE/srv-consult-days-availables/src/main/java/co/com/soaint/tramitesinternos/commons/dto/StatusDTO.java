package co.com.soaint.tramitesinternos.commons.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class StatusDTO {
    private String status;
    private String statusCode;
    private String msg;
}
