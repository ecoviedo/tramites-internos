package co.com.soaint.tramitesinternos.commons.constants;

public interface ClientEndpoint {
    String BASE_ENDPOINT = "/Dev/api/core/days/available/employee/v1";
    String GET_DAYS_AVAILABLE = "/{idTipoIntificacion}/{identificacion}";
}
