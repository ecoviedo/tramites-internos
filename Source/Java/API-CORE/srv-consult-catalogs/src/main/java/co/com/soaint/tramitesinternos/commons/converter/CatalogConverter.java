package co.com.soaint.tramitesinternos.commons.converter;

import co.com.soaint.tramitesinternos.commons.dto.CatalogsDTO;
import co.com.soaint.tramitesinternos.commons.dto.DataCatalogsDTO;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;

public class CatalogConverter {

    public static void Converte(ModelMapper modelMapper, List<?> objects, CatalogsDTO catalogsDTO){
        List<DataCatalogsDTO> dataDTOS = new ArrayList<>();
        for(int i=0;i<objects.size();i++){
            dataDTOS.add(modelMapper.map(objects.get(i), DataCatalogsDTO.class));
        }
        catalogsDTO.setData(dataDTOS);
    }
}
