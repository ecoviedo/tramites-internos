package co.com.soaint.tramitesinternos.controller;

import co.com.soaint.tramitesinternos.commons.constants.ClientEndpoint;
import co.com.soaint.tramitesinternos.commons.dto.CatalogRequestDTO;
import co.com.soaint.tramitesinternos.commons.dto.ResponseDTO;
import co.com.soaint.tramitesinternos.services.IServicesCatalogs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(ClientEndpoint.BASE_ENDPOINT)
@Component
public class CatalogsController {

    private final IServicesCatalogs ServicesCatalogs;

    @Autowired
    public CatalogsController(IServicesCatalogs servicesCatalogs) {
        ServicesCatalogs = servicesCatalogs;
    }

    @PostMapping
    public ResponseEntity<?> ConsultCatalogs(@RequestBody CatalogRequestDTO catalogs){

        ResponseDTO catalogsDTOS = ServicesCatalogs.findAllCatalogs(catalogs);
        return new ResponseEntity<>(catalogsDTOS, HttpStatus.OK);
    }
}
