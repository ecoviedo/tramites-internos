package co.com.soaint.tramitesinternos.services;

import co.com.soaint.tramitesinternos.commons.dto.CatalogRequestDTO;
import co.com.soaint.tramitesinternos.commons.dto.ResponseDTO;

public interface IServicesCatalogs {

    ResponseDTO findAllCatalogs(CatalogRequestDTO catalogs);
}
