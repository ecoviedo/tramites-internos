package co.com.soaint.tramitesinternos.commons.dto;

import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class CatalogsDTO {

    private String catalogo;
    private List<DataCatalogsDTO> data;

}
