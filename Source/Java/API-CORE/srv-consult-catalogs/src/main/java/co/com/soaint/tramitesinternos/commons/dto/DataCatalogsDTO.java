package co.com.soaint.tramitesinternos.commons.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class DataCatalogsDTO {

    private String id;
    private String nombre;
    private String descripcion;
    private String nombreBpm;
}
