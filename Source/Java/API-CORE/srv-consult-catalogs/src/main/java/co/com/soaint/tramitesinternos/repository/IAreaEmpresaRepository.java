package co.com.soaint.tramitesinternos.repository;


import co.com.soaint.tramitesinternos.commonscomponents.entities.AreaEmpresa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IAreaEmpresaRepository extends JpaRepository<AreaEmpresa, Integer> {
}
