package co.com.soaint.tramitesinternos.services.impl;

import co.com.soaint.tramitesinternos.commons.converter.CatalogConverter;
import co.com.soaint.tramitesinternos.commons.dto.CatalogRequestDTO;
import co.com.soaint.tramitesinternos.commons.dto.CatalogsDTO;
import co.com.soaint.tramitesinternos.commons.dto.ResponseDTO;
import co.com.soaint.tramitesinternos.commons.dto.StatusDTO;
import co.com.soaint.tramitesinternos.commons.exception.business.ResponseNotFoundException;
import co.com.soaint.tramitesinternos.commons.exception.system.SystemException;
import co.com.soaint.tramitesinternos.commonscomponents.entities.AreaEmpresa;
import co.com.soaint.tramitesinternos.commonscomponents.entities.RolTramite;
import co.com.soaint.tramitesinternos.commonscomponents.entities.TipoIdentificacion;
import co.com.soaint.tramitesinternos.commonscomponents.entities.TipoTramite;
import co.com.soaint.tramitesinternos.repository.IAreaEmpresaRepository;
import co.com.soaint.tramitesinternos.repository.IRolTramiteRepository;
import co.com.soaint.tramitesinternos.repository.ITipoIdentificacionRepository;
import co.com.soaint.tramitesinternos.repository.ITipoTramiteRepository;
import co.com.soaint.tramitesinternos.services.IServicesCatalogs;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Component
public class ServicesCatalogsImpl implements IServicesCatalogs {

    @Autowired
    private ModelMapper modelMapper;

    private final IAreaEmpresaRepository areaEmpresaRepository;
    private final IRolTramiteRepository rolTramiteRepository;
    private final ITipoIdentificacionRepository tipoIdentificacionRepository;
    private final ITipoTramiteRepository tipoTramiteRepository;

    @Autowired
    public ServicesCatalogsImpl(IAreaEmpresaRepository areaEmpresaRepository, IRolTramiteRepository rolTramiteRepository, ITipoIdentificacionRepository tipoIdentificacionRepository, ITipoTramiteRepository tipoTramiteRepository) {
        this.areaEmpresaRepository = areaEmpresaRepository;
        this.rolTramiteRepository = rolTramiteRepository;
        this.tipoIdentificacionRepository = tipoIdentificacionRepository;
        this.tipoTramiteRepository = tipoTramiteRepository;
    }

    @Override
    public ResponseDTO findAllCatalogs(CatalogRequestDTO catalogs) {
        try {
            List<CatalogsDTO> response = new ArrayList<>();
            for (CatalogsDTO dto : catalogs.getCatalogs()) {
                CatalogsDTO catalogsDTO = new CatalogsDTO();
                catalogsDTO.setCatalogo(dto.getCatalogo());
                String catalogue = dto.getCatalogo();
                switch (catalogue) {
                    case "AreaEmpresa":
                        List<AreaEmpresa> areaEmpresas = areaEmpresaRepository.findAll();
                        if (areaEmpresas.size() == 0) {
                            throw new ResponseNotFoundException();
                        }
                        CatalogConverter.Converte(modelMapper, areaEmpresas, catalogsDTO);
                        response.add(catalogsDTO);
                        break;
                    case "RolTramite":
                        List<RolTramite> rolTramites = rolTramiteRepository.findAll();
                        if (rolTramites.size() == 0) {
                            throw new ResponseNotFoundException();
                        }
                        CatalogConverter.Converte(modelMapper, rolTramites, catalogsDTO);
                        response.add(catalogsDTO);
                        break;
                    case "TipoIdentificacion":
                        List<TipoIdentificacion> tipoIdentificacions = tipoIdentificacionRepository.findAll();
                        if (tipoIdentificacions.size() == 0) {
                            throw new ResponseNotFoundException();
                        }
                        CatalogConverter.Converte(modelMapper, tipoIdentificacions, catalogsDTO);
                        response.add(catalogsDTO);
                        break;
                    case "TipoTramite":
                        List<TipoTramite> tipoTramites = tipoTramiteRepository.findAllByPadreIdIsNull();
                        if (tipoTramites.size()==0){
                            throw new ResponseNotFoundException();
                        }
                        CatalogConverter.Converte(modelMapper, tipoTramites, catalogsDTO);
                        response.add(catalogsDTO);
                        break;
                    default:
                        break;
                }
            }
            StatusDTO statusDTO = StatusDTO.builder()
                    .status(HttpStatus.OK.name())
                    .statusCode(String.valueOf(HttpStatus.OK.value()))
                    .msg("Consult successful").build();
            ResponseDTO responseDTO = ResponseDTO.builder()
                    .catalogs(response)
                    .response(statusDTO)
                    .build();
            return responseDTO;
        } catch (SystemException ex) {
            throw new SystemException();
        }
    }
}
