package co.com.soaint.tramitesinternos.commons.dto;


import lombok.*;

import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class CatalogRequestDTO {
    private List<CatalogsDTO> catalogs;
}
