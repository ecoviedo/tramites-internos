package co.com.soaint.tramitesinternos.repository;

import co.com.soaint.tramitesinternos.commonscomponents.entities.GestorTramite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface IGestorTramiteRepository extends JpaRepository<GestorTramite, String> {

    @Query("SELECT g FROM GestorTramite g WHERE g.idArea.id = :idArea and g.idRol.id = :idRol")
    GestorTramite findByIdAreaAndIdRol(@Param("idArea") int idArea,
                                       @Param("idRol") int idRol);

    @Query("SELECT g FROM GestorTramite g WHERE g.idRol.id = :idRol")
    GestorTramite findByIdRol(@Param("idRol") int idRol);

    @Query("select g FROM GestorTramite g WHERE g.usuario = :user")
    GestorTramite findByUsuario(@Param("user") String user);
}
