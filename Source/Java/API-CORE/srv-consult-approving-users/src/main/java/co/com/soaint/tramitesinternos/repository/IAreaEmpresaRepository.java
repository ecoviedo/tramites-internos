package co.com.soaint.tramitesinternos.repository;

import co.com.soaint.tramitesinternos.commonscomponents.entities.AreaEmpresa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface IAreaEmpresaRepository extends JpaRepository<AreaEmpresa, Integer> {

    @Query("SELECT a.id from AreaEmpresa a where a.nombreBpm = :nomArea")
    int findByNomArea(@Param("nomArea") String area);
}
