package co.com.soaint.tramitesinternos.services;
import co.com.soaint.tramitesinternos.commons.dto.*;
import co.com.soaint.tramitesinternos.commonscomponents.dtos.AreaEmpresaDTO;

public interface IServicesConsultApproving {

    public ResponseDTO findUsers(RequestDTO requestDTO);

    public AreaEmpresaDTO createArea(AreaEmpresaDTO areaEmpresaDTO);
}
