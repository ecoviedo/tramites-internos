package co.com.soaint.tramitesinternos.services.impl;

import co.com.soaint.tramitesinternos.commons.constants.EndpointApiConsult;
import co.com.soaint.tramitesinternos.commons.dto.NvlAprobacionDTO;
import co.com.soaint.tramitesinternos.commons.dto.RequestDTO;
import co.com.soaint.tramitesinternos.commons.dto.ResponseDTO;
import co.com.soaint.tramitesinternos.commonscomponents.dtos.AreaEmpresaDTO;
import co.com.soaint.tramitesinternos.commonscomponents.entities.AreaEmpresa;
import co.com.soaint.tramitesinternos.commonscomponents.entities.GestorTramite;
import co.com.soaint.tramitesinternos.repository.IAreaEmpresaRepository;
import co.com.soaint.tramitesinternos.repository.IGestorTramiteRepository;
import co.com.soaint.tramitesinternos.repository.IRolTramiteRepository;
import co.com.soaint.tramitesinternos.services.IServicesConsultApproving;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.websocket.ClientEndpoint;
import javax.websocket.EndpointConfig;
import java.util.ArrayList;
import java.util.List;

@Service
public class ServicesConsultApproving implements IServicesConsultApproving {

    private final IAreaEmpresaRepository areaRepository;
    private final IRolTramiteRepository rolRepository;
    private final IGestorTramiteRepository gestorTramiteRepository;

    @Autowired
    public ServicesConsultApproving(IAreaEmpresaRepository areaRepository, IRolTramiteRepository rolRepository, IGestorTramiteRepository gestorTramiteRepository) {
        this.areaRepository = areaRepository;
        this.rolRepository = rolRepository;
        this.gestorTramiteRepository = gestorTramiteRepository;
    }

    @Override
    public ResponseDTO findUsers(RequestDTO requestDTO) {
        int idArea = areaRepository.findByNomArea(requestDTO.getArea());
        List<NvlAprobacionDTO> nvlAprobacionDTOS = requestDTO.getNvlAprobacion();
        List<NvlAprobacionDTO> nvlAprobacionResponse = new ArrayList<>();
        GestorTramite gestorTramite = new GestorTramite();
        RestTemplate clientConsumer = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.ALL_VALUE);
        headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        for (NvlAprobacionDTO nvl : nvlAprobacionDTOS) {
            int idRol = rolRepository.findByNomRol(nvl.getRol());
            if (!nvl.getRol().equalsIgnoreCase("JefeInmediato")) {
                if (nvl.getRol().equalsIgnoreCase("gerenteGestionHumana")) {
                    gestorTramite = gestorTramiteRepository.findByIdRol(idRol);
                } else {
                    gestorTramite = gestorTramiteRepository.findByIdAreaAndIdRol(idArea, idRol);
                }
            } else {
                gestorTramite = gestorTramiteRepository.findByUsuario(nvl.getUsuario());
            }
            String url = EndpointApiConsult.FINDMAIL+gestorTramite.getUsuario();
            ResponseEntity<String> mail = clientConsumer.exchange(url, HttpMethod.GET, new HttpEntity(null, headers), String.class);
            nvl = NvlAprobacionDTO.builder()
                    .rol(nvl.getRol())
                    .usuario(gestorTramite.getUsuario())
                    .email(mail.getBody())
                    .nivel(nvl.getNivel()).build();

            nvlAprobacionResponse.add(nvl);
        }
        ResponseDTO responseDTO = new ResponseDTO();
        responseDTO.setNvlAprobacion(nvlAprobacionResponse);
        responseDTO.setStatus(HttpStatus.OK.name());
        responseDTO.setStatusCode("200");
        return responseDTO;
    }

    @Override
    public AreaEmpresaDTO createArea(AreaEmpresaDTO areaEmpresaDTO) {
        AreaEmpresa areaEmpresa = AreaEmpresa.builder()
                .id(areaEmpresaDTO.getId())
                .nombre(areaEmpresaDTO.getNombre())
                .descripcion(areaEmpresaDTO.getDescripcion())
                .build();
        areaRepository.save(areaEmpresa);
        return areaEmpresaDTO;
    }
}
