package co.com.soaint.tramitesinternos.commons.dto;

import lombok.*;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class RequestDTO {
    private String area;
    private List<NvlAprobacionDTO> nvlAprobacion;
}
