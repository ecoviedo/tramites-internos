package co.com.soaint.tramitesinternos.controller;

import co.com.soaint.tramitesinternos.commons.dto.RequestDTO;
import co.com.soaint.tramitesinternos.commons.dto.ResponseDTO;
import co.com.soaint.tramitesinternos.services.IServicesConsultApproving;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import co.com.soaint.tramitesinternos.commons.constants.*;

@RestController
@RequestMapping(EndpointApiConsult.CONSULT_APPROVING_USER_API_V1)
@Component
public class ConcultApprovingUsers {
    private final IServicesConsultApproving iServicesConsultApproving;

    @Autowired
    public ConcultApprovingUsers(IServicesConsultApproving iServicesConsultApproving) {
        this.iServicesConsultApproving = iServicesConsultApproving;
    }

    @PostMapping(EndpointApiConsult.FINDUSERS)
    public ResponseEntity<?> findUsers(@RequestBody RequestDTO requestDTO){
        ResponseDTO responseDTO = iServicesConsultApproving.findUsers(requestDTO);
        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }

}
