package co.com.soaint.tramitesinternos.commons.dto;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class NvlAprobacionDTO {
    private String rol;
    private String nivel;
    private String usuario;
    private String email;
}
