package co.com.soaint.tramitesinternos.commons.dto;

import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class ResponseDTO {
    List<NvlAprobacionDTO> nvlAprobacion;
    String status;
    String statusCode;
    String msg;
}
