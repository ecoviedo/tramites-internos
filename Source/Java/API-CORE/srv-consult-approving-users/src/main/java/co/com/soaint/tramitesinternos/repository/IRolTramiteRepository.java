package co.com.soaint.tramitesinternos.repository;

import co.com.soaint.tramitesinternos.commonscomponents.entities.RolTramite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface IRolTramiteRepository extends JpaRepository<RolTramite, Integer> {

    @Query("select r.id FROM RolTramite r WHERE r.nombreBpm = :nomRol")
    int findByNomRol(@Param("nomRol") String nomRol);
}
