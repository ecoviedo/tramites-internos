package co.com.soaint.srvaprobrrhh.commons.constants.api.aprobrrhh;

public interface EndpointAprobRrhhSolicitud {

    String APROB_JEFE_INMEDIATO_API_V1 = "api/v1";
    String SET_APROB_JEFE_INMEDIATO = "/aprobrrhh/";
    String FIND_APROB_JEFE_INMEDIATO = "/aprobrrhh/";
}


