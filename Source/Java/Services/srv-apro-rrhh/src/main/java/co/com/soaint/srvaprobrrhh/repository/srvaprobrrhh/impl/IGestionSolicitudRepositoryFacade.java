package co.com.soaint.srvaprobrrhh.repository.srvaprobrrhh.impl;

import co.com.soaint.componentescomunes.entidades.GestionSolicitud;

public interface IGestionSolicitudRepositoryFacade {
    GestionSolicitud setAprobRrhh(GestionSolicitud gestionSolicitud);
    Integer findIdTablaBasicaByCodigoApp(String codigoApp);
}
