package co.com.soaint.srvaprobrrhh.repository.srvaprobrrhh;

import co.com.soaint.componentescomunes.entidades.GestionSolicitud;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface IGestionSolicitudRepository extends JpaRepository<GestionSolicitud, Integer> {
   @Query("select (tablabasica.idTablaBasica) from TablaBasica tablabasica where (tablabasica.codigoApp) like (:codigoApp)")
   Integer findIdTablaBasicaByNumeroRadicado(@Param("codigoApp") String codigoApp);
}