package co.com.soaint.srvaprobrrhh.service.srvaprobrrhh.impl;
import co.com.soaint.srvaprobrrhh.commons.exception.BusinessException;
import co.com.soaint.srvaprobrrhh.commons.Dto.AprobacionRrhhDto;
import co.com.soaint.srvaprobrrhh.repository.srvaprobrrhh.impl.IGestionSolicitudRepositoryFacade;
import co.com.soaint.srvaprobrrhh.repository.srvaprobrrhh.impl.ISolicitudRepositoryFacade;
import co.com.soaint.srvaprobrrhh.service.srvaprobrrhh.IAprobRrhh;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class AprobRrhhImpl implements IAprobRrhh {
    private final IGestionSolicitudRepositoryFacade gestionSolicitudRepositoryFacade;
    private final ISolicitudRepositoryFacade solicitudRepositoryFacade;

    @Autowired
    public AprobRrhhImpl(IGestionSolicitudRepositoryFacade gestionSolicitudRepositoryFacade, ISolicitudRepositoryFacade solicitudRepositoryFacade) {
        this.gestionSolicitudRepositoryFacade = gestionSolicitudRepositoryFacade;
        this.solicitudRepositoryFacade = solicitudRepositoryFacade;
    }

    @Transactional
    @Override
    public void setAprobRrhh(AprobacionRrhhDto aprobacionRrhhDto) throws Exception {
        Solicitud solicitud = solicitudRepositoryFacade.findIdSolicitudByNumeroRadicado(aprobacionRrhhDto.getNumeroRadicado());
        if(solicitud.getGestionSolicitud().getFechaAprobRrhh() != null){
            throw BusinessException.alreadyExistManagement("Encargado Gestion Humana");
        } else if(solicitud == null) {
            throw BusinessException.notFound("Solicitud");
        }
        solicitud.getGestionSolicitud().setAprobRrhh(aprobacionRrhhDto.getAprobRrhh());
        TablaBasica tablaBasica = new TablaBasica();
        tablaBasica.setIdTablaBasica(gestionSolicitudRepositoryFacade.findIdTablaBasicaByCodigoApp(aprobacionRrhhDto.getClasificacion()));
        solicitud.getGestionSolicitud().setClasificaciones(tablaBasica);
        solicitud.getGestionSolicitud().setFechaAprobRrhh(aprobacionRrhhDto.getFechaAprob());
        solicitud.getGestionSolicitud().setObservaciones(aprobacionRrhhDto.getObservaciones());
        gestionSolicitudRepositoryFacade.setAprobRrhh(solicitud.getGestionSolicitud());
    }

    @Override
    public AprobacionRrhhDto findAprobRrhh(String numeroRadicado) throws Exception {
       Solicitud solicitud = solicitudRepositoryFacade.findIdSolicitudByNumeroRadicado(numeroRadicado);
        if(solicitud == null) {
            throw BusinessException.notFound("Solicitud");
        }else if(solicitud.getGestionSolicitud().getFechaAprobRrhh() == null){
            throw BusinessException.notAprobeYet();
        }
       GestionSolicitud gestionSolicitud = solicitud.getGestionSolicitud();
       AprobacionRrhhDto aprobacionRrhhDto = new AprobacionRrhhDto();
       aprobacionRrhhDto.setNumeroRadicado(solicitud.getNumeroRadicado());
       aprobacionRrhhDto.setAprobRrhh(gestionSolicitud.getAprobRrhh());
       aprobacionRrhhDto.setFechaAprob(gestionSolicitud.getFechaAprobRrhh());
       if(gestionSolicitud.getClasificaciones() != null) {
           aprobacionRrhhDto.setClasificacion(gestionSolicitud.getClasificaciones().getCodigoApp());
       }
           aprobacionRrhhDto.setObservaciones(gestionSolicitud.getObservaciones());

        return aprobacionRrhhDto;
    }
}
