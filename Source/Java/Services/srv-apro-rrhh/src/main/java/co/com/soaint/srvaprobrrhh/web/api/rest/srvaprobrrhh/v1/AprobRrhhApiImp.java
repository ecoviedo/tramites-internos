package co.com.soaint.srvaprobrrhh.web.api.rest.srvaprobrrhh.v1;
import co.com.soaint.srvaprobrrhh.commons.Dto.AprobacionRrhhDto;
import co.com.soaint.srvaprobrrhh.commons.constants.api.aprobrrhh.EndpointAprobRrhhSolicitud;
import co.com.soaint.srvaprobrrhh.commons.domains.response.builder.ResponseBuilder;
import co.com.soaint.srvaprobrrhh.commons.enums.TransactionState;
import co.com.soaint.srvaprobrrhh.service.srvaprobrrhh.IAprobRrhh;
import co.com.soaint.srvaprobrrhh.web.api.rest.IAprobRrhhApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(EndpointAprobRrhhSolicitud.APROB_JEFE_INMEDIATO_API_V1)
@CrossOrigin("*")
public class AprobRrhhApiImp implements IAprobRrhhApi {

    private final IAprobRrhh iAprobRrhh;

    @Autowired
    public AprobRrhhApiImp(IAprobRrhh iAprobRrhh) {
        this.iAprobRrhh = iAprobRrhh;
    }

    @GetMapping("/")
    @RequestMapping(value = EndpointAprobRrhhSolicitud.FIND_APROB_JEFE_INMEDIATO + "{numeroRadicado}")
    @Override
    public ResponseEntity findSolicitudByNumeroRadicado(@PathVariable("numeroRadicado") String numeroRadicado) {
        try {
            return ResponseBuilder.newBuilder()
                    .withStatus(HttpStatus.OK)
                    .withTransactionState(TransactionState.OK)
                    .withResponse(iAprobRrhh.findAprobRrhh(numeroRadicado))
                    .withMessage("Success")
                    .withPath(EndpointAprobRrhhSolicitud.SET_APROB_JEFE_INMEDIATO)
                    .buildResponse();
        } catch (Exception e){
            e.printStackTrace();
            return ResponseBuilder.newBuilder()
                    .withStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                    .withTransactionState(TransactionState.FAIL)
                    .withPath(EndpointAprobRrhhSolicitud.SET_APROB_JEFE_INMEDIATO)
                    .withMessage(e.getMessage())
                    .buildResponse();
        } }

    @PostMapping(EndpointAprobRrhhSolicitud.SET_APROB_JEFE_INMEDIATO)
    @Override
    public ResponseEntity setAprobJefeInmediato(@RequestBody AprobacionRrhhDto aprobJefeInmediato) {
        try {
            iAprobRrhh.setAprobRrhh(aprobJefeInmediato);
            return ResponseBuilder.newBuilder()
                    .withStatus(HttpStatus.OK)
                    .withTransactionState(TransactionState.OK)
                    .withMessage("La Aprobacion del Jefe Inmediato se ha realizado Con Exito")
                    .withPath(EndpointAprobRrhhSolicitud.SET_APROB_JEFE_INMEDIATO)
                    .buildResponse();
        } catch (Exception e){
            e.printStackTrace();
            return ResponseBuilder.newBuilder()
                    .withStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                    .withTransactionState(TransactionState.FAIL)
                    .withPath(EndpointAprobRrhhSolicitud.SET_APROB_JEFE_INMEDIATO)
                    .withMessage(e.getMessage())
                    .buildResponse();
        }
    }

}
