package co.com.soaint.srvaprobrrhh.web.api.rest;

import co.com.soaint.srvaprobrrhh.commons.Dto.AprobacionRrhhDto;
import org.springframework.http.ResponseEntity;
public interface IAprobRrhhApi {

    ResponseEntity setAprobJefeInmediato(AprobacionRrhhDto aprobJefeInmediato);
    ResponseEntity findSolicitudByNumeroRadicado(String numeroRadicado);
}
