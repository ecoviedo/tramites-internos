package co.com.soaint.srvaprobrrhh.commons.exception;

public class BusinessException extends Exception {

    public BusinessException(String exception) {
        super(exception);
    }

    public static BusinessException alreadyExistManagement(String enteSuperior){
      return new BusinessException("El "+enteSuperior+" ya realizo la gestion Correspondiente ");
    }
    public static BusinessException notFound(String tupla){
        return new BusinessException("El registro no existe en la tabla "+tupla);
    }
    public static BusinessException notAprobeYet(){
        return new BusinessException("La Solicitud No ha sido aprobada por Gestion Humana");
    }
}
