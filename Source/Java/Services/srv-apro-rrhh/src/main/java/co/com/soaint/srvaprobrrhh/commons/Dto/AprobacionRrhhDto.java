package co.com.soaint.srvaprobrrhh.commons.Dto;

import java.io.Serializable;
import java.util.Date;

public class AprobacionRrhhDto implements Serializable {
    private String numeroRadicado;
    private Boolean aprobRrhh;
    private String clasificacion;
    private Date fechaAprob;
    private String observaciones;

    public AprobacionRrhhDto(String numeroRadicado, Boolean aprobRrhh, String clasificacion, Date fechaAprob, String observaciones) {
        this.numeroRadicado = numeroRadicado;
        this.aprobRrhh = aprobRrhh;
        this.clasificacion = clasificacion;
        this.fechaAprob = fechaAprob;
        this.observaciones = observaciones;
    }

    public AprobacionRrhhDto() {

    }

    public String getNumeroRadicado() {
        return numeroRadicado;
    }

    public void setNumeroRadicado(String numeroRadicado) {
        this.numeroRadicado = numeroRadicado;
    }

    public Boolean getAprobRrhh() {
        return aprobRrhh;
    }

    public void setAprobRrhh(Boolean aprobRrhh) {
        this.aprobRrhh = aprobRrhh;
    }

    public String getClasificacion() {
        return clasificacion;
    }

    public void setClasificacion(String clasificacion) {
        this.clasificacion = clasificacion;
    }

    public Date getFechaAprob() {
        return fechaAprob;
    }

    public void setFechaAprob(Date fechaAprob) {
        this.fechaAprob = fechaAprob;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }
}
