package co.com.soaint.srvaprobrrhh.repository.srvaprobrrhh.impl;

import co.com.soaint.componentescomunes.entidades.Solicitud;

public interface ISolicitudRepositoryFacade {
        Solicitud findIdSolicitudByNumeroRadicado(String numeroRadicado);
}
