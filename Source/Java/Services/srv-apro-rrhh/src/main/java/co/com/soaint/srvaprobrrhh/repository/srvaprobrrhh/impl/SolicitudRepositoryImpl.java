package co.com.soaint.srvaprobrrhh.repository.srvaprobrrhh.impl;

import co.com.soaint.componentescomunes.entidades.Solicitud;
import co.com.soaint.srvaprobrrhh.repository.srvaprobrrhh.ISolicitudRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SolicitudRepositoryImpl implements  ISolicitudRepositoryFacade{
    private final ISolicitudRepository repository;
    @Autowired
    public SolicitudRepositoryImpl(ISolicitudRepository repository) {
        this.repository = repository;
    }

    @Override
    public Solicitud findIdSolicitudByNumeroRadicado(String numeroRadicado) {
        return repository.findByNumeroRadicado(numeroRadicado);
    }


}
