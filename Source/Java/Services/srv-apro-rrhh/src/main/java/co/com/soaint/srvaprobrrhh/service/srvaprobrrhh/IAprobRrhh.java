package co.com.soaint.srvaprobrrhh.service.srvaprobrrhh;


import co.com.soaint.srvaprobrrhh.commons.Dto.AprobacionRrhhDto;

public interface IAprobRrhh {

    void setAprobRrhh(AprobacionRrhhDto aprobJefeInmediato) throws Exception;

    AprobacionRrhhDto findAprobRrhh(String numeroRadicado) throws Exception;

}
