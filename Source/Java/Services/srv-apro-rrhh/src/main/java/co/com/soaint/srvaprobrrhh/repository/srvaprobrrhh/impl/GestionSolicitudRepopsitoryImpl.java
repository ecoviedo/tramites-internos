package co.com.soaint.srvaprobrrhh.repository.srvaprobrrhh.impl;

import co.com.soaint.componentescomunes.entidades.GestionSolicitud;
import co.com.soaint.srvaprobrrhh.repository.srvaprobrrhh.IGestionSolicitudRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GestionSolicitudRepopsitoryImpl implements IGestionSolicitudRepositoryFacade {
    private final IGestionSolicitudRepository repository;

    @Autowired
    public GestionSolicitudRepopsitoryImpl(IGestionSolicitudRepository repository) {
        this.repository = repository;
    }


    @Override
    public GestionSolicitud setAprobRrhh(GestionSolicitud gestionSolicitud) {
        return repository.save(gestionSolicitud);
    }

      @Override
    public Integer findIdTablaBasicaByCodigoApp(String codigoApp) {
        return repository.findIdTablaBasicaByNumeroRadicado(codigoApp);
    }

}
