package co.com.soaint.srvaprobrrhh.repository.srvaprobrrhh;

import co.com.soaint.componentescomunes.entidades.Solicitud;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ISolicitudRepository extends JpaRepository<Solicitud, String> {

    Solicitud findByNumeroRadicado(String numeroRadicado);
}