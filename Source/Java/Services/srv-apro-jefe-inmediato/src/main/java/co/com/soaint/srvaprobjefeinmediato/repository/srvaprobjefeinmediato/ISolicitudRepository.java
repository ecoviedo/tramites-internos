package co.com.soaint.srvaprobjefeinmediato.repository.srvaprobjefeinmediato;

import co.com.soaint.componentescomunes.entidades.Solicitud;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ISolicitudRepository extends JpaRepository<Solicitud, String> {

    Solicitud findByNumeroRadicado(String numeroRadicado);
}