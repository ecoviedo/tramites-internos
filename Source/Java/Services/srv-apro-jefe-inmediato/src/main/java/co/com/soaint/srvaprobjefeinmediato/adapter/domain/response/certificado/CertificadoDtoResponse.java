package co.com.soaint.srvaprobjefeinmediato.adapter.domain.response.certificado;

import co.com.soaint.componentescomunes.Dto.CertificadoDto;

import java.io.Serializable;

public class CertificadoDtoResponse implements Serializable {
    public CertificadoDto body;
    public String status;
    public String timeResponse;
    public String message;
    public String path;
    public String transactionState;

    public CertificadoDtoResponse(CertificadoDto body, String status, String timeResponse, String message, String path, String transactionState) {
        this.body = body;
        this.status = status;
        this.timeResponse = timeResponse;
        this.message = message;
        this.path = path;
        this.transactionState = transactionState;
    }

    public CertificadoDto getBody() {
        return body;
    }

    public void setBody(CertificadoDto body) {
        this.body = body;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimeResponse() {
        return timeResponse;
    }

    public void setTimeResponse(String timeResponse) {
        this.timeResponse = timeResponse;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getTransactionState() {
        return transactionState;
    }

    public void setTransactionState(String transactionState) {
        this.transactionState = transactionState;
    }
}
