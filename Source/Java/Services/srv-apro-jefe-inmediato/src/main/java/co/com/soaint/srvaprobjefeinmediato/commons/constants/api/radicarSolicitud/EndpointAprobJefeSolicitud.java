package co.com.soaint.srvaprobjefeinmediato.commons.constants.api.radicarSolicitud;

public interface EndpointAprobJefeSolicitud {

    String APROB_JEFE_INMEDIATO_API_V1 = "api/v1";
    String SET_APROB_JEFE_INMEDIATO = "/aprobjefeinmediato/";
    String FIND_APROB_JEFE_INMEDIATO = "/aprobjefeinmediato/";
}


