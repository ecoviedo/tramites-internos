package co.com.soaint.srvaprobjefeinmediato.repository.srvaprobjefeinmediato.impl;

import co.com.soaint.componentescomunes.entidades.Empleado;
import co.com.soaint.srvaprobjefeinmediato.repository.srvaprobjefeinmediato.IEmpleadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EmpleadoRepostiroryImpl implements  IEmpleadoRepositoryFacade{
private final IEmpleadoRepository empleadoRepository;
    @Autowired
    public EmpleadoRepostiroryImpl(IEmpleadoRepository empleadoRepository) {
        this.empleadoRepository = empleadoRepository;
    }

    @Override
    public Empleado findEmpleadoByNumeroDocumento(String numeroDocumento) {
        return empleadoRepository.findByNumeroDocumento(numeroDocumento);
    }
}
