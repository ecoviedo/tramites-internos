package co.com.soaint.srvaprobjefeinmediato.commons.exception;

public class BusinessException extends Exception {

    public  BusinessException alreadyExistManagement(String enteSuperior){
     super("El "+enteSuperior+" ya realizo la gestion Correspondiente ");
        return;
    }
    public static BusinessException notFound(String tupla){
        return new Exception("El registro no existe en la tabla "+tupla);
    }
}
