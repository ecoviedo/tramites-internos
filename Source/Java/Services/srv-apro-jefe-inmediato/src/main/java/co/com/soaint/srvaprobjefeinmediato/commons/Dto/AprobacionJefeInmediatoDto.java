package co.com.soaint.srvaprobjefeinmediato.commons.Dto;

import java.io.Serializable;
import java.util.Date;

public class AprobacionJefeInmediatoDto implements Serializable {
    private String numeroRadicado;
    private String empleadoRemplazo;
    private Boolean reemplazo;
    private Boolean remunerado;
    private String vistoBueno;
    private String observaciones;
    private Date fechaAprobacion;

    public AprobacionJefeInmediatoDto(){}

    public AprobacionJefeInmediatoDto(String numeroRadicado, String empleadoRemplazo, boolean reemplazo, boolean remunerado, String vistoBueno, String observaciones, Date fechaAprobacion) {
        this.numeroRadicado = numeroRadicado;
        this.empleadoRemplazo = empleadoRemplazo;
        this.reemplazo = reemplazo;
        this.remunerado = remunerado;
        this.vistoBueno = vistoBueno;
        this.observaciones = observaciones;
        this.fechaAprobacion = fechaAprobacion;
    }

    public String getNumeroRadicado() {
        return numeroRadicado;
    }

    public void setNumeroRadicado(String numeroRadicado) {
        this.numeroRadicado = numeroRadicado;
    }

    public String getEmpleadoRemplazo() {
        return empleadoRemplazo;
    }

    public void setEmpleadoRemplazo(String empleadoRemplazo) {
        this.empleadoRemplazo = empleadoRemplazo;
    }

    public Boolean isReemplazo() {
        return reemplazo;
    }

    public void setReemplazo(Boolean reemplazo) {
        this.reemplazo = reemplazo;
    }

    public Boolean isRemunerado() {
        return remunerado;
    }

    public void setRemunerado(Boolean remunerado) {
        this.remunerado = remunerado;
    }

    public String getVistoBueno() {
        return vistoBueno;
    }

    public void setVistoBueno(String vistoBueno) {
        this.vistoBueno = vistoBueno;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Date getFechaAprobacion() {
        return fechaAprobacion;
    }

    public void setFechaAprobacion(Date fechaAprobacion) {
        this.fechaAprobacion = fechaAprobacion;
    }
}
