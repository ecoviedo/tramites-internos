package co.com.soaint.srvaprobjefeinmediato.repository.srvaprobjefeinmediato.impl;

import co.com.soaint.componentescomunes.entidades.Empleado;

public interface IEmpleadoRepositoryFacade {
    Empleado findEmpleadoByNumeroDocumento(String numeroDocumento);
}
