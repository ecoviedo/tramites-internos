package co.com.soaint.srvaprobjefeinmediato.repository.srvaprobjefeinmediato;

import co.com.soaint.componentescomunes.entidades.GestionSolicitud;
import co.com.soaint.componentescomunes.entidades.Solicitud;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface IGestionSolicitudRepository extends JpaRepository<GestionSolicitud, Integer> {
   GestionSolicitud findByIdGestionSolicitud(Integer idgestionSolicitud);


   @Query("select (tablabasica.idTablaBasica) from TablaBasica tablabasica where (tablabasica.codigoApp) like (:codigoApp)")
   Integer findIdTablaBasicaByNumeroRadicado(@Param("codigoApp") String codigoApp);
}