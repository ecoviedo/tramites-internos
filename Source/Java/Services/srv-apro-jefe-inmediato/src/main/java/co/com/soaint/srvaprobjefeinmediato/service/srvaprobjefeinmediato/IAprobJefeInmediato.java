package co.com.soaint.srvaprobjefeinmediato.service.srvaprobjefeinmediato;


import co.com.soaint.srvaprobjefeinmediato.commons.Dto.AprobacionJefeInmediatoDto;

public interface IAprobJefeInmediato {

    void setAprobJefeInmediato(AprobacionJefeInmediatoDto aprobJefeInmediato) throws Exception;

    AprobacionJefeInmediatoDto findAprobJefeInmediato(String numeroRadicado) throws Exception;

}
