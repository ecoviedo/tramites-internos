package co.com.soaint.srvaprobjefeinmediato.service.srvaprobjefeinmediato.impl;
import co.com.soaint.srvaprobjefeinmediato.commons.exception.BusinessException;
import co.com.soaint.srvaprobjefeinmediato.commons.Dto.AprobacionJefeInmediatoDto;
import co.com.soaint.srvaprobjefeinmediato.repository.srvaprobjefeinmediato.impl.IEmpleadoRepositoryFacade;
import co.com.soaint.srvaprobjefeinmediato.repository.srvaprobjefeinmediato.impl.IGestionSolicitudRepositoryFacade;
import co.com.soaint.srvaprobjefeinmediato.repository.srvaprobjefeinmediato.impl.IGestionTramitesEntesSuperioresRepositoryFacade;
import co.com.soaint.srvaprobjefeinmediato.repository.srvaprobjefeinmediato.impl.ISolicitudRepositoryFacade;
import co.com.soaint.srvaprobjefeinmediato.service.srvaprobjefeinmediato.IAprobJefeInmediato;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class AprobJefeInmediatoImpl implements IAprobJefeInmediato {
    private final IGestionSolicitudRepositoryFacade gestionSolicitudRepositoryFacade;
    private final IGestionTramitesEntesSuperioresRepositoryFacade gestionTramitesEntesSuperioresRepositoryFacade;
    private final ISolicitudRepositoryFacade solicitudRepositoryFacade;
    private final IEmpleadoRepositoryFacade empleadoRepositoryFacade;
    @Autowired
    public AprobJefeInmediatoImpl(IGestionSolicitudRepositoryFacade gestionSolicitudRepositoryFacade, IGestionTramitesEntesSuperioresRepositoryFacade gestionTramitesEntesSuperioresRepositoryFacade, ISolicitudRepositoryFacade solicitudRepositoryFacade, IEmpleadoRepositoryFacade empleadoRepositoryFacade) {
        this.gestionSolicitudRepositoryFacade = gestionSolicitudRepositoryFacade;
        this.gestionTramitesEntesSuperioresRepositoryFacade = gestionTramitesEntesSuperioresRepositoryFacade;
        this.solicitudRepositoryFacade = solicitudRepositoryFacade;
        this.empleadoRepositoryFacade = empleadoRepositoryFacade;
    }


    @Transactional
    @Override
    public void setAprobJefeInmediato(AprobacionJefeInmediatoDto aprobJefeInmediato) throws Exception {
        Solicitud solicitud = solicitudRepositoryFacade.findIdSolicitudByNumeroRadicado(aprobJefeInmediato.getNumeroRadicado());
        if(solicitud.getGestionSolicitud() != null){
            throw BusinessException.alreadyExistManagement("Jefe Inmediato");
        } else if(solicitud == null) {
            throw BusinessException.notFound("Solicitud");
        }
        GestionSolicitud gestionSolicitud = new GestionSolicitud();
        gestionSolicitud.setAprobJefe(createGestionTramiteJefe(aprobJefeInmediato));
        solicitud.setGestionSolicitud(gestionSolicitudRepositoryFacade.setAprobJefeInmediato(gestionSolicitud));
        solicitudRepositoryFacade.updateSolicitud(solicitud);

    }

    @Override
    public AprobacionJefeInmediatoDto findAprobJefeInmediato(String numeroRadicado) throws Exception {
       Solicitud solicitud = solicitudRepositoryFacade.findIdSolicitudByNumeroRadicado(numeroRadicado);
        if(solicitud == null) {
            throw BusinessException.notFound("Solicitud");
        }
       GestionSolicitud gestionSolicitud = solicitud.getGestionSolicitud();
       AprobacionJefeInmediatoDto aprobacionJefeInmediatoDto = new AprobacionJefeInmediatoDto();
       aprobacionJefeInmediatoDto.setNumeroRadicado(solicitud.getNumeroRadicado());
       aprobacionJefeInmediatoDto.setEmpleadoRemplazo(gestionSolicitud.getAprobJefe().getEmpleadoReemplazo().getNumeroDocumento());
       aprobacionJefeInmediatoDto.setFechaAprobacion(gestionSolicitud.getAprobJefe().getFechaAprobacion());
        aprobacionJefeInmediatoDto.setObservaciones(gestionSolicitud.getAprobJefe().getObservaciones());
        aprobacionJefeInmediatoDto.setReemplazo(gestionSolicitud.getAprobJefe().getReemplazo());
        aprobacionJefeInmediatoDto.setRemunerado(gestionSolicitud.getAprobJefe().getRemunerado());
        aprobacionJefeInmediatoDto.setVistoBueno(gestionSolicitud.getAprobJefe().getVistoBueno().getCodigoApp());
        return aprobacionJefeInmediatoDto;
    }

    private GestionTramiteEnteSuperior createGestionTramiteJefe(AprobacionJefeInmediatoDto aprobJefeInmediato){
        GestionTramiteEnteSuperior gestionTramiteEnteSuperior = new GestionTramiteEnteSuperior();
        TablaBasica tablaBasica = new TablaBasica();
        tablaBasica.setIdTablaBasica(gestionSolicitudRepositoryFacade.findIdTablaBasicaByCodigoApp(aprobJefeInmediato.getVistoBueno()));
        if(aprobJefeInmediato.getEmpleadoRemplazo() != null){
            Empleado empleado = empleadoRepositoryFacade.findEmpleadoByNumeroDocumento(aprobJefeInmediato.getEmpleadoRemplazo());
            gestionTramiteEnteSuperior.setEmpleadoReemplazo(empleado);
        }
        gestionTramiteEnteSuperior.setFechaAprobacion(aprobJefeInmediato.getFechaAprobacion());
        gestionTramiteEnteSuperior.setObservaciones(aprobJefeInmediato.getObservaciones());
        gestionTramiteEnteSuperior.setReemplazo(aprobJefeInmediato.isReemplazo());
        gestionTramiteEnteSuperior.setRemunerado(aprobJefeInmediato.isRemunerado());
        gestionTramiteEnteSuperior.setVistoBueno(tablaBasica);
        gestionTramitesEntesSuperioresRepositoryFacade.createGestionEnteSuperior(gestionTramiteEnteSuperior);
        return gestionTramiteEnteSuperior;
    }
}
