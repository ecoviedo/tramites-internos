package co.com.soaint.srvaprobjefeinmediato.adapter.domain.response.detalleSolicitud;

import co.com.soaint.componentescomunes.Dto.DetalleSolicitudDto;

import java.io.Serializable;

public class DetalleSolicitudDtoResponse implements Serializable {
    public DetalleSolicitudDto body;
    public String status;
    public String timeResponse;
    public String message;
    public String path;
    public String transactionState;

    public DetalleSolicitudDtoResponse(DetalleSolicitudDto body, String status, String timeResponse, String message, String path, String transactionState) {
        this.body = body;
        this.status = status;
        this.timeResponse = timeResponse;
        this.message = message;
        this.path = path;
        this.transactionState = transactionState;
    }

    public DetalleSolicitudDto getBody() {
        return body;
    }

    public void setBody(DetalleSolicitudDto body) {
        this.body = body;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimeResponse() {
        return timeResponse;
    }

    public void setTimeResponse(String timeResponse) {
        this.timeResponse = timeResponse;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getTransactionState() {
        return transactionState;
    }

    public void setTransactionState(String transactionState) {
        this.transactionState = transactionState;
    }
}
