package co.com.soaint.srvaprobjefeinmediato.web.api.rest.srvaprobjefeinmediato.v1;
import co.com.soaint.srvaprobjefeinmediato.commons.Dto.AprobacionJefeInmediatoDto;
import co.com.soaint.srvaprobjefeinmediato.commons.constants.api.radicarSolicitud.EndpointAprobJefeSolicitud;
import co.com.soaint.srvaprobjefeinmediato.commons.domains.response.builder.ResponseBuilder;
import co.com.soaint.srvaprobjefeinmediato.commons.enums.TransactionState;
import co.com.soaint.srvaprobjefeinmediato.service.srvaprobjefeinmediato.IAprobJefeInmediato;
import co.com.soaint.srvaprobjefeinmediato.web.api.rest.IAprobJefeInmediatoApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(EndpointAprobJefeSolicitud.APROB_JEFE_INMEDIATO_API_V1)
@CrossOrigin("*")
public class AprobJefeInmediatoApiImp implements IAprobJefeInmediatoApi {

    private final IAprobJefeInmediato iAprobJefeInmediato;

    @Autowired
    public AprobJefeInmediatoApiImp(IAprobJefeInmediato iAprobJefeInmediato) {
        this.iAprobJefeInmediato = iAprobJefeInmediato;
    }

    @GetMapping("/")
    @RequestMapping(value = EndpointAprobJefeSolicitud.FIND_APROB_JEFE_INMEDIATO + "{numeroRadicado}")
    @Override
    public ResponseEntity findSolicitudByNumeroRadicado(@PathVariable("numeroRadicado") String numeroRadicado) {
        try {
            return ResponseBuilder.newBuilder()
                    .withStatus(HttpStatus.OK)
                    .withTransactionState(TransactionState.OK)
                    .withResponse( iAprobJefeInmediato.findAprobJefeInmediato(numeroRadicado))
                    .withMessage("La Aprobacion del Jefe Inmediato se ha realizado Con Exito")
                    .withPath(EndpointAprobJefeSolicitud.SET_APROB_JEFE_INMEDIATO)
                    .buildResponse();
        } catch (Exception e){
            e.printStackTrace();
            return ResponseBuilder.newBuilder()
                    .withStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                    .withTransactionState(TransactionState.FAIL)
                    .withPath(EndpointAprobJefeSolicitud.SET_APROB_JEFE_INMEDIATO)
                    .withMessage(e.getMessage())
                    .buildResponse();
        } }

    @PostMapping(EndpointAprobJefeSolicitud.SET_APROB_JEFE_INMEDIATO)
    @Override
    public ResponseEntity setAprobJefeInmediato(@RequestBody AprobacionJefeInmediatoDto aprobJefeInmediato) {
        try {
            iAprobJefeInmediato.setAprobJefeInmediato(aprobJefeInmediato);
            return ResponseBuilder.newBuilder()
                    .withStatus(HttpStatus.OK)
                    .withTransactionState(TransactionState.OK)
                    .withMessage("La Aprobacion del Jefe Inmediato se ha realizado Con Exito")
                    .withPath(EndpointAprobJefeSolicitud.SET_APROB_JEFE_INMEDIATO)
                    .buildResponse();
        } catch (Exception e){
            e.printStackTrace();
            return ResponseBuilder.newBuilder()
                    .withStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                    .withTransactionState(TransactionState.FAIL)
                    .withPath(EndpointAprobJefeSolicitud.SET_APROB_JEFE_INMEDIATO)
                    .withMessage(e.getMessage())
                    .buildResponse();
        }
    }

}
