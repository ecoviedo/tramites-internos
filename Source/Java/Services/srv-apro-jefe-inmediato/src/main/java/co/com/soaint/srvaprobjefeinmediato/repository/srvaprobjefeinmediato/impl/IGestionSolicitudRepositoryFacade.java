package co.com.soaint.srvaprobjefeinmediato.repository.srvaprobjefeinmediato.impl;

import co.com.soaint.componentescomunes.entidades.GestionSolicitud;

public interface IGestionSolicitudRepositoryFacade {
    GestionSolicitud setAprobJefeInmediato(GestionSolicitud gestionSolicitud);

    GestionSolicitud findAprobJefeInmediato(Integer numeroSolicitud);

    Integer findIdTablaBasicaByCodigoApp(String codigoApp);
}
