package co.com.soaint.srvaprobjefeinmediato.repository.srvaprobjefeinmediato.impl;

import co.com.soaint.componentescomunes.entidades.GestionTramiteEnteSuperior;
import co.com.soaint.srvaprobjefeinmediato.repository.srvaprobjefeinmediato.IGestionTramiteEntesSuperioresRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GestionTramitesEntesSuperioresRepositoryImpl implements IGestionTramitesEntesSuperioresRepositoryFacade{
    private final IGestionTramiteEntesSuperioresRepository repository;
    @Autowired
    public GestionTramitesEntesSuperioresRepositoryImpl(IGestionTramiteEntesSuperioresRepository repository) {
        this.repository = repository;
    }

    @Override
    public void createGestionEnteSuperior(GestionTramiteEnteSuperior gestionTramiteEnteSuperior) {
        repository.save(gestionTramiteEnteSuperior);
    }
}
