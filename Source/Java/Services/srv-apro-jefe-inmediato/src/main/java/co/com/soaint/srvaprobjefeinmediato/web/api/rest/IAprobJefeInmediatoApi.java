package co.com.soaint.srvaprobjefeinmediato.web.api.rest;

import co.com.soaint.srvaprobjefeinmediato.commons.Dto.AprobacionJefeInmediatoDto;
import org.springframework.http.ResponseEntity;
public interface IAprobJefeInmediatoApi {

    ResponseEntity setAprobJefeInmediato(AprobacionJefeInmediatoDto aprobJefeInmediato);
    ResponseEntity findSolicitudByNumeroRadicado(String numeroRadicado);
}
