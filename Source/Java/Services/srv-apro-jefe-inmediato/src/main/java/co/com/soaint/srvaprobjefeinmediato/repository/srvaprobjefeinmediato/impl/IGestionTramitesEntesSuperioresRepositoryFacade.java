package co.com.soaint.srvaprobjefeinmediato.repository.srvaprobjefeinmediato.impl;


import co.com.soaint.componentescomunes.entidades.GestionTramiteEnteSuperior;

public interface IGestionTramitesEntesSuperioresRepositoryFacade {
    void createGestionEnteSuperior(GestionTramiteEnteSuperior gestionTramiteEnteSuperior);
}
