/**
 * 
 */
package co.com.soaint.trasnformData;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import co.com.soaint.dto.ContainerDto;
import co.com.soaint.dto.InstanceDto;
import co.com.soaint.dto.ParametrosDto;
import co.com.soaint.dto.ProcessRequestDto;
import co.com.soaint.dto.ProcessResponseDto;
import co.com.soaint.dto.ProcessesDto;
import co.com.soaint.dto.ResponseDto;

/**
 * @author ovillamil
 *
 */
@Service
public class VariableData {

	/**
	 * Trasnform Data to Process Response
	 * @param requestDto
	 * @param parametros
	 * @return
	 */
	public ProcessResponseDto parametrosToResponseDto (ProcessRequestDto requestDto, ParametrosDto parametros) {
		ProcessResponseDto processResponseDto = new ProcessResponseDto();
		
		if (!parametros.getValues().isEmpty()) {
        	List<ContainerDto> containers = new ArrayList<ContainerDto>();
        	
			List<ProcessesDto> processes = new ArrayList<ProcessesDto>();
			// Asignación de Proceso
			InstanceDto instance = new InstanceDto();
			            instance.setInstanceId(requestDto.getProcessInstance());
			ProcessesDto process = new ProcessesDto();
						 process.setParametros(parametros);
		 			 	 process.setInstance(instance);
		 			 	 
			processes.add(process);			 		
        	
        	// Asignacion contenedor
        	ContainerDto container = new ContainerDto();
			 			 container.setContainerId(requestDto.getContainerId());
			 			 container.setProcesses(processes);
			containers.add(container);
        	processResponseDto.setContainers(containers);
        	
        	// Asignación de Respuesta
        	ResponseDto responseDto = new ResponseDto();
        				responseDto.setMsg("SUCCESS");
        				responseDto.setType("SUCCESS");
        				responseDto.setStatusCode("200");
        	processResponseDto.setResponse(responseDto);
        }
		
		return processResponseDto;
	}
}
