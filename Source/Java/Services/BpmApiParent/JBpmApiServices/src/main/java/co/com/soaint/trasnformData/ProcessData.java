/**
 * 
 */
package co.com.soaint.trasnformData;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import co.com.soaint.dto.ContainerDto;
import co.com.soaint.dto.InstanceDto;
import co.com.soaint.dto.ProcessResponseDto;
import co.com.soaint.dto.ProcessesDto;
import co.com.soaint.dto.ResponseDto;
import co.com.soaint.model.KieProcess;
import co.com.soaint.model.KieProcesses;

/**
 * @author ovillamil
 *
 */
@Service
public class ProcessData {
	
	/**
	 * Trasnform Kie Process Data to Response Dto
	 * @param kieProcesses
	 * @param containerId
	 * @return
	 */
	public ProcessResponseDto kieProcessesToResponseDto (KieProcesses kieProcesses, String containerId) {
		ProcessResponseDto processResponseDto = new ProcessResponseDto();
		
		if (kieProcesses.getProcesses() != null) {
        	List<ContainerDto> containers = new ArrayList<ContainerDto>();
        	
			List<ProcessesDto> processes = new ArrayList<ProcessesDto>();			 
        	// Logica para el proceso
        	for (KieProcess e : kieProcesses.getProcesses()) {
				ProcessesDto process = new ProcessesDto();
							 process.setProcessId(e.getProcessId());
							 process.setProcessName(e.getProcessName());
							 process.setProcessVersion(e.getProcessVersion());
							 process.setPackages(e.get_package());
							 process.setInitiator(e.getInitiator());
							 process.setStartDate(e.getStartDate() != null ? e.getStartDate().getJavaUtilDate() : null);
							 InstanceDto instance = new InstanceDto();
							 			 instance.setInstanceId(e.getProcessInstanceId());
							 process.setInstance(instance);
				processes.add(process);			 		
        	}
        	
        	// Asignacion contenedor
        	ContainerDto container = new ContainerDto();
			 			 container.setContainerId(containerId);
			 			 container.setProcesses(processes);
			containers.add(container);
        	processResponseDto.setContainers(containers);
        	
        	// Asignación de Respuesta
        	ResponseDto responseDto = new ResponseDto();
        				responseDto.setMsg("SUCCESS");
        				responseDto.setType("SUCCESS");
        				responseDto.setStatusCode("200");
        	processResponseDto.setResponse(responseDto);
        }
		
		return processResponseDto;
	}
	
	/**
	 * Response services to Response Dto
	 * @param postResponse
	 * @return
	 */
	public ProcessResponseDto responseToResponseDto(ResponseEntity<String> postResponse) {
		ProcessResponseDto processResponseDto = new ProcessResponseDto();
		
		// Asignación de Respuesta
    	ResponseDto responseDto = new ResponseDto();
    				responseDto.setStatusCode(Integer.toString(postResponse.getStatusCodeValue()));
    				responseDto.setMsg("SUCCESS");
    				responseDto.setType("SUCCESS");
    	processResponseDto.setResponse(responseDto);
		
		return processResponseDto;
	}

	/**
	 * 
	 * @param kieProcesses
	 * @return
	 */
	public ProcessResponseDto kieProcessesToResponseDto(KieProcesses kieProcesses) {
		ProcessResponseDto processResponseDto = new ProcessResponseDto();
		
		if (kieProcesses.getProcesses() != null) {
			List<ContainerDto> containers = new ArrayList<ContainerDto>();
			ContainerDto containerDto = new ContainerDto();
			List<ProcessesDto> processes = new ArrayList<ProcessesDto>();
			
			Iterator<KieProcess> it = kieProcesses.getProcesses().iterator();
			while (it.hasNext()) {
				KieProcess kieProcess= it.next();
				
				ProcessesDto process = new ProcessesDto();
				process.setProcessId(kieProcess.getProcessId());
				process.setProcessName(kieProcess.getProcessName());
				process.setProcessVersion(kieProcess.getProcessVersion());
				process.setStartDate(kieProcess.getStartDate() != null ? kieProcess.getStartDate().getJavaUtilDate() : null);
				process.setInitiator(kieProcess.getInitiator() != null ? kieProcess.getInitiator() : null);
				process.setPackages(kieProcess.get_package());
				InstanceDto instance = new InstanceDto();
							 instance.setInstanceId(kieProcess.getProcessInstanceId());
				process.setInstance(instance);
				process.setContainerId(kieProcess.getContainerId());
				processes.add(process);
			}
			
			containerDto.setProcesses(processes);
			containers.add(containerDto);
			processResponseDto.setContainers(containers);

			// Asignación de Respuesta
			ResponseDto responseDto = new ResponseDto();
						responseDto.setStatusCode("200");
						responseDto.setMsg("SUCCESS");
        				responseDto.setType("SUCCESS");
			processResponseDto.setResponse(responseDto);
		}
		return processResponseDto;
	}

}
