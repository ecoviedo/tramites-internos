
/**
 * 
 */
package co.com.soaint.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.com.soaint.dto.ProcessRequestDto;
import co.com.soaint.dto.ProcessResponseDto;
import co.com.soaint.dto.UserDto;
import co.com.soaint.service.ProcessService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author ovillamil
 *
 */
@RestController
@RequestMapping("/api/process")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Api(description = "Controls the list of processes per container and creates a new process instance")
public class
ProcessController {
	
	@Autowired
	ProcessService service;

	/**
	 * Get all process by container
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/processesByContainer", method = RequestMethod.POST)
	@ApiOperation("Returns list of all process by container.")
    public ResponseEntity<ProcessResponseDto> allProcessByContainer(@RequestBody ProcessRequestDto request) {
        if (service.getProcessByContainerId(request) == null) {
            return new ResponseEntity<ProcessResponseDto>(HttpStatus.NO_CONTENT);
        }         
        return new ResponseEntity<ProcessResponseDto>(service.getProcessByContainerId(request), HttpStatus.OK);
    }
	
	/**
	 * Create new Process Instance
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/startProcessInstance", method = RequestMethod.POST)
	@ApiOperation("Create new Process Instance in the container.")
    public ResponseEntity<ProcessResponseDto> addProcessInstance (@RequestBody ProcessRequestDto request) {
		String processInstance = service.startProcessInstance(request);
        if (processInstance == null) {
            return new ResponseEntity<ProcessResponseDto>(HttpStatus.NO_CONTENT);
        } 
        request.setProcessInstance(processInstance);
        return new ResponseEntity<ProcessResponseDto>(service.getPropertiesByProcessInstance(request), HttpStatus.OK);
    }
	
	/**
	 * Get all process Instances By Container
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/intancesByContainer", method = RequestMethod.POST)
	@ApiOperation("Returns list of all process instance by container.")
    public ResponseEntity<ProcessResponseDto> allProcessInstanceByContainer(@RequestBody ProcessRequestDto request) {
        if (service.getProcessInstanceByContainer(request) == null) {
            return new ResponseEntity<ProcessResponseDto>(HttpStatus.NO_CONTENT);
        }         
        return new ResponseEntity<ProcessResponseDto>(service.getProcessInstanceByContainer(request), HttpStatus.OK);
    }
	
	/**
	 * Get all process definitions
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/listDefinitions", method = RequestMethod.POST)
	@ApiOperation("Returns all process definitions.")
    public ResponseEntity<ProcessResponseDto> allProcessDefinitions(@RequestBody ProcessRequestDto request) {
		ProcessResponseDto processResponseDto = service.getProcessDefinitions(request);
        if (processResponseDto == null) {
            return new ResponseEntity<ProcessResponseDto>(HttpStatus.NO_CONTENT);
        }         
        return new ResponseEntity<ProcessResponseDto>(processResponseDto, HttpStatus.OK);
    }
	
	/**
	 * Cancel the active process instance
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/processIntance", method = RequestMethod.DELETE)
	@ApiOperation("Cancel the active process instance.")
    public ResponseEntity<ProcessResponseDto> cancelProcessIntance(@RequestBody ProcessRequestDto request) {
        if (service.cancelProcessInstance(request) == null) {
            return new ResponseEntity<ProcessResponseDto>(HttpStatus.NO_CONTENT);
        }         
        return new ResponseEntity<ProcessResponseDto>(service.cancelProcessInstance(request), HttpStatus.OK);
    }
	
	/**
	 * Get all process Instances
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/allInstances", method = RequestMethod.POST)
	@ApiOperation("Returns all process instances.")
    public ResponseEntity<ProcessResponseDto> allProcessInstances(@RequestBody ProcessRequestDto request) {
		ProcessResponseDto processResponseDto = service.getProcessInstances(request);
        if (processResponseDto == null) {
            return new ResponseEntity<ProcessResponseDto>(HttpStatus.NO_CONTENT);
        }         
        return new ResponseEntity<ProcessResponseDto>(processResponseDto, HttpStatus.OK);
    }
	
	/**
	 * 
	 * @param request
	 * @return
	 *
	@RequestMapping(value = "/authentication", method = RequestMethod.POST)
	@ApiOperation("Returns all process instances.")
    public ResponseEntity<UserDto> dummyUsers(@RequestBody UserDto request) {      
        return new ResponseEntity<UserDto>(request, HttpStatus.OK);
    }
    */
}
