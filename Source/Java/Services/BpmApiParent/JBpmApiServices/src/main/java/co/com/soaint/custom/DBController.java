package co.com.soaint.custom;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.com.soaint.dto.ProcessRequestDto;
import co.com.soaint.dto.ProcessResponseDto;
import io.swagger.annotations.ApiOperation;

/**
 * 
 * @author jjmorales
 *
 */

@RestController
@RequestMapping("/api/db")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class DBController {
	
	@Autowired
	private Environment environment;
	
	@RequestMapping(value = "/taskDataByInstanceId", method = RequestMethod.POST)  
    public ResponseEntity<AssignmentResponseDto> taskDataByInstanceId(@RequestBody AssignmentRequestDto requestDto){
//    		@RequestParam("instanceId") String instanceId, 
//    		@RequestParam("activityName") String activityName,
//    		@RequestParam("actualOwner") String actualOwner) {
        String actualOwnerDB = "";
        String createdOn = "";
        String taskid = "";
        String lastModificationDate = "";
        boolean reassignment = false;
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;
        Timestamp tsCreatedOn = null;
        Timestamp tsLastModificationDate = null;
        Timestamp tsDelegated = null;
        
        AssignmentResponseDto assignmentDto = new AssignmentResponseDto();

        try {
			Class.forName(environment.getProperty("spring.datasource.driver-class-name").toString());
	        String url = environment.getProperty("spring.datasource.url").toString();
	        String user = environment.getProperty("spring.datasource.username").toString();
	        String password = environment.getProperty("spring.datasource.password").toString();
			connection = DriverManager.getConnection(url, user, password);
			stmt = connection.createStatement();
	        rs = stmt.executeQuery("select * from jbpm.audittaskimpl where processinstanceid = "+requestDto.getInstanceId()+" and name = '"+requestDto.getActivityName()+"';");
	
	        if  ( rs.next() ) {
	        	actualOwnerDB = rs.getString(3);
//	        	createdOn = rs.getString(5);
	        	tsCreatedOn = rs.getTimestamp(5) ;
	        	tsLastModificationDate = rs.getTimestamp(9) ;
	        	taskid = rs.getString(17);
	        }
	        
	        rs.close();
	        rs = stmt.executeQuery("SELECT * FROM jbpm.taskevent where taskid = " + taskid + " and type = 'DELEGATED';");
	        
	        if  ( rs.next() ) {
	        	System.out.println(rs.getString(1));
	        	tsDelegated = rs.getTimestamp(2);
	        }
	        
	        Date dateCreatedOn = new Date(tsCreatedOn.getTime());
        	Date dateLastModification = new Date(tsLastModificationDate.getTime());
        	
        	String timeOut = "";
        	if(tsDelegated != null) {
        		if(requestDto.getActualOwner().compareToIgnoreCase(actualOwnerDB) == 0) {
        			timeOut = "N/A";
        			reassignment = false;
        		}else {
        			Date dateDelegated = new Date(tsDelegated.getTime());
        			timeOut = calculateTimeOut(dateDelegated, dateLastModification, requestDto.getOriginalTime());
                	reassignment = true;
        		}
        		
        	}else {
        		timeOut = "N/A";
        		reassignment = false;
        	}
        	
        	System.out.println("TimeOut->" + timeOut);
        	
        	assignmentDto.setActualOwner(actualOwnerDB);
        	assignmentDto.setReassignment(reassignment);
        	
        	
        	
        	
        	assignmentDto.setTimeOut(timeOut);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} catch (ClassNotFoundException e1) {
			System.out.println(e1.getMessage());
		} finally {
		
			try {
				rs.close();
				stmt.close(); 
		        connection.close();
			} catch (SQLException e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
        
        return new ResponseEntity<AssignmentResponseDto>(assignmentDto, HttpStatus.OK);
    }
	
	private String calculateTimeOut(Date fechaInicio, Date fechaFin, String originalTimeJbpm) {
		String tiempoServicio = "";
		String tiempoFormatoBpm = "";
		long minOriginalTime =0;
		long tiempoInicial=fechaInicio.getTime();		
		long tiempoFinal=fechaFin.getTime(); 
		long milisegundos=tiempoFinal - tiempoInicial;		
		long horas = milisegundos/3600000;
		long restohoras = milisegundos%3600000;
		long minutos = restohoras/60000;
		long restominutos = restohoras%60000;
		long segundos = restominutos/1000;
		long restosegundos = restominutos%1000;
		tiempoServicio = horas + ":" + minutos + ":" + segundos + "." + restosegundos;
		System.out.println("Duracion->" + tiempoServicio);
		
		minOriginalTime = extractMinutes(originalTimeJbpm);
		
		tiempoFormatoBpm = "PT" + (minOriginalTime-minutos) + "M";
		
		return tiempoFormatoBpm;
	}

	private long extractMinutes(String originalTimeJbpm) {
		long lTime = 0; 
		int posIni = originalTimeJbpm.indexOf("PT");
		int posFina = originalTimeJbpm.indexOf("M");
		String time = originalTimeJbpm.substring(posIni+2, posFina);
		lTime = Long.parseLong(time);
		return lTime;
	}
}
