package co.com.soaint.custom;

import java.io.Serializable;

public class AssignmentResponseDto implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String actualOwner;
	
	private String timeOut;
	
	private boolean reassignment;

	public String getActualOwner() {
		return actualOwner;
	}

	public void setActualOwner(String actualOwner) {
		this.actualOwner = actualOwner;
	}

	public String getTimeOut() {
		return timeOut;
	}

	public void setTimeOut(String timeOut) {
		this.timeOut = timeOut;
	}

	public boolean isReassignment() {
		return reassignment;
	}

	public void setReassignment(boolean reassignment) {
		this.reassignment = reassignment;
	}

	

}
