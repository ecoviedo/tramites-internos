/**
 * 
 */
package co.com.soaint.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.google.gson.Gson;

import co.com.soaint.dto.ProcessRequestDto;
import co.com.soaint.dto.ProcessResponseDto;
import co.com.soaint.model.TaskResponse;
import co.com.soaint.model.TaskSummary;
import co.com.soaint.trasnformData.TaskData;

/**
 * @author jjmorales
 *
 */
@Service
public class TaskService {

	@Autowired
	private Environment environment;

	@Autowired
	private TaskData taskData;

	private RestTemplate restTemplate;

	/**
	 * 
	 */
	public TaskService() {
		restTemplate = new RestTemplate();
	}

	/**
	 * Get active tasks list
	 * @param processRequestDto
	 * @return
	 */
	public ProcessResponseDto getActiveTasks(ProcessRequestDto processRequestDto) {
		String url = environment.getProperty("jbpm_end_point").toString() + "/queries/tasks/instances";
		
		String user = processRequestDto.getOwnerUser().getUser();
		String password = processRequestDto.getOwnerUser().getPassword();
		
		HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON);
					headers.setBasicAuth(user, password);
		
		HttpEntity<?> request = new HttpEntity<>(headers);
		
		ResponseEntity<String> postResponse = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
		TaskResponse response = new Gson().fromJson(postResponse.getBody(), TaskResponse.class);
		
        return taskData.kieResponseToResponseDto(response);
	}


	/**
	 * Get All Task by Process
	 * @param requestDto
	 * @return
	 */
	public ProcessResponseDto getTasksByProcessInstance(ProcessRequestDto processRequestDto) {
		String url = environment.getProperty("jbpm_end_point").toString()
				+ "/queries/tasks/instances/process/" + processRequestDto.getProcessInstance();
		
		String user = processRequestDto.getOwnerUser().getUser();
		String password = processRequestDto.getOwnerUser().getPassword();
		
		HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON);
					headers.setBasicAuth(user, password);

		HttpEntity<?> request = new HttpEntity<>(headers);
		
		ResponseEntity<String> postResponse = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
		TaskResponse response = new Gson().fromJson(postResponse.getBody(), TaskResponse.class);
		
		return taskData.kieResponseToResponseDto(response);
	}
	
	/**
	 * Change de Task status
	 * @param processRequestDto
	 * @return
	 */
	public ProcessResponseDto changeTaskStatus(ProcessRequestDto processRequestDto) {
		String url = environment.getProperty("jbpm_end_point").toString() + "/containers/"+processRequestDto.getContainerId()
			+"/tasks/"+processRequestDto.getTaskId()+"/states/"+processRequestDto.getTaskStatus();
		
		String user = processRequestDto.getOwnerUser().getUser();
		String password = processRequestDto.getOwnerUser().getPassword();
		
		HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON);
					headers.setBasicAuth(user, password);
					
		Map<String, Object> map = new HashMap<String, Object>(); 
		if (processRequestDto.getParametros() != null && processRequestDto.getParametros().getValues() != null) {
			map = processRequestDto.getParametros().getValues();
		}
		
		HttpEntity<?> request = new HttpEntity<>(map, headers);
		
		ResponseEntity<String> postResponse = null;
		try {
			postResponse = restTemplate.exchange(url, HttpMethod.PUT, request, String.class);
		} catch (Exception e) {
			return taskData.kieResponseToResponseDto(e);
		}
		
        return taskData.kieResponseToResponseDto(postResponse);
	}

	/**
	 * Reassing task to user
	 * @param processRequestDto
	 * @return
	 */
	public ProcessResponseDto reassign_forwardedTask(ProcessRequestDto processRequestDto, String operation) {
		String url = environment.getProperty("jbpm_end_point").toString() + "/containers/"+processRequestDto.getContainerId()
			   +"/tasks/"+processRequestDto.getTaskId()+"/states/"+operation;
	
		String user = processRequestDto.getOwnerUser().getUser();
		String password = processRequestDto.getOwnerUser().getPassword();
		
		HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON);
					headers.setBasicAuth(user, password);
		HttpEntity<?> request = new HttpEntity<>(headers);
		
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(url)
	              .queryParam("user", processRequestDto.getAssignment().getUser())
	              .queryParam("targetUser", processRequestDto.getAssignment().getTargetUser());
		url = uriBuilder.toUriString().replaceAll("%20", " ");
		
		ResponseEntity<String> postResponse = null;
		try {
			postResponse = restTemplate.exchange(url, HttpMethod.PUT, request, String.class);
		} catch (Exception e) {
			return taskData.kieResponseToResponseDto(e);
		}
		
	    return taskData.kieResponseToResponseDto(postResponse);
	}

	/**
	 * 
	 * @param processRequestDto
	 * @return
	 */
	public ProcessResponseDto getTasksByOwner(ProcessRequestDto processRequestDto) {
		String url = environment.getProperty("jbpm_end_point").toString() + "/queries/tasks/instances/owners";
			
		String user = processRequestDto.getOwnerUser().getUser();
		String password = processRequestDto.getOwnerUser().getPassword();
		
		HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON);
					headers.setBasicAuth(user, password);
		HttpEntity<?> request = new HttpEntity<>(headers);
		UriComponentsBuilder uriBuilder = loadUriBuilder(processRequestDto, url);
		ResponseEntity<String> postResponse = restTemplate.exchange(uriBuilder.toUriString(), HttpMethod.GET, request, String.class);
		TaskResponse response = new Gson().fromJson(postResponse.getBody(), TaskResponse.class);
		
		return taskData.kieResponseToResponseDto(response);
	}
	
	/**
	 * 
	 * @param requestDto
	 * @return
	 */
	public ProcessResponseDto getTasksByGroups(ProcessRequestDto requestDto) {
		String url = environment.getProperty("jbpm_end_point").toString() 
				+ "/queries/tasks/instances/pot-owners";
		
		String user = requestDto.getOwnerUser().getUser();
		String password = requestDto.getOwnerUser().getPassword();
		HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON);
					headers.setBasicAuth(user, password);
		HttpEntity<?> request = new HttpEntity<>(headers);
		
		UriComponentsBuilder uriBuilder = loadUriBuilder(requestDto, url);
		ResponseEntity<String> postResponse = restTemplate.exchange(uriBuilder.toUriString(), HttpMethod.GET, request, String.class);		
		TaskResponse response = new Gson().fromJson(postResponse.getBody(), TaskResponse.class);
       
		return taskData.kieResponseToResponseDto(response);	
	}
	
	/**
	 * 
	 * @param requestDto
	 * @return
	 */
	public int countTasksByGroups(ProcessRequestDto requestDto) {
		String url = environment.getProperty("jbpm_end_point").toString() 
				+ "/queries/tasks/instances/pot-owners";
		String user = requestDto.getOwnerUser().getUser();
		String password = requestDto.getOwnerUser().getPassword();
		HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON);
					headers.setBasicAuth(user, password);
		HttpEntity<?> request = new HttpEntity<>(headers);
		UriComponentsBuilder uriBuilder = loadUriBuilder(requestDto, url);
		ResponseEntity<String> postResponse = restTemplate.exchange(uriBuilder.toUriString(), HttpMethod.GET, request, String.class);		
		TaskResponse response = new Gson().fromJson(postResponse.getBody(), TaskResponse.class);
		
		if (!response.getTaskSummary().isEmpty())
			return response.getTaskSummary().size();
		else
			return 0;
	} 

	/**
	 * 
	 * @param requestDto
	 * @return
	 */
	public ProcessResponseDto getTask(ProcessRequestDto processRequestDto) {
		String url = environment.getProperty("jbpm_end_point").toString() + "/containers/"
				+processRequestDto.getContainerId() + "/tasks/" + processRequestDto.getTaskId();
		
		String user = processRequestDto.getOwnerUser().getUser();
		String password = processRequestDto.getOwnerUser().getPassword();
		
		HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON);
					headers.setBasicAuth(user, password);
		HttpEntity<?> request = new HttpEntity<>(headers);
		
		ResponseEntity<String> postResponse= null;
		try {
			postResponse = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
		} catch (Exception e) {
			return taskData.kieResponseToResponseDto(e);
		}
		
		TaskSummary taskSummary = new Gson().fromJson(postResponse.getBody(), TaskSummary.class);
		
		TaskResponse response = new TaskResponse();
		List<TaskSummary> taskSummarylist = new ArrayList<TaskSummary>();
		taskSummarylist.add(taskSummary);
		response.setTaskSummary(taskSummarylist);
		
		return taskData.kieResponseToResponseDto(response);
	}

	

	/**
	 * 
	 * @param requestDto
	 * @param url
	 * @return
	 */
	private UriComponentsBuilder loadUriBuilder(ProcessRequestDto requestDto, String url) {
		UriComponentsBuilder uriBuilder = null;
		final MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		
		if (requestDto.getOwnerUser() != null)
			params.add("user", requestDto.getOwnerUser().getUser());
		
		if (requestDto.getGroups() != null && requestDto.getGroups().size() > 0)
			params.put("groups", requestDto.getGroups());
		
		if (requestDto.getTaskStates() != null && requestDto.getTaskStates().size() > 0)
			params.put("status", requestDto.getTaskStates());
		
		if (requestDto.getPage() != null) 
			params.add("page", Integer.toString(requestDto.getPage()) );
		
		if (requestDto.getPageSize() != null) 
			params.add("pageSize", Integer.toString(requestDto.getPageSize()) );

		uriBuilder = UriComponentsBuilder.fromHttpUrl(url).queryParams(params);
		return uriBuilder;
	}
}
