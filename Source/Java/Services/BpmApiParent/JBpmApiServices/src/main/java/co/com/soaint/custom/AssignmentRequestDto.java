package co.com.soaint.custom;

import java.io.Serializable;

public class AssignmentRequestDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String instanceId;
	private String activityName;
	private String actualOwner;
	private String originalTime;
	
	public String getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	public String getActualOwner() {
		return actualOwner;
	}
	public void setActualOwner(String actualOwner) {
		this.actualOwner = actualOwner;
	}
	public String getOriginalTime() {
		return originalTime;
	}
	public void setOriginalTime(String originalTime) {
		this.originalTime = originalTime;
	}
}
