/**
 * 
 */
package co.com.soaint.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.com.soaint.dto.ProcessRequestDto;
import co.com.soaint.dto.ProcessResponseDto;
import co.com.soaint.service.TaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author jjmorales
 *
 */
@RestController
@RequestMapping("/api/task")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Api(description = "Controls the basic functionalities of the tasks, lists them, creates tasks and changes the status during the life cycle of the process.")
public class TaskController {
	
	@Autowired
	TaskService service;
	
	
	@RequestMapping(value = "/task", method = RequestMethod.POST)  
	@ApiOperation("Returns the instance of the task.")
    public ResponseEntity<ProcessResponseDto> getTask(@RequestBody ProcessRequestDto requestDto) {
		ProcessResponseDto  processResponseDto = service.getTask(requestDto);
        if (processResponseDto.getResponse().getType().equalsIgnoreCase("FAIL")) {
            return new ResponseEntity<ProcessResponseDto>(processResponseDto,HttpStatus.OK);
        }         
        return new ResponseEntity<ProcessResponseDto>(processResponseDto, HttpStatus.OK);
    }
	
	/**
	 * Get all active tasks
	 * @param requestDto
	 * @return
	 */
	@RequestMapping(value = "/activeTasks", method = RequestMethod.POST)  
	@ApiOperation("Returns the list of all active tasks in the project.")
    public ResponseEntity<ProcessResponseDto> listAllActiveTasks(@RequestBody ProcessRequestDto requestDto) {
		ProcessResponseDto  processResponseDto = service.getActiveTasks(requestDto);
        if (processResponseDto == null) {
            return new ResponseEntity<ProcessResponseDto>(HttpStatus.NO_CONTENT);
        }         
        return new ResponseEntity<ProcessResponseDto>(processResponseDto, HttpStatus.OK);
    }
	
	/**
	 * Change the status of a task
	 * @param requestDto
	 * @return
	 */
	@RequestMapping(value = "/taskStatus", method = RequestMethod.PUT) 
	@ApiOperation("Changes the status during the life cycle of the process.")
    public ResponseEntity<ProcessResponseDto> changeTaskStatus(@RequestBody ProcessRequestDto requestDto) {
		ProcessResponseDto  processResponseDtoChange = service.changeTaskStatus(requestDto);

        if (processResponseDtoChange.getResponse().getType().equalsIgnoreCase("FAIL")) {
            return new ResponseEntity<ProcessResponseDto>(processResponseDtoChange,HttpStatus.OK);
        } 
        
        ProcessResponseDto  processResponseDtoGet = service.getTask(requestDto);
        processResponseDtoGet.setResponse(processResponseDtoChange.getResponse());
        return new ResponseEntity<ProcessResponseDto>(processResponseDtoGet, HttpStatus.CREATED);
    }
	
	/**
	 * Reassign a task
	 * @param requestDto
	 * @return
	 */
	@RequestMapping(value = "/reassignTask", method = RequestMethod.PUT)
	@ApiOperation("Assign the selected task to a specific user.")
    public ResponseEntity<ProcessResponseDto> reassignTask(@RequestBody ProcessRequestDto requestDto) {
		ProcessResponseDto  processResponseDto = service.reassign_forwardedTask(requestDto, "delegated");
		if (processResponseDto.getResponse().getType().equalsIgnoreCase("FAIL")) {
            return new ResponseEntity<ProcessResponseDto>(processResponseDto,HttpStatus.OK);
        }
		ProcessResponseDto  processResponseDtoGet = service.getTask(requestDto);
        processResponseDtoGet.setResponse(processResponseDto.getResponse());
        return new ResponseEntity<ProcessResponseDto>(processResponseDtoGet, HttpStatus.CREATED);
    }
	
	/**
	 * Forwards a task
	 * @param requestDto
	 * @return
	 */
	@RequestMapping(value = "/forwardsTask", method = RequestMethod.PUT)
	@ApiOperation("Assign the selected task to a specific user.")
    public ResponseEntity<ProcessResponseDto> forwardsTask(@RequestBody ProcessRequestDto requestDto) {
		ProcessResponseDto  processResponseDto = service.reassign_forwardedTask(requestDto, "forwarded");
        if (processResponseDto == null) {
            return new ResponseEntity<ProcessResponseDto>(HttpStatus.NO_CONTENT);
        }  
        return new ResponseEntity<ProcessResponseDto>(processResponseDto, HttpStatus.OK);
    }
	
	/**
	 * Get task By Process Instance
	 * @param requestDto
	 * @return
	 */
	@RequestMapping(value = "/tasksByProcessInstance", method = RequestMethod.POST)
	@ApiOperation("Returns list of all task by Process Instance.")
    public ResponseEntity<ProcessResponseDto> listAllTaskByProcesss (@RequestBody ProcessRequestDto request) {
        if (service.getTasksByProcessInstance(request) == null) {
            return new ResponseEntity<ProcessResponseDto>(HttpStatus.NO_CONTENT);
        }         
        return new ResponseEntity<ProcessResponseDto>(service.getTasksByProcessInstance(request), HttpStatus.OK);
    }
	
	/**
	 * Get task By Process Owner
	 * @param requestDto
	 * @return
	 */
	@RequestMapping(value = "/tasksByOwner", method = RequestMethod.POST)
	@ApiOperation("Returns list of all active tasks by owner.")
    public ResponseEntity<ProcessResponseDto> listAllTaskByOwner (@RequestBody ProcessRequestDto requestDto) {
		ProcessResponseDto processResponseDto = service.getTasksByOwner(requestDto);
        if (processResponseDto == null) {
            return new ResponseEntity<ProcessResponseDto>(HttpStatus.NO_CONTENT);
        }         
        return new ResponseEntity<ProcessResponseDto>(processResponseDto, HttpStatus.OK);
    }
	
	
	@RequestMapping(value = "/tasksByGroups", method = RequestMethod.POST)
	@ApiOperation("Returns list of tasks by groups and state.")
    public ResponseEntity<ProcessResponseDto> listAllTaskByGroups (@RequestBody ProcessRequestDto requestDto) {
		ProcessResponseDto processResponseDto = service.getTasksByGroups(requestDto);
        if (processResponseDto == null) {
            return new ResponseEntity<ProcessResponseDto>(HttpStatus.NO_CONTENT);
        }         
        return new ResponseEntity<ProcessResponseDto>(processResponseDto, HttpStatus.OK);
    }
	
	@RequestMapping(value = "/countTasksByGroups", method = RequestMethod.POST)
	@ApiOperation("Returns number of tasks by groups and state.")
    public int countTasksByGroups (@RequestBody ProcessRequestDto requestDto) {
		return service.countTasksByGroups(requestDto);
    }
}
