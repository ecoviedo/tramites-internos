package co.com.soaint.trasnformData;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.google.gson.internal.LinkedTreeMap;

import co.com.soaint.dto.ContainerDto;
import co.com.soaint.dto.DecisionDto;
import co.com.soaint.dto.ProcessResponseDto;
import co.com.soaint.dto.ResponseDto;
import co.com.soaint.dto.RulesDto;
import co.com.soaint.dto.InputDto;
import co.com.soaint.model.Decision;
import co.com.soaint.model.Input;
import co.com.soaint.model.Model;
import co.com.soaint.model.Response;

/**
 * 
 * @author jjmorales
 *
 */
@Service
public class RuleData {

	public RuleData() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ProcessResponseDto kieResponseToResponseDto(Response response) {
		ProcessResponseDto processResponseDto = new ProcessResponseDto();
		
		if (response.getType().equals("SUCCESS") && response.getResult().getDmnModelInfoList() != null) {
			processResponseDto = getModelInfoList(response);	
        }

		if (response.getType().equals("SUCCESS") && response.getResult().getDmnEvaluationResult() != null) {
			processResponseDto = getEvaluationResult(response);
        }
		
		return processResponseDto;
	}

	/**
	 * 
	 * @param response
	 * @return
	 */
	private ProcessResponseDto getModelInfoList(Response response) {
		ProcessResponseDto processResponseDto = new ProcessResponseDto();
		List<ContainerDto> containers = new ArrayList<ContainerDto>();
    	List<RulesDto> rules = new ArrayList<RulesDto>();
    	ContainerDto container = new ContainerDto();
    	
    	if (response.getResult().getDmnModelInfoList() != null && response.getResult().getDmnModelInfoList().getModels() != null) {
        	for (Model model : response.getResult().getDmnModelInfoList().getModels()) {
				RulesDto rule = new RulesDto();
					rule.setRuleId(model.getModelId());
					rule.setRuleName(model.getModelName());
					rule.setRuleNamespace(model.getModelNamespace());
								
				List<InputDto> inputs = new ArrayList<InputDto>();
				for (Input input : model.getInputs()) {
					InputDto inputDto = new InputDto();
						inputDto.setInputId(input.getInputdataId());
						inputDto.setInputName(input.getInputdataName());
					inputs.add(inputDto);
				}
				rule.setInputList(inputs);
				
				List<DecisionDto> decisions = new ArrayList<DecisionDto>();
				for (Decision decision : model.getDecisions()) {
					DecisionDto decisionDto = new DecisionDto();
						decisionDto.setDecisionId(decision.getDecisionId());
						decisionDto.setDecisionName(decision.getDecisionName());
					decisions.add(decisionDto);
				}
				rule.setDecisionList(decisions);
				rules.add(rule);
        	}
        	container.setRules(rules);
        	containers.add(container);
        	processResponseDto.setContainers(containers);;
    	}
    	ResponseDto responseDto = new ResponseDto();
    				responseDto.setMsg(response.getMsg());
    				responseDto.setType(response.getType());
    				responseDto.setStatusCode("200");
    	processResponseDto.setResponse(responseDto);
    	
    	return processResponseDto;
	}

	/**
	 * 
	 * @param response
	 * @return
	 */
	private ProcessResponseDto getEvaluationResult(Response response) {
		ProcessResponseDto processResponseDto = new ProcessResponseDto();
		
		List<ContainerDto> containers = new ArrayList<ContainerDto>();
    	List<RulesDto> rules = new ArrayList<RulesDto>();
    	ContainerDto container = new ContainerDto();
    	
    	RulesDto rule = new RulesDto();
    	rule.setRuleNamespace(response.getResult().getDmnEvaluationResult().getModelNamespace());
    	rule.setRuleName(response.getResult().getDmnEvaluationResult().getModelName());
    	
    	List<DecisionDto> decisions = new ArrayList<DecisionDto>();
    	DecisionDto decisionDto = new DecisionDto();
    	
    	LinkedTreeMap<String, Object> mapDecisionResults = response.getResult().getDmnEvaluationResult().getDecisionResults();
    	Set<String> mapKeysDecisionResults = mapDecisionResults.keySet();
    	for (String keyDecisionResults : mapKeysDecisionResults) {
    		LinkedTreeMap<String, Object> mapObject = (LinkedTreeMap<String, Object>) mapDecisionResults.get(keyDecisionResults);
    		Set<String> mapKeysObject = mapObject.keySet();
    		for (String keyObject : mapKeysObject) {
    			switch(keyObject) 
    	        { 
    	            case "decision-id":
    	            	decisionDto.setDecisionId( (String) mapObject.get(keyObject) );
    	                break; 
    	            case "decision-name": 
    	            	decisionDto.setDecisionName( (String) mapObject.get(keyObject) );
    	                break; 
    	            case "result": 
    	            	decisionDto.setDecisionResult( (boolean) mapObject.get(keyObject) ); 
    	                break; 
    	            case "status": 
    	            	decisionDto.setDecisionStatus( (String) mapObject.get(keyObject) );
    	                break;
    	        }         			
    		}
        }
    	decisions.add(decisionDto);
    	rule.setDecisionList(decisions);
    	
    	rule.setContext(response.getResult().getDmnEvaluationResult().getDmnContext());
    	
    	
    	rules.add(rule);
    	container.setRules(rules);
    	containers.add(container);
    	processResponseDto.setContainers(containers);;
    	
    	ResponseDto responseDto = new ResponseDto();
    				responseDto.setMsg(response.getMsg());
    				responseDto.setType(response.getType());
    				responseDto.setStatusCode("200");
    	processResponseDto.setResponse(responseDto);
    	
    	return processResponseDto;
	}

}
