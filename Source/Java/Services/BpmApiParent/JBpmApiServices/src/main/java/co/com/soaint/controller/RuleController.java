package co.com.soaint.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.com.soaint.dto.ProcessRequestDto;
import co.com.soaint.dto.ProcessResponseDto;
import co.com.soaint.service.RuleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 
 * @author jjmorales
 *
 */
@RestController
@RequestMapping("/api/rule")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Api(description = "Controls the basic functionalities of the rules generated in the BPM, lists them and checks their status.")
public class RuleController {

	@Autowired
	RuleService service;
	
	@RequestMapping(value = "/rules", method = RequestMethod.POST)  
	@ApiOperation("Retrieve the rules for a given container.")
    public ResponseEntity<ProcessResponseDto> getRules(@RequestBody ProcessRequestDto requestDto) {
		ProcessResponseDto  processResponseDto = service.getRules(requestDto);
        if (processResponseDto.getResponse().getType().equalsIgnoreCase("FAIL")) {
            return new ResponseEntity<ProcessResponseDto>(processResponseDto,HttpStatus.OK);
        }         
        return new ResponseEntity<ProcessResponseDto>(processResponseDto, HttpStatus.OK);
    }
	
	@RequestMapping(value = "/evaluateRule", method = RequestMethod.POST)  
	@ApiOperation("Retrieve the rules for a given container.")
    public ResponseEntity<ProcessResponseDto> evaluateRule(@RequestBody ProcessRequestDto requestDto) {
		ProcessResponseDto  processResponseDto = service.evaluateRule(requestDto);
        if (processResponseDto.getResponse().getType().equalsIgnoreCase("FAIL")) {
            return new ResponseEntity<ProcessResponseDto>(processResponseDto,HttpStatus.OK);
        }         
        return new ResponseEntity<ProcessResponseDto>(processResponseDto, HttpStatus.OK);
    }
}
