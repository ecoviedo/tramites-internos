
/**
 * 
 */
package co.com.soaint.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.google.gson.Gson;

import co.com.soaint.dto.ProcessRequestDto;
import co.com.soaint.dto.ProcessResponseDto;
import co.com.soaint.model.KieProcess;
import co.com.soaint.model.KieProcesses;
import co.com.soaint.model.ProcessesInstances;
import co.com.soaint.trasnformData.ProcessData;

/**
 * @author ovillamil
 *
 */
@Service
public class ProcessService {

	@Autowired
	private Environment environment;
	
	@Autowired
	private ProcessData processData;
	
	private RestTemplate restTemplate;
	
	/**
	 * 
	 */
	public ProcessService(){
		restTemplate = new RestTemplate();
	}
	
	/**
	 * Search all process by container
	 * @param requestDto
	 * @return
	 */
	public ProcessResponseDto getProcessByContainerId (ProcessRequestDto requestDto) {
		String url = environment.getProperty("jbpm_end_point").toString() + "/containers/" + requestDto.getContainerId() + "/processes";
		String user = requestDto.getOwnerUser().getUser();
		String password = requestDto.getOwnerUser().getPassword();
		
		HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON);
					headers.setBasicAuth(user, password);
		
		HttpEntity<?> request = new HttpEntity<>(headers);
		
		ResponseEntity<String> postResponse = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
		KieProcesses response = new Gson().fromJson(postResponse.getBody(), KieProcesses.class);
		
        return processData.kieProcessesToResponseDto(response, requestDto.getContainerId());
	} 
	
	/**
	 * Create Process Instance
	 * @param requestDto
	 * @return
	 */
	public String startProcessInstance (ProcessRequestDto requestDto) {
		String url = environment.getProperty("jbpm_end_point").toString() + 
					"/containers/" + requestDto.getContainerId() + "/processes/" + requestDto.getProcessesId() + "/instances";
		String user = requestDto.getOwnerUser().getUser();
		String password = requestDto.getOwnerUser().getPassword();
		
		HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON);
					headers.setBasicAuth(user, password);
					
		Map<String, Object> map = new HashMap<String, Object>(); 
		if (requestDto.getParametros() != null && requestDto.getParametros().getValues() != null) {
			map = requestDto.getParametros().getValues();
		}
		
		HttpEntity<?> request = new HttpEntity<>(map, headers);
		
		ResponseEntity<String> postResponse = restTemplate.exchange(url, HttpMethod.POST, request, String.class);
		
		return postResponse.getBody();
	}
	
	/**
	 * Return Properties to process instance
	 * @param requestDto
	 * @return
	 */
	public ProcessResponseDto getPropertiesByProcessInstance (ProcessRequestDto requestDto) {
		String url = environment.getProperty("jbpm_end_point").toString() + 
					"/containers/" + requestDto.getContainerId() + "/processes/instances/" + requestDto.getProcessInstance();
		String user = requestDto.getOwnerUser().getUser();
		String password = requestDto.getOwnerUser().getPassword();
		
		HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON);
					headers.setBasicAuth(user, password);
		
		HttpEntity<?> request = new HttpEntity<>(headers);
		
		ResponseEntity<String> postResponse = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
		KieProcess kieProcess = new Gson().fromJson(postResponse.getBody(), KieProcess.class);
		
		List<KieProcess> processes = new ArrayList<KieProcess>();
						 processes.add(kieProcess);
		KieProcesses response = new KieProcesses();
					 response.setProcesses(processes);
		
        return processData.kieProcessesToResponseDto(response, requestDto.getContainerId());
	} 
	
	/**
	 * Search all Instances by container
	 * @param requestDto
	 * @return
	 */
	public ProcessResponseDto getProcessInstanceByContainer (ProcessRequestDto requestDto) {
		String url = environment.getProperty("jbpm_end_point").toString() + 
					"/containers/" + requestDto.getContainerId() + "/processes/instances";
		String user = requestDto.getOwnerUser().getUser();
		String password = requestDto.getOwnerUser().getPassword();
		
		HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON);
					headers.setBasicAuth(user, password);
		
		HttpEntity<?> request = new HttpEntity<>(headers);
		
		ResponseEntity<String> postResponse = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
		ProcessesInstances instances = new Gson().fromJson(postResponse.getBody(), ProcessesInstances.class);
		
		KieProcesses response = new KieProcesses();
					 response.setProcesses(instances.getProcessInstance());
		
        return processData.kieProcessesToResponseDto(response, requestDto.getContainerId());
	} 
	
	/**
	 * 
	 * @param requestDto
	 * @return
	 */
	public ProcessResponseDto getProcessDefinitions (ProcessRequestDto requestDto) {
		String url = environment.getProperty("jbpm_end_point").toString() + "/queries/processes/definitions";
		String user = requestDto.getOwnerUser().getUser();
		String password = requestDto.getOwnerUser().getPassword();
		
		HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON);
					headers.setBasicAuth(user, password);
		
		HttpEntity<?> request = new HttpEntity<>(headers);
		
		ResponseEntity<String> postResponse = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
		
		KieProcesses response = new Gson().fromJson(postResponse.getBody(), KieProcesses.class);
		
        return processData.kieProcessesToResponseDto(response);
	} 
	
	/**
	 * Cancel active Process Instance
	 * @param requestDto
	 * @return
	 */
	public ProcessResponseDto cancelProcessInstance (ProcessRequestDto requestDto) {
		String url = environment.getProperty("jbpm_end_point").toString() 
				+ "/containers/" + requestDto.getContainerId() + "/processes/instances";
		String user = requestDto.getOwnerUser().getUser();
		String password = requestDto.getOwnerUser().getPassword();
		
		HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON);
					headers.setBasicAuth(user, password);
		
		HttpEntity<?> request = new HttpEntity<>(headers);
		
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(url)
  															  .queryParam("instanceId", requestDto.getProcessInstance());
		
		ResponseEntity<String> postResponse = restTemplate.exchange(uriBuilder.toUriString(), HttpMethod.DELETE, request, String.class);
		
        return processData.responseToResponseDto(postResponse);
	}

	/**
	 * 
	 * @param requestDto
	 * @return
	 */
	public ProcessResponseDto getProcessInstances(ProcessRequestDto requestDto) {
		String url = environment.getProperty("jbpm_end_point").toString() + "/queries/processes/instances";
			
		String user = requestDto.getOwnerUser().getUser();
		String password = requestDto.getOwnerUser().getPassword();
		
		HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON);
					headers.setBasicAuth(user, password);
		
		HttpEntity<?> request = new HttpEntity<>(headers);		
		
		ResponseEntity<String> postResponse = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
		ProcessesInstances instances = new Gson().fromJson(postResponse.getBody(), ProcessesInstances.class);
		
		KieProcesses response = new KieProcesses();
					 response.setProcesses(instances.getProcessInstance());
		
        return processData.kieProcessesToResponseDto(response);
	} 
	
}