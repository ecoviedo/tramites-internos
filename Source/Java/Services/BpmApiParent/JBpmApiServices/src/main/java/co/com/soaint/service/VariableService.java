
/**
 * 
 */
package co.com.soaint.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import co.com.soaint.dto.ParametrosDto;
import co.com.soaint.dto.ProcessRequestDto;
import co.com.soaint.dto.ProcessResponseDto;
import co.com.soaint.dto.ResponseDto;
import co.com.soaint.model.KieProcesses;
import co.com.soaint.model.ProcessesInstances;
import co.com.soaint.model.TaskResponse;
import co.com.soaint.trasnformData.ProcessData;
import co.com.soaint.trasnformData.TaskData;
import co.com.soaint.trasnformData.VariableData;

/**
 * @author ovillamil
 *
 */
@Service
public class VariableService {

	@Autowired
	private Environment environment;
	
	@Autowired
	private VariableData variableData;
	
	@Autowired
	private TaskData taskData;
	
	@Autowired
	private ProcessData processData;
	
	private RestTemplate restTemplate;
	
	/**
	 * 
	 */
	public VariableService(){
		restTemplate = new RestTemplate();
	}
	
	/**
	 * Get Process Variable By Instance
	 * @param requestDto
	 * @return
	 */
	public ProcessResponseDto getProcessVariables (ProcessRequestDto requestDto) {
		String url = environment.getProperty("jbpm_end_point").toString() + "/containers/" + requestDto.getContainerId()
			+ "/processes/instances/" + requestDto.getProcessInstance() + "/variables";
		String user = requestDto.getOwnerUser().getUser();
		String password = requestDto.getOwnerUser().getPassword();
		
		HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON);
					headers.setBasicAuth(user, password);
		
		HttpEntity<?> request = new HttpEntity<>(headers);
		
		ResponseEntity<String> postResponse = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
		Map<?, ?> map = new Gson().fromJson(postResponse.getBody(), Map.class);
		
		Map<String, Object> newMap = new HashMap<String, Object>();		
		map.entrySet().forEach(entry -> {
			newMap.put(entry.getKey().toString(), entry.getValue());
        });
		
		ParametrosDto parametrosDto = new ParametrosDto();
					  parametrosDto.setValues(newMap);
		
        return variableData.parametrosToResponseDto(requestDto, parametrosDto);
	} 
	
	/**
	 * 
	 * @param requestDto
	 * @return
	 */
	public ProcessResponseDto getValuesProcessVariables (ProcessRequestDto requestDto) {
		String url = environment.getProperty("jbpm_end_point").toString() + "/containers/" + requestDto.getContainerId()
			+ "/processes/instances/" + requestDto.getProcessInstance() + "/variable/";
		String user = requestDto.getOwnerUser().getUser();
		String password = requestDto.getOwnerUser().getPassword();
		
		HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON);
					headers.setBasicAuth(user, password);
		
		HttpEntity<?> request = new HttpEntity<>(headers);

		Map<String, Object> map = new HashMap<String, Object>(); 
		if (requestDto.getParametros() != null && requestDto.getParametros().getValues() != null) {
			requestDto.getParametros().getValues().entrySet().forEach(entry -> {
				String newUrl = url + entry.getKey().toString();
				String value = "";
				try {
					ResponseEntity<String> postResponse = restTemplate.exchange(newUrl, HttpMethod.GET, request, String.class);
					value = new Gson().fromJson(postResponse.getBody(), String.class);
				} catch (Exception e) {
					value = "NOT_FOUND";
				}
				map.put(entry.getKey(), value);
	        });
		}
		
		ParametrosDto parametrosDto = new ParametrosDto();
					  parametrosDto.setValues(map);
		
        return variableData.parametrosToResponseDto(requestDto, parametrosDto);
	}

	/**
	 * 
	 * @param requestDto
	 * @return
	 */
	public ProcessResponseDto getTasksByVariable(ProcessRequestDto requestDto) {
		ProcessResponseDto processResponseDto= new ProcessResponseDto();
		String url = environment.getProperty("jbpm_end_point").toString() + "/queries/tasks/instances/variables/";
		
		if(requestDto.getParametros() != null && requestDto.getParametros().getValue() != null) {
			url += requestDto.getParametros().getValue().getKey();
			
			String user = requestDto.getOwnerUser().getUser();
			String password = requestDto.getOwnerUser().getPassword();
			
			HttpHeaders headers = new HttpHeaders();
						headers.setContentType(MediaType.APPLICATION_JSON);
						headers.setBasicAuth(user, password);
				
			HttpEntity<?> request = new HttpEntity<>(headers);
			
			ResponseEntity<String> postResponse = null;
			try {
				postResponse = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
			} catch (Exception e) {
				return taskData.kieResponseToResponseDto(e);
			}
			
			TaskResponse response = new Gson().fromJson(postResponse.getBody(), TaskResponse.class);
			
			processResponseDto = taskData.kieResponseToResponseDto(response);
		} else {
			// Asignación de Respuesta
			ResponseDto responseDto = new ResponseDto();
						responseDto.setStatusCode("404");
						responseDto.setMsg("Attribute not found");
	    				responseDto.setType("FAIL");
	    	processResponseDto.setResponse(responseDto);
		}
			
		return processResponseDto;
	}

	public ProcessResponseDto getInstancesByVariable(ProcessRequestDto requestDto) {
		ProcessResponseDto processResponseDto= new ProcessResponseDto();
		String url = environment.getProperty("jbpm_end_point").toString() + "/queries/processes/instances/variables/";
		
		if(requestDto.getParametros() != null && requestDto.getParametros().getValue() != null) {
			url += requestDto.getParametros().getValue().getValue();
			
			String user = requestDto.getOwnerUser().getUser();
			String password = requestDto.getOwnerUser().getPassword();
			
			HttpHeaders headers = new HttpHeaders();
						headers.setContentType(MediaType.APPLICATION_JSON);
						headers.setBasicAuth(user, password);
				
			HttpEntity<?> request = new HttpEntity<>(headers);
			
			ResponseEntity<String> postResponse = null;
			try {
				postResponse = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
			} catch (Exception e) {
				return taskData.kieResponseToResponseDto(e);
			}
			
			ProcessesInstances instances = new Gson().fromJson(postResponse.getBody(), ProcessesInstances.class);			
			KieProcesses response = new KieProcesses();
						 response.setProcesses(instances.getProcessInstance());
						
			processResponseDto = processData.kieProcessesToResponseDto(response);
		} else {
			// Asignación de Respuesta
			ResponseDto responseDto = new ResponseDto();
						responseDto.setStatusCode("404");
						responseDto.setMsg("Attribute not found");
	    				responseDto.setType("FAIL");
	    	processResponseDto.setResponse(responseDto);
		}
			
		return processResponseDto;
	} 
}
