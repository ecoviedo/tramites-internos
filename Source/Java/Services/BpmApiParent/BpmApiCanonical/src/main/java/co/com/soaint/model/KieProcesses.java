/**
 * 
 */
package co.com.soaint.model;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author ovillamil
 *
 */
public class KieProcesses {
	
	@SerializedName("processes")
	@Expose
	private List<KieProcess> processes;

	/**
	 * 
	 */
	public KieProcesses() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the processes
	 */
	public List<KieProcess> getProcesses() {
		return processes;
	}

	/**
	 * @param processes the processes to set
	 */
	public void setProcesses(List<KieProcess> processes) {
		this.processes = processes;
	}

}
