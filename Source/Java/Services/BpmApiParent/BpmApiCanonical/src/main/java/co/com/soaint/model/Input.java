
package co.com.soaint.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * 
 * @author jjmorales
 *
 */
public class Input {
	
	@SerializedName("inputdata-id")
    @Expose
    private String inputdataId;
    @SerializedName("inputdata-name")
    @Expose
    private String inputdataName;
    @SerializedName("inputdata-typeRef")
    @Expose
    private InputdataTypeRef inputdataTypeRef;

    public Input() {
		super();
		// TODO Auto-generated constructor stub
	}
    
    /**
     * 
     * @return the inputdataId
     */
    public String getInputdataId() {
        return inputdataId;
    }

    /**
     * 
     * @param inputdataId the inputdataId to set
     */
    public void setInputdataId(String inputdataId) {
        this.inputdataId = inputdataId;
    }

    /**
     * 
     * @return the inputdataName
     */
    public String getInputdataName() {
        return inputdataName;
    }

    /**
     * 
     * @param inputdataName the inputdataName to set
     */
    public void setInputdataName(String inputdataName) {
        this.inputdataName = inputdataName;
    }

    /**
     * 
     * @return the inputdataTypeRef
     */
    public InputdataTypeRef getInputdataTypeRef() {
        return inputdataTypeRef;
    }

    /**
     * 
     * @param inputdataTypeRef the inputdataTypeRef to set
     */
    public void setInputdataTypeRef(InputdataTypeRef inputdataTypeRef) {
        this.inputdataTypeRef = inputdataTypeRef;
    }

}
