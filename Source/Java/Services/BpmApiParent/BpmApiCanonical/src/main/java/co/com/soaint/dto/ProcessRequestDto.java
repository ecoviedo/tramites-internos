/**
 * 
 */
package co.com.soaint.dto;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author ovillamil
 *
 */
@ApiModel(description = "Representative class of the process request")
public class ProcessRequestDto implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(notes = "Unique identifier of the container.")
	private String containerId;
	
	@ApiModelProperty(notes = "Identifier of the process.")
	private String processesId;
	
	@ApiModelProperty(notes = "Identifier of the process instance.")
	private String processInstance;
	
	@ApiModelProperty(notes = "Unique identifier of the task.")
	private String taskId;
	
	@ApiModelProperty(notes = "Status of process task.")
	private String taskStatus;
	
	@ApiModelProperty(notes = "User owner of the process task or process instance")
	private UserDto ownerUser;
	
	@ApiModelProperty(notes = "Value representing the assignment of tasks between users")
	private UserDto assignment;
	
	@ApiModelProperty(notes = "Values that represent the process variables.")
	private ParametrosDto parametros;
	
	@ApiModelProperty(notes = "Value that sends or receives general communications within the life cycle of the process.")
	private SignalDto signal;

	@ApiModelProperty(notes = "Optional group names to include in the query.")
	private List<String> groups;
	
	@ApiModelProperty(notes = "Optional states names to include in the query.")
	private List<String> taskStates;
	
	@ApiModelProperty(notes = "Optional pagination - at which page to start, defaults to 0 (meaning first).")
	private Integer page;
	
	@ApiModelProperty(notes = "Optional pagination - size of the result, defaults to 10.")
	private Integer pageSize;

	/**
	 * 
	 */
	public ProcessRequestDto() {
		super();
	}

	/**
	 * @return the containerId
	 */
	public String getContainerId() {
		return containerId;
	}

	/**
	 * @param containerId the containerId to set
	 */
	public void setContainerId(String containerId) {
		this.containerId = containerId;
	}

	/**
	 * @return the processesId
	 */
	public String getProcessesId() {
		return processesId;
	}

	/**
	 * @param processesId the processesId to set
	 */
	public void setProcessesId(String processesId) {
		this.processesId = processesId;
	}

	/**
	 * @return the processInstance
	 */
	public String getProcessInstance() {
		return processInstance;
	}

	/**
	 * @param processInstance the processInstance to set
	 */
	public void setProcessInstance(String processInstance) {
		this.processInstance = processInstance;
	}

	/**
	 * @return the taskId
	 */
	public String getTaskId() {
		return taskId;
	}

	/**
	 * @param taskId the taskId to set
	 */
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	/**
	 * @return the taskStatus
	 */
	public String getTaskStatus() {
		return taskStatus;
	}

	/**
	 * @param taskStatus the taskStatus to set
	 */
	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus;
	}

	/**
	 * @return the ownerUser
	 */
	public UserDto getOwnerUser() {
		return ownerUser;
	}

	/**
	 * @param ownerUser the ownerUser to set
	 */
	public void setOwnerUser(UserDto ownerUser) {
		this.ownerUser = ownerUser;
	}

	/**
	 * @return the assignment
	 */
	public UserDto getAssignment() {
		return assignment;
	}

	/**
	 * @param assignment the assignment to set
	 */
	public void setAssignment(UserDto assignment) {
		this.assignment = assignment;
	}

	/**
	 * @return the parametros
	 */
	public ParametrosDto getParametros() {
		return parametros;
	}

	/**
	 * @param parametros the parametros to set
	 */
	public void setParametros(ParametrosDto parametros) {
		this.parametros = parametros;
	}

	public SignalDto getSignal() {
		return signal;
	}

	public void setSignal(SignalDto signal) {
		this.signal = signal;
	}

	/**
	 * 
	 * @return the page
	 */
	public Integer getPage() {
		return page;
	}

	/**
	 * 
	 * @param page the page to set
	 */
	public void setPage(Integer page) {
		this.page = page;
	}

	/**
	 * 
	 * @return the pageSize
	 */
	public Integer getPageSize() {
		return pageSize;
	}

	/**
	 * 
	 * @param pageSize the pageSize to set
	 */
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * 
	 * @return the groups
	 */
	public List<String> getGroups() {
		return groups;
	}

	/**
	 * 
	 * @param groups the groups to set
	 */
	public void setGroups(List<String> groups) {
		this.groups = groups;
	}

	/**
	 * 
	 * @return the taskStates
	 */
	public List<String> getTaskStates() {
		return taskStates;
	}

	/**
	 * 
	 * @param taskStates the taskStates to set
	 */
	public void setTaskStates(List<String> taskStates) {
		this.taskStates = taskStates;
	}

	
}