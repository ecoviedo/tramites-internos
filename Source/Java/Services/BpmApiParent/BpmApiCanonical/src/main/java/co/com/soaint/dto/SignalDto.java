/**
 * 
 */
package co.com.soaint.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author ovillamil
 *
 */
@ApiModel(description = "Representative class of the signals of process")
public class SignalDto implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(notes = "Name identifier of the signal of process.")
	private String signalName;
	
	@ApiModelProperty(notes = "Values that represent the process variables send in the signal.")
	private ParametrosDto parametros;

	/**
	 * 
	 */
	public SignalDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the signalName
	 */
	public String getSignalName() {
		return signalName;
	}

	/**
	 * @param signalName the signalName to set
	 */
	public void setSignalName(String signalName) {
		this.signalName = signalName;
	}

	/**
	 * @return the parametros
	 */
	public ParametrosDto getParametros() {
		return parametros;
	}

	/**
	 * @param parametros the parametros to set
	 */
	public void setParametros(ParametrosDto parametros) {
		this.parametros = parametros;
	}
	
}
