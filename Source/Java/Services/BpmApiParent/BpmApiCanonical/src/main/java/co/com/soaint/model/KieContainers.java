
package co.com.soaint.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class KieContainers {

    @SerializedName("kie-container")
    @Expose
    private List<KieContainer> kieContainer;

	/**
	 * 
	 */
	public KieContainers() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the kieContainer
	 */
	public List<KieContainer> getKieContainer() {
		return kieContainer;
	}

	/**
	 * @param kieContainer the kieContainer to set
	 */
	public void setKieContainer(List<KieContainer> kieContainer) {
		this.kieContainer = kieContainer;
	}

}
