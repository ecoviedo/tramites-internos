
/**
 * 
 */
package co.com.soaint.dto;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author ovillamil
 *
 */
@ApiModel(description = "Representative class of the process.")
public class ProcessesDto implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(notes = "Unique identifier of the process.")
	private String processId;
	
	@ApiModelProperty(notes = "Name of the process.")
	private String processName;
	
	@ApiModelProperty(notes = "Version of the process.")
	private String processVersion;
	
	@ApiModelProperty(notes = "Date of creation of the process.")
	private BigInteger startDate;
	
	@ApiModelProperty(notes = "User initiator of the process.")
	private String initiator;
	
	@ApiModelProperty(notes = "Information of the package to which the process belongs..")
	private String packages;

	@ApiModelProperty(notes = "Identifier of the process instance.")
	private InstanceDto instance;
	
	@ApiModelProperty(notes = "List of tasks associated with the process.")
	private List<TaskDto> taskList;
	
	@ApiModelProperty(notes = "Values that represent the process variables.")
	private ParametrosDto parametros;
	
	@ApiModelProperty(notes = "Unique identifier of the container.")
	private String containerId;

	/**
	 * 
	 */
	public ProcessesDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the processId
	 */
	public String getProcessId() {
		return processId;
	}

	/**
	 * @param processId the processId to set
	 */
	public void setProcessId(String processId) {
		this.processId = processId;
	}

	/**
	 * @return the processName
	 */
	public String getProcessName() {
		return processName;
	}

	/**
	 * @param processName the processName to set
	 */
	public void setProcessName(String processName) {
		this.processName = processName;
	}

	/**
	 * @return the processVersion
	 */
	public String getProcessVersion() {
		return processVersion;
	}

	/**
	 * @param processVersion the processVersion to set
	 */
	public void setProcessVersion(String processVersion) {
		this.processVersion = processVersion;
	}

	/**
	 * @return the packages
	 */
	public String getPackages() {
		return packages;
	}

	/**
	 * @param packages the packages to set
	 */
	public void setPackages(String packages) {
		this.packages = packages;
	}

	/**
	 * @return the instance
	 */
	public InstanceDto getInstance() {
		return instance;
	}

	/**
	 * @param instance the instance to set
	 */
	public void setInstance(InstanceDto instance) {
		this.instance = instance;
	}

	/**
	 * @return the taskList
	 */
	public List<TaskDto> getTaskList() {
		return taskList;
	}

	/**
	 * @param taskList the taskList to set
	 */
	public void setTaskList(List<TaskDto> taskList) {
		this.taskList = taskList;
	}

	/**
	 * @return the startDate
	 */
	public BigInteger getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(BigInteger startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the initiator
	 */
	public String getInitiator() {
		return initiator;
	}

	/**
	 * @param initiator the initiator to set
	 */
	public void setInitiator(String initiator) {
		this.initiator = initiator;
	}

	/**
	 * 
	 * @return the parametros
	 */
	public ParametrosDto getParametros() {
		return parametros;
	}

	/**
	 * 
	 * @param parametros the parametros to set
	 */
	public void setParametros(ParametrosDto parametros) {
		this.parametros = parametros;
	}

	/**
	 * 
	 * @return the containerId
	 */
	public String getContainerId() {
		return containerId;
	}

	/**
	 * 
	 * @param containerId the containerId to set
	 */
	public void setContainerId(String containerId) {
		this.containerId = containerId;
	}
	
}
