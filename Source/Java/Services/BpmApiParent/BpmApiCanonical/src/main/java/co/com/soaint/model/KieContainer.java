
package co.com.soaint.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class KieContainer {

    @SerializedName("container-id")
    @Expose
    private String containerId;
    @SerializedName("release-id")
    @Expose
    private Release releaseId;
    @SerializedName("resolved-release-id")
    @Expose
    private Release resolvedReleaseId;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("scanner")
    @Expose
    private Scanner scanner;
    @SerializedName("config-items")
    @Expose
    private List<ConfigItem> configItems = null;
    @SerializedName("messages")
    @Expose
    private List<Message> messages = null;
    @SerializedName("container-alias")
    @Expose
    private String containerAlias;
    
	/**
	 * 
	 */
	public KieContainer() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the containerId
	 */
	public String getContainerId() {
		return containerId;
	}

	/**
	 * @param containerId the containerId to set
	 */
	public void setContainerId(String containerId) {
		this.containerId = containerId;
	}

	/**
	 * @return the releaseId
	 */
	public Release getReleaseId() {
		return releaseId;
	}

	/**
	 * @param releaseId the releaseId to set
	 */
	public void setReleaseId(Release releaseId) {
		this.releaseId = releaseId;
	}

	/**
	 * @return the resolvedReleaseId
	 */
	public Release getResolvedReleaseId() {
		return resolvedReleaseId;
	}

	/**
	 * @param resolvedReleaseId the resolvedReleaseId to set
	 */
	public void setResolvedReleaseId(Release resolvedReleaseId) {
		this.resolvedReleaseId = resolvedReleaseId;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the scanner
	 */
	public Scanner getScanner() {
		return scanner;
	}

	/**
	 * @param scanner the scanner to set
	 */
	public void setScanner(Scanner scanner) {
		this.scanner = scanner;
	}

	/**
	 * @return the configItems
	 */
	public List<ConfigItem> getConfigItems() {
		return configItems;
	}

	/**
	 * @param configItems the configItems to set
	 */
	public void setConfigItems(List<ConfigItem> configItems) {
		this.configItems = configItems;
	}

	/**
	 * @return the messages
	 */
	public List<Message> getMessages() {
		return messages;
	}

	/**
	 * @param messages the messages to set
	 */
	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}

	/**
	 * @return the containerAlias
	 */
	public String getContainerAlias() {
		return containerAlias;
	}

	/**
	 * @param containerAlias the containerAlias to set
	 */
	public void setContainerAlias(String containerAlias) {
		this.containerAlias = containerAlias;
	}

}
