
package co.com.soaint.model;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.internal.LinkedTreeMap;

/**
 * 
 * @author jjmorales
 *
 */
public class DmnEvaluationResult {

    @SerializedName("messages")
    @Expose
    private List<Object> messages = null;
    @SerializedName("model-namespace")
    @Expose
    private String modelNamespace;
    @SerializedName("model-name")
    @Expose
    private String modelName;
    @SerializedName("decision-name")
    @Expose
    private List<Object> decisionName = null;
    @SerializedName("dmn-context")
    @Expose
    private LinkedTreeMap<String, Object> dmnContext;
    @SerializedName("decision-results")
    @Expose
    private LinkedTreeMap<String, Object> decisionResults;

    public DmnEvaluationResult() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
     * 
     * @return the messages
     */
    public List<Object> getMessages() {
        return messages;
    }

    /**
     * 
     * @param messages the messages to set
     */
    public void setMessages(List<Object> messages) {
        this.messages = messages;
    }

    /**
     * 
     * @return the modelNamespace
     */
    public String getModelNamespace() {
        return modelNamespace;
    }

    /**
     * 
     * @param modelNamespace the modelNamespace to set
     */
    public void setModelNamespace(String modelNamespace) {
        this.modelNamespace = modelNamespace;
    }

    /**
     * 
     * @return the modelName
     */
    public String getModelName() {
        return modelName;
    }

    /**
     * 
     * @param modelName the modelName to set
     */
    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    /**
     * 
     * @return the decisionName
     */
    public List<Object> getDecisionName() {
        return decisionName;
    }

    /**
     * 
     * @param decisionName the decisionName to set
     */
    public void setDecisionName(List<Object> decisionName) {
        this.decisionName = decisionName;
    }

    /**
     * 
     * @return the dmnContext
     */
    public LinkedTreeMap<String, Object> getDmnContext() {
        return dmnContext;
    }

    /**
     * 
     * @param dmnContext the dmnContext to set
     */
    public void setDmnContext(LinkedTreeMap<String, Object> dmnContext) {
        this.dmnContext = dmnContext;
    }

    /**
     * 
     * @return the decisionResults
     */
    public LinkedTreeMap<String, Object> getDecisionResults() {
        return decisionResults;
    }

    /**
     * 
     * @param decisionResults the decisionResults to set
     */
    public void setDecisionResults(LinkedTreeMap<String, Object> decisionResults) {
        this.decisionResults = decisionResults;
    }

}
