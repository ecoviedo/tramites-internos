package co.com.soaint.srvDiasHabiles.web.api.rest;

import co.com.soaint.srvDiasHabiles.commons.Dto.DiasHabilesDto;
import org.springframework.http.ResponseEntity;

import java.util.Date;

public interface IDiasHabilesApi {

    ResponseEntity obtenerDiasHabilesOfRange(String fechaInicial, String fechaFinal);
}
