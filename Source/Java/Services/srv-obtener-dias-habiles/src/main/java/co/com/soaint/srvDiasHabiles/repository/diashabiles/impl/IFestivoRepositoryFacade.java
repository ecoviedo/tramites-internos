package co.com.soaint.srvDiasHabiles.repository.diashabiles.impl;

import co.com.soaint.componentescomunes.entidades.Festivos;

import java.util.Date;
import java.util.List;

public interface IFestivoRepositoryFacade {
    List<Date> findFestivosByYear(Date fechaInicio, Date fechaFin);
}
