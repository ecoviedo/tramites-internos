package co.com.soaint.srvDiasHabiles.commons.Dto;

import java.io.Serializable;
import java.util.Date;

public class DiasHabilesDto implements Serializable {
    private Date fechaInicio;
    private Date fechaFin;
    private Integer diasHabiles;
    private Date fechaReintegro;

    public DiasHabilesDto(){}
    public DiasHabilesDto(final Date fechaInicio, final Date fechaFin, final Integer diasHabiles) {
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.diasHabiles = diasHabiles;
    }

    public DiasHabilesDto(final Date fechaInicio, final Date fechaFin, final Integer diasHabiles, final Date fechaReintegro) {
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.diasHabiles = diasHabiles;
        this.fechaReintegro = fechaReintegro;
    }

    public Date getFechaReintegro() {
        return this.fechaReintegro;
    }

    public void setFechaReintegro(final Date fechaReintegro) {
        this.fechaReintegro = fechaReintegro;
    }

    public Integer getDiasHabiles() {
        return this.diasHabiles;
    }

    public void setDiasHabiles(final Integer diasHabiles) {
        this.diasHabiles = diasHabiles;
    }

    public Date getFechaInicio() {
        return this.fechaInicio;
    }

    public void setFechaInicio(final Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return this.fechaFin;
    }

    public void setFechaFin(final Date fechaFin) {
        this.fechaFin = fechaFin;
    }
}
