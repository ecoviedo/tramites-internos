package co.com.soaint.srvDiasHabiles.web.api.rest.diashabiles.v1;

import co.com.soaint.srvDiasHabiles.commons.Dto.DiasHabilesDto;
import co.com.soaint.srvDiasHabiles.commons.constants.api.diashabiles.EndpointDiasHabilesApi;
import co.com.soaint.srvDiasHabiles.commons.enums.TransactionState;
import co.com.soaint.srvDiasHabiles.commons.response.builder.ResponseBuilder;
import co.com.soaint.srvDiasHabiles.service.diashabiles.IDiasHabiles;
import co.com.soaint.srvDiasHabiles.web.api.rest.IDiasHabilesApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping(value = EndpointDiasHabilesApi.DIAS_HABILES_API_V1)
@CrossOrigin("*")
public class DiasHabilesApiImp implements IDiasHabilesApi {

    private final IDiasHabiles iDiasHabiles;

    @Autowired
    public DiasHabilesApiImp(IDiasHabiles iDiasHabiles) {
        this.iDiasHabiles = iDiasHabiles;
    }

    @GetMapping(EndpointDiasHabilesApi.GET_DIAS_HABILES)
    @RequestMapping(value = EndpointDiasHabilesApi.GET_DIAS_HABILES+
            EndpointDiasHabilesApi.FECHA_INICIAL + "{fechainicial}"+
            EndpointDiasHabilesApi.FECHA_FINAL + "{fechafinal}")
    @Override
    public ResponseEntity obtenerDiasHabilesOfRange(@PathVariable("fechainicial") String fechaInicial,
                                                    @PathVariable("fechafinal") String fechaFinal) {
        try {
            return ResponseBuilder.newBuilder()
                    .withStatus(HttpStatus.OK)
                    .withResponse(iDiasHabiles.obtenerDiasDisponibles(fechaInicial,fechaFinal))
                    .withTransactionState(TransactionState.OK)
                    .withMessage("Success")
                    .withPath(EndpointDiasHabilesApi.GET_DIAS_HABILES)
                    .buildResponse();
        } catch (Exception e){
            e.printStackTrace();
            return ResponseBuilder.newBuilder()
                    .withStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                    .withTransactionState(TransactionState.FAIL)
                    .withPath(EndpointDiasHabilesApi.GET_DIAS_HABILES)
                    .withMessage(e.getMessage())
                    .buildResponse();
        }
    }
}
