package co.com.soaint.srvDiasHabiles.repository.diashabiles.impl;

import co.com.soaint.componentescomunes.entidades.Festivos;
import co.com.soaint.srvDiasHabiles.repository.diashabiles.IFestivosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
@Component
public class FestivosRepositoryImpl implements IFestivoRepositoryFacade {
    private final IFestivosRepository repository;
    @Autowired
    public FestivosRepositoryImpl(IFestivosRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Date> findFestivosByYear(Date fechaInicio, Date fechaFin) {
        return repository.findByDateRange(fechaInicio, fechaFin);
    }
}
