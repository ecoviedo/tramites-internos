package co.com.soaint.srvDiasHabiles.repository.diashabiles;

import co.com.soaint.componentescomunes.entidades.Festivos;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface IFestivosRepository extends JpaRepository<Festivos, Integer> {

    @Query("select fecha from Festivos fest where fest.fecha between  :fechaInicio and :fechaFin")
    List<Date> findByDateRange(@Param("fechaInicio") Date fechaInicio,@Param("fechaFin") Date fechaFin);

}
