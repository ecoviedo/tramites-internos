package co.com.soaint.srvDiasHabiles.service.diashabiles;

import co.com.soaint.srvDiasHabiles.commons.Dto.DiasHabilesDto;

public interface IDiasHabiles {

    public DiasHabilesDto obtenerDiasDisponibles(String fechaInicial, String fechaFinal) throws Exception;

}
