package co.com.soaint.srvDiasHabiles.service.diashabiles.impl;
import co.com.soaint.componentescomunes.entidades.Festivos;
import co.com.soaint.srvDiasHabiles.commons.Dto.DiasHabilesDto;
import co.com.soaint.srvDiasHabiles.repository.diashabiles.impl.IFestivoRepositoryFacade;
import co.com.soaint.srvDiasHabiles.service.diashabiles.IDiasHabiles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class DiasHabilesImpl implements IDiasHabiles {
    private final IFestivoRepositoryFacade repositoryFacade;

    @Autowired
    public DiasHabilesImpl(IFestivoRepositoryFacade repositoryFacade) {
        this.repositoryFacade = repositoryFacade;
    }

    @Override
    public DiasHabilesDto obtenerDiasDisponibles(String fechaInicioIn, String fechaFinalIn) throws Exception {
        DiasHabilesDto diasHabilesDto = new DiasHabilesDto();
        Date fechaTemporal = new Date();
        final Date fechaInicio = new SimpleDateFormat("yyyy-MM-dd").parse(fechaInicioIn);
        final Date fechaFin = new SimpleDateFormat("yyyy-MM-dd").parse(fechaFinalIn);
        List<Date> diasFestivos = repositoryFacade.findFestivosByYear(fechaInicio,fechaFin);
        long rangoSolicitado =  fechaFin.getTime() - fechaInicio.getTime();
        long diasSolicitados = rangoSolicitado / (24 * 60 * 60 * 1000);
        Calendar calendar = Calendar.getInstance();
        long fechaInicial = fechaInicio.getTime();
        long rangoDiasSolicitados= diasSolicitados;
        for(int i =0;i <= rangoDiasSolicitados;i++){
            fechaTemporal.setTime(fechaInicial);
            calendar.setTime(fechaTemporal);
            if(calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY ||
                    calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY||
                    isHoliday(diasFestivos,fechaTemporal)){
                diasSolicitados -= 1;
            }
            fechaInicial = fechaInicial + 86400000;
        }
        diasHabilesDto.setFechaFin(fechaFin);
        diasHabilesDto.setFechaInicio(fechaInicio);
        diasHabilesDto.setDiasHabiles((int)diasSolicitados+1);
        diasHabilesDto.setFechaReintegro(fechaReintegro(fechaFin,diasFestivos));
        return diasHabilesDto;
    }

    public boolean isHoliday(List<Date> diasFestivos, Date fechaTemporal){
        for (Date diafestivo: diasFestivos) {
            if(diafestivo.getTime() == fechaTemporal.getTime()){
                return true;
            }
        }
        return false;
    }

    public Date fechaReintegro(Date fechaFin,List<Date> diasFestivos){
        Date fechaTemporal = new Date();
        Calendar calendar = Calendar.getInstance();
        boolean isValid = true;
        long fechaFinal = fechaFin.getTime();
        fechaFinal = fechaFinal + 86400000;
        while(isValid) {
            fechaTemporal.setTime(fechaFinal);
            calendar.setTime(fechaTemporal);
            if (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY &&
                    calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY &&
                    !isHoliday(diasFestivos, fechaTemporal)) {
                isValid = false;
            }
            fechaFinal = fechaFinal + 86400000;
        }
        return fechaTemporal;

    }
}
