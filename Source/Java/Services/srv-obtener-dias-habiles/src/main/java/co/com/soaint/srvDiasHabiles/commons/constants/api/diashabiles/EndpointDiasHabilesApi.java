package co.com.soaint.srvDiasHabiles.commons.constants.api.diashabiles;

public interface EndpointDiasHabilesApi {

    String DIAS_HABILES_API_V1 = "api/v1";
    String GET_DIAS_HABILES = "/";
    String FECHA_INICIAL = "fechainicio/";
    String FECHA_FINAL = "/fechafin/";
}
