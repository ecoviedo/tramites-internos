package co.com.soaint.detallesolicitud.adapter.specific.ApiSolicitud;

import co.com.soaint.detallesolicitud.commons.exception.webClient.WebClientException;

public interface IClienteSolicitud {
    Integer findIdSolicitudByNumeroRadicado(String NumeroRadicado) throws WebClientException;
}
