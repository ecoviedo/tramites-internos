package co.com.soaint.detallesolicitud.service.detallesolicitud.impl;
import co.com.soaint.componentescomunes.Dto.DetalleSolicitudDto;
import co.com.soaint.componentescomunes.entidades.DetalleSolicitud;
import co.com.soaint.componentescomunes.entidades.Solicitud;
import co.com.soaint.componentescomunes.entidades.TablaBasica;
import co.com.soaint.detallesolicitud.adapter.specific.ApiSolicitud.ClienteSolicitud;
import co.com.soaint.detallesolicitud.adapter.specific.ApiTablaBasica.ClienteTablaBasica;
import co.com.soaint.detallesolicitud.commons.exception.BusinessException;
import co.com.soaint.detallesolicitud.commons.exception.webClient.WebClientException;
import co.com.soaint.detallesolicitud.repository.detallesolicitud.impl.IDetalleSolicitudRepositoryFacade;
import co.com.soaint.detallesolicitud.service.detallesolicitud.IDetalleSolicitud;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
public class DetalleSolicitudImpl implements IDetalleSolicitud {
    private final ClienteTablaBasica clienteTablaBasica;
    private  final ClienteSolicitud clienteSolicitud;
    private final IDetalleSolicitudRepositoryFacade detalleSolicitudRepository;

    @Autowired
    public DetalleSolicitudImpl(ClienteTablaBasica clienteTablaBasica, ClienteSolicitud clienteSolicitud, IDetalleSolicitudRepositoryFacade detalleSolicitudRepository) {
        this.clienteTablaBasica = clienteTablaBasica;
        this.clienteSolicitud = clienteSolicitud;
        this.detalleSolicitudRepository = detalleSolicitudRepository;
    }

    @Override
    public Optional<DetalleSolicitudDto> findCertificadoByNumeroRadicado(String numeroRadicado) throws WebClientException {
        Solicitud solicitud = new Solicitud();
        solicitud.setIdSolicitud(clienteSolicitud.findIdSolicitudByNumeroRadicado(numeroRadicado));
        Optional<DetalleSolicitud> detalleSolicitudDao = detalleSolicitudRepository.findByNumeroRadicado(solicitud);
        Optional<DetalleSolicitudDto> detalleSolicitudDto = Optional.of(new DetalleSolicitudDto());
        detalleSolicitudDto.get().setIdDetalleSolicitud(detalleSolicitudDao.get().getIdDetalleSolicitud());
        detalleSolicitudDto.get().setDescripcionMotivo(detalleSolicitudDao.get().getDescripcionMotivo());
        detalleSolicitudDto.get().setDiaSolicitado(detalleSolicitudDao.get().getDiaSolicitado());
        detalleSolicitudDto.get().setDiasSolicitados(detalleSolicitudDao.get().getDiasSolicitados());
        detalleSolicitudDto.get().setFechaFin(detalleSolicitudDao.get().getFechaFin());
        detalleSolicitudDto.get().setFechaInicio(detalleSolicitudDao.get().getFechaInicio());
        detalleSolicitudDto.get().setHoraFin(detalleSolicitudDao.get().getHoraFin());
        detalleSolicitudDto.get().setHoraInicio(detalleSolicitudDao.get().getHoraInicio());
        detalleSolicitudDto.get().setMotivoSolicitud(detalleSolicitudDao.get().getMotivoSolicitud().getCodigoApp());
        detalleSolicitudDto.get().setNumeroRadicado(detalleSolicitudDao.get().getNumeroRadicado().getNumeroRadicado());
        return detalleSolicitudDto;
    }

    @Override
    public Optional<DetalleSolicitudDto> createDetalleSolicitud(DetalleSolicitudDto detalleSolicitudDto) throws Exception {
            if(!detalleSolicitudRepository.existencesSolicitud(detalleSolicitudDto.getNumeroRadicado())){
                throw BusinessException.doesntExist("Solicitud");
            }
            TablaBasica motivoSolicitud = clienteTablaBasica.findTablaBasicaByCodigoApp(detalleSolicitudDto.getMotivoSolicitud());
            Solicitud solicitud = new Solicitud();
            solicitud.setIdSolicitud(clienteSolicitud.findIdSolicitudByNumeroRadicado(detalleSolicitudDto.getNumeroRadicado()));
            if(detalleSolicitudRepository.existenceDetalleSolicitud(solicitud)){
                throw BusinessException.alreadyExist("Detalle_Solicitud");
            }
            DetalleSolicitud detallesolicitud = new DetalleSolicitud();
            detallesolicitud.setIdDetalleSolicitud(detallesolicitud.getIdDetalleSolicitud());
            detallesolicitud.setMotivoSolicitud(motivoSolicitud);
            detallesolicitud.setHoraInicio(detalleSolicitudDto.getHoraInicio());
            detallesolicitud.setHoraFin(detalleSolicitudDto.getHoraFin());
            detallesolicitud.setFechaInicio(detalleSolicitudDto.getFechaInicio());
            detallesolicitud.setFechaFin(detalleSolicitudDto.getFechaFin());
            detallesolicitud.setDiasSolicitados(detalleSolicitudDto.getDiasSolicitados());
            detallesolicitud.setDiaSolicitado(detalleSolicitudDto.getDiaSolicitado());
            detallesolicitud.setDescripcionMotivo(detallesolicitud.getDescripcionMotivo());
            detallesolicitud.setNumeroRadicado(solicitud);
            detalleSolicitudRepository.createDetalleSolicitud(detallesolicitud);
            return Optional.of(detalleSolicitudDto);
    }


}
