package co.com.soaint.detallesolicitud.commons.dto;

import java.io.Serializable;

public class GetForeignKeysDto implements Serializable {
    public Integer idTablaBasica;
    public Integer idSolicitud;
}
