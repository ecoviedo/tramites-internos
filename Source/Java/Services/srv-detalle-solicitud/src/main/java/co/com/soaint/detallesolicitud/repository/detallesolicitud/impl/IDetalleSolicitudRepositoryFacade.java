package co.com.soaint.detallesolicitud.repository.detallesolicitud.impl;

import co.com.soaint.componentescomunes.Dto.DetalleSolicitudDto;
import co.com.soaint.componentescomunes.entidades.Certificado;
import co.com.soaint.componentescomunes.entidades.DetalleSolicitud;
import co.com.soaint.componentescomunes.entidades.Solicitud;

import java.util.Optional;

public interface IDetalleSolicitudRepositoryFacade {
    Optional<DetalleSolicitud> findByNumeroRadicado(Solicitud solicitud);

    boolean existencesSolicitud(String numeroRadicado);

    Optional<DetalleSolicitud> createDetalleSolicitud(DetalleSolicitud detalleSolicitud);

    boolean existenceDetalleSolicitud(Solicitud Solicitud);
}
