package co.com.soaint.detallesolicitud.repository.detallesolicitud.impl;

import co.com.soaint.componentescomunes.entidades.DetalleSolicitud;
import co.com.soaint.componentescomunes.entidades.Solicitud;
import co.com.soaint.detallesolicitud.repository.detallesolicitud.IDetalleSolicitudRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;
@Component
public class DetalleSolicitudRepositoryImpl implements IDetalleSolicitudRepositoryFacade {
    private final IDetalleSolicitudRepository repository;
    @Autowired
    public DetalleSolicitudRepositoryImpl(IDetalleSolicitudRepository repository) {this.repository = repository;}

    @Override
    public Optional<DetalleSolicitud> findByNumeroRadicado(Solicitud solicitud) {
        return Optional.of(repository.findByNumeroRadicado(solicitud));
    }


    @Override
    public boolean existencesSolicitud(String numeroRadicado) {
        return repository.existenceSolicitud(numeroRadicado);
    }

    @Override
    public Optional<DetalleSolicitud> createDetalleSolicitud(DetalleSolicitud detalleSolicitud) {
        return Optional.of(repository.save(detalleSolicitud));
    }

    @Override
    public boolean existenceDetalleSolicitud(Solicitud detalleSolicitud) {
        return repository.existsDetalleSolicitudByNumeroRadicado(detalleSolicitud);
    }
}
