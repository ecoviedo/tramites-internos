package co.com.soaint.detallesolicitud.web.api.rest.detallesolicitud;

import co.com.soaint.componentescomunes.Dto.DetalleSolicitudDto;
import co.com.soaint.detallesolicitud.commons.constants.api.certificado.EndpointCertificado;
import co.com.soaint.detallesolicitud.commons.domains.response.builder.ResponseBuilder;
import co.com.soaint.detallesolicitud.commons.enums.TransactionState;
import co.com.soaint.detallesolicitud.commons.exception.webClient.WebClientException;
import co.com.soaint.detallesolicitud.service.detallesolicitud.IDetalleSolicitud;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(EndpointCertificado.DETALLE_SOLICITUD_API_V1)
@CrossOrigin("*")
public class DetalleSolicitudApiImp implements IDetalleSolicitudApi {

    private final IDetalleSolicitud iCertificado;

    @Autowired
    public DetalleSolicitudApiImp(IDetalleSolicitud iCertificado) {
        this.iCertificado = iCertificado;
    }

    @PostMapping(EndpointCertificado.CREATE_CERTIFICADO)
    public ResponseEntity createCertificado(@RequestBody DetalleSolicitudDto detalleSolicitudDto) {
        try {
            return ResponseBuilder.newBuilder()
                    .withStatus(HttpStatus.OK)
                    .withResponse(iCertificado.createDetalleSolicitud(detalleSolicitudDto))
                    .buildResponse();
        }
        catch (Exception e) {
            e.printStackTrace();
            return ResponseBuilder.newBuilder()
                    .withStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                    .withTransactionState(TransactionState.FAIL)
                    .withPath(EndpointCertificado.CREATE_CERTIFICADO)
                    .withMessage(e.getMessage() + " --Caused by:  --"+e.getLocalizedMessage())
                    .buildResponse();
        }
    }

    @GetMapping(EndpointCertificado.FIND_DETALLE_SOLICITUD)
    @RequestMapping(value = EndpointCertificado.FIND_DETALLE_SOLICITUD + "{numeroRadicado}")
    @Override
    public ResponseEntity findCertificadoByIdCertificado(@PathVariable("numeroRadicado") String numeroRadicado) throws WebClientException {
        return ResponseEntity.status(HttpStatus.OK).body(iCertificado.findCertificadoByNumeroRadicado(numeroRadicado));
    }
}
