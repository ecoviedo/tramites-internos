package co.com.soaint.detallesolicitud.repository.detallesolicitud;

import co.com.soaint.componentescomunes.entidades.Certificado;
import co.com.soaint.componentescomunes.entidades.DetalleSolicitud;
import co.com.soaint.componentescomunes.entidades.Solicitud;
import co.com.soaint.componentescomunes.entidades.SolicitudVacaciones;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface IDetalleSolicitudRepository extends JpaRepository<DetalleSolicitud, Integer> {

    DetalleSolicitud findByNumeroRadicado(Solicitud numeroRadicado);

    boolean existsDetalleSolicitudByNumeroRadicado(Solicitud solicitud);

    @Query("select case when count(numero_radicado)> 0 then true else false end from Solicitud solicitud where (solicitud.numeroRadicado) like :numeroRadicado")
    boolean existenceSolicitud(@Param("numeroRadicado") String numeroRadicado);


}
