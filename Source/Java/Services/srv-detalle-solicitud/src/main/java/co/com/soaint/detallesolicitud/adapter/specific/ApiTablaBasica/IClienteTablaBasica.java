package co.com.soaint.detallesolicitud.adapter.specific.ApiTablaBasica;

import co.com.soaint.componentescomunes.entidades.TablaBasica;
import co.com.soaint.detallesolicitud.commons.exception.webClient.WebClientException;

public interface IClienteTablaBasica {
    TablaBasica findTablaBasicaByCodigoApp (String codigoApp) throws WebClientException;
}
