package co.com.soaint.detallesolicitud.service.detallesolicitud;


import co.com.soaint.componentescomunes.Dto.CertificadoDto;
import co.com.soaint.componentescomunes.Dto.DetalleSolicitudDto;
import co.com.soaint.detallesolicitud.commons.exception.webClient.WebClientException;

import java.util.Optional;

public interface IDetalleSolicitud {

    Optional<DetalleSolicitudDto> findCertificadoByNumeroRadicado(String numeroRadicado) throws WebClientException;

    Optional<DetalleSolicitudDto> createDetalleSolicitud(DetalleSolicitudDto certificadoDto) throws Exception;
}
