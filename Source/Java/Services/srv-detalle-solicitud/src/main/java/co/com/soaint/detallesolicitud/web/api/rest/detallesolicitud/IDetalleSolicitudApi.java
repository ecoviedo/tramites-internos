package co.com.soaint.detallesolicitud.web.api.rest.detallesolicitud;

import co.com.soaint.componentescomunes.Dto.DetalleSolicitudDto;
import co.com.soaint.detallesolicitud.commons.exception.webClient.WebClientException;
import org.springframework.http.ResponseEntity;

public interface IDetalleSolicitudApi {

    ResponseEntity createCertificado(DetalleSolicitudDto detalleSolicitudDto);
    ResponseEntity findCertificadoByIdCertificado(String numeroRadicado) throws WebClientException;
}
