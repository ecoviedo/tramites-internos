package co.com.soaint.detallesolicitud.adapter.specific.ApiSolicitud;

import co.com.soaint.detallesolicitud.commons.exception.webClient.WebClientException;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

@Component
@JsonAutoDetect(getterVisibility = JsonAutoDetect.Visibility.NON_PRIVATE)
public class ClienteSolicitud implements IClienteSolicitud {

    private RestTemplate template = new RestTemplate();

    @Override
    public Integer findIdSolicitudByNumeroRadicado(String numeroRadicado) throws WebClientException{
        try {

            String url = "http://localhost:8004/api/v1/"+numeroRadicado;


            URI uri = new URI(url);
            Integer responseService = template.getForObject(uri, Integer.class);
            return responseService;

        } catch (RestClientException | URISyntaxException e) {

            throw new WebClientException(" Error Invocando servicio Tablas Basicas: " + e.getMessage());
        }
    }
}