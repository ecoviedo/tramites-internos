package co.com.soaint.detallesolicitud.commons.constants.api.certificado;

public interface EndpointCertificado {

    String DETALLE_SOLICITUD_API_V1 = "api/v1";
    String FIND_DETALLE_SOLICITUD = "/";
    String CREATE_CERTIFICADO = "/detallesolicitud";
    String UPDATE_CERTIFICADO = "/detallesolicitud/";
    String SRV_TABLAS_BASICAS = "http://localhost:8002/"+ DETALLE_SOLICITUD_API_V1 + FIND_DETALLE_SOLICITUD;
}
