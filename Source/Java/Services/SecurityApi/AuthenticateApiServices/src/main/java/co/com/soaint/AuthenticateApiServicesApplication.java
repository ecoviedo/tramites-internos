package co.com.soaint;

import java.util.Collections;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 
 * @author jjmorales
 *
 */
@SpringBootApplication
@EnableSwagger2
@Import(BeanValidatorPluginsConfiguration.class)
public class AuthenticateApiServicesApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(AuthenticateApiServicesApplication.class, args);
	}
	
	/**
	 * Indica que la información vendra de las propiedades
	 */
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
	    return application.sources(AuthenticateApiServicesApplication.class);
	}
	
	/**
	 * Swagger Doc
	 * @return
	 */
	@Bean
    public Docket swagger() {
        return new Docket(DocumentationType.SWAGGER_2)
        		.groupName("UserServices")
                .select()
                .apis(RequestHandlerSelectors.basePackage("co.com.soaint"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(getApiInfo());
    }
	
	/**
	 * Aplication Info
	 * @return
	 */
	private ApiInfo getApiInfo() {
	    return new ApiInfo(
	            "USER REST API",
	            "This API provides a layer to directly interact with the user management services",
	            "1.0.0",
	            "Rest",
	            new Contact("SOAINT","http://soaint.com/","soaint@email.com"),
	            "2019 - Soaint",
	            "http://soaint.com/",
	            Collections.emptyList()
	    );
	}

}
