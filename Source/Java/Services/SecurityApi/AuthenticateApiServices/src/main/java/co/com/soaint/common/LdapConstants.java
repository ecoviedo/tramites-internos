package co.com.soaint.common;

/**
 * 
 * @author jjmorales
 *
 */
public class LdapConstants {
	
	public static final String UID = "uid";
	public static final String OBJECT_CLASS = "objectClass";
	public static final String INET_ORG_PERSON = "inetOrgPerson";
	public static final String POSIX_ACCOUNT = "posixAccount";
	public static final String SHADOW_ACCOUNT = "shadowAccount";
	public static final String CN = "cn";
	public static final String SN = "sn";
	public static final String MAIL = "mail";
	public static final String GIVEN_NAME = "givenName";
	public static final String UID_NUMBER = "uidNumber";
	public static final String GID_NUMBER = "gidNumber";
	public static final String HOME_DIRECTORY = "homeDirectory";
	public static final String LOGIN_SHELL = "loginShell";
	public static final String USER_PASSWORD = "userpassword";
	public static final String GROUP_SEPARATOR = " ";
	public static final String SUPERVISOR_GROUP_NAME = "Supervisor";
	public static final String ABOGADO_GROUP_NAME = "Abogado";
}
