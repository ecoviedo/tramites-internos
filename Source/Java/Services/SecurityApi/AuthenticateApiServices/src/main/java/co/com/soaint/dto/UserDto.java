
/**
 * 
 */
package co.com.soaint.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author jjmorales
 *
 */
//@ApiModel(description = "Representative class for handling user data of the process or task.")
public class UserDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
//	@ApiModelProperty(notes = "User owner of the task.")
	private String user;
	
	private String identType;
	
	private String identNumber;
	
	private String name;
	
	private String lastName;
	
	private String fullName;
	
	private String email;
	
	private String state;
	
	private String role;
	
	private String newRole;
	
//	@ApiModelProperty(notes = "Owner user password.")
	private String password;
	
	private int assignedTasks;
	
	
//	@ApiModelProperty(notes = "target user for reassignments.")
//	private String targetUser;

	/**
	 * 
	 */
	public UserDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the user
	 */
	public String getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(String user) {
		this.user = user;
	}

	/**
	 * 
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * 
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getIdentType() {
		return identType;
	}

	public void setIdentType(String identType) {
		this.identType = identType;
	}

	public String getIdentNumber() {
		return identNumber;
	}

	public void setIdentNumber(String identNumber) {
		this.identNumber = identNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getFullName() {
		return fullName;
	}

	public void setGivenName(String fullName) {
		this.fullName = fullName;
	}

	public String getNewRole() {
		return newRole;
	}

	public void setNewRole(String newRole) {
		this.newRole = newRole;
	}

	public int getAssignedTasks() {
		return assignedTasks;
	}

	public void setAssignedTasks(int assignedTasks) {
		this.assignedTasks = assignedTasks;
	}
	
}