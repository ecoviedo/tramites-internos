/**
 * 
 */
package co.com.soaint.dto;

import java.io.Serializable;

//import io.swagger.annotations.ApiModel;
//import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author jjmorales
 *
 */
//@ApiModel(description = "Representative class of the process request")
public class ProcessRequestDto implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

//	@ApiModelProperty(notes = "User owner of the process task or process instance")
	private UserDto user;

	/**
	 * 
	 */
	public ProcessRequestDto() {
		super();
	}

	public UserDto getUser() {
		return user;
	}

	public void setUser(UserDto user) {
		this.user = user;
	}
}