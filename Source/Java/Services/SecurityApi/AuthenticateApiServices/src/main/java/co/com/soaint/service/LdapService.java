package co.com.soaint.service;

import java.util.Properties;
import java.util.Random;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.Attribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.filter.AndFilter;
import org.springframework.ldap.filter.EqualsFilter;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import co.com.soaint.common.LdapConstants;
import co.com.soaint.dto.ProcessRequestDto;
import co.com.soaint.dto.ProcessResponseDto;
import co.com.soaint.dto.ResponseDto;
import co.com.soaint.dto.UserDto;
import co.com.soaint.model.LdapDto;
import co.com.soaint.model.UserTaskDto;
import co.com.soaint.trasnformData.LdapData;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

/**
 * 
 * @author jjmorales
 *
 */
@Service
public class LdapService {
	
	@Autowired
	private Environment environment;
	
	@Autowired
	private LdapData ldapData;

	private RestTemplate restTemplate;
	
	public LdapService() {
		restTemplate = new RestTemplate();
	}

	/**
	 * 
	 * @param requestDto
	 * @return
	 */
	public ProcessResponseDto authentication(ProcessRequestDto requestDto) {
		SearchResult result=null;
		LdapDto ldapDto = loadConfiguration();

	    DirContext serviceCtx = null;
	    try {
	    	Properties serviceEnv = loadProperties(ldapDto, true);
	        serviceCtx = new InitialDirContext(serviceEnv);
	        
	        SearchControls sc=loadControls();
	        
	        String searchFilter = "(" + LdapConstants.UID + "=" + requestDto.getUser().getUser() + ")";
	        NamingEnumeration<SearchResult> results = serviceCtx.search(ldapDto.getBase(), searchFilter, sc);
	        
	        if (results.hasMore()) {
	        	result = results.next();
	            String distinguishedName = result.getNameInNamespace();
	            
	            LdapDto ldapDtoUsuario = ldapDto;	            
		            ldapDtoUsuario.setServiceUserDN(distinguishedName);
		            ldapDtoUsuario.setServiceUserPassword(requestDto.getUser().getPassword());
	            
	            Properties authEnv = loadProperties(ldapDto, false);	            
	            new InitialDirContext(authEnv);
	        }
	    } catch (Exception e) {
	        return ldapData.ldapResponseToResponseDto(e);
	    } finally {
	        if (serviceCtx != null) {
	            try {
	                serviceCtx.close();
	            } catch (NamingException e) {
	                e.printStackTrace();
	            }
	        }
	    }
	    return ldapData.ldapResponseToResponseDto(result);
	}
	
	/**
	 * 
	 * @param requestDto
	 * @return
	 */
	public ProcessResponseDto getUsers() {
		LdapDto ldapDto = loadConfiguration();
		NamingEnumeration<SearchResult> results = null;
	    DirContext serviceCtx = null;
	    try {
	    	Properties serviceEnv = loadProperties(ldapDto, true);
	        serviceCtx = new InitialDirContext(serviceEnv);
	        
	        SearchControls sc=loadControls();
	        
	        String searchFilter = "(uid=*)";
	        results = serviceCtx.search(ldapDto.getBase(), searchFilter, sc);
	    } catch (Exception e) {
	        return ldapData.ldapResponseToResponseDto(e);
	    } finally {
	        if (serviceCtx != null) {
	            try {
	                serviceCtx.close();
	            } catch (NamingException e) {
	                e.printStackTrace();
	            }
	        }
	    }
	    return ldapData.ldapResponseToResponseDto(results, "user");
	}
	
	/**
	 * 
	 * @param requestDto
	 * @return
	 */
	public ProcessResponseDto getUsersByGroup(ProcessRequestDto requestDto) {
		ProcessResponseDto processResponseDto = new ProcessResponseDto();
		List<UserDto> users = new ArrayList<UserDto>();
		LdapDto ldapDto = loadConfiguration();
		NamingEnumeration<SearchResult> results = null;
		String[] arGroup = null;
		String role = null;
	    DirContext serviceCtx = null;
	    try {
	    	Properties serviceEnv = loadProperties(ldapDto, true);
	        serviceCtx = new InitialDirContext(serviceEnv);	        
	        SearchControls sc=loadControls();	        
	        String searchFilter = "(uid=*)";	        
	        arGroup = requestDto.getUser().getRole().split(LdapConstants.GROUP_SEPARATOR);	        
	        for (int i = 0; i < arGroup.length; i++) {
	        	if(role==null)
	        		role = "ou="+arGroup[i];
	        	else
	        		role += ",ou="+arGroup[i];
			}
	        results = serviceCtx.search(role + "," + ldapDto.getBase(), searchFilter, sc);
	    } catch (Exception e) {
	        return ldapData.ldapResponseToResponseDto(e);
	    } finally {
	        if (serviceCtx != null) {
	            try {
	                serviceCtx.close();
	            } catch (NamingException e) {
	                e.printStackTrace();
	            }
	        }
	    }
	     
	    //Check workload
	    ProcessResponseDto responseDtoUsersByGroup = ldapData.ldapResponseToResponseDto(results, "user");
	    processResponseDto.setResponse( responseDtoUsersByGroup.getResponse() );
	    
	    Iterator<UserDto> it = responseDtoUsersByGroup.getUsers().iterator();
		while (it.hasNext()) {
			UserDto userDto= it.next();
			int totalTasksByUser = getTasksByGroups_APIBPM(userDto);
			userDto.setAssignedTasks(totalTasksByUser);
			users.add(userDto);
		}
		processResponseDto.setUsers(users);
	    
	    return processResponseDto;
	}

	/**
	 * 
	 * @param requestDto
	 * @return
	 */
	public ProcessResponseDto authorization(ProcessRequestDto requestDto) {
		SearchResult result=null;
		ProcessResponseDto processResponseDto = null;
		LdapDto ldapDto = loadConfiguration();
		
	    String identifyingAttribute = LdapConstants.UID;
	    String identifier = requestDto.getUser().getUser();
	    String role = requestDto.getUser().getRole();

	    DirContext serviceCtx = null;
	    try {
	    	Properties serviceEnv = loadProperties(ldapDto, true);
	        serviceCtx = new InitialDirContext(serviceEnv);
	        
	        SearchControls sc=loadControls();
	        
	        String searchFilter = "(" + identifyingAttribute + "=" + identifier + ")";
	        NamingEnumeration<SearchResult> results = serviceCtx.search(ldapDto.getBase(), searchFilter, sc);
	        if (results.hasMore()) {
	        	result = results.next();
	        }
	    } catch (Exception e) {
	        return ldapData.ldapResponseToResponseDto(e);
	    } finally {
	        if (serviceCtx != null) {
	            try {
	                serviceCtx.close();
	            } catch (NamingException e) {
	                e.printStackTrace();
	            }
	        }
	    }
	    processResponseDto = ldapData.ldapResponseToResponseDto(result);
	    
	    if(processResponseDto.getUsers().size() > 0) {
	    	if(processResponseDto.getUsers().get(0).getRole().equalsIgnoreCase(role)) {
	    		ResponseDto responseDto = new ResponseDto();
		        responseDto.setMsg("Authorized role");
		        responseDto.setStatusCode("200");
		        responseDto.setType("SUCCESS");
		        
		        processResponseDto.setResponse(responseDto);
	    	}else {
	    		ResponseDto responseDto = new ResponseDto();
		        responseDto.setMsg("Unauthorized role");
		        responseDto.setStatusCode("401");
		        responseDto.setType("FAIL");
		        
		        processResponseDto.setResponse(responseDto);
	    	}
	    }
	    
	    return processResponseDto;
	}
	
	/**
	 * 
	 * @return
	 */
	public ProcessResponseDto getGroups() {
		LdapDto ldapDto = loadConfiguration();
		NamingEnumeration<SearchResult> results = null;
	    DirContext serviceCtx = null;
	    try {
	    	
	    	Properties serviceEnv = loadProperties(ldapDto, true);
	        serviceCtx = new InitialDirContext(serviceEnv);
	        
	        SearchControls sc=loadControls();
	        
	        String searchFilter = "(objectclass=posixGroup)";
	        results = serviceCtx.search(ldapDto.getBase(), searchFilter, sc);
	        
	    } catch (Exception e) {
	        return ldapData.ldapResponseToResponseDto(e);
	    } finally {
	        if (serviceCtx != null) {
	            try {
	                serviceCtx.close();
	            } catch (NamingException e) {
	                e.printStackTrace();
	            }
	        }
	    }
	    
	    return ldapData.ldapResponseToResponseDto(results,"group");
	}
	
	/**
	 * 
	 * @param requestDto
	 * @return
	 */
	public ProcessResponseDto addUser(ProcessRequestDto requestDto) {
		ProcessResponseDto processResponseDto = new ProcessResponseDto();
		LdapDto ldapDto = loadConfiguration();
		DirContext dctx = null;
		
		try {
			Properties serviceEnv = loadProperties(ldapDto, true);
			dctx = new InitialDirContext(serviceEnv);
			Attributes container = loadContainer(requestDto, ldapDto);
			dctx.createSubcontext(getUserDN(requestDto.getUser().getUser(), requestDto.getUser().getRole(), ldapDto), container);
			ResponseDto responseDto = new ResponseDto();
		        responseDto.setMsg("Created user");
		        responseDto.setStatusCode("201");
		        responseDto.setType("SUCCESS");
	        processResponseDto.setResponse(responseDto);
		} catch (NamingException e) {
			return ldapData.ldapResponseToResponseDto(e);
		} finally {
			if (null != dctx) {
				try {
					dctx.close();
				} catch (final NamingException e) {
					e.printStackTrace();
				}
			}
		}
		return processResponseDto;
	}
	
	/**
	 * 
	 * @param requestDto
	 * @return
	 */
	public ProcessResponseDto deleteUser(ProcessRequestDto requestDto) {
		ProcessResponseDto processResponseDto = new ProcessResponseDto();
		LdapDto ldapDto = loadConfiguration();
		DirContext dctx = null;
		String[] arGroup = null;
		String role = null;
		try {
			Properties serviceEnv = loadProperties(ldapDto, true);
			dctx = new InitialDirContext(serviceEnv);
			arGroup = requestDto.getUser().getRole().split(LdapConstants.GROUP_SEPARATOR);	        
	        for (int i = 0; i < arGroup.length; i++) {
	        	if(role==null)
	        		role = "ou="+arGroup[i];
	        	else
	        		role += ",ou="+arGroup[i];
			}
			dctx.destroySubcontext("uid=" + requestDto.getUser().getUser() + "," + role + "," + ldapDto.getBase());
			ResponseDto responseDto = new ResponseDto();
		        responseDto.setMsg("Delete user");
		        responseDto.setStatusCode("200");
		        responseDto.setType("SUCCESS");
	        processResponseDto.setResponse(responseDto);
		} catch (NamingException e) {
			return ldapData.ldapResponseToResponseDto(e);
		} finally {
			if (null != dctx) {
				try {
					dctx.close();
				} catch (final NamingException e) {
					e.printStackTrace();
				}
			}
		}
		return processResponseDto;
	}
	
	/**
	 * 
	 * @param requestDto
	 * @return
	 */
	public ProcessResponseDto modifyUser(ProcessRequestDto requestDto) {
		ProcessResponseDto processResponseDto = new ProcessResponseDto();
		LdapDto ldapDto = loadConfiguration();
		DirContext dctx = null;
		String[] arNewGroup = null;
		String newRole = null;
		
		String[] arGroup = null;
		String role = null;
		try {
			Properties serviceEnv = loadProperties(ldapDto, true);
			dctx = new InitialDirContext(serviceEnv);			
	        ModificationItem[] mods = loadModification(requestDto);
	        
	        arGroup = requestDto.getUser().getRole().split(LdapConstants.GROUP_SEPARATOR);	        
	        for (int i = 0; i < arGroup.length; i++) {
	        	if(role==null)
	        		role = "ou="+arGroup[i];
	        	else
	        		role += ",ou="+arGroup[i];
			}
	        
	        dctx.modifyAttributes("uid=" + requestDto.getUser().getUser() + "," + role + "," + ldapDto.getBase(), mods);
		    if(requestDto.getUser().getNewRole() != null) {
		    	arNewGroup = requestDto.getUser().getNewRole().split(LdapConstants.GROUP_SEPARATOR);	        
		        for (int i = 0; i < arNewGroup.length; i++) {
		        	if(newRole==null)
		        		newRole = "ou="+arNewGroup[i];
		        	else
		        		newRole += ",ou="+arNewGroup[i];
				}
		    	dctx.rename("uid=" + requestDto.getUser().getUser() + "," + role + "," + ldapDto.getBase()
		    		, "uid=" + requestDto.getUser().getUser() + "," + newRole + "," + ldapDto.getBase());
		    }
		    ResponseDto responseDto = new ResponseDto();
	        responseDto.setMsg("Modified user");
	        responseDto.setStatusCode("200");
	        responseDto.setType("SUCCESS");
		        
	        processResponseDto.setResponse(responseDto);
		} catch (NamingException e) {
			return ldapData.ldapResponseToResponseDto(e);
		} finally {
			if (null != dctx) {
				try {
					dctx.close();
				} catch (final NamingException e) {
					e.printStackTrace();
				}
			}
		}
		return processResponseDto;
	}
	
	/**
	 * 
	 * @param requestDto
	 * @return
	 */
	public ProcessResponseDto getSupervisor(ProcessRequestDto requestDto) {
		ProcessResponseDto processResponseDto = null;
		LdapDto ldapDto = loadConfiguration();
		NamingEnumeration<SearchResult> results = null;
	    DirContext serviceCtx = null;
	    try {
	    	Properties serviceEnv = loadProperties(ldapDto, true);
	        serviceCtx = new InitialDirContext(serviceEnv);
	        
	        SearchControls sc=loadControls();
	        
	        String searchFilter = "(uid=" + requestDto.getUser().getUser() + ")";
	        results = serviceCtx.search(ldapDto.getBase(), searchFilter, sc);
	    } catch (Exception e) {
	        return ldapData.ldapResponseToResponseDto(e);
	    } finally {
	        if (serviceCtx != null) {
	            try {
	                serviceCtx.close();
	            } catch (NamingException e) {
	                e.printStackTrace();
	            }
	        }
	    }
	    processResponseDto = ldapData.ldapResponseToResponseDto(results);
	    
	    if(processResponseDto.getResponse().getStatusCode().equalsIgnoreCase("200")) {
	    	String[] arGroup = null;
	    	String role = null;
	    	if(processResponseDto.getUsers().size() > 0) {
	    		if(processResponseDto.getUsers().get(0).getRole()!=null)
	    			role = processResponseDto.getUsers().get(0).getRole().replaceFirst(LdapConstants.ABOGADO_GROUP_NAME, LdapConstants.SUPERVISOR_GROUP_NAME);
	    	}
	    	
	    	if(role.indexOf(LdapConstants.SUPERVISOR_GROUP_NAME) == -1)
	    		role = null;
	    	
	    	if(role!=null) {
	    		ProcessRequestDto processRequestDto = new ProcessRequestDto();
		    	UserDto user = new UserDto();
		    	user.setRole(role);
				processRequestDto.setUser(user);
		    	ProcessResponseDto processResponseDtoSupervisor = getUsersByGroup(processRequestDto);
		    	
		    	processResponseDto = processResponseDtoSupervisor;
	    	}else {
	    		ResponseDto responseDto = new ResponseDto();
		        responseDto.setMsg("User " + LdapConstants.SUPERVISOR_GROUP_NAME + " not found");
		        responseDto.setStatusCode("404");
		        responseDto.setType("FAIL");
	        
		        processResponseDto.setResponse(responseDto);
	    	}
	    	
	    }
	    
	    return processResponseDto;
	}
	
	/**
	 * 
	 * @param requestDto
	 * @return
	 */
	public ProcessResponseDto lawyerLessBurden(ProcessRequestDto requestDto) {
		LdapDto ldapDto = loadConfiguration();
		NamingEnumeration<SearchResult> results = null;
	    DirContext serviceCtx = null;
	    ProcessResponseDto responseDto_lawyerLessBurden = new ProcessResponseDto();
	    try {
	    	Properties serviceEnv = loadProperties(ldapDto, true);
	        serviceCtx = new InitialDirContext(serviceEnv);
	        
	        SearchControls sc=loadControls();
	        
	        String searchFilter = "(uid=*)";
	        results = serviceCtx.search(ldapDto.getBase(), searchFilter, sc);
	    } catch (Exception e) {
	        return ldapData.ldapResponseToResponseDto(e);
	    } finally {
	        if (serviceCtx != null) {
	            try {
	                serviceCtx.close();
	            } catch (NamingException e) {
	                e.printStackTrace();
	            }
	        }
	    }
	    
	    ProcessResponseDto processResponseDto = ldapData.ldapResponseToResponseDto(results, "user");
	    if(processResponseDto.getResponse().getStatusCode().equalsIgnoreCase("200")) {
	    	
	    	List list_lawyerLessBurden = new ArrayList<UserTaskDto>();
	    	list_lawyerLessBurden = loadListLawyers_withLessBurden(processResponseDto);
	    	
	    	UserTaskDto UserTaskDto_lawyerLessBurden = null;
	    	UserTaskDto_lawyerLessBurden = random_lawyerLessBurden(list_lawyerLessBurden);
	    	
	    	
			
			
			//extraer el abogado de manera random
			
			if(UserTaskDto_lawyerLessBurden != null) {
				List<UserDto> users = new ArrayList<UserDto>();
				users.add(UserTaskDto_lawyerLessBurden.getUserDto());
				responseDto_lawyerLessBurden.setUsers(users);
				
				ResponseDto responseDto = new ResponseDto();
					responseDto.setStatusCode("200");
					responseDto.setMsg("SUCCESS");
					responseDto.setType("SUCCESS");
		        responseDto_lawyerLessBurden.setResponse(responseDto);
			}else {
				ResponseDto responseDto = new ResponseDto();
			        responseDto.setMsg("Failure in the system does not identify the lawyer with the least burden");
			        responseDto.setStatusCode("404");
			        responseDto.setType("FAIL");
				responseDto_lawyerLessBurden.setResponse(responseDto);
			}
	    }else {
	    	responseDto_lawyerLessBurden.setResponse(processResponseDto.getResponse());
	    }
	    return responseDto_lawyerLessBurden;
	}
	
	private UserTaskDto random_lawyerLessBurden(List list_lawyerLessBurden) {
		UserTaskDto UserTaskDto_lawyerLessBurden = null;
		
		if(list_lawyerLessBurden.size() > 0) {
			Random r = new Random();
			int valorDado = r.nextInt(list_lawyerLessBurden.size());
			
			UserTaskDto_lawyerLessBurden = (UserTaskDto) list_lawyerLessBurden.get(valorDado);
		}
		
		return UserTaskDto_lawyerLessBurden;
	}

	/**
	 * 
	 * @param processResponseDto
	 * @return
	 */
	private List loadListLawyers_withLessBurden(ProcessResponseDto processResponseDto) {
		List list_lawyerLessBurden = new ArrayList<UserTaskDto>();
		
		UserTaskDto UserTaskDto_lawyerLessBurden = null;
    	Iterator<UserDto> it = processResponseDto.getUsers().iterator();
		while (it.hasNext()) {
			UserDto userDto= it.next();
			if(userDto.getRole().indexOf(LdapConstants.ABOGADO_GROUP_NAME) != -1) {
				int totalTasksByUser = getTasksByGroups_APIBPM(userDto);
				if(totalTasksByUser == 0) {
					
					
					if(UserTaskDto_lawyerLessBurden == null) {
//						UserTaskDto_lawyerLessBurden = UserTaskDto_lawyerLessBurden.getInstance();							
						UserTaskDto_lawyerLessBurden = new UserTaskDto();
						UserTaskDto_lawyerLessBurden.setUserDto(userDto);
						UserTaskDto_lawyerLessBurden.setTotalTasks(totalTasksByUser);
						
						list_lawyerLessBurden.add(UserTaskDto_lawyerLessBurden);
					}else {
						if(totalTasksByUser < UserTaskDto_lawyerLessBurden.getTotalTasks()) {
							UserTaskDto_lawyerLessBurden = new UserTaskDto();
							UserTaskDto_lawyerLessBurden.setUserDto(userDto);
							UserTaskDto_lawyerLessBurden.setTotalTasks(totalTasksByUser);
							
							list_lawyerLessBurden = new ArrayList<String>();
							list_lawyerLessBurden.add(UserTaskDto_lawyerLessBurden);
						}else if(totalTasksByUser == UserTaskDto_lawyerLessBurden.getTotalTasks()) {
//							UserTaskDto_lawyerLessBurden = UserTaskDto_lawyerLessBurden.getInstance();	
							UserTaskDto_lawyerLessBurden = new UserTaskDto();
							UserTaskDto_lawyerLessBurden.setUserDto(userDto);
							UserTaskDto_lawyerLessBurden.setTotalTasks(totalTasksByUser);
							
							list_lawyerLessBurden.add(UserTaskDto_lawyerLessBurden);
						}
					}	
					
					
				}else {
					
					
					if(UserTaskDto_lawyerLessBurden == null) {
//						UserTaskDto_lawyerLessBurden = UserTaskDto_lawyerLessBurden.getInstance();
						UserTaskDto_lawyerLessBurden = new UserTaskDto();
						UserTaskDto_lawyerLessBurden.setUserDto(userDto);
						UserTaskDto_lawyerLessBurden.setTotalTasks(totalTasksByUser);
						
						list_lawyerLessBurden.add(UserTaskDto_lawyerLessBurden);
					}else {
						if(totalTasksByUser < UserTaskDto_lawyerLessBurden.getTotalTasks()) {
							UserTaskDto_lawyerLessBurden = new UserTaskDto();
							UserTaskDto_lawyerLessBurden.setUserDto(userDto);
							UserTaskDto_lawyerLessBurden.setTotalTasks(totalTasksByUser);
							
							list_lawyerLessBurden = new ArrayList<String>();
							list_lawyerLessBurden.add(UserTaskDto_lawyerLessBurden);
						}else if(totalTasksByUser == UserTaskDto_lawyerLessBurden.getTotalTasks()) {
//							UserTaskDto_lawyerLessBurden = UserTaskDto_lawyerLessBurden.getInstance();
							UserTaskDto_lawyerLessBurden = new UserTaskDto();
							UserTaskDto_lawyerLessBurden.setUserDto(userDto);
							UserTaskDto_lawyerLessBurden.setTotalTasks(totalTasksByUser);
							
							list_lawyerLessBurden.add(UserTaskDto_lawyerLessBurden);
						}
					}	
					
					
				}
			}	
		}
		return list_lawyerLessBurden;
	}

	/**
	 * 
	 * @param userDto
	 * @return
	 */
	private int getTasksByGroups_APIBPM(UserDto userDto) {
		int iRet=0;
		try {
			String url = environment.getProperty("bpm.service.countTasksByGroups").toString();
			HttpHeaders requestHeader = new HttpHeaders();
			requestHeader.setContentType(MediaType.APPLICATION_JSON);
			
			String groups = "\"Abogado\"";
			String taskStates = "\"InProgress\",\"Reserved\"";
			
			String sRequestBody = "{" + 
					"	\"ownerUser\": {" + 
					"		\"user\": \"" + userDto.getUser() + "\"," + 
					"		\"password\": \"" + userDto.getPassword() + "\"" + 
					"	}," + 
					"	\"groups\":[" + groups + "]," + 
					"	\"taskStates\":[" + taskStates + "]," + 
					"	\"page\": 0," + 
					"	\"pageSize\": -1" + 
					"}";
			HttpEntity<String> entity = new HttpEntity<>(sRequestBody, requestHeader);
			iRet = restTemplate.postForObject(url, entity, int.class);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return iRet;
	}
	
	
	
	/**
	 * 
	 * @param requestDto
	 * @return
	 */
	private ModificationItem[] loadModification(ProcessRequestDto requestDto) {
		List<Attribute> listAttribute = new ArrayList<>();
		
//		if(requestDto.getUser().getUser() != null) {
//			listAttribute.add(new BasicAttribute(LdapConstants.UID, requestDto.getUser().getUser()));
//		}
		
		if(requestDto.getUser().getIdentType() != null) {
			listAttribute.add(new BasicAttribute(LdapConstants.GID_NUMBER, requestDto.getUser().getIdentType()));
		}
		
		if(requestDto.getUser().getIdentNumber() != null) {
			listAttribute.add(new BasicAttribute(LdapConstants.UID_NUMBER, requestDto.getUser().getIdentNumber()));
		}
		
		if(requestDto.getUser().getName() != null) {
			listAttribute.add(new BasicAttribute(LdapConstants.CN, requestDto.getUser().getName()));
		}
		
		
		if(requestDto.getUser().getLastName() != null) {
			listAttribute.add(new BasicAttribute("employeeType", requestDto.getUser().getState()));
		}
		
		
		if(requestDto.getUser().getLastName() != null) {
			listAttribute.add(new BasicAttribute(LdapConstants.SN, requestDto.getUser().getLastName()));
		}
		
		if(requestDto.getUser().getFullName() != null) {
			listAttribute.add(new BasicAttribute(LdapConstants.GIVEN_NAME, requestDto.getUser().getFullName()));
		}
		
		if(requestDto.getUser().getEmail() != null) {
			listAttribute.add(new BasicAttribute(LdapConstants.MAIL, requestDto.getUser().getEmail()));
		}
		
		if(requestDto.getUser().getPassword() != null) {
			listAttribute.add(new BasicAttribute(LdapConstants.USER_PASSWORD, requestDto.getUser().getPassword()));
		}
		
		Iterator<Attribute> it = listAttribute.iterator();
		int i =0;
		ModificationItem[] mods = new ModificationItem[listAttribute.size()];
		while (it.hasNext()) {
			mods[i] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, it.next());
			i++;
		}
		
		return mods;
	}

	/**
	 * 
	 * @return
	 */
	private SearchControls loadControls() {
		SearchControls sc = new SearchControls();
		String[] attributeFilter = { "uid" ,"sn", "cn", "uidNumber", "gidNumber", "mail", "givenName", "userPassword", "employeeType"};
        sc.setReturningAttributes(attributeFilter);
        sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
		return sc;
	}
	
	/**
	 * 
	 * @param requestDto
	 * @return
	 */
	private Attributes loadContainer(ProcessRequestDto requestDto, LdapDto ldapDto) {
		Attributes container = new BasicAttributes();
		
		Attribute objClasses = new BasicAttribute(LdapConstants.OBJECT_CLASS);
		objClasses.add(LdapConstants.INET_ORG_PERSON);
		objClasses.add(LdapConstants.POSIX_ACCOUNT);
		objClasses.add(LdapConstants.SHADOW_ACCOUNT);
		
		Attribute commonName = new BasicAttribute(LdapConstants.CN, requestDto.getUser().getName());
		Attribute surName = new BasicAttribute(LdapConstants.SN, requestDto.getUser().getLastName());
		Attribute email = new BasicAttribute(LdapConstants.MAIL, requestDto.getUser().getEmail());
		Attribute givenName = new BasicAttribute(LdapConstants.GIVEN_NAME, requestDto.getUser().getFullName());
		Attribute uid = new BasicAttribute(LdapConstants.UID, requestDto.getUser().getUser());
		
		
		Attribute shadowInactive = new BasicAttribute("employeeType", requestDto.getUser().getState());
		
		
		Attribute uidNumber = new BasicAttribute(LdapConstants.UID_NUMBER, requestDto.getUser().getIdentNumber());
		Attribute gidNumber = new BasicAttribute(LdapConstants.GID_NUMBER, requestDto.getUser().getIdentType());
		
		Attribute homeDirectory = new BasicAttribute(LdapConstants.HOME_DIRECTORY, ldapDto.getUserHomeDirectory() + requestDto.getUser().getUser());
		Attribute loginShell = new BasicAttribute(LdapConstants.LOGIN_SHELL, ldapDto.getLoginShellCommand());
		
		Attribute userPassword = new BasicAttribute(LdapConstants.USER_PASSWORD, requestDto.getUser().getPassword());
		
		container.put(objClasses);
		container.put(commonName);
		container.put(surName);
		container.put(email);
		container.put(givenName);
		container.put(uid);
		
		
		container.put(shadowInactive);
		container.put(uidNumber);
		container.put(gidNumber);
				
		container.put(homeDirectory);
		container.put(loginShell);
		
		container.put(userPassword);
		
		return container;
	}

	/**
	 * 
	 * @param userName
	 * @param userGroup
	 * @param ldapDto
	 * @return
	 */
	private String getUserDN(String userName,String userGroup, LdapDto ldapDto) {
		String[] arGroup = null;
		String role = null;
		arGroup = userGroup.split(LdapConstants.GROUP_SEPARATOR);	        
        for (int i = 0; i < arGroup.length; i++) {
        	if(role==null)
        		role = "ou="+arGroup[i];
        	else
        		role += ",ou="+arGroup[i];
		}
		
		String userDN = new StringBuffer().append("uid=").append(userName).append("," + role + "," + ldapDto.getBase()).toString();
		return userDN;
	}
	
	/**
	 * 
	 * @param ldapDto
	 * @param authentication
	 * @return
	 */
	private Properties loadProperties(LdapDto ldapDto, boolean authentication) {
		Properties serviceEnv = new Properties();
        serviceEnv.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        serviceEnv.put(Context.PROVIDER_URL, ldapDto.getLdapUrl());
        if(authentication) {
        	serviceEnv.put(Context.SECURITY_AUTHENTICATION, "simple");
        }
        serviceEnv.put(Context.SECURITY_PRINCIPAL, ldapDto.getServiceUserDN());
        serviceEnv.put(Context.SECURITY_CREDENTIALS, ldapDto.getServiceUserPassword());

        return serviceEnv;
	}

	private LdapDto loadConfiguration() {
		LdapDto ldapDto = new LdapDto();
			ldapDto.setServiceUserDN("cn=" + environment.getProperty("ldap.root.username").toString() + "," 
	    		+ environment.getProperty("ldap.dn.o").toString());
			
			ldapDto.setServiceUserPassword(environment.getProperty("ldap.root.password").toString());
			ldapDto.setBase(environment.getProperty("ldap.dn.ou").toString());
			
		    ldapDto.setIp(environment.getProperty("ldap.ip").toString());
		    ldapDto.setPort(Integer.parseInt(environment.getProperty("ldap.port").toString()));
		    ldapDto.setLdapUrl("ldap://" + ldapDto.getIp() + ":" + ldapDto.getPort());
		    ldapDto.setUserHomeDirectory(environment.getProperty("ldap.user.home_directory").toString());
		    ldapDto.setLoginShellCommand(environment.getProperty("login.shell.command").toString());
		    
	    return ldapDto;
	}
	
	
	
	/**
	 * 
	 * @param requestDto
	 * @return
	 */
	public ProcessResponseDto lawyerLessBurdenAndSupervisor(ProcessRequestDto requestDto) {
		String urlAbogado = environment.getProperty("authenticate.service.lawyerLessBurden").toString();
		String urlSupervisor = environment.getProperty("authenticate.service.supervisor").toString();
//		 String urlAbogado="http://192.168.1.46:8082/api/user/lawyerLessBurden";
//		 String urlSupervisor ="http://192.168.1.46:8082/api/user/supervisor";

		 ResponseEntity<ProcessResponseDto> responseAbogado= restTemplate.postForEntity(urlAbogado, requestDto,ProcessResponseDto.class);
		 ProcessResponseDto response = responseAbogado.getBody();
		 requestDto.setUser(response.getUsers().get(0));
		 ResponseEntity<ProcessResponseDto> responseSupervisor= restTemplate.postForEntity(urlSupervisor, requestDto,ProcessResponseDto.class);
		 response.getUsers().add(responseSupervisor.getBody().getUsers().get(0));	
		 
		 return response;
	}
	
	
}
