package co.com.soaint.model;

/**
 * 
 * @author jjmorales
 *
 */
public class LdapDto {

	private String serviceUserDN;
	private String serviceUserPassword;
	private String base;
	private String ip;
	private int port;
	private String ldapUrl;
	private String userHomeDirectory;
	private String loginShellCommand;
	
	
	public String getServiceUserDN() {
		return serviceUserDN;
	}
	public void setServiceUserDN(String serviceUserDN) {
		this.serviceUserDN = serviceUserDN;
	}
	public String getServiceUserPassword() {
		return serviceUserPassword;
	}
	public void setServiceUserPassword(String serviceUserPassword) {
		this.serviceUserPassword = serviceUserPassword;
	}
	public String getBase() {
		return base;
	}
	public void setBase(String base) {
		this.base = base;
	}
	
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public String getLdapUrl() {
		return ldapUrl;
	}
	public void setLdapUrl(String ldapUrl) {
		this.ldapUrl = ldapUrl;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getUserHomeDirectory() {
		return userHomeDirectory;
	}
	public void setUserHomeDirectory(String userHomeDirectory) {
		this.userHomeDirectory = userHomeDirectory;
	}
	public String getLoginShellCommand() {
		return loginShellCommand;
	}
	public void setLoginShellCommand(String loginShellCommand) {
		this.loginShellCommand = loginShellCommand;
	}
}
