package co.com.soaint.trasnformData;

import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.Attribute;
import javax.naming.directory.SearchResult;

import org.springframework.stereotype.Service;

import co.com.soaint.common.LdapConstants;
import co.com.soaint.dto.GroupDto;
import co.com.soaint.dto.ProcessResponseDto;
import co.com.soaint.dto.ResponseDto;
import co.com.soaint.dto.UserDto;

/**
 * 
 * @author jjmorales
 *
 */
@Service
public class LdapData {
	
	public LdapData() {
		super();
	}

	/**
	 * 
	 * @param searchResult
	 * @return
	 */
	public ProcessResponseDto ldapResponseToResponseDto(SearchResult searchResult) {
		ProcessResponseDto processResponseDto = new ProcessResponseDto();
		if(searchResult != null) {
	        List<UserDto> users = new ArrayList<UserDto>();
	        UserDto userDto = loadUserDto(searchResult);        
	        users.add(userDto); 
	        
	        ResponseDto responseDto = new ResponseDto();
	        responseDto.setMsg("Authentication successful");
	        responseDto.setStatusCode("200");
	        responseDto.setType("SUCCESS");
	        
	        processResponseDto.setResponse(responseDto);
	        processResponseDto.setUsers(users);
		}else {
			ResponseDto responseDto = new ResponseDto();
		        responseDto.setMsg("User not found");
		        responseDto.setStatusCode("404");
		        responseDto.setType("FAIL");
	        
	        processResponseDto.setResponse(responseDto);
		}
		
		return processResponseDto;
	}

	/**
	 * 
	 * @param e
	 * @return
	 */
	public ProcessResponseDto ldapResponseToResponseDto(Exception e) {
		ResponseDto responseDto = new ResponseDto();
        responseDto.setMsg(e.toString());
        responseDto.setStatusCode("204");
        responseDto.setType("FAIL");
        
        ProcessResponseDto processResponseDto = new ProcessResponseDto();
        processResponseDto.setResponse(responseDto);
		
		return processResponseDto;
	}

	/**
	 * 
	 * @param results
	 * @return
	 */
	public ProcessResponseDto ldapResponseToResponseDto(NamingEnumeration<SearchResult> results, String objectSearch) {
		ProcessResponseDto processResponseDto = new ProcessResponseDto();
		List<UserDto> users = new ArrayList<UserDto>();
		List<GroupDto> groups = new ArrayList<GroupDto>();
		try {
			String[] arName = null;
			while (results.hasMore()) {
	        	SearchResult searchResult = results.next();
	            Attributes attrs = searchResult.getAttributes ();
	            
	            switch(objectSearch.toLowerCase()) 
	            { 
	                case "user": 
	                	UserDto userDto = loadUserDto(searchResult);  
				        users.add(userDto); 
	                    break; 
	                case "group": 
	                    GroupDto groupDto = new GroupDto();
	                    arName = searchResult.getName().split(",");
	                    groupDto.setName(findValue(arName,"cn="));
	                    
	                    groups.add(groupDto);
	                    break;
	            }   
	        }
			
			ResponseDto responseDto = new ResponseDto();
	        responseDto.setMsg("SUCCESS");
	        responseDto.setStatusCode("200");
	        responseDto.setType("SUCCESS");
	        processResponseDto.setResponse(responseDto);
			
	        switch(objectSearch.toLowerCase()) 
            { 
                case "user": 
                	processResponseDto.setUsers(users);
                    break; 
                case "group": 
                	processResponseDto.setGroups(groups);
                    break; 
            }
		} catch (Exception e) {
			ResponseDto responseDto = new ResponseDto();
	        responseDto.setMsg(e.toString());
	        responseDto.setStatusCode("204");
	        responseDto.setType("FAIL");
	        
	        processResponseDto.setResponse(responseDto);
		}
		
		return processResponseDto;
	}
	
	
	public ProcessResponseDto ldapResponseToResponseDto(NamingEnumeration<SearchResult> results) {
		ProcessResponseDto processResponseDto = new ProcessResponseDto();
		List<UserDto> users = new ArrayList<UserDto>();
		try {
			while (results.hasMore()) {
	        	SearchResult searchResult = results.next();
	            UserDto userDto = loadUserDto(searchResult);  
		        users.add(userDto); 
	        }
			ResponseDto responseDto = new ResponseDto();
	        responseDto.setMsg("SUCCESS");
	        responseDto.setStatusCode("200");
	        responseDto.setType("SUCCESS");
	        processResponseDto.setResponse(responseDto);
			
	        processResponseDto.setUsers(users);
		} catch (Exception e) {
			ResponseDto responseDto = new ResponseDto();
	        responseDto.setMsg(e.toString());
	        responseDto.setStatusCode("204");
	        responseDto.setType("FAIL");
	        
	        processResponseDto.setResponse(responseDto);
		}
		return processResponseDto;
	}
	

	/**
	 * 
	 * @param arName
	 * @param name
	 * @return
	 */
	private String findValue(String[] arName, String name) {
		String sRet = null;
		String separator = LdapConstants.GROUP_SEPARATOR;
		for (int i = 0; i < arName.length; i++) {
			int intIndex = arName[i].indexOf(name);
	        if(intIndex != - 1){
	        	String sAux = arName[i].substring(intIndex + name.length());
	        	if(sRet==null) {
	        		sRet=sAux;
	        	}else {
	        		sRet+=separator+sAux;
	        	}
	        }
		}
		return sRet;
	}


	/**
	 * 
	 * @param value
	 * @return
	 */
	private String extractValue(String value) {
		String a=" ";
        int intIndex = value.indexOf(a);
        if(intIndex != - 1){
        	value=value.substring(++intIndex);
        }
		return value;
	}

	/**
	 * 
	 * @param searchResult
	 * @return
	 */
	private UserDto loadUserDto(SearchResult searchResult) {		
		Attributes attrs = searchResult.getAttributes ();		
		UserDto userDto = new UserDto();
		String[] arName = null;
		userDto.setUser( extractValue(attrs.get("uid").toString()) );
		userDto.setLastName( extractValue(attrs.get("sn").toString()) );
		userDto.setName( extractValue(attrs.get("cn").toString()) );
		userDto.setIdentNumber( extractValue(attrs.get("uidNumber").toString()) );
		userDto.setIdentType( extractValue(attrs.get("gidNumber").toString()) );
        userDto.setEmail( extractValue(attrs.get("mail").toString()) );
        userDto.setGivenName( extractValue(attrs.get("givenName").toString()) );        
        userDto.setState( extractValue(attrs.get("employeeType") != null? attrs.get("employeeType").toString(): "") );	
        
        Attribute attributeAux = attrs.get(LdapConstants.USER_PASSWORD);
        String password = "";
        try {
        	password = new String((byte[]) attributeAux.get());
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        userDto.setPassword(password);
//        userDto.setPassword( extractValue(attrs.get(LdapConstants.USER_PASSWORD) != null? attrs.get(LdapConstants.USER_PASSWORD).toString(): "") );        
        arName = searchResult.getName().split(",");
        String role = findValue(arName,"ou=");
        if(role != null && role.length() >0) {
        	userDto.setRole(role);
        }else {
        	String[] arNameAux = searchResult.getNameInNamespace().split(",");
        	if(arNameAux.length>2) {
        		String[] arRoleAux = findValue(arNameAux,"ou=").split(" ");
        		role = arRoleAux[0] + " " + arRoleAux[1];
        	}
        	userDto.setRole(role);
        }
        
		return userDto;
	}
	
}
