package co.com.soaint.model;

import co.com.soaint.dto.UserDto;

/**
 * 
 * @author jjmorales
 *
 */
public class UserTaskDto {
//	private static UserTaskDto intance;
	private UserDto userDto;
	private int totalTasks;
	
//	public static UserTaskDto getInstance() {
//		if(intance == null) {
//			intance = new UserTaskDto();
//		}
//		return intance;
//	}

//	private UserTaskDto() {
//		super();
//	}
	
	public UserTaskDto() {
		super();
	}

	public int getTotalTasks() {
		return totalTasks;
	}

	public void setTotalTasks(int totalTasks) {
		this.totalTasks = totalTasks;
	}

	public UserDto getUserDto() {
		return userDto;
	}

	public void setUserDto(UserDto userDto) {
		this.userDto = userDto;
	}
}
