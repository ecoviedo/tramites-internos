/**
 * 
 */
package co.com.soaint.dto;

import java.io.Serializable;
import java.util.List;

//import io.swagger.annotations.ApiModel;
//import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author jjmorales
 *
 */
//@ApiModel(description = "Representative class of the process response.")
public class ProcessResponseDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
//	@ApiModelProperty(notes = "list of containers created in the engine.")
//	private List<ContainerDto> containers;
//	
//	@ApiModelProperty(notes = "Operation response performed by the service.")
	
	
	private List<UserDto> users;
	
	private List<GroupDto> groups;
	
	private ResponseDto response;

	/**
	 * 
	 */
	public ProcessResponseDto() {
		super();
	}

	/**
	 * @return the response
	 */
	public ResponseDto getResponse() {
		return response;
	}

	/**
	 * @param response the response to set
	 */
	public void setResponse(ResponseDto response) {
		this.response = response;
	}

	public List<UserDto> getUsers() {
		return users;
	}

	public void setUsers(List<UserDto> users) {
		this.users = users;
	}

	public List<GroupDto> getGroups() {
		return groups;
	}

	public void setGroups(List<GroupDto> groups) {
		this.groups = groups;
	}
	
}
