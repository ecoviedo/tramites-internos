package co.com.soaint.dto;

import java.io.Serializable;

/**
 * 
 * @author jjmorales
 *
 */
public class GroupDto implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String name;
	
	
	public GroupDto() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
