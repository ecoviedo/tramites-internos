package co.com.soaint.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.com.soaint.dto.ProcessRequestDto;
import co.com.soaint.dto.ProcessResponseDto;
import co.com.soaint.service.LdapService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 
 * @author jjmorales
 *
 */

@RestController
@RequestMapping("/api/user")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Api(description = "Control basic functionalities for user management.")
public class UserController {
	
	@Autowired
	LdapService service;

	@RequestMapping(value = "/users", method = RequestMethod.POST)  
//	@ApiOperation("Returns the instance of the task.")
    public ResponseEntity<ProcessResponseDto> getUsers() {
		ProcessResponseDto  processResponseDto = service.getUsers();
        if (processResponseDto.getResponse().getType().equalsIgnoreCase("FAIL")) {
            return new ResponseEntity<ProcessResponseDto>(processResponseDto,HttpStatus.OK);
        }         
        return new ResponseEntity<ProcessResponseDto>(processResponseDto, HttpStatus.OK);
    }
	
	@RequestMapping(value = "/usersByGroup", method = RequestMethod.POST)  
//	@ApiOperation("Returns the instance of the task.")
    public ResponseEntity<ProcessResponseDto> listAllUserByGroup(@RequestBody ProcessRequestDto requestDto) {
		ProcessResponseDto  processResponseDto = service.getUsersByGroup(requestDto);
        if (processResponseDto.getResponse().getType().equalsIgnoreCase("FAIL")) {
            return new ResponseEntity<ProcessResponseDto>(processResponseDto,HttpStatus.OK);
        }         
        return new ResponseEntity<ProcessResponseDto>(processResponseDto, HttpStatus.OK);
    }
	
	@RequestMapping(value = "/supervisor", method = RequestMethod.POST)  
//	@ApiOperation("Returns the instance of the task.")
    public ResponseEntity<ProcessResponseDto> getSupervisor(@RequestBody ProcessRequestDto requestDto) {
		ProcessResponseDto  processResponseDto = service.getSupervisor(requestDto);
        if (processResponseDto.getResponse().getType().equalsIgnoreCase("FAIL")) {
            return new ResponseEntity<ProcessResponseDto>(processResponseDto,HttpStatus.OK);
        }         
        return new ResponseEntity<ProcessResponseDto>(processResponseDto, HttpStatus.OK);
    }
	
	
	@RequestMapping(value = "/groups", method = RequestMethod.POST)  
//	@ApiOperation("Returns the instance of the task.")
    public ResponseEntity<ProcessResponseDto> getGroups() {
		ProcessResponseDto  processResponseDto = service.getGroups();
        if (processResponseDto.getResponse().getType().equalsIgnoreCase("FAIL")) {
            return new ResponseEntity<ProcessResponseDto>(processResponseDto,HttpStatus.OK);
        }
        return new ResponseEntity<ProcessResponseDto>(processResponseDto, HttpStatus.OK);
    }
	
	@RequestMapping(value = "/authentication", method = RequestMethod.POST)
	@ApiOperation("validates user authentication according to user administration source.")
    public ResponseEntity<ProcessResponseDto> authentication(@RequestBody ProcessRequestDto requestDto) {
		ProcessResponseDto  processResponseDto = service.authentication(requestDto);
        if (processResponseDto.getResponse().getType().equalsIgnoreCase("FAIL")) {
            return new ResponseEntity<ProcessResponseDto>(processResponseDto,HttpStatus.OK);
        }         
        return new ResponseEntity<ProcessResponseDto>(processResponseDto, HttpStatus.OK);
    }
	
	
	@RequestMapping(value = "/authorization", method = RequestMethod.POST)  
//	@ApiOperation("Returns the instance of the task.")
    public ResponseEntity<ProcessResponseDto> authorization(@RequestBody ProcessRequestDto requestDto) {
		ProcessResponseDto  processResponseDto = service.authorization(requestDto);
        if (processResponseDto.getResponse().getType().equalsIgnoreCase("FAIL")) {
            return new ResponseEntity<ProcessResponseDto>(processResponseDto,HttpStatus.OK);
        }         
        return new ResponseEntity<ProcessResponseDto>(processResponseDto, HttpStatus.OK);
    }
	
	@RequestMapping(value = "/user", method = RequestMethod.DELETE)  
//	@ApiOperation("Returns the instance of the task.")
    public ResponseEntity<ProcessResponseDto> deleteUser(@RequestBody ProcessRequestDto requestDto) {
		ProcessResponseDto  processResponseDto = service.deleteUser(requestDto);
        if (processResponseDto.getResponse().getType().equalsIgnoreCase("FAIL")) {
            return new ResponseEntity<ProcessResponseDto>(processResponseDto,HttpStatus.OK);
        }         
        return new ResponseEntity<ProcessResponseDto>(processResponseDto, HttpStatus.OK);
    }
	
	@RequestMapping(value = "/user", method = RequestMethod.PUT)  
//	@ApiOperation("Returns the instance of the task.")
    public ResponseEntity<ProcessResponseDto> addUser(@RequestBody ProcessRequestDto requestDto) {
		ProcessResponseDto  processResponseDto = service.addUser(requestDto);
        if (processResponseDto.getResponse().getType().equalsIgnoreCase("FAIL")) {
            return new ResponseEntity<ProcessResponseDto>(processResponseDto,HttpStatus.OK);
        }
        return new ResponseEntity<ProcessResponseDto>(processResponseDto, HttpStatus.CREATED);
    }
	
	@RequestMapping(value = "/modifyUser", method = RequestMethod.POST)
//	@ApiOperation("Returns the instance of the task.")
    public ResponseEntity<ProcessResponseDto> modifyUser(@RequestBody ProcessRequestDto requestDto) {
		ProcessResponseDto  processResponseDto = service.modifyUser(requestDto);
        if (processResponseDto.getResponse().getType().equalsIgnoreCase("FAIL")) {
            return new ResponseEntity<ProcessResponseDto>(processResponseDto,HttpStatus.OK);
        }         
        return new ResponseEntity<ProcessResponseDto>(processResponseDto, HttpStatus.OK);
    }
	
	@RequestMapping(value = "/lawyerLessBurden", method = RequestMethod.POST)
//	@ApiOperation("Returns the instance of the task.")
    public ResponseEntity<ProcessResponseDto> lawyerLessBurden(@RequestBody ProcessRequestDto requestDto) {
		ProcessResponseDto  processResponseDto = service.lawyerLessBurden(requestDto);
        if (processResponseDto.getResponse().getType().equalsIgnoreCase("FAIL")) {
            return new ResponseEntity<ProcessResponseDto>(processResponseDto,HttpStatus.OK);
        }         
        return new ResponseEntity<ProcessResponseDto>(processResponseDto, HttpStatus.OK);
    }
	
	
	
	
	@RequestMapping(value = "/lawyerLessBurdenAndSupervisor", method = RequestMethod.POST)
//	@ApiOperation("Returns the instance of the task.")
    public ResponseEntity<ProcessResponseDto> lawyerLessBurdenAndSupervisor(@RequestBody ProcessRequestDto requestDto) {
		
		ProcessResponseDto  processResponseDto = service.lawyerLessBurdenAndSupervisor(requestDto);
        if (processResponseDto.getResponse().getType().equalsIgnoreCase("FAIL")) {
            return new ResponseEntity<ProcessResponseDto>(processResponseDto,HttpStatus.CONFLICT);
        }         
        return new ResponseEntity<ProcessResponseDto>(processResponseDto, HttpStatus.OK);
    }
	
	
	
}
