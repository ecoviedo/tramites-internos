package co.com.soaint.srvcertificado.service.certificado.impl;
import co.com.soaint.componentescomunes.Dto.CertificadoDto;
import co.com.soaint.componentescomunes.entidades.Certificado;
import co.com.soaint.componentescomunes.entidades.Solicitud;
import co.com.soaint.componentescomunes.entidades.TablaBasica;
import co.com.soaint.srvcertificado.adapter.specific.ApiSolicitud.ClienteSolicitud;
import co.com.soaint.srvcertificado.adapter.specific.ApiTablaBasica.ClienteTablaBasica;
import co.com.soaint.srvcertificado.commons.BusinessException;
import co.com.soaint.srvcertificado.commons.exception.webClient.WebClientException;
import co.com.soaint.srvcertificado.repository.certificado.impl.ICertificadoRepositoryFacade;
import co.com.soaint.srvcertificado.service.certificado.ICertificado;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
public class CertificadoImpl implements ICertificado {
    private final ClienteTablaBasica clienteTablaBasica;
    private  final ClienteSolicitud clienteSolicitud;
    private final ICertificadoRepositoryFacade certificadoRepository;

    @Autowired
    public CertificadoImpl(ClienteTablaBasica clienteTablaBasica, ClienteSolicitud clienteSolicitud, ICertificadoRepositoryFacade certificadoRepository) {
        this.clienteTablaBasica = clienteTablaBasica;
        this.clienteSolicitud = clienteSolicitud;
        this.certificadoRepository = certificadoRepository;
    }

    @Override
    public Optional<CertificadoDto> findCertificadoByNumeroRadicado(String numeroRadicado) throws WebClientException {
        Solicitud solicitud = new Solicitud();
        solicitud.setIdSolicitud(clienteSolicitud.findIdSolicitudByNumeroRadicado(numeroRadicado));
        Optional<Certificado> certificadoDao = certificadoRepository.findByNumeroRadicado(solicitud);
        Optional<CertificadoDto> certificadoDto = Optional.of(new CertificadoDto());
        certificadoDto.get().setIdCertificado(certificadoDao.get().getIdCertificado());
        certificadoDto.get().setNumero_radicado(certificadoDao.get().getNumeroRadicado().getNumeroRadicado());
        certificadoDto.get().setObservaciones(certificadoDao.get().getObservaciones());
        certificadoDto.get().setTipoCertificado(certificadoDao.get().getTipoCertificado().getCodigoApp());
        return certificadoDto;
    }

    @Override
    public Optional<CertificadoDto> createCertificado(CertificadoDto certificadoDto) throws Exception {

           if(!certificadoRepository.existenceCertificado(certificadoDto.getNumero_radicado())){
                throw  BusinessException.notFound("Solicitud");
            }
            TablaBasica tipoCertifiacdo = clienteTablaBasica.findTablaBasicaByCodigoApp(certificadoDto.getTipoCertificado());
            Solicitud solicitud = new Solicitud();
            solicitud.setIdSolicitud(clienteSolicitud.findIdSolicitudByNumeroRadicado(certificadoDto.getNumero_radicado()));
            if(certificadoRepository.existenceCertificadoByNumeroRadicado(solicitud)){
                throw BusinessException.alreadyExist("Certificado");
            }
            Certificado certificado = new Certificado();
            certificado.setTipoCertificado(tipoCertifiacdo);
            certificado.setObservaciones(certificadoDto.getObservaciones());
            certificado.setNumeroRadicado(solicitud);
            certificado = certificadoRepository.createCertificado(certificado);
            certificadoDto.setIdCertificado(certificado.getIdCertificado());
            return Optional.of(certificadoDto);
    }


}
