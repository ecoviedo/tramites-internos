package co.com.soaint.srvcertificado.repository.certificado.impl;


import co.com.soaint.componentescomunes.Dto.CertificadoDto;
import co.com.soaint.componentescomunes.Dto.SolicitudDto;
import co.com.soaint.componentescomunes.entidades.Certificado;

import co.com.soaint.componentescomunes.entidades.Solicitud;
import co.com.soaint.srvcertificado.repository.certificado.ICertificadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;
@Component
public class CertificadoRepositoryImpl implements ICertificadoRepositoryFacade {
    private final ICertificadoRepository repository;
    @Autowired
    public CertificadoRepositoryImpl(ICertificadoRepository repository) {this.repository = repository;}

    @Override
    public Optional<Certificado> findByNumeroRadicado(Solicitud solicitud) {
        return Optional.of(repository.findByNumeroRadicado(solicitud));
    }

    @Override
    public boolean existenceCertificado(String numeroradicado) {
        return repository.existenceSolicitud(numeroradicado);
    }

    @Override
    public Certificado createCertificado(Certificado certificado) {
        return repository.save(certificado);
    }

    @Override
    public boolean existenceCertificadoByNumeroRadicado(Solicitud solicitud) {
        return repository.existsCertificadoByNumeroRadicado(solicitud);
    }
}
