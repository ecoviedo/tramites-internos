package co.com.soaint.srvcertificado.repository.certificado;

import co.com.soaint.componentescomunes.entidades.Certificado;
import co.com.soaint.componentescomunes.entidades.Solicitud;
import co.com.soaint.srvcertificado.commons.dto.GetForeignKeysDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ICertificadoRepository extends JpaRepository<Certificado, Integer> {

    Certificado findByNumeroRadicado(Solicitud numeroRadicado);

    @Query("select case when count(solicitud.numeroRadicado)> 0 then true else false end from Solicitud solicitud where lower(solicitud.numeroRadicado) like lower(:numeroRadicado)")
    boolean existenceSolicitud(@Param("numeroRadicado") String numeroRadicado);

    boolean existsCertificadoByNumeroRadicado(Solicitud solicitud);
}
