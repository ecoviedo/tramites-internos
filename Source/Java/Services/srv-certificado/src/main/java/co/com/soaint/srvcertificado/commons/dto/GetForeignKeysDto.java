package co.com.soaint.srvcertificado.commons.dto;

import java.io.Serializable;

public class GetForeignKeysDto implements Serializable {
    public Integer idTablaBasica;
    public Integer idSolicitud;
}
