package co.com.soaint.srvcertificado.repository.certificado.impl;

import co.com.soaint.componentescomunes.entidades.Certificado;
import co.com.soaint.componentescomunes.entidades.Solicitud;

import java.util.Optional;

public interface ICertificadoRepositoryFacade {
    Optional<Certificado> findByNumeroRadicado(Solicitud solicitud);

    boolean existenceCertificado(String idCertificado);

    Certificado createCertificado(Certificado certificado);

    boolean existenceCertificadoByNumeroRadicado(Solicitud solicitud);
}
