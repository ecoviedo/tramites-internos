package co.com.soaint.srvcertificado.web.api.rest.certificado;

import co.com.soaint.componentescomunes.Dto.CertificadoDto;
import co.com.soaint.componentescomunes.Dto.SolicitudDto;
import co.com.soaint.srvcertificado.commons.exception.webClient.WebClientException;
import org.springframework.http.ResponseEntity;

public interface ICertificadoApi {

    ResponseEntity createCertificado(CertificadoDto certificadoDto);
    ResponseEntity findCertificadoByIdCertificado(String numeroRadicado) throws WebClientException;
}
