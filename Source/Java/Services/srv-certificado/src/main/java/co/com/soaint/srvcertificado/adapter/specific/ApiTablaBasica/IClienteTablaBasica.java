package co.com.soaint.srvcertificado.adapter.specific.ApiTablaBasica;

import co.com.soaint.componentescomunes.entidades.TablaBasica;
import co.com.soaint.srvcertificado.commons.exception.webClient.WebClientException;

public interface IClienteTablaBasica {
    TablaBasica findTablaBasicaByCodigoApp (String codigoApp) throws WebClientException;
}
