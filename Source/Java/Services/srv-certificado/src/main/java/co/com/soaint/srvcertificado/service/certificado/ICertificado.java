package co.com.soaint.srvcertificado.service.certificado;


import co.com.soaint.componentescomunes.Dto.CertificadoDto;
import co.com.soaint.componentescomunes.Dto.SolicitudDto;
import co.com.soaint.componentescomunes.entidades.Certificado;
import co.com.soaint.componentescomunes.entidades.Solicitud;
import co.com.soaint.srvcertificado.commons.exception.webClient.WebClientException;

import java.util.Optional;

public interface ICertificado {

    Optional<CertificadoDto> findCertificadoByNumeroRadicado(String numeroRadicado) throws WebClientException;

    Optional<CertificadoDto> createCertificado(CertificadoDto certificadoDto) throws Exception;
}
