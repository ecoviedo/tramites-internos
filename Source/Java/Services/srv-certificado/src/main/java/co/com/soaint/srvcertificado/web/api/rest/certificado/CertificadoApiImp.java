package co.com.soaint.srvcertificado.web.api.rest.certificado;

import co.com.soaint.componentescomunes.Dto.CertificadoDto;
import co.com.soaint.srvcertificado.commons.constants.api.certificado.EndpointCertificado;
import co.com.soaint.srvcertificado.commons.domains.response.builder.ResponseBuilder;
import co.com.soaint.srvcertificado.commons.enums.TransactionState;
import co.com.soaint.srvcertificado.commons.exception.webClient.WebClientException;
import co.com.soaint.srvcertificado.service.certificado.ICertificado;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(EndpointCertificado.SOLICITUD_API_V1)
@CrossOrigin("*")
public class CertificadoApiImp implements ICertificadoApi {

    private final ICertificado iCertificado;

    @Autowired
    public CertificadoApiImp(ICertificado iCertificado) {
        this.iCertificado = iCertificado;
    }

    @PostMapping(EndpointCertificado.CREATE_CERTIFICADO)
    public ResponseEntity createCertificado(@RequestBody CertificadoDto certificadoDto) {
        try {
            return ResponseBuilder.newBuilder()
                    .withStatus(HttpStatus.OK)
                    .withResponse(iCertificado.createCertificado(certificadoDto))
                    .buildResponse();
        } catch (Exception e){
            e.printStackTrace();
            return ResponseBuilder.newBuilder()
                    .withStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                    .withTransactionState(TransactionState.FAIL)
                    .withPath(EndpointCertificado.CREATE_CERTIFICADO)
                    .withMessage(e.getMessage()+"/n-Caused by:  -")
                    .buildResponse();
        }
    }
    @GetMapping(EndpointCertificado.FIND_CERTIFICADO)
    @RequestMapping(value = EndpointCertificado.FIND_CERTIFICADO + "{numeroRadicado}")
    @Override
    public ResponseEntity findCertificadoByIdCertificado(@PathVariable("numeroRadicado") String numeroRadicado) throws WebClientException {
        return ResponseEntity.status(HttpStatus.OK).body(iCertificado.findCertificadoByNumeroRadicado(numeroRadicado));
    }
}
