package co.com.soaint.srvcertificado.adapter.specific.ApiSolicitud;

import co.com.soaint.componentescomunes.entidades.TablaBasica;
import co.com.soaint.srvcertificado.commons.exception.webClient.WebClientException;

public interface IClienteSolicitud {
    Integer findIdSolicitudByNumeroRadicado(String NumeroRadicado) throws WebClientException;
}
