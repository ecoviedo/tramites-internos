package co.com.soaint.srvempleados.repository.empleados;

import co.com.soaint.componentescomunes.entidades.Empleado;
import co.com.soaint.componentescomunes.entidades.TablaBasica;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface IEmpleadoRepository extends JpaRepository<Empleado, String> {

    Empleado findByNumeroDocumento(String numeroDocumento);
    List<Empleado> findByCargo(TablaBasica cargo);

    @Query("select (tablabasica.idTablaBasica) from TablaBasica tablabasica where (tablabasica.codigoApp) like (:codigoApp)")
    Integer findIdTablaBasicaByNumeroRadicado(@Param("codigoApp") String codigoApp);
}


