package co.com.soaint.srvempleados.web.api.rest.empleado.v1;

import co.com.soaint.srvempleados.commons.constants.api.tablas_basicas.EndpointEmpleadoApi;
import co.com.soaint.srvempleados.commons.domains.response.builder.ResponseBuilder;
import co.com.soaint.srvempleados.commons.enums.TransactionState;
import co.com.soaint.srvempleados.service.empleados.IEmpleado;
import co.com.soaint.srvempleados.web.api.rest.IEmpleadoApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = EndpointEmpleadoApi.EMPLEADO_API_V1)
@CrossOrigin("*")
public class EmpleadoApiImp implements IEmpleadoApi {

    private final IEmpleado iEmpleado;

    @Autowired
    public EmpleadoApiImp(IEmpleado iEmpleado) {
        this.iEmpleado = iEmpleado;
    }

    @GetMapping(EndpointEmpleadoApi.FIND_ALL_EMPLEADOS)
    @Override
    public ResponseEntity findAllEmpleados() {
        try {
             return ResponseBuilder.newBuilder()
                    .withStatus(HttpStatus.OK)
                    .withResponse(iEmpleado.findEmpleados())
                    .withTransactionState(TransactionState.OK)
                    .withMessage("Query Successful")
                    .withPath(EndpointEmpleadoApi.FIND_EMPLEADO_BY_NUMERO_RADICADO)
                    .buildResponse();
        } catch (Exception e){
            e.printStackTrace();
            return ResponseBuilder.newBuilder()
                    .withStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                    .withTransactionState(TransactionState.FAIL)
                    .withPath(EndpointEmpleadoApi.FIND_EMPLEADO_BY_NUMERO_RADICADO)
                    .withMessage(e.getMessage())
                    .buildResponse();
        }
    }


    @GetMapping("/")
    @RequestMapping(value = EndpointEmpleadoApi.FIND_EMPLEADO_BY_NUMERO_RADICADO+"{numeroRadicado}")
    @Override
    public ResponseEntity findEmpleadoByNumeroRadicado(@PathVariable("numeroRadicado") String numeroRadicado) {
        try {
            return ResponseBuilder.newBuilder()
                    .withStatus(HttpStatus.OK)
                    .withResponse(iEmpleado.findEmpleadoByNumeroRadicado(numeroRadicado))
                    .withTransactionState(TransactionState.OK)
                    .withMessage("Query Successful")
                    .withPath(EndpointEmpleadoApi.FIND_EMPLEADO_BY_NUMERO_DOCUMENTO)
                    .buildResponse();
        } catch (Exception e){
            e.printStackTrace();
            return ResponseBuilder.newBuilder()
                    .withStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                    .withTransactionState(TransactionState.FAIL)
                    .withPath(EndpointEmpleadoApi.FIND_EMPLEADO_BY_NUMERO_DOCUMENTO)
                    .withMessage(e.getMessage())
                    .buildResponse();
        }
    }

    @GetMapping("/")
    @RequestMapping(value = EndpointEmpleadoApi.FIND_EMPLEADO_BY_NUMERO_DOCUMENTO + "{numeroDocumento}")
    @Override
    public ResponseEntity findEmpleadoByNumeroIdentificacion(@PathVariable("numeroDocumento") String numeroDocumento) {
        try {
            return ResponseBuilder.newBuilder()
                    .withStatus(HttpStatus.OK)
                    .withResponse(iEmpleado.findEmpleadoByNumeroDocumento(numeroDocumento))
                    .withTransactionState(TransactionState.OK)
                    .withMessage("Query Successful")
                    .withPath(EndpointEmpleadoApi.FIND_EMPLEADO_BY_NUMERO_DOCUMENTO)
                    .buildResponse();
        } catch (Exception e){
            e.printStackTrace();
            return ResponseBuilder.newBuilder()
                    .withStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                    .withTransactionState(TransactionState.FAIL)
                    .withPath(EndpointEmpleadoApi.FIND_EMPLEADO_BY_NUMERO_DOCUMENTO)
                    .withMessage(e.getMessage())
                    .buildResponse();
        }
    }

    @GetMapping(EndpointEmpleadoApi.FIND_EMPLEADOS_BY_CARGO)
    @RequestMapping(value = EndpointEmpleadoApi.FIND_EMPLEADOS_BY_CARGO + "{cargo}")
    @Override
    public ResponseEntity findEmpleadosByCargo(@PathVariable("cargo") String cargo) {
        try {
            return ResponseBuilder.newBuilder()
                    .withStatus(HttpStatus.OK)
                    .withResponse(iEmpleado.findEmpleadosByCago(cargo))
                    .withTransactionState(TransactionState.OK)
                    .withMessage("Query Successful")
                    .withPath(EndpointEmpleadoApi.FIND_EMPLEADO_BY_NUMERO_RADICADO)
                    .buildResponse();
        } catch (Exception e){
            e.printStackTrace();
            return ResponseBuilder.newBuilder()
                    .withStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                    .withTransactionState(TransactionState.FAIL)
                    .withPath(EndpointEmpleadoApi.FIND_EMPLEADO_BY_NUMERO_RADICADO)
                    .withMessage(e.getMessage())
                    .buildResponse();
        }
    }


}
