package co.com.soaint.srvempleados.service.empleados.impl;

import co.com.soaint.componentescomunes.Dto.EmpleadoDto;
import co.com.soaint.componentescomunes.Dto.InformacionVacacionesDto;
import co.com.soaint.componentescomunes.entidades.Empleado;
import co.com.soaint.componentescomunes.entidades.InformacionVacaciones;
import co.com.soaint.componentescomunes.entidades.TablaBasica;
import co.com.soaint.srvempleados.commons.exceptions.BusinessException;
import co.com.soaint.srvempleados.repository.empleados.impl.IEmpleadoRepositoryFacade;
import co.com.soaint.srvempleados.repository.empleados.impl.ISolicitudRepositoryFacade;
import co.com.soaint.srvempleados.service.empleados.IEmpleado;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class EmpleadoImpl implements IEmpleado {
    private final IEmpleadoRepositoryFacade repository;
    private  final ISolicitudRepositoryFacade repositorySolicitudFacade;

    @Autowired
    public EmpleadoImpl(IEmpleadoRepositoryFacade repository, ISolicitudRepositoryFacade repositorySolicitudFacade) {
        this.repository = repository;
        this.repositorySolicitudFacade = repositorySolicitudFacade;
    }

    @Override
    public Optional<List<EmpleadoDto>> findEmpleados() {
        List<EmpleadoDto> empleadoDto = new ArrayList<EmpleadoDto>();
        List<Empleado> empleadoDao = repository.findEmpleados();
        empleadoDao.forEach(obj ->{
            empleadoDto.add(MappingEmpleadoDto(obj));
        });
        return Optional.of(empleadoDto);
    }

    @Override
    public Optional<EmpleadoDto> findEmpleadoByNumeroRadicado(String numeroRadicado) throws Exception {
        Empleado empleado = repositorySolicitudFacade.findIdSolicitanteByNumeroRadicado(numeroRadicado);
        if(empleado == null){
            throw BusinessException.notfound();
        }
        empleado.getInformacionVacaciones().setDiasDisponibles(
                obtenerDiasDisponibles(empleado.getFechaIngreso(),
                        empleado.getInformacionVacaciones().getDiasSolicitados()));

        EmpleadoDto empleadoDto = MappingEmpleadoDto(empleado);
        return Optional.of(empleadoDto);
    }

    @Override
    public EmpleadoDto findEmpleadoByNumeroDocumento(String numeroDocumento) throws Exception {
        Empleado empleado = repository.findEmpleadoByNumeroDocumento(numeroDocumento);
        if(empleado == null){
            throw BusinessException.notfound();
        }
        empleado.getInformacionVacaciones().setDiasDisponibles(
                obtenerDiasDisponibles(empleado.getFechaIngreso(),
                        empleado.getInformacionVacaciones().getDiasSolicitados()));
        EmpleadoDto empleadoDto = MappingEmpleadoDto(empleado);
        return empleadoDto;
    }

    @Override
    public List<EmpleadoDto> findEmpleadosByCago(String cargo) {
        List<EmpleadoDto> empleadoDto = new ArrayList<EmpleadoDto>();
        TablaBasica tablaBasica = new TablaBasica();
        tablaBasica.setIdTablaBasica(repository.findIdTablaBasicaByNumeroRadicado(cargo));
        List<Empleado> empleadoDao = repository.findEmpleadosByCargo(tablaBasica);
        empleadoDao.forEach(obj ->{
            empleadoDto.add(MappingEmpleadoDto(obj));
        });
        return empleadoDto;
    }
    public Integer obtenerDiasDisponibles(Date fechaIngreso,int diasSolicitados){
        Date today = new Date();
        fechaIngreso.setYear(today.getYear());
        if(today.getTime()<fechaIngreso.getTime()){
            fechaIngreso.setYear(today.getYear()-1);
        }
        long diff =  today.getTime() - fechaIngreso.getTime();
        long diffDays = diff / (24 * 60 * 60 * 1000);
        double diasDisponibles = 1.25*(diffDays /30.4375);
        double totaldiasDisponibles=0;
        if(diasDisponibles > diasSolicitados){
            totaldiasDisponibles = diasDisponibles - diasSolicitados;
        }
        return (int)totaldiasDisponibles;
    }
    private EmpleadoDto MappingEmpleadoDto(Empleado empleado){
        EmpleadoDto empleadoDto = new EmpleadoDto();
        empleadoDto.setCargo(empleado.getCargo().getCodigoApp());
        empleadoDto.setCorreoElectronico(empleado.getCorreoElectronico());
        empleadoDto.setEstado(empleado.getEstado());
        empleadoDto.setEstadoEmpleado(empleado.getEstadoEmpleado().getCodigoApp());
        empleadoDto.setFechaCreacion(empleado.getFechaCreacion());
        empleadoDto.setFechaIngreso(empleado.getFechaIngreso());
        empleadoDto.setFechaModificacion(empleado.getFechaModificacion());
        empleadoDto.setGenero(empleado.getGenero());
        empleadoDto.setIdEmpleado(empleado.getIdEmpleado());
        empleadoDto.setImagen(empleado.getImagen());
        empleadoDto.setInformacionVacacionesDto(MappingInformacionVacacionesDto(empleado.getInformacionVacaciones()));
        if(empleado.getJefeInmediato() != null){
            empleadoDto.setJefeInmediato(MappingEmpleadoDto(empleado.getJefeInmediato()));}
        empleadoDto.setNumeroDocumento(empleado.getNumeroDocumento());
        empleadoDto.setPrimerApellido(empleado.getPrimerApellido());
        empleadoDto.setSegundoApellido(empleado.getSegundoApellido());
        empleadoDto.setPrimerNombre(empleado.getPrimerNombre());
        empleadoDto.setSegundoNombre(empleado.getSegundoNombre());
        empleadoDto.setTelefono(empleado.getTelefono());
        empleadoDto.setTipoContrato(empleado.getTipoContrato().getCodigoApp());
        empleadoDto.setTipoDocumento(empleado.getTipoDocumento().getCodigoApp());
        return empleadoDto;
    }
    private InformacionVacacionesDto MappingInformacionVacacionesDto(InformacionVacaciones informacionVacaciones){
        InformacionVacacionesDto informacionVacacionesDto = new InformacionVacacionesDto();
        informacionVacacionesDto.setIdInformacionVacaciones(informacionVacaciones.getIdInformacionVacaciones());
        informacionVacacionesDto.setDiasDisponibles(informacionVacaciones.getDiasDisponibles());
        informacionVacacionesDto.setDiasSolicitados(informacionVacaciones.getDiasSolicitados());
        informacionVacacionesDto.setDiasAnticipados(informacionVacaciones.getDiasanticipados());
        return  informacionVacacionesDto;
    }
}
