package co.com.soaint.srvempleados.repository.empleados.impl;


import co.com.soaint.componentescomunes.Dto.EmpleadoDto;
import co.com.soaint.componentescomunes.entidades.Empleado;
import co.com.soaint.componentescomunes.entidades.TablaBasica;

import java.util.List;
import java.util.Optional;

public interface IEmpleadoRepositoryFacade {

    List<Empleado> findEmpleados();
    Empleado findEmpleadoByNumeroDocumento(String numeroDocumento);
    List<Empleado> findEmpleadosByCargo(TablaBasica cargo);
    Integer findIdTablaBasicaByNumeroRadicado(String codigoApp);
}

