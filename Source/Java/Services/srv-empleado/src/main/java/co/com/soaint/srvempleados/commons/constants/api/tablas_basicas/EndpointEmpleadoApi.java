package co.com.soaint.srvempleados.commons.constants.api.tablas_basicas;

public interface EndpointEmpleadoApi {

    String EMPLEADO_API_V1 = "api/v1";
    String FIND_ALL_EMPLEADOS = "/";
    String FIND_EMPLEADO_BY_NUMERO_RADICADO = "/numeroRadicado/";
    String FIND_EMPLEADO_BY_NUMERO_DOCUMENTO = "/numeroDocumento/";
    String FIND_EMPLEADOS_BY_CARGO = "/cargo/";
}
