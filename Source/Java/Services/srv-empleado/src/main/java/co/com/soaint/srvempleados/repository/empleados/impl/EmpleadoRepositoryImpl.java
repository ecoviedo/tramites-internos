package co.com.soaint.srvempleados.repository.empleados.impl;

import co.com.soaint.componentescomunes.entidades.Empleado;
import co.com.soaint.componentescomunes.entidades.Solicitud;
import co.com.soaint.componentescomunes.entidades.TablaBasica;
import co.com.soaint.srvempleados.repository.empleados.IEmpleadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Component
public class EmpleadoRepositoryImpl implements IEmpleadoRepositoryFacade {

    private final IEmpleadoRepository repository;

    @Autowired
    public EmpleadoRepositoryImpl(IEmpleadoRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Empleado> findEmpleados() {
        return repository.findAll();
    }

    @Override
    public Empleado findEmpleadoByNumeroDocumento(String numeroDocumento) {
        return repository.findByNumeroDocumento(numeroDocumento);
    }

    @Override
    public List<Empleado> findEmpleadosByCargo(TablaBasica cargo) {
        return repository.findByCargo(cargo);
    }

    @Override
    public Integer findIdTablaBasicaByNumeroRadicado(String codigoApp) {
        return repository.findIdTablaBasicaByNumeroRadicado(codigoApp);
    }


}
