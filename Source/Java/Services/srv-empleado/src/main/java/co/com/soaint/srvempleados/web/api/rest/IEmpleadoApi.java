package co.com.soaint.srvempleados.web.api.rest;

import co.com.soaint.componentescomunes.entidades.Empleado;
import org.springframework.http.ResponseEntity;

public interface IEmpleadoApi {

    ResponseEntity findAllEmpleados();
    ResponseEntity<Empleado> findEmpleadoByNumeroRadicado(String numeroIdentificacion);
    ResponseEntity<Empleado> findEmpleadoByNumeroIdentificacion(String numeroIdentificacion);
    ResponseEntity findEmpleadosByCargo(String cargo);
}
