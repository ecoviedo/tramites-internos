package co.com.soaint.srvempleados.repository.empleados.impl;

import co.com.soaint.componentescomunes.entidades.Empleado;

public interface ISolicitudRepositoryFacade {
    Empleado findIdSolicitanteByNumeroRadicado(String numeroRadicado);
}
