package co.com.soaint.srvempleados.repository.empleados;

import co.com.soaint.componentescomunes.entidades.Empleado;
import co.com.soaint.componentescomunes.entidades.Solicitud;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ISolicitudRepository extends JpaRepository<Solicitud, String> {

    @Query("select (solicitud.solicitante) from Solicitud solicitud where (solicitud.numeroRadicado) like lower(:numeroRadicado)")
    Empleado findIdSolicitanteByNumeroRadicado(@Param("numeroRadicado") String numeroRadicado);


}
