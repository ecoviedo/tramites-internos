package co.com.soaint.srvempleados.service.empleados;

import co.com.soaint.componentescomunes.Dto.EmpleadoDto;
import co.com.soaint.componentescomunes.entidades.Empleado;

import java.util.List;
import java.util.Optional;

public interface IEmpleado {

    Optional<List<EmpleadoDto>> findEmpleados();

    Optional<EmpleadoDto> findEmpleadoByNumeroRadicado(String numeroRadicado) throws Exception;

    EmpleadoDto findEmpleadoByNumeroDocumento(String numeroDocumento) throws Exception;

    List<EmpleadoDto> findEmpleadosByCago(String cargo);
}
