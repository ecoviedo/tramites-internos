package co.com.soaint.srvempleados.repository.empleados.impl;

import co.com.soaint.componentescomunes.entidades.Empleado;
import co.com.soaint.srvempleados.repository.empleados.ISolicitudRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SolicitudRepositoryImpl implements  ISolicitudRepositoryFacade {
    private final ISolicitudRepository repository;
    @Autowired
    public SolicitudRepositoryImpl(ISolicitudRepository repository) {
        this.repository = repository;
    }

    @Override
    public Empleado findIdSolicitanteByNumeroRadicado(String numeroRadicado) {
        Empleado empleado = repository.findIdSolicitanteByNumeroRadicado(numeroRadicado);
        return empleado;
    }
}
