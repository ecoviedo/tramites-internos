package co.com.soaint.internalprocedures.canonicalmodel.dto.requestDetail;

import co.com.soaint.internalprocedures.canonicalmodel.entities.parameter.ParameterDAO;
import co.com.soaint.internalprocedures.canonicalmodel.entities.request.RequestDAO;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RequestDetailDTO implements Serializable {
    private Integer id;
    @Temporal(TemporalType.TIMESTAMP)
    private Date requestDay;
    @Temporal(TemporalType.TIME)
    private Date startHour;
    @Temporal(TemporalType.TIME)
    private Date endHour;
    private String reasonDescription;
    private Integer requestDays;
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    private String numberFiled;
    private String reasonRequest;
}
