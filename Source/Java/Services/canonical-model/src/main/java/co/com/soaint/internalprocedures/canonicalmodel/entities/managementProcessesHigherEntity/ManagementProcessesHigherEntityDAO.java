package co.com.soaint.internalprocedures.canonicalmodel.entities.managementProcessesHigherEntity;

import co.com.soaint.internalprocedures.canonicalmodel.entities.employee.EmployeeDAO;
import co.com.soaint.internalprocedures.canonicalmodel.entities.parameter.ParameterDAO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "management_processes_higher_entity")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ManagementProcessesHigherEntityDAO implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Column(name = "replacement")
    private Boolean replacement;

    @Column(name = "recompensed")
    private Boolean recompensed;

    @Basic(optional = false)
    @Column(name = "observations")
    private String observations;

    @Column(name = "approval_date",nullable = false)
    @Temporal(TemporalType.DATE)
    private Date approvalDate;

    @JoinColumn(name = "replacement_employee", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private EmployeeDAO replacementEmployee;

    @JoinColumn(name = "approval", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private ParameterDAO approval;

}
