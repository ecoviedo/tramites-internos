package co.com.soaint.internalprocedures.canonicalmodel.entities.requestManagement;

import co.com.soaint.internalprocedures.canonicalmodel.entities.managementProcessesHigherEntity.ManagementProcessesHigherEntityDAO;
import co.com.soaint.internalprocedures.canonicalmodel.entities.parameter.ParameterDAO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "request_management")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class RequestManagementDAO implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Column(name = "approval_rrhh")
    private Boolean approvalRrhh;

    @Column(name = "approval_rrhh_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAprobRrhh;

    @Column(name = "observations")
    private String observations;

    @JoinColumn(name = "approval_manager", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private ManagementProcessesHigherEntityDAO approvalManager;

    @JoinColumn(name = "approval_immediate_boss", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private ManagementProcessesHigherEntityDAO approvalImmediateBoss;

    @JoinColumn(name = "classifications", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private ParameterDAO classifications;

}
