package co.com.soaint.internalprocedures.canonicalmodel.entities.employee;


import co.com.soaint.internalprocedures.canonicalmodel.entities.holidayInformation.HolidayInformationDAO;
import co.com.soaint.internalprocedures.canonicalmodel.entities.parameter.ParameterDAO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "employee")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class EmployeeDAO implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @JoinColumn(name = "immediate_boss", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY,optional = false)
    private EmployeeDAO immediateBoss;

    @JoinColumn(name = "holiday_information", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY,optional = false)
    private HolidayInformationDAO holidayInformationDAO;

    @JoinColumn(name = "position", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY,optional = false)
    private ParameterDAO position;

    @JoinColumn(name = "employed_status", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY,optional = false)
    private ParameterDAO employedStatus;

    @JoinColumn(name = "contract_type", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY,optional = false)
    private ParameterDAO contractType;

    @JoinColumn(name = "doc_id_type", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY,optional = false)
    private ParameterDAO docType;

    @Column(name = "doc_id_number",nullable = false,unique = true)
    private String docIdNumber;

    @Column(name = "first_name",nullable = false)
    private String firstName;

    @Column(name = "middle_name",nullable = false)
    private String middleName;

    @Column(name = "last_name",nullable = false)
    private String lastNmae;

    @Column(name = "mothers_last_name",nullable = false)
    private String mothersLastName;

    @JoinColumn(name = "gender", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY,optional = false)
    private ParameterDAO gender;

    @Column(name = "email")
    private String email;

    @Column(name = "phone")
    private String phone;

    @Column(name = "image")
    private String image;

    @Column(name ="admission_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date admissionDate;

    @Column(name = "logical_status")
    private Boolean logicalStatus;

    @Column(name = "creation_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;

    @Column(name ="last_modified_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;

}
