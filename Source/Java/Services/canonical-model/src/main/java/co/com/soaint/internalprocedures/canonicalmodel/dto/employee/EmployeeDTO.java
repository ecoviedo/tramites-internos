package co.com.soaint.internalprocedures.canonicalmodel.dto.employee;

import co.com.soaint.internalprocedures.canonicalmodel.entities.employee.EmployeeDAO;
import co.com.soaint.internalprocedures.canonicalmodel.entities.holidayInformation.HolidayInformationDAO;
import co.com.soaint.internalprocedures.canonicalmodel.entities.parameter.ParameterDAO;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EmployeeDTO implements Serializable {
    private Integer id;
    private EmployeeDAO immediateBoss;
    private Integer holidayInformationDAO;
    private String position;
    private String employedStatus;
    private String contractType;
    private String docIdType;
    private String docIdNumber;
    private String firstName;
    private String middleName;
    private String lastNmae;
    private String mothersLastName;
    private String gender;
    private String email;
    private String phone;
    private String image;
    @Temporal(TemporalType.TIMESTAMP)
    private Date admissionDate;
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;
}
