package co.com.soaint.internalprocedures.canonicalmodel.entities.holidayRequest;

import co.com.soaint.internalprocedures.canonicalmodel.entities.request.RequestDAO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "holiday_request")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class HolidayRequestDAO implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Column(name = "start_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    @Column(name = "end_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;

    @Column(name = "available_days",nullable = false)
    private Integer availableDays;

    @Column(name = "requested_days",nullable = false)
    private Integer requestedDays;

    @Column(name = "anticipated_days",nullable = false)
    private Integer anticipatedDays;

    @JoinColumn(name = "number_filed", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.LAZY, optional = false)
    private RequestDAO numberFiled;

    @Column(name = "refund_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date refundDate;

}
