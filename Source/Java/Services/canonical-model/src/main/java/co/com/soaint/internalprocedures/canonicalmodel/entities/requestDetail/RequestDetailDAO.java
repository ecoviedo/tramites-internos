package co.com.soaint.internalprocedures.canonicalmodel.entities.requestDetail;

import co.com.soaint.internalprocedures.canonicalmodel.entities.parameter.ParameterDAO;
import co.com.soaint.internalprocedures.canonicalmodel.entities.request.RequestDAO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "request_detail")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class RequestDetailDAO implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Column(name = "request_day")
    @Temporal(TemporalType.TIMESTAMP)
    private Date requestDay;

    @Column(name = "start_hour")
    @Temporal(TemporalType.TIME)
    private Date startHour;

    @Column(name = "end_hour")
    @Temporal(TemporalType.TIME)
    private Date endHour;

    @Column(name = "reason_description")
    private String reasonDescription;

    @Column(name = "request_days")
    private Integer requestDays;

    @Column(name = "start_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    @Column(name = "end_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;

    @JoinColumn(name = "number_filed", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.LAZY, optional = false)
    private RequestDAO numberFiled;

    @JoinColumn(name = "reason_request", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private ParameterDAO reasonRequest;

}