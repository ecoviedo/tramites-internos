package co.com.soaint.internalprocedures.canonicalmodel.dto.requestManagement;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RequestManagementDTO implements Serializable {
    private Integer id;
    private Boolean approvalRrhh;
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAprobRrhh;
    private String observations;
    private String approvalManager;
    private String approvalImmediateBoss;
    private String classifications;
}
