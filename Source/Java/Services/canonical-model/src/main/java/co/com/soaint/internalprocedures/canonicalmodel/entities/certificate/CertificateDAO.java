package co.com.soaint.internalprocedures.canonicalmodel.entities.certificate;

import co.com.soaint.internalprocedures.canonicalmodel.entities.request.RequestDAO;
import co.com.soaint.internalprocedures.canonicalmodel.entities.parameter.ParameterDAO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "certificate")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class CertificateDAO implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Column(name = "observations")
    private String observations;

    @JoinColumn(name ="number_filed", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.LAZY,optional = false)
    private RequestDAO numberFiled;

    @JoinColumn(name = "certificate_type", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY,optional = false)
    private ParameterDAO certificateType;

}
