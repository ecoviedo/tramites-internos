package co.com.soaint.internalprocedures.canonicalmodel.enums.constants.parameter;

public enum ParameterCONST {
    SUCCESS("OK"),
    FAIL("ER"),
    PARAMETER_NOT_EXIST_CODE(FAIL.mensaje+"-P001"),
    PARAMETER_NOT_EXIST_MESSAGE("Parameter not exist"),
    PARAMETER_ALREADY_EXIST_CODE(FAIL.mensaje+"-P002"),
    PARAMETER_ALREADY_EXIST_MESSAGE("Parameter already exist"),
    PARAMETER_TYPE_NOT_EXIST_CODE(FAIL.mensaje+"-P003"),
    PARAMETER_TYPE_NOT_EXIST_CODE_MESSAGE("There are no parameters with this Type"),
    PARAMETER_TABLE_IS_EMPTY_CODE(FAIL.mensaje+"-P003"),
    PARAMETER_TABLE_IS_EMPTY_MESSAGE("There are no records in the parameter table"),
    ERROR_INSERT_PARAMETERS_CODE(FAIL.mensaje+"-P004"),
    ERROR_INSERT_PARAMETERS_MESSAGE("Error Trying insert parameters on persistence"),
    /** Message **/
    PARAMETER_CREATED("Parameter Created"),
    LIST_PARAMETER_CREATED("List Parameters Created"),
    PARAMETER_UPDATE("Parameter Updated");
    private String mensaje;

    ParameterCONST(String mensaje) {
        this.mensaje = mensaje;
    }
    public String getMensaje() {
        return mensaje;
    }
}
