package co.com.soaint.internalprocedures.canonicalmodel.dto.historicalEmployeeProject;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class HistoricalEmployeeProjectDTO implements Serializable {

    private Integer id;
    private String employee;
    private String manager;
    private String project;
}