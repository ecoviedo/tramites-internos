package co.com.soaint.internalprocedures.canonicalmodel.dto.certificate;

import co.com.soaint.internalprocedures.canonicalmodel.entities.parameter.ParameterDAO;
import co.com.soaint.internalprocedures.canonicalmodel.entities.request.RequestDAO;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CertificateDTO implements Serializable {
    private Integer id;
    private String observations;
    private String numberFiled;
    private String certificateType;
}
