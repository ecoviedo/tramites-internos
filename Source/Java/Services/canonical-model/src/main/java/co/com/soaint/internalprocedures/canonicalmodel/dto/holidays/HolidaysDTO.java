package co.com.soaint.internalprocedures.canonicalmodel.dto.holidays;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class HolidaysDTO implements Serializable {
    private Integer id;
    private Date date;
    private String description;
}
