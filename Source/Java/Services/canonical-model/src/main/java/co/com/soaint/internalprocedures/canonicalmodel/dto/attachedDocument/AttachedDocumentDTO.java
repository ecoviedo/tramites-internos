package co.com.soaint.internalprocedures.canonicalmodel.dto.attachedDocument;

import co.com.soaint.internalprocedures.canonicalmodel.entities.parameter.ParameterDAO;
import co.com.soaint.internalprocedures.canonicalmodel.entities.requestDetail.RequestDetailDAO;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AttachedDocumentDTO implements Serializable {
    private Integer id;
    private String name;
    private String docType;
    private String ecmId;
    private Date creationDate;
    private String requestDetailDAO;
}
