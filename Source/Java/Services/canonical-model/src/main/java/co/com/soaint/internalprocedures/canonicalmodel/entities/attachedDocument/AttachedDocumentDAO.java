package co.com.soaint.internalprocedures.canonicalmodel.entities.attachedDocument;

import co.com.soaint.internalprocedures.canonicalmodel.entities.requestDetail.RequestDetailDAO;
import co.com.soaint.internalprocedures.canonicalmodel.entities.parameter.ParameterDAO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "attached_document")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class AttachedDocumentDAO implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "name",length = 50)
    private String name;

    @JoinColumn(name = "doc_type", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY,optional = false)
    private ParameterDAO docType;

    @Column(name ="ecm_id",length = 2147483647)
    private String ecmId;

    @Column(name = "creation_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;

    @JoinColumn(name = "request_detail", referencedColumnName = "id", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private RequestDetailDAO requestDetailDAO;
}