package co.com.soaint.internalprocedures.canonicalmodel.entities.request;

import co.com.soaint.internalprocedures.canonicalmodel.entities.requestManagement.RequestManagementDAO;
import co.com.soaint.internalprocedures.canonicalmodel.entities.employee.EmployeeDAO;
import co.com.soaint.internalprocedures.canonicalmodel.entities.parameter.ParameterDAO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "request")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class RequestDAO implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Column(name = "request_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date requestDate;

    @Column(name = "logical_status")
    private Boolean logicalStatus;

    @Column(name = "number_filed")
    private String numberFiled;

    @JoinColumn(name = "applicant", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private EmployeeDAO applicant;

    @JoinColumn(name = "request_management", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private RequestManagementDAO requestManagement;

    @JoinColumn(name = "request_type", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private ParameterDAO requestType;

}
