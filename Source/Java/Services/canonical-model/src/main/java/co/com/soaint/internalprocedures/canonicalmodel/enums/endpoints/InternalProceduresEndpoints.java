package co.com.soaint.internalprocedures.canonicalmodel.enums.endpoints;

public enum InternalProceduresEndpoints {
        HOST_SERVICES("http://3.136.218.255:8089/"),
        /**Action Endpoints**/
        ACTION_URI(HOST_SERVICES.mensaje + "service-action/v1/action"),
        FIND_ACTION_BY_NUMBER_URI(ACTION_URI.mensaje +"/number/"),
        UPDATE_ACTION_BY_NUMBER_URI(ACTION_URI.mensaje +"/"),
        /**Bdpu Endpoints**/
        BDPU_URI(HOST_SERVICES.mensaje+"service-bdpu/v1/bdpu/"),
        FIND_BDPU_BY_NUMBER_URI(BDPU_URI.mensaje +"number/"),
        /**Unspsc Endpoints**/
        UNSPSC_URI(HOST_SERVICES.mensaje+"service-unspsc/v1/unspsc/"),
        FIND_UNSPSC_BY_NUMBER_URI (UNSPSC_URI.mensaje +"number/"),
        /** Parameter Enpoints**/
         PARAMETER_URI(HOST_SERVICES.mensaje+"service-parameter/v1/parameter/"),
         FIND_PARAMETER_BY_CODE_URI (PARAMETER_URI.mensaje+"/code/"),
        /**Contract Endpoints**/
        CONTRACT_URI(HOST_SERVICES.mensaje+"service-contract/v1/contract/"),
        FIND_CONTRACT_BY_NUMBER_URI (CONTRACT_URI.mensaje +"number/");

        private String mensaje;

        InternalProceduresEndpoints(String mensaje) {this.mensaje = mensaje;}
        public String getMensaje() {
        return mensaje;
    }
}
