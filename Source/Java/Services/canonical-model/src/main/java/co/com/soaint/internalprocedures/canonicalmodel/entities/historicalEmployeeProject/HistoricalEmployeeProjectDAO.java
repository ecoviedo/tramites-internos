package co.com.soaint.internalprocedures.canonicalmodel.entities.historicalEmployeeProject;

import co.com.soaint.internalprocedures.canonicalmodel.entities.employee.EmployeeDAO;
import co.com.soaint.internalprocedures.canonicalmodel.entities.project.ProjectDAO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "historical_employee_project")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class HistoricalEmployeeProjectDAO implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;

    @JoinColumn(name = "employee", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY,optional = false)
    private EmployeeDAO employee;

    @JoinColumn(name = "manager", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY,optional = false)
    private EmployeeDAO manager;

    @JoinColumn(name = "project", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY,optional = false)
    private ProjectDAO project;

}