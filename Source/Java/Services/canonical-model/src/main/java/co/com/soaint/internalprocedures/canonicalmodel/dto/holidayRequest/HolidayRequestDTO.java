package co.com.soaint.internalprocedures.canonicalmodel.dto.holidayRequest;

import co.com.soaint.internalprocedures.canonicalmodel.entities.request.RequestDAO;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class HolidayRequestDTO implements Serializable {
    private Integer id;
    private Date startDate;
    private Date endDate;
    private Integer availableDays;
    private Integer requestedDays;
    private Integer anticipatedDays;
    private String numberFiled;
    @Temporal(TemporalType.TIMESTAMP)
    private Date refundDate;
}
