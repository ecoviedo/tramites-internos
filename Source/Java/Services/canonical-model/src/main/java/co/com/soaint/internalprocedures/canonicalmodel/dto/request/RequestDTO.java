package co.com.soaint.internalprocedures.canonicalmodel.dto.request;

import co.com.soaint.internalprocedures.canonicalmodel.entities.employee.EmployeeDAO;
import co.com.soaint.internalprocedures.canonicalmodel.entities.parameter.ParameterDAO;
import co.com.soaint.internalprocedures.canonicalmodel.entities.requestManagement.RequestManagementDAO;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RequestDTO implements Serializable {
    private Integer id;
    @Temporal(TemporalType.TIMESTAMP)
    private Date requestDate;
    private Boolean logicalStatus;
    private String numberFiled;
    private String applicant;
    private String requestManagement;
    private String requestType;
}
