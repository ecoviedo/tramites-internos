package co.com.soaint.internalprocedures.canonicalmodel.enums.constants.employee;

public enum EmployeeCONST {
    /**Business Errors**/
    ACTION_MANAGEMENT_NOT_EXIST_CODE("ER-ACM0001"),
    ACTION_MANAGEMENT_NOT_EXIST_MESSAGE("Action Management does not exist"),
    ACTION_MANAGEMENT_ALREADY_EXIST_CODE("ER-ACM0002"),
    ACTION_MANAGEMENT_ALREADY_EXIST_MESSAGE("Action Management already exist"),
    NO_ACTIVITIES_TO_ACTION_CODE("ER-ACM0004"),
    NO_ACTIVITIES_TO_ACTION_MESSAGE("No action management for this action was found"),
    WEIGHT_OF_ACTIVITIES_EXCEED_LIMIT_AVAILABLE_CODE("ER-ACM0005"),
    WEIGHT_OF_ACTIVITIES_EXCEED_LIMIT_AVAILABLE_MESSAGE("The weight of all Activities exceed the limit available"),
    /** Messages **/
    ACTION_MANAGEMENT_UPDATED("Action Management Updated"),
    ACTION_MANAGEMENT_CREATED("Action Management Created"),
    ACTION_MANAGEMENT_DELETED("Action Management Deleted");
    private String mensaje;

    EmployeeCONST(String mensaje) {
        this.mensaje = mensaje;
    }
    public String getMensaje() {
        return mensaje;
    }
}
