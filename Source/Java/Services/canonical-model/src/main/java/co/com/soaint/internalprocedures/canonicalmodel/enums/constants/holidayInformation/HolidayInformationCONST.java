package co.com.soaint.internalprocedures.canonicalmodel.enums.constants.holidayInformation;

public enum HolidayInformationCONST {
    CONTRACT_NOT_EXIST_CODE("ER-C0001"),
    CONTRACT_NOT_EXIST_MESSAGE("Contract does not exist"),
    CONTRACT_ALREADY_EXIST_CODE("ER-C0002"),
    CONTRACT_ALREADY_EXIST_MESSAGE("Contract already exist");
    private String mensaje;

    HolidayInformationCONST(String mensaje) {
        this.mensaje = mensaje;
    }
    public String getMensaje() {
        return mensaje;
    }
}
