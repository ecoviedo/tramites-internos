package co.com.soaint.internalprocedures.canonicalmodel.dto.managementProcessesHigherEntity;

import co.com.soaint.internalprocedures.canonicalmodel.entities.employee.EmployeeDAO;
import co.com.soaint.internalprocedures.canonicalmodel.entities.parameter.ParameterDAO;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ManagementProcessesHigherEntityDTO implements Serializable {
    private Integer id;
    private Boolean replacement;
    private Boolean recompensed;
    private String observations;
    private Date approvalDate;
    private String replacementEmployee;
    private String approval;
}
