package co.com.soaint.srvsolicitudvacaciones.adapter.domain.response.starProcessInstance;

import java.io.Serializable;

public class Containers implements Serializable {
    public String containerId;
    public String version;
    public String status;
    public String containerAlias;
    public Processes [] processes;
    public String messages;
    public String rules;

    public String getContainerId() {
        return containerId;
    }

    public void setContainerId(String containerId) {
        this.containerId = containerId;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getContainerAlias() {
        return containerAlias;
    }

    public void setContainerAlias(String containerAlias) {
        this.containerAlias = containerAlias;
    }

    public Processes [] getProcesses() {
        return processes;
    }

    public void setProcesses(Processes [] processes) {
        this.processes = processes;
    }

    public String getMessages() {
        return messages;
    }

    public void setMessages(String messages) {
        this.messages = messages;
    }

    public String getRules() {
        return rules;
    }

    public void setRules(String rules) {
        this.rules = rules;
    }
}
