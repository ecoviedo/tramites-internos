package co.com.soaint.srvsolicitudvacaciones.repository.solicitudvacaciones.impl;

import co.com.soaint.componentescomunes.entidades.Solicitud;
import co.com.soaint.componentescomunes.entidades.SolicitudVacaciones;
import co.com.soaint.srvsolicitudvacaciones.repository.solicitudvacaciones.ISolicitudVacacionesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class SolicitudVacacionesRepositoryImpl implements  ISolicitudVacacionesRepositoryFacade{
   public final ISolicitudVacacionesRepository repository;
    @Autowired
    public SolicitudVacacionesRepositoryImpl(ISolicitudVacacionesRepository repository) {
        this.repository = repository;
    }

    @Override
    public SolicitudVacaciones findByNumeroRadicado(Solicitud numeroRadicado) {
        return repository.findByNumeroRadicado(numeroRadicado);
    }

    @Override
    public boolean existenceSolicitud(String numeroDocumento) {
        return repository.existenceSolicitud(numeroDocumento);
    }

    @Override
    public Optional<SolicitudVacaciones> createSolicitudVacaciones(SolicitudVacaciones solicitudVacaciones) {
        return Optional.of(repository.save(solicitudVacaciones));
    }

    @Override
    public boolean existenceSolicitudVacacionesByNumeroRadicado(Solicitud solicitud) {
        return repository.existsSolicitudVacacionesByNumeroRadicado(solicitud);
    }
}
