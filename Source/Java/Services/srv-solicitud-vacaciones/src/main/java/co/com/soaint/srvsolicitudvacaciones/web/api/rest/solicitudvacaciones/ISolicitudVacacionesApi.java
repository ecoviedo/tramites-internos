package co.com.soaint.srvsolicitudvacaciones.web.api.rest.solicitudvacaciones;

import co.com.soaint.componentescomunes.Dto.SolicitudVacacionesDto;
import co.com.soaint.srvsolicitudvacaciones.commons.exception.webClient.WebClientException;
import org.springframework.http.ResponseEntity;

public interface ISolicitudVacacionesApi {

    ResponseEntity createCertificado(SolicitudVacacionesDto certificadoDto);
    ResponseEntity findSolicitudVacacionesByIdCertificado(String numeroRadicado) throws WebClientException;
}
