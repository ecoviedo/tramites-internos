package co.com.soaint.srvsolicitudvacaciones.adapter.domain.request.startProcessInstance;

import java.io.Serializable;

public class RequestStartProcessInstanceDto implements Serializable {
    public String containerId;
    public String processesId;
    public OwnerUser ownerUser;
    public Parametros parametros;

    public RequestStartProcessInstanceDto(String containerId, String processesId, OwnerUser ownerUser, Parametros parametros) {
        this.containerId = containerId;
        this.processesId = processesId;
        this.ownerUser = ownerUser;
        this.parametros = parametros;
    }

    public String getContainerId() {
        return containerId;
    }

    public void setContainerId(String containerId) {
        this.containerId = containerId;
    }

    public String getProcessesId() {
        return processesId;
    }

    public void setProcessesId(String processesId) {
        this.processesId = processesId;
    }

    public OwnerUser getOwnerUser() {
        return ownerUser;
    }

    public void setOwnerUser(OwnerUser ownerUser) {
        this.ownerUser = ownerUser;
    }

    public Parametros getParametros() {
        return parametros;
    }

    public void setParametros(Parametros parametros) {
        this.parametros = parametros;
    }
}
