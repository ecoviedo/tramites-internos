package co.com.soaint.srvsolicitudvacaciones.adapter.domain.response.starProcessInstance;

public class ResponseStartProcessInstanceDto {
    public Containers [] containers;
    public Response response;

    public Containers [] getContainers() {
        return containers;
    }

    public void setContainers(Containers [] containers) {
        this.containers = containers;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }
}
