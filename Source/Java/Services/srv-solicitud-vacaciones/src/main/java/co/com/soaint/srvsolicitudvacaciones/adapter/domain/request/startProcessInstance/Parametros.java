package co.com.soaint.srvsolicitudvacaciones.adapter.domain.request.startProcessInstance;

import java.io.Serializable;
import java.lang.reflect.Array;

public class Parametros implements Serializable {
    public Array values;

    public Parametros() {
    }

    public Parametros(Array values) {
        this.values = values;
    }

    public Array getValues() {
        return values;
    }

    public void setValues(Array values) {
        this.values = values;
    }
}
