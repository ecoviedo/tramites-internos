package co.com.soaint.srvsolicitudvacaciones.web.api.rest.solicitudvacaciones.v1;
import co.com.soaint.componentescomunes.Dto.SolicitudVacacionesDto;
import co.com.soaint.srvsolicitudvacaciones.commons.constants.api.certificado.EndpointCertificado;
import co.com.soaint.srvsolicitudvacaciones.commons.domains.response.builder.ResponseBuilder;
import co.com.soaint.srvsolicitudvacaciones.commons.enums.TransactionState;
import co.com.soaint.srvsolicitudvacaciones.commons.exception.webClient.WebClientException;
import co.com.soaint.srvsolicitudvacaciones.service.solicitudvacaciones.ISolicitudVacaciones;
import co.com.soaint.srvsolicitudvacaciones.web.api.rest.solicitudvacaciones.ISolicitudVacacionesApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(EndpointCertificado.SOLICITUD_VACACIONES_API_V1)
@CrossOrigin("*")
public class SolicitudVacacionesApiImp implements ISolicitudVacacionesApi {

    private final ISolicitudVacaciones iSolicitudVacaciones;

    @Autowired
    public SolicitudVacacionesApiImp(ISolicitudVacaciones iSolicitudVacaciones) {
        this.iSolicitudVacaciones = iSolicitudVacaciones;
    }

    @PostMapping(EndpointCertificado.CREATE_CERTIFICADO)
    public ResponseEntity createCertificado(@RequestBody SolicitudVacacionesDto solicitudVacacionesDto) {
        try {
            return ResponseBuilder.newBuilder()
                    .withStatus(HttpStatus.OK)
                    .withResponse(iSolicitudVacaciones.createSolicitudvacaciones(solicitudVacacionesDto))
                    .buildResponse();
        } catch (Exception e){
            e.printStackTrace();
            return ResponseBuilder.newBuilder()
                    .withStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                    .withTransactionState(TransactionState.FAIL)
                    .withPath(EndpointCertificado.CREATE_CERTIFICADO)
                    .withMessage(e.getMessage()+"/n-Caused by:  -")
                    .buildResponse();
        }
    }
    @GetMapping(EndpointCertificado.FIND_SOLICITUD_VACACIONES)
    @RequestMapping(value = EndpointCertificado.FIND_SOLICITUD_VACACIONES + "{numeroRadicado}")
    @Override
    public ResponseEntity findSolicitudVacacionesByIdCertificado(@PathVariable("numeroRadicado") String numeroRadicado) throws WebClientException {
        return ResponseEntity.status(HttpStatus.OK).body(iSolicitudVacaciones.findSolicitudVacacionesBynumeroRadicado(numeroRadicado));
    }
}
