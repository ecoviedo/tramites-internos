package co.com.soaint.srvsolicitudvacaciones.service.solicitudvacaciones.impl;
import co.com.soaint.componentescomunes.Dto.SolicitudVacacionesDto;
import co.com.soaint.componentescomunes.entidades.Solicitud;
import co.com.soaint.componentescomunes.entidades.SolicitudVacaciones;
import co.com.soaint.srvsolicitudvacaciones.adapter.specific.ApiSolicitud.ClienteSolicitud;
import co.com.soaint.srvsolicitudvacaciones.commons.exception.BusinessException;
import co.com.soaint.srvsolicitudvacaciones.commons.exception.webClient.WebClientException;
import co.com.soaint.srvsolicitudvacaciones.repository.solicitudvacaciones.impl.*;
import co.com.soaint.srvsolicitudvacaciones.service.solicitudvacaciones.ISolicitudVacaciones;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class SolicitudVacacionesImpl implements ISolicitudVacaciones {
    private final ClienteSolicitud clienteSolicitud;
    private final ISolicitudVacacionesRepositoryFacade solicitudVacacionesRepository;
    @Autowired
    public SolicitudVacacionesImpl(ClienteSolicitud clienteSolicitud, ISolicitudVacacionesRepositoryFacade solicitudVacacionesRepository) {
        this.clienteSolicitud = clienteSolicitud;
        this.solicitudVacacionesRepository = solicitudVacacionesRepository;
    }

    @Override
    public Optional<SolicitudVacacionesDto> findSolicitudVacacionesBynumeroRadicado(String numeroRadicado) throws WebClientException {
        SolicitudVacacionesDto solicitudVacacionesDto = new SolicitudVacacionesDto();
        Solicitud solicitud = new Solicitud();
        solicitud.setIdSolicitud(clienteSolicitud.findIdSolicitudByNumeroRadicado(numeroRadicado));
        SolicitudVacaciones solicitudVacacionesDao = solicitudVacacionesRepository.findByNumeroRadicado(solicitud);
        solicitudVacacionesDto.setNumero_radicado(solicitudVacacionesDao.getNumeroRadicado().getNumeroRadicado());
        solicitudVacacionesDto.setFechaInicio(solicitudVacacionesDao.getFechaInicio());
        solicitudVacacionesDto.setFechaFin(solicitudVacacionesDao.getFechaFin());
        solicitudVacacionesDto.setDiasSolicitados(solicitudVacacionesDao.getDiasSolicitados());
        solicitudVacacionesDto.setDiasDisponibles(solicitudVacacionesDao.getDiasDisponibles());
        solicitudVacacionesDto.setDiasAnticipados(solicitudVacacionesDao.getDiasAnticipados());
        solicitudVacacionesDto.setIdSolicitudVacaciones(solicitudVacacionesDao.getIdSolicitudVacaciones());
        return Optional.of(solicitudVacacionesDto);
    }

    @Override
    public Optional<SolicitudVacacionesDto> createSolicitudvacaciones(SolicitudVacacionesDto solicitudVacacionesDto) throws Exception {
        if(!solicitudVacacionesRepository.existenceSolicitud(solicitudVacacionesDto.getNumero_radicado())){
            throw BusinessException.notFound("Solicitud");
        }
        Solicitud solicitud = new Solicitud();
        solicitud.setIdSolicitud(clienteSolicitud.findIdSolicitudByNumeroRadicado(solicitudVacacionesDto.getNumero_radicado()));
        if(solicitudVacacionesRepository.existenceSolicitudVacacionesByNumeroRadicado(solicitud)){
            throw BusinessException.alreadyExist("Solicitud Vacaciones");
        }
        SolicitudVacaciones solicitudVacacionesDao = new SolicitudVacaciones();
        solicitudVacacionesDao.setDiasAnticipados(solicitudVacacionesDto.getDiasAnticipados());
        solicitudVacacionesDao.setDiasDisponibles(solicitudVacacionesDto.getDiasDisponibles());
        solicitudVacacionesDao.setDiasSolicitados(solicitudVacacionesDto.getDiasSolicitados());
        solicitudVacacionesDao.setFechaFin(solicitudVacacionesDto.getFechaFin());
        solicitudVacacionesDao.setFechaInicio(solicitudVacacionesDto.getFechaInicio());
        solicitudVacacionesDao.setNumeroRadicado(solicitud);
        solicitudVacacionesRepository.createSolicitudVacaciones(solicitudVacacionesDao);
        return Optional.of(solicitudVacacionesDto);
    }


}
