package co.com.soaint.srvsolicitudvacaciones.adapter.domain.response.starProcessInstance;

import java.io.Serializable;

public class Instance implements Serializable {
    public String instanceId;

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }
}
