package co.com.soaint.srvsolicitudvacaciones.adapter.specific.ApiSolicitud;

import co.com.soaint.srvsolicitudvacaciones.commons.exception.webClient.WebClientException;

public interface IClienteSolicitud {
    Integer findIdSolicitudByNumeroRadicado(String NumeroRadicado) throws WebClientException;
}
