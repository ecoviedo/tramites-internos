package co.com.soaint.srvsolicitudvacaciones.adapter.domain.response.starProcessInstance;

public class Response {
    public String type;
    public String msg;
    public Integer statusCode;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }
}
