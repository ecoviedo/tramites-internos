package co.com.soaint.srvsolicitudvacaciones.commons.constants.api.radicarSolicitud;

public interface EndpointRadicarSolicitud {

    String SOLICITUD_API_V1 = "api/v1";
    String FIND_SOLICITUD = "/";
    String RADICAR_SOLICITUD = "/solicitud";
    String UPDATE_SOLICITUD = "/solicitud/";
    String SRV_TABLAS_BASICAS = "http://localhost:8003/"+SOLICITUD_API_V1+FIND_SOLICITUD;
}
