package co.com.soaint.srvsolicitudvacaciones.repository.solicitudvacaciones;

import co.com.soaint.componentescomunes.entidades.Solicitud;
import co.com.soaint.componentescomunes.entidades.SolicitudVacaciones;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ISolicitudVacacionesRepository extends JpaRepository<SolicitudVacaciones, Integer> {
    SolicitudVacaciones findByNumeroRadicado(Solicitud numeroRadicado);

    @Query("select case when count(numero_radicado)> 0 then true else false end from Solicitud solicitud where lower(solicitud.numeroRadicado) like lower(:numeroRadicado)")
    boolean existenceSolicitud(@Param("numeroRadicado") String numeroRadicado);

    boolean existsSolicitudVacacionesByNumeroRadicado(Solicitud solicitud);
}
