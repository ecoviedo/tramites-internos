package co.com.soaint.srvsolicitudvacaciones.commons.constants.api.certificado;

public interface EndpointCertificado {

    String SOLICITUD_VACACIONES_API_V1 = "api/v1";
    String FIND_SOLICITUD_VACACIONES = "/";
    String CREATE_CERTIFICADO = "/solicitudvacaciones";
    String UPDATE_CERTIFICADO = "/solicitudvacaciones/";
    String SRV_TABLAS_BASICAS = "http://localhost:8002/"+ SOLICITUD_VACACIONES_API_V1 + FIND_SOLICITUD_VACACIONES;
}
