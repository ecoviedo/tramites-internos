package co.com.soaint.srvsolicitudvacaciones.repository.solicitudvacaciones.impl;

import co.com.soaint.componentescomunes.entidades.Solicitud;
import co.com.soaint.componentescomunes.entidades.SolicitudVacaciones;

import java.util.Optional;

public interface ISolicitudVacacionesRepositoryFacade {
    SolicitudVacaciones findByNumeroRadicado(Solicitud numeroRadicado);

    boolean existenceSolicitud(String numeroDocumento);

    Optional<SolicitudVacaciones> createSolicitudVacaciones(SolicitudVacaciones solicitudVacaciones);

    boolean existenceSolicitudVacacionesByNumeroRadicado(Solicitud solicitud);
}
