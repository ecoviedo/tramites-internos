package co.com.soaint.srvsolicitudvacaciones.commons.exception;

public class BusinessException extends Exception {

    public static Exception alreadyExist(String tupla){
     return new Exception("El Registro Ya Existe Existe  en la Tabla "+tupla);
    }
    public static Exception notFound(String tupla){
        return new Exception("Valor no encontrado en la Tabla "+tupla);
    }
}
