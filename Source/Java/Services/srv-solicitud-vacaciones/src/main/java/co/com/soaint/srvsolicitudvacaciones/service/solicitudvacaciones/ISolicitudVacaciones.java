package co.com.soaint.srvsolicitudvacaciones.service.solicitudvacaciones;


import co.com.soaint.componentescomunes.Dto.SolicitudDto;
import co.com.soaint.componentescomunes.Dto.SolicitudVacacionesDto;
import co.com.soaint.componentescomunes.entidades.Solicitud;
import co.com.soaint.srvsolicitudvacaciones.commons.exception.webClient.WebClientException;

import java.util.Optional;

public interface ISolicitudVacaciones {

    Optional<SolicitudVacacionesDto> findSolicitudVacacionesBynumeroRadicado(String numeroRadicado) throws WebClientException;

    Optional<SolicitudVacacionesDto> createSolicitudvacaciones (SolicitudVacacionesDto solicitudVacacionesDto) throws Exception;
   // Optional<SolicitudVacacionesDto> updateSolicitud(Integer idSolicitudVacaciones, SolicitudVacacionesDto solicitudVacacionesDto) throws Exception;
}
