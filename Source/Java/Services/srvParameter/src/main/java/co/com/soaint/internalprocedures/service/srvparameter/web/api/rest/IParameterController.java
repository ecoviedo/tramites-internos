package co.com.soaint.internalprocedures.service.srvparameter.web.api.rest;

import co.com.soaint.internalprocedures.canonicalmodel.dto.generic.GenericRequestDTO;
import co.com.soaint.internalprocedures.canonicalmodel.dto.generic.GenericResponseDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

public interface IParameterController {

    ResponseEntity<GenericResponseDTO> findAll();

    ResponseEntity<GenericResponseDTO> create(@RequestBody GenericRequestDTO genericRequestDTO);

    ResponseEntity<GenericResponseDTO> findParameterByCode(@PathVariable String code);

    ResponseEntity<GenericResponseDTO> findParameterByType(@PathVariable String type);

    ResponseEntity<GenericResponseDTO> update(@PathVariable String code, @RequestBody GenericRequestDTO genericRequestDTO);


}
