package co.com.soaint.internalprocedures.service.srvparameter.commons.converter;


import co.com.soaint.internalprocedures.canonicalmodel.dto.parameter.ParameterDTO;
import co.com.soaint.internalprocedures.canonicalmodel.entities.parameter.ParameterDAO;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ParameterConverter {
    /**
     * @param parameterDTO
     * @return ParameterDAO
     * @author ncespedes
     * method to convert the parameterDTO to parameterDAO
     */
    public ParameterDAO parameterDTOToParameterDAO(ParameterDTO parameterDTO){
        return ParameterDAO.builder()
                .code(parameterDTO.getCode())
                .name(parameterDTO.getName())
                .image(parameterDTO.getImage())
                .type(parameterDTO.getType())
                .description(parameterDTO.getDescription())
                .build();
    }
    /**
     * @param parameterDAO
     * @return ParameterDTO
     * @author ncespedes
     * method to convert the parameterDAO to parameterDTO
     */
    public ParameterDTO parameterDAOToParameterDTO(ParameterDAO parameterDAO){
        return ParameterDTO.builder()
                .id(parameterDAO.getId())
                .code(parameterDAO.getCode())
                .name(parameterDAO.getName())
                .image(parameterDAO.getImage())
                .type(parameterDAO.getType())
                .description(parameterDAO.getDescription())
                .build();
    }
    /**
     * @param listparameterDAO
     * @return List<ParameterDTO>
     * @author ncespedes
     * method to convert a listparameterDAO to listparameterDTO
     */
    public List<ParameterDTO> listParameterDAOToListParameterDTO(List<ParameterDAO> listparameterDAO){
       List<ParameterDTO> parameterDTOList = new ArrayList<>();
       for(ParameterDAO parameterDAOTemp:listparameterDAO){
           parameterDTOList.add(parameterDAOToParameterDTO(parameterDAOTemp));
       }
       return parameterDTOList;
    }
    /**
     * @param listparameterDTO
     * @return List<ParameterDAO>
     * @author ncespedes
     * method to convert a listparameterDTO to listparameterDAO
     */
    public List<ParameterDAO> listParameterDTOToListParameterDAO(List<ParameterDTO> listparameterDTO){
        List<ParameterDAO> parameterDAOList = new ArrayList<>();
        for(ParameterDTO parameterDTOTemp:listparameterDTO){
            parameterDAOList.add(parameterDTOToParameterDAO(parameterDTOTemp));
        }
        return parameterDAOList;
    }
}
