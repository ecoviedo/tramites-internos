package co.com.soaint.internalprocedures.service.srvparameter.repository;

import co.com.soaint.internalprocedures.canonicalmodel.entities.parameter.ParameterDAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IParameterRepository extends JpaRepository<ParameterDAO, Integer> {

    @Query("SELECT parameter from ParameterDAO parameter where parameter.code=:code")
    Optional<ParameterDAO> findParameterByCode(String code);

    @Query("SELECT parameter from ParameterDAO parameter where parameter.type=:type")
    Optional<List<ParameterDAO>> findParameterByType(String type);

    @Query("SELECT  parameter FROM ParameterDAO parameter WHERE parameter.code=:code  AND parameter.type=:typeParameter ")
    Optional<ParameterDAO> findByCodeAndType(String code, String typeParameter);

    List<ParameterDAO> findAll();


}
