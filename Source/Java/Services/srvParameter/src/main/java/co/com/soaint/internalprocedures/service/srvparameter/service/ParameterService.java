package co.com.soaint.internalprocedures.service.srvparameter.service;
import co.com.soaint.internalprocedures.canonicalmodel.dto.generic.GenericRequestDTO;
import co.com.soaint.internalprocedures.canonicalmodel.dto.generic.GenericResponseDTO;
import co.com.soaint.internalprocedures.canonicalmodel.dto.parameter.ParameterDTO;
import co.com.soaint.internalprocedures.canonicalmodel.entities.parameter.ParameterDAO;
import co.com.soaint.internalprocedures.canonicalmodel.enums.constants.parameter.ParameterCONST;
import co.com.soaint.internalprocedures.service.srvparameter.commons.converter.ParameterConverter;
import co.com.soaint.internalprocedures.service.srvparameter.repository.IParameterRepository;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ParameterService implements IParameterService {

    private final IParameterRepository repository;
    private final ParameterConverter parameterConverter;

    ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    public ParameterService(IParameterRepository repository, ParameterConverter parameterConverter) {
        this.repository = repository;
        this.parameterConverter = parameterConverter;
    }

    @Override
    public ResponseEntity<GenericResponseDTO> findAll() {
        List<ParameterDAO> parameterDAOList = repository.findAll();
        if(parameterDAOList.isEmpty()){
            return new ResponseEntity<>(GenericResponseDTO.builder()
                    .codeResponse(ParameterCONST.PARAMETER_TABLE_IS_EMPTY_CODE.getMensaje())
                    .message(ParameterCONST.PARAMETER_TABLE_IS_EMPTY_MESSAGE.getMensaje())
                    .statusCode(HttpStatus.CONFLICT.value())
                    .build(), HttpStatus.OK);
        }
        List<ParameterDTO> parameterDTOList = parameterConverter.listParameterDAOToListParameterDTO(parameterDAOList);

        return new ResponseEntity<>(GenericResponseDTO.builder()
                .codeResponse(ParameterCONST.SUCCESS.getMensaje())
                .objectResponse(parameterDTOList)
                .statusCode(HttpStatus.OK.value())
                .build(), HttpStatus.OK);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<GenericResponseDTO> create(GenericRequestDTO genericRequestDTO) {
        ArrayList<ParameterDTO> listContractDeliverableDTO = objectMapper
                .convertValue(genericRequestDTO.getRequest(), new TypeReference<List<ParameterDTO>>() {});
        try {
            for (ParameterDTO parameterDTO:listContractDeliverableDTO   ) {
                Optional<ParameterDAO> optionalParameterDAO =
                        repository.findByCodeAndType(parameterDTO.getCode(), parameterDTO.getType());

                if (optionalParameterDAO.isPresent())
                    return new ResponseEntity<>(GenericResponseDTO.builder()
                            .codeResponse(ParameterCONST.PARAMETER_ALREADY_EXIST_CODE.getMensaje())
                            .message(ParameterCONST.PARAMETER_ALREADY_EXIST_MESSAGE.getMensaje()+parameterDTO.getCode())
                            .statusCode(HttpStatus.CONFLICT.value())
                            .build(), HttpStatus.OK);

                repository.save(parameterConverter.parameterDTOToParameterDAO(parameterDTO));
            }
            return new ResponseEntity<>(GenericResponseDTO.builder()
                    .codeResponse(ParameterCONST.SUCCESS.getMensaje())
                    .message(ParameterCONST.LIST_PARAMETER_CREATED.getMensaje())
                    .statusCode(HttpStatus.CREATED.value())
                    .build(), HttpStatus.CREATED);
        }catch (Exception e){
            return new ResponseEntity<>(GenericResponseDTO.builder()
                    .codeResponse(ParameterCONST.ERROR_INSERT_PARAMETERS_CODE.getMensaje())
                    .message(ParameterCONST.ERROR_INSERT_PARAMETERS_MESSAGE.getMensaje())
                    .objectResponse(e.getMessage())
                    .statusCode(HttpStatus.CONFLICT.value())
                    .build(), HttpStatus.CONFLICT);

        }
    }

    @Override
    public ResponseEntity<GenericResponseDTO> findParameterByCode(String code) {
        Optional<ParameterDAO> parameterDAO = repository.findParameterByCode(code);
        if(!parameterDAO.isPresent()){
            return new ResponseEntity<>(GenericResponseDTO.builder()
                    .codeResponse(ParameterCONST.PARAMETER_NOT_EXIST_CODE.getMensaje())
                    .message(ParameterCONST.PARAMETER_NOT_EXIST_MESSAGE.getMensaje())
                    .statusCode(HttpStatus.CONFLICT.value())
                    .build(), HttpStatus.OK);
        }
        ParameterDTO parameterDTO = parameterConverter.parameterDAOToParameterDTO(parameterDAO.get());

        return new ResponseEntity<>(GenericResponseDTO.builder()
                .codeResponse(ParameterCONST.SUCCESS.getMensaje())
                .objectResponse(parameterDTO)
                .statusCode(HttpStatus.OK.value())
                .build(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<GenericResponseDTO> findParameterByType(String type) {
        Optional<List<ParameterDAO>> listParameterDAO = repository.findParameterByType(type);
        if(!listParameterDAO.isPresent()){
            return new ResponseEntity<>(GenericResponseDTO.builder()
                    .codeResponse(ParameterCONST.PARAMETER_NOT_EXIST_CODE.getMensaje())
                    .message(ParameterCONST.PARAMETER_NOT_EXIST_MESSAGE.getMensaje())
                    .statusCode(HttpStatus.CONFLICT.value())
                    .build(), HttpStatus.OK);
        }
        List<ParameterDTO> listParameterDTO = parameterConverter.listParameterDAOToListParameterDTO(listParameterDAO.get());

        return new ResponseEntity<>(GenericResponseDTO.builder()
                .codeResponse(ParameterCONST.SUCCESS.getMensaje())
                .objectResponse(listParameterDTO)
                .statusCode(HttpStatus.OK.value())
                .build(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<GenericResponseDTO> update(String code, GenericRequestDTO genericRequestDTO) {
        return null;
    }
}
