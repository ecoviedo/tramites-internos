package co.com.soaint.internalprocedures.service.srvparameter.web.api.rest;

import co.com.soaint.internalprocedures.canonicalmodel.dto.generic.GenericRequestDTO;
import co.com.soaint.internalprocedures.canonicalmodel.dto.generic.GenericResponseDTO;
import co.com.soaint.internalprocedures.service.srvparameter.service.IParameterService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/parameter")
@CrossOrigin({"*"})
public class ParameterController implements IParameterController{
    private final IParameterService service;

    @Autowired
    public ParameterController(IParameterService service) {
        this.service = service;
    }

    @Override
    @GetMapping("/")
    @ApiOperation("Find All Parameters")
    public ResponseEntity<GenericResponseDTO> findAll() {
        try {

            return service.findAll();

        } catch (Exception e) {
            return new ResponseEntity<>(GenericResponseDTO.builder()
                    .statusCode(HttpStatus.CONFLICT.value())
                    .message(e.getMessage())
                    .objectResponse(e)
                    .build(), HttpStatus.CONFLICT);
        }
    }

    @Override
    @PostMapping("/")
    @ApiOperation("Create Parameters' List")
    public ResponseEntity<GenericResponseDTO> create(@RequestBody GenericRequestDTO genericRequestDTO) {
        try {

            return service.create(genericRequestDTO);

        } catch (Exception e) {
            return new ResponseEntity<>(GenericResponseDTO.builder()
                    .statusCode(HttpStatus.CONFLICT.value())
                    .message(e.getMessage())
                    .objectResponse(e)
                    .build(), HttpStatus.CONFLICT);
        }
    }

    @Override
    @GetMapping("/code/{code}")
    @ApiOperation("Find All Parameters")
    public ResponseEntity<GenericResponseDTO> findParameterByCode(@PathVariable String code) {
        try {

            return service.findParameterByCode(code);

        } catch (Exception e) {
            return new ResponseEntity<>(GenericResponseDTO.builder()
                    .statusCode(HttpStatus.CONFLICT.value())
                    .message(e.getMessage())
                    .objectResponse(e)
                    .build(), HttpStatus.CONFLICT);
        }
    }

    @Override
    @GetMapping("/type/{type}")
    @ApiOperation("Find All Parameters By Type")
    public ResponseEntity<GenericResponseDTO> findParameterByType(@PathVariable String type) {
        try {

            return service.findParameterByType(type);

        } catch (Exception e) {
            return new ResponseEntity<>(GenericResponseDTO.builder()
                    .statusCode(HttpStatus.CONFLICT.value())
                    .message(e.getMessage())
                    .objectResponse(e)
                    .build(), HttpStatus.CONFLICT);
        }
    }

    @Override
    @PutMapping("/code/{code}")
    @ApiOperation("Update Parameter")
    public ResponseEntity<GenericResponseDTO> update(@PathVariable String code, @RequestBody GenericRequestDTO genericRequestDTO) {
        return null;
    }
}
