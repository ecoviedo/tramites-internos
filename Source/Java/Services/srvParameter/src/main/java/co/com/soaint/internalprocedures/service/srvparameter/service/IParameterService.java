package co.com.soaint.internalprocedures.service.srvparameter.service;

import co.com.soaint.internalprocedures.canonicalmodel.dto.generic.GenericRequestDTO;
import co.com.soaint.internalprocedures.canonicalmodel.dto.generic.GenericResponseDTO;
import org.springframework.http.ResponseEntity;

public interface IParameterService {
    ResponseEntity<GenericResponseDTO> findAll();

    ResponseEntity<GenericResponseDTO> create(GenericRequestDTO genericRequestDTO);

    ResponseEntity<GenericResponseDTO> findParameterByCode(String code);

    ResponseEntity<GenericResponseDTO> findParameterByType(String type);

    ResponseEntity<GenericResponseDTO> update(String code, GenericRequestDTO genericRequestDTO);
}
