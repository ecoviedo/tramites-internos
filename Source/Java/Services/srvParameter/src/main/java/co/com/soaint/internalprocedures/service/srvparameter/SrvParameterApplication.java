package co.com.soaint.internalprocedures.service.srvparameter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@SpringBootApplication
@EnableEurekaClient
@EnableSwagger2
@EntityScan(basePackages = {"co.com.soaint.internalprocedures.canonicalmodel"})
public class SrvParameterApplication {

    public static void main(String[] args) {
        SpringApplication.run(SrvParameterApplication.class, args);
    }

    /**
     * @author Jpeña
     * Swagger Doc
     * @return Docket
     */
    @Bean
    public Docket swagger() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Parameter-Service")
                .select()
                .apis(RequestHandlerSelectors.basePackage("co.com.soaint"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(getApiInfo());
    }

    /**@author Jpeña
     * Aplication Info
     * @return ApiInfo
     */
    private ApiInfo getApiInfo() {
        return new ApiInfo(
                "Parameter Service API",
                "This API provides a all parameter of project",
                "1.0.0",
                "Rest",
                new Contact("Soaint Software S.A.S", "https://soaint.com//", "soaint@email.com"),
                "2019 - Soaint Software",
                "https://soaint.com/",
                Collections.emptyList()
        );
    }

}
