package co.com.soaint.srvradicarsolicitud.web.api.rest.radicarsolicitud.v1;
import co.com.soaint.componentescomunes.Dto.SolicitudDto;
import co.com.soaint.srvradicarsolicitud.commons.constants.api.radicarSolicitud.EndpointRadicarSolicitud;
import co.com.soaint.srvradicarsolicitud.commons.domains.response.builder.ResponseBuilder;
import co.com.soaint.srvradicarsolicitud.commons.enums.TransactionState;
import co.com.soaint.srvradicarsolicitud.service.radicarsolicitud.ISolicitud;
import co.com.soaint.srvradicarsolicitud.web.api.rest.ISolicitudApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(EndpointRadicarSolicitud.SOLICITUD_API_V1)
@CrossOrigin("*")
public class SolicitudApiImp implements ISolicitudApi {

    private final ISolicitud iSolicitud;

    @Autowired
    public SolicitudApiImp(ISolicitud iSolicitud) {
        this.iSolicitud = iSolicitud;
    }

    @GetMapping(EndpointRadicarSolicitud.FIND_SOLICITUD)
    @RequestMapping(value = EndpointRadicarSolicitud.FIND_SOLICITUD + "{numeroRadicado}")
    @Override
    public ResponseEntity findIdSolicitudByNumeroRadicado(@PathVariable("numeroRadicado") String numeroRadicado) {
        return ResponseEntity.status(HttpStatus.OK).body(iSolicitud.findIdSolicitudByNumeroRadicado(numeroRadicado));
    }

    @GetMapping(EndpointRadicarSolicitud.FIND_ID_SOLICITUD)
    @RequestMapping(value = EndpointRadicarSolicitud.FIND_ID_SOLICITUD + "{numeroRadicado}")
    @Override
    public ResponseEntity findSolicitudByNumeroRadicado(@PathVariable("numeroRadicado") String numeroRadicado) {
        try {
            return ResponseBuilder.newBuilder()
                    .withStatus(HttpStatus.OK)
                    .withResponse(iSolicitud.findSolicitudByNumeroRadicado(numeroRadicado))
                    .withTransactionState(TransactionState.OK)
                    .withMessage("Success")
                    .withPath(EndpointRadicarSolicitud.RADICAR_SOLICITUD)
                    .buildResponse();
        } catch (Exception e){
            e.printStackTrace();
            return ResponseBuilder.newBuilder()
                    .withStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                    .withTransactionState(TransactionState.FAIL)
                    .withPath(EndpointRadicarSolicitud.RADICAR_SOLICITUD)
                    .withMessage(e.getMessage())
                    .buildResponse();
        }

    }
    @PostMapping(EndpointRadicarSolicitud.RADICAR_SOLICITUD)
    public ResponseEntity radicarSolicitud(@RequestBody SolicitudDto solicitudDto) {
        try {
            iSolicitud.radicarSolicitud(solicitudDto);
            return ResponseBuilder.newBuilder()
                    .withStatus(HttpStatus.OK)
                    .withTransactionState(TransactionState.OK)
                    .withMessage("La radicacion de la Solicitud se ha realizado Con Exito")
                    .withPath(EndpointRadicarSolicitud.RADICAR_SOLICITUD)
                    .buildResponse();
        } catch (Exception e){
            e.printStackTrace();
            return ResponseBuilder.newBuilder()
                    .withStatus(HttpStatus.OK)
                    .withTransactionState(TransactionState.FAIL)
                    .withPath(EndpointRadicarSolicitud.RADICAR_SOLICITUD)
                    .withMessage(e.getMessage())
                    .buildResponse();
        }

    }
/*
    @GetMapping(EndpointRadicarSolicitud.FIND_SOLICITUD)
    @RequestMapping(value = EndpointRadicarSolicitud.FIND_SOLICITUD + "{numeroRadicado}")
    @Override
    public ResponseEntity findSolicitudByNumeroRadicado(@PathVariable("numeroRadicado") String numeroRadicado) {
        return ResponseEntity.status(HttpStatus.OK).body(iSolicitud.findSolicitudByIdSolicitud(numeroRadicado));
    }
  */
}
