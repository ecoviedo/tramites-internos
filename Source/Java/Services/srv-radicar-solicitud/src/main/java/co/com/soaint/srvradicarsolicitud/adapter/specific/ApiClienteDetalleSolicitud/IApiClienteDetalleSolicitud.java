package co.com.soaint.srvradicarsolicitud.adapter.specific.ApiClienteDetalleSolicitud;

import co.com.soaint.componentescomunes.Dto.DetalleSolicitudDto;
import co.com.soaint.srvradicarsolicitud.adapter.domain.response.detalleSolicitud.DetalleSolicitudDtoResponse;
import co.com.soaint.srvradicarsolicitud.commons.exception.webClient.WebClientException;

public interface IApiClienteDetalleSolicitud {
    DetalleSolicitudDtoResponse createDetalleSolicitud(DetalleSolicitudDto startProcess) throws WebClientException;
}
