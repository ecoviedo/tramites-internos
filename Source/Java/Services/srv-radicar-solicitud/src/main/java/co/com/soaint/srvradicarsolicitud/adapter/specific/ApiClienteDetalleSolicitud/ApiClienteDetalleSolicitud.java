package co.com.soaint.srvradicarsolicitud.adapter.specific.ApiClienteDetalleSolicitud;

import co.com.soaint.componentescomunes.Dto.DetalleSolicitudDto;
import co.com.soaint.srvradicarsolicitud.adapter.domain.response.detalleSolicitud.DetalleSolicitudDtoResponse;
import co.com.soaint.srvradicarsolicitud.commons.constants.api.radicarSolicitud.EndpointRadicarSolicitud;
import co.com.soaint.srvradicarsolicitud.commons.exception.webClient.WebClientException;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.google.gson.Gson;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;


@Component
@JsonAutoDetect(getterVisibility = JsonAutoDetect.Visibility.NON_PRIVATE)
public class ApiClienteDetalleSolicitud implements IApiClienteDetalleSolicitud {

    private RestTemplate template = new RestTemplate();

    @Override
    public DetalleSolicitudDtoResponse createDetalleSolicitud(DetalleSolicitudDto startProcess) throws WebClientException {
        try {
            String url = EndpointRadicarSolicitud.SRV_DETALLE_SOLICITUD;
            Gson gson = new Gson();
            HttpEntity<DetalleSolicitudDto> entity = new HttpEntity<>(startProcess);
            String responseService = template.postForObject(url, entity, String.class);
            DetalleSolicitudDtoResponse responseMethodt = gson.fromJson(responseService, DetalleSolicitudDtoResponse.class);
            return responseMethodt;
        } catch (RestClientException e) {
            throw new WebClientException(" Error Invocando servicio DetalleSolicitud: " + e.getMessage());
        }
    }
}