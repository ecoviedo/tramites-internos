package co.com.soaint.srvradicarsolicitud.web.api.rest;

import co.com.soaint.componentescomunes.Dto.SolicitudDto;
import co.com.soaint.componentescomunes.entidades.Solicitud;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

public interface ISolicitudApi {

    ResponseEntity radicarSolicitud(SolicitudDto solicitudDto);
    ResponseEntity findSolicitudByNumeroRadicado(String numeroRadicado);
    //ResponseEntity updateSolicitud(String numeroRadicado,SolicitudDto solicitudDto);
    ResponseEntity findIdSolicitudByNumeroRadicado(String numeroRadicado);
}
