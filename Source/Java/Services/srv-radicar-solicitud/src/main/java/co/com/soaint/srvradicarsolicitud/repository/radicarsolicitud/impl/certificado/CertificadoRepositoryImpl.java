package co.com.soaint.srvradicarsolicitud.repository.radicarsolicitud.impl.certificado;

import co.com.soaint.componentescomunes.entidades.Certificado;
import co.com.soaint.componentescomunes.entidades.Solicitud;
import co.com.soaint.srvradicarsolicitud.repository.radicarsolicitud.ICertificadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CertificadoRepositoryImpl implements  ICertificadoRepositoryFacade{
    private final ICertificadoRepository repository;
    @Autowired
    public CertificadoRepositoryImpl(ICertificadoRepository repository) {
        this.repository = repository;
    }

    @Override
    public Certificado createCertificado(Certificado certificado) {
        return repository.save(certificado);
    }

    @Override
    public Certificado findCertificadoById(Solicitud idCertificado) {
        return repository.findByNumeroRadicado(idCertificado);
    }


}
