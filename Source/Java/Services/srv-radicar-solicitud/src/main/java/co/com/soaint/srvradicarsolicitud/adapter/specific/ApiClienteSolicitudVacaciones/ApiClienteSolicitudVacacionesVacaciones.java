package co.com.soaint.srvradicarsolicitud.adapter.specific.ApiClienteSolicitudVacaciones;

import co.com.soaint.componentescomunes.Dto.DetalleSolicitudDto;
import co.com.soaint.componentescomunes.Dto.SolicitudVacacionesDto;
import co.com.soaint.srvradicarsolicitud.commons.constants.api.radicarSolicitud.EndpointRadicarSolicitud;
import co.com.soaint.srvradicarsolicitud.commons.exception.webClient.WebClientException;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.google.gson.Gson;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;


@Component
@JsonAutoDetect(getterVisibility = JsonAutoDetect.Visibility.NON_PRIVATE)
public class ApiClienteSolicitudVacacionesVacaciones implements IApiClienteSolicitudVacaciones {

    private RestTemplate template = new RestTemplate();

    @Override
    public SolicitudVacacionesDto createSolicitudVacaciones(SolicitudVacacionesDto startProcess) throws WebClientException {
        try {
            String url = EndpointRadicarSolicitud.SRV_SOLICITUD_VACACIONES;
            Gson gson = new Gson();
            HttpEntity<SolicitudVacacionesDto> entity = new HttpEntity<>(startProcess);
            String responseService = template.postForObject(url, entity, String.class);
            SolicitudVacacionesDto responseMethodt = gson.fromJson(responseService, SolicitudVacacionesDto.class);
            return responseMethodt;
        } catch (RestClientException e) {
            throw new WebClientException(" Error Invocando servicio DetalleSolicitud: " + e.getMessage());
        }
    }
}