package co.com.soaint.srvradicarsolicitud.adapter.specific.ApiClienteCertificado;

import co.com.soaint.componentescomunes.Dto.CertificadoDto;
import co.com.soaint.srvradicarsolicitud.adapter.domain.response.certificado.CertificadoDtoResponse;
import co.com.soaint.srvradicarsolicitud.commons.exception.webClient.WebClientException;

public interface IApiClienteCertificado {
    CertificadoDtoResponse createCertificado(CertificadoDto startProcess) throws WebClientException;
}
