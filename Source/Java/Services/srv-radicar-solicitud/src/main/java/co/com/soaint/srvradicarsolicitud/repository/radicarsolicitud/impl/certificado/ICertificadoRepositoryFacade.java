package co.com.soaint.srvradicarsolicitud.repository.radicarsolicitud.impl.certificado;

import co.com.soaint.componentescomunes.entidades.Certificado;
import co.com.soaint.componentescomunes.entidades.Solicitud;

public interface ICertificadoRepositoryFacade {
    Certificado createCertificado(Certificado certificado);

    Certificado findCertificadoById(Solicitud idCertificado);
}
