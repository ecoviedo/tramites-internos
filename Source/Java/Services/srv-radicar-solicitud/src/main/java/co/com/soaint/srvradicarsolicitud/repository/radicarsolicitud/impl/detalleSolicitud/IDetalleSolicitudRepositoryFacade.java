package co.com.soaint.srvradicarsolicitud.repository.radicarsolicitud.impl.detalleSolicitud;


import co.com.soaint.componentescomunes.entidades.DetalleSolicitud;
import co.com.soaint.componentescomunes.entidades.Solicitud;

public interface IDetalleSolicitudRepositoryFacade{
    DetalleSolicitud createDetalleSolicitud(DetalleSolicitud detalleSolicitud);
    DetalleSolicitud findDetalleSolciitudByNumeroRadicado(Solicitud idDetalleSolicitud);
}