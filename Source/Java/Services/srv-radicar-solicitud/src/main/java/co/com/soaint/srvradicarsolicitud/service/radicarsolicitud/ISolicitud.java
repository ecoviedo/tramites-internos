package co.com.soaint.srvradicarsolicitud.service.radicarsolicitud;


import co.com.soaint.componentescomunes.Dto.SolicitudDto;
import co.com.soaint.componentescomunes.entidades.Solicitud;
import java.util.Optional;

public interface ISolicitud {

    Optional<SolicitudDto> findSolicitudByNumeroRadicado(String numeroRadicado) throws Exception;

    Optional<Integer> findIdSolicitudByNumeroRadicado(String numeroRadicad);

    void radicarSolicitud(SolicitudDto solicitudDto) throws Exception;
}
