package co.com.soaint.srvradicarsolicitud.repository.radicarsolicitud.impl.empleado;

import co.com.soaint.componentescomunes.entidades.Empleado;
import co.com.soaint.srvradicarsolicitud.repository.radicarsolicitud.IEmpleadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EmpleadoRepositoryImpl implements IEmpleadoRepositoryFacade {
    private final IEmpleadoRepository iEmpleadoRepository;
    @Autowired
    public EmpleadoRepositoryImpl(IEmpleadoRepository iEmpleadoRepository) {
        this.iEmpleadoRepository = iEmpleadoRepository;
    }

    @Override
    public Empleado findEmpleadoByNumeroDocumento(String numeroDocumento) {
        return iEmpleadoRepository.findByNumeroDocumento(numeroDocumento);
    }
}
