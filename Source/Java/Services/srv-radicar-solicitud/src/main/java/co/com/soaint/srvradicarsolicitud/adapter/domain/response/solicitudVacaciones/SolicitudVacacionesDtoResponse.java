package co.com.soaint.srvradicarsolicitud.adapter.domain.response.solicitudVacaciones;

import co.com.soaint.componentescomunes.Dto.CertificadoDto;
import co.com.soaint.componentescomunes.Dto.SolicitudVacacionesDto;

import java.io.Serializable;

public class SolicitudVacacionesDtoResponse implements Serializable {
    public SolicitudVacacionesDto body;
    public String status;
    public String timeResponse;
    public String message;
    public String path;
    public String transactionState;

    public SolicitudVacacionesDtoResponse(SolicitudVacacionesDto body, String status, String timeResponse, String message, String path, String transactionState) {
        this.body = body;
        this.status = status;
        this.timeResponse = timeResponse;
        this.message = message;
        this.path = path;
        this.transactionState = transactionState;
    }

    public SolicitudVacacionesDto getBody() {
        return body;
    }

    public void setBody(SolicitudVacacionesDto body) {
        this.body = body;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimeResponse() {
        return timeResponse;
    }

    public void setTimeResponse(String timeResponse) {
        this.timeResponse = timeResponse;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getTransactionState() {
        return transactionState;
    }

    public void setTransactionState(String transactionState) {
        this.transactionState = transactionState;
    }
}
