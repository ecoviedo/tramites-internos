package co.com.soaint.srvradicarsolicitud.repository.radicarsolicitud;

import co.com.soaint.componentescomunes.entidades.Empleado;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IEmpleadoRepository extends JpaRepository<Empleado, String> {
    Empleado findByNumeroDocumento(String numeroDocumento);
}
