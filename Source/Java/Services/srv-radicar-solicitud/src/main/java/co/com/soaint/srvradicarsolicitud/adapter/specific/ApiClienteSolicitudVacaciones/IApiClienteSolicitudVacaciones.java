package co.com.soaint.srvradicarsolicitud.adapter.specific.ApiClienteSolicitudVacaciones;

import co.com.soaint.componentescomunes.Dto.DetalleSolicitudDto;
import co.com.soaint.componentescomunes.Dto.SolicitudVacacionesDto;
import co.com.soaint.srvradicarsolicitud.commons.exception.webClient.WebClientException;

public interface IApiClienteSolicitudVacaciones {
    SolicitudVacacionesDto createSolicitudVacaciones(SolicitudVacacionesDto startProcess) throws WebClientException;
}
