package co.com.soaint.srvradicarsolicitud.repository.radicarsolicitud;

import co.com.soaint.componentescomunes.entidades.GestionSolicitud;
import co.com.soaint.componentescomunes.entidades.Solicitud;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IGestionSolicitudRepository extends JpaRepository<GestionSolicitud, String> {

}