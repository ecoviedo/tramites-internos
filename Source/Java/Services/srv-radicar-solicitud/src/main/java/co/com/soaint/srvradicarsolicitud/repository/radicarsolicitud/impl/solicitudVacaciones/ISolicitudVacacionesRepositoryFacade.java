package co.com.soaint.srvradicarsolicitud.repository.radicarsolicitud.impl.solicitudVacaciones;
import co.com.soaint.componentescomunes.entidades.Solicitud;
import co.com.soaint.componentescomunes.entidades.SolicitudVacaciones;

public interface ISolicitudVacacionesRepositoryFacade {

    SolicitudVacaciones createSolicitudVacaciones(SolicitudVacaciones solicitudVacaciones);

    SolicitudVacaciones findSolicitudVacacionesById (Solicitud idSolicitudVacaciones);
}