package co.com.soaint.srvradicarsolicitud.repository.radicarsolicitud.impl.solicitud;


import co.com.soaint.componentescomunes.entidades.Empleado;
import co.com.soaint.componentescomunes.entidades.Solicitud;
import co.com.soaint.componentescomunes.entidades.TablaBasica;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.Optional;

public interface ISolicitudRepositoryFacade {

    Solicitud findByNumeroRadicado(String numeroRadicado);

    Integer findIdSolicitudByNumeroRadicado(String numeroRadicado);

    boolean existenceSolicitud(String numeroRadicado);

    Solicitud createSolicitud(Solicitud solicitud);

    Integer findIdTablaBasica(String codigoApp);

}

