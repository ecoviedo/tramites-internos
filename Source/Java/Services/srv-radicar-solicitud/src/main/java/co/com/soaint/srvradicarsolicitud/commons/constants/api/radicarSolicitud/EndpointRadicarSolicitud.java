package co.com.soaint.srvradicarsolicitud.commons.constants.api.radicarSolicitud;

public interface EndpointRadicarSolicitud {

    String SOLICITUD_API_V1 = "api/v1";
    String FIND_ID_SOLICITUD = "/numeroRadicado/";
    String FIND_SOLICITUD = "/";
    String RADICAR_SOLICITUD = "/solicitud";
    String UPDATE_SOLICITUD = "/solicitud/";
    String SRV_TABLAS_BASICAS_CONTROLLER = "api/v1";
    String CREATE_TABLA_BASICA = "/";
    String SRV_DETALLE_SOLICITUD_CONTROLLER = "api/v1";
    String CREATE_DETALLE_SOLICITUD = "/detallesolicitud";
    String SRV_CERTIFICADO_CONTROLLER = "api/v1";
    String CREATE_CERTIFICADO = "/updateRadicacionState/";
    String SRV_SOLICITUD_VACACIONES_CONTROLLER = "api/v1";
    String CREATE_SOLICITUD_VACACIONES = "/solicitudvacaciones/";

    String SRV_DETALLE_SOLICITUD = "http://192.168.1.59:8089/srv-detallesolicitud/"+SRV_DETALLE_SOLICITUD_CONTROLLER+CREATE_DETALLE_SOLICITUD;
    String SRV_CERTIFICADO = "http://192.168.1.59:8089/srv-certificado/"+SRV_CERTIFICADO_CONTROLLER+CREATE_CERTIFICADO;
    String SRV_SOLICITUD_VACACIONES = "http://192.168.1.59:8089/srv-solicitud-vacaciones/"+SRV_SOLICITUD_VACACIONES_CONTROLLER+CREATE_SOLICITUD_VACACIONES;
   /* String SRV_DETALLE_SOLICITUD = "http://localhost:8007/"+SRV_DETALLE_SOLICITUD_CONTROLLER+CREATE_DETALLE_SOLICITUD;
    String SRV_CERTIFICADO = "http://localhost:8005/"+SRV_CERTIFICADO_CONTROLLER+CREATE_CERTIFICADO;
    String SRV_SOLICITUD_VACACIONES = "http://localhost:8006/"+SRV_SOLICITUD_VACACIONES_CONTROLLER+CREATE_SOLICITUD_VACACIONES;
    */
}


