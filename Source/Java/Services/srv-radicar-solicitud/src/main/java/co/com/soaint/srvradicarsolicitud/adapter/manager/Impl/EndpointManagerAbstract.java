package co.com.soaint.srvradicarsolicitud.adapter.manager.Impl;

import co.com.soaint.srvradicarsolicitud.adapter.manager.IEndPointManager;
import co.com.soaint.srvradicarsolicitud.adapter.specific.util.ClientUtil;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import java.util.HashMap;

public class EndpointManagerAbstract implements IEndPointManager {


    public ResponseEntity endpointConsumerClient(final String pathEndpoint,
                                                 final Class<?> typeResponse,
                                                 final HttpMethod method,
                                                 final HashMap<String, String> headers) {

        RestTemplate clientConsumer = new RestTemplate();
        HttpHeaders httpHeadersConsumer = ClientUtil.addHeaders(headers);
        return clientConsumer.exchange(pathEndpoint, method, new HttpEntity<>(httpHeadersConsumer), typeResponse);
    }



    public ResponseEntity endpointConsumerClient(final String pathEndpoint,
                                                 final Class<?> typeResponse,
                                                 final HttpMethod method,
                                                 final Object body,
                                                 final HashMap<String, String> headers) {

        RestTemplate clientConsumer = new RestTemplate();
        HttpHeaders httpHeadersConsumer = ClientUtil.addHeaders(headers);
        return clientConsumer.exchange(pathEndpoint, method, new HttpEntity<>(body, httpHeadersConsumer), typeResponse);
    }
}
