package co.com.soaint.srvradicarsolicitud.repository.radicarsolicitud.impl.empleado;

import co.com.soaint.componentescomunes.entidades.Empleado;

public interface IEmpleadoRepositoryFacade {
    Empleado findEmpleadoByNumeroDocumento(String numeroDocumento);
}
