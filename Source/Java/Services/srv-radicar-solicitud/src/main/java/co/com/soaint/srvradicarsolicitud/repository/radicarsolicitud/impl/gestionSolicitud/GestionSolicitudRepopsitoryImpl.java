package co.com.soaint.srvradicarsolicitud.repository.radicarsolicitud.impl.gestionSolicitud;

import co.com.soaint.componentescomunes.entidades.GestionSolicitud;
import co.com.soaint.srvradicarsolicitud.repository.radicarsolicitud.IGestionSolicitudRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GestionSolicitudRepopsitoryImpl implements IGestionSolicitudRepositoryFacade {
    private final IGestionSolicitudRepository repository;
    @Autowired
    public GestionSolicitudRepopsitoryImpl(IGestionSolicitudRepository repository) {
        this.repository = repository;
    }

    @Override
    public GestionSolicitud createGestionSolicitud(GestionSolicitud gestionSolicitud) {
        return repository.save(gestionSolicitud);
    }
}
