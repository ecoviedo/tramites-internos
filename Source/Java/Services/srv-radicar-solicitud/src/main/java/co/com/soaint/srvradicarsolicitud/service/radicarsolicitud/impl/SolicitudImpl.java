package co.com.soaint.srvradicarsolicitud.service.radicarsolicitud.impl;
import co.com.soaint.srvradicarsolicitud.commons.BusinessException;
import co.com.soaint.srvradicarsolicitud.repository.radicarsolicitud.impl.certificado.ICertificadoRepositoryFacade;
import co.com.soaint.srvradicarsolicitud.repository.radicarsolicitud.impl.detalleSolicitud.IDetalleSolicitudRepositoryFacade;
import co.com.soaint.srvradicarsolicitud.repository.radicarsolicitud.impl.empleado.IEmpleadoRepositoryFacade;
import co.com.soaint.srvradicarsolicitud.repository.radicarsolicitud.impl.gestionSolicitud.IGestionSolicitudRepositoryFacade;
import co.com.soaint.srvradicarsolicitud.repository.radicarsolicitud.impl.solicitud.ISolicitudRepositoryFacade;
import co.com.soaint.srvradicarsolicitud.repository.radicarsolicitud.impl.solicitudVacaciones.ISolicitudVacacionesRepositoryFacade;
import co.com.soaint.srvradicarsolicitud.service.radicarsolicitud.ISolicitud;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

@Service
public class SolicitudImpl implements ISolicitud {
    private final IDetalleSolicitudRepositoryFacade iDetalleSolicitudRepositoryFacade;
    private final ICertificadoRepositoryFacade iCertificadoRepositoryFacade;
    private final IGestionSolicitudRepositoryFacade gestionSolicitudRepositoryFacade;
    private final ISolicitudVacacionesRepositoryFacade iSolicitudVacacionesRepositoryFacade;
    private final ISolicitudRepositoryFacade solicitudRepository;
    private final IEmpleadoRepositoryFacade iEmpleadoRepositoryFacade;
    @Autowired
    public SolicitudImpl(IDetalleSolicitudRepositoryFacade iDetalleSolicitudRepositoryFacade, ICertificadoRepositoryFacade iCertificadoRepositoryFacade, IGestionSolicitudRepositoryFacade iGestionSolicitudRepositoryFacade, ISolicitudVacacionesRepositoryFacade iSolicitudVacacionesRepositoryFacade, ISolicitudRepositoryFacade iSolicitudRepositoryFacade, IEmpleadoRepositoryFacade iEmpleadoRepositoryFacade) {
        this.iDetalleSolicitudRepositoryFacade = iDetalleSolicitudRepositoryFacade;
        this.iCertificadoRepositoryFacade = iCertificadoRepositoryFacade;
        this.gestionSolicitudRepositoryFacade = iGestionSolicitudRepositoryFacade;
        this.iSolicitudVacacionesRepositoryFacade = iSolicitudVacacionesRepositoryFacade;
        this.solicitudRepository = iSolicitudRepositoryFacade;
        this.iEmpleadoRepositoryFacade = iEmpleadoRepositoryFacade;
    }
    @Override
    public Optional<SolicitudDto> findSolicitudByNumeroRadicado(String numeroRadicado) throws Exception {
        Solicitud solicitud = solicitudRepository.findByNumeroRadicado(numeroRadicado);
        if(solicitud == null){
            throw BusinessException.notFound("Solicitud");
        }
        SolicitudDto solicitudDto = new SolicitudDto();
        solicitudDto.setIdSolicitud(solicitud.getIdSolicitud());
        solicitudDto.setEstado(solicitud.getEstado());
        solicitudDto.setNumeroRadicado(solicitud.getNumeroRadicado());
        solicitudDto.setSolicitante(mappingEmpleadoDaoToDto(solicitud.getSolicitante()));
        if(solicitud.getGestionSolicitud() != null) {
            solicitudDto.setGestionSolicitud(mappingGestionSolicitudDaoToDto(solicitud.getGestionSolicitud()));
        }
        solicitudDto.setFechaSolicitud(solicitud.getFechaSolicitud());
        solicitudDto.setTipoSolicitud(solicitud.getTipoSolicitud().getCodigoApp());
        SolicitudVacaciones solicitudVacaciones = iSolicitudVacacionesRepositoryFacade.findSolicitudVacacionesById(solicitud);
        Certificado certificado = iCertificadoRepositoryFacade.findCertificadoById(solicitud);
        if(solicitudVacaciones != null) {
            solicitudDto.setSolicitudVacacionesDto(mappingSolicitudVacacionesDaoToDto(solicitudVacaciones));
        }else if(certificado != null) {
            solicitudDto.setCertificado(mappingCertificadoDaoToDto(certificado));
        }
        DetalleSolicitud detalleSolicitud = iDetalleSolicitudRepositoryFacade.findDetalleSolciitudByNumeroRadicado(solicitud);
        solicitudDto.setDetalleSolicitudDto(mappingDetalleSolicitudDaoToDto(detalleSolicitud));
        return Optional.of(solicitudDto);
    }


    @Override
    public Optional<Integer> findIdSolicitudByNumeroRadicado(String numeroRadicad) {
        return Optional.of(solicitudRepository.findIdSolicitudByNumeroRadicado(numeroRadicad));
    }

    @Transactional
    @Override
    public void radicarSolicitud(SolicitudDto solicitudDto) throws Exception {
        if (solicitudRepository.existenceSolicitud(solicitudDto.getNumeroRadicado())) {
            throw BusinessException.alreadyExist("Solicitud");
        }
        Solicitud solicitudDao = createSolicitud(solicitudDto);
        if(solicitudDto.getTipoSolicitud().equals("@TS_CE")){
           iCertificadoRepositoryFacade.createCertificado(
                    mappingCertificadoDtoToDao(solicitudDto.getCertificado(),solicitudDao));
        }else if(solicitudDto.getTipoSolicitud().equals("@TS_VA")){
           iSolicitudVacacionesRepositoryFacade.createSolicitudVacaciones(
                    mappingSolicitudVacacionesDtoToDao(solicitudDto.getSolicitudVacacionesDto(),solicitudDao));
        }
           iDetalleSolicitudRepositoryFacade.createDetalleSolicitud(mappingDetalleSolicitudDtoToDao(
                  solicitudDto.getDetalleSolicitudDto(),solicitudDao));

    }

    private DetalleSolicitudDto mappingDetalleSolicitudDaoToDto(DetalleSolicitud detalleSolicitud) {
        DetalleSolicitudDto detalleSolicitudDto = new DetalleSolicitudDto();
        detalleSolicitudDto.setIdDetalleSolicitud(detalleSolicitud.getIdDetalleSolicitud());
        detalleSolicitudDto.setNumeroRadicado(detalleSolicitud.getNumeroRadicado().getNumeroRadicado());
        detalleSolicitudDto.setMotivoSolicitud(detalleSolicitud.getMotivoSolicitud().getCodigoApp());
        detalleSolicitudDto.setHoraInicio(detalleSolicitud.getHoraInicio());
        detalleSolicitudDto.setHoraFin(detalleSolicitud.getHoraFin());
        detalleSolicitudDto.setFechaInicio(detalleSolicitud.getFechaInicio());
        detalleSolicitudDto.setFechaFin(detalleSolicitud.getFechaFin());
        detalleSolicitudDto.setDiasSolicitados(detalleSolicitud.getDiasSolicitados());
        detalleSolicitudDto.setDiaSolicitado(detalleSolicitud.getDiaSolicitado());
        detalleSolicitudDto.setDescripcionMotivo(detalleSolicitud.getDescripcionMotivo());
        return detalleSolicitudDto;
    }

    private Solicitud createSolicitud(SolicitudDto solicitudDto){
        Solicitud solicitudDao = new Solicitud();
        TablaBasica tablaBasica = new TablaBasica();
        tablaBasica.setIdTablaBasica(solicitudRepository.findIdTablaBasica(solicitudDto.getTipoSolicitud()));
        solicitudDao.setTipoSolicitud(tablaBasica);
        solicitudDao.setNumeroRadicado(solicitudDto.getNumeroRadicado());
        Empleado empleado = iEmpleadoRepositoryFacade.findEmpleadoByNumeroDocumento(solicitudDto.getSolicitante().getNumeroDocumento());
        solicitudDao.setSolicitante(empleado);
        solicitudDao.setEstado(solicitudDto.getEstado());
        solicitudDao.setFechaSolicitud(solicitudDto.getFechaSolicitud());
        solicitudDao = solicitudRepository.createSolicitud(solicitudDao);
        return solicitudDao;
    }

    private SolicitudVacaciones mappingSolicitudVacacionesDtoToDao(SolicitudVacacionesDto solicitudVacacionesDto, Solicitud solicitud) throws Exception {
            SolicitudVacaciones solicitudVacaciones = new SolicitudVacaciones();
            solicitudVacaciones.setNumeroRadicado(solicitud);
            solicitudVacaciones.setFechaInicio(solicitudVacacionesDto.getFechaInicio());
            solicitudVacaciones.setFechaFin(solicitudVacacionesDto.getFechaFin());
            solicitudVacaciones.setDiasSolicitados(solicitudVacacionesDto.getDiasSolicitados());
            solicitudVacaciones.setDiasDisponibles(solicitudVacacionesDto.getDiasDisponibles());
            solicitudVacaciones.setDiasAnticipados(solicitudVacacionesDto.getDiasAnticipados());
            solicitudVacaciones.setFechaReintegro(solicitudVacacionesDto.getFechaReintegro());
            return solicitudVacaciones;
    }
    private SolicitudVacacionesDto mappingSolicitudVacacionesDaoToDto(SolicitudVacaciones solicitudVacaciones){
        SolicitudVacacionesDto solicitudVacacionesDto = new SolicitudVacacionesDto();
        solicitudVacacionesDto.setIdSolicitudVacaciones(solicitudVacaciones.getIdSolicitudVacaciones());
        solicitudVacacionesDto.setDiasAnticipados(solicitudVacaciones.getDiasAnticipados());
        solicitudVacacionesDto.setDiasDisponibles(solicitudVacaciones.getDiasDisponibles());
        solicitudVacacionesDto.setDiasSolicitados(solicitudVacaciones.getDiasSolicitados());
        solicitudVacacionesDto.setFechaFin(solicitudVacaciones.getFechaFin());
        solicitudVacacionesDto.setFechaInicio(solicitudVacaciones.getFechaInicio());
        solicitudVacacionesDto.setNumero_radicado(solicitudVacaciones.getNumeroRadicado().getNumeroRadicado());
        solicitudVacacionesDto.setFechaReintegro(solicitudVacaciones.getFechaReintegro());
        return  solicitudVacacionesDto;
    }
    private Certificado mappingCertificadoDtoToDao(CertificadoDto certificadoDto, Solicitud solicitud){
        Certificado certificado = new Certificado();
        TablaBasica tablaBasica = new TablaBasica();
        certificado.setNumeroRadicado(solicitud);
        certificado.setObservaciones(certificadoDto.getObservaciones());
        tablaBasica.setIdTablaBasica(
                solicitudRepository.findIdTablaBasica(certificadoDto.getTipoCertificado()));
        certificado.setTipoCertificado(tablaBasica);
        return  certificado;
    }

    private CertificadoDto mappingCertificadoDaoToDto(Certificado certificado) {
        CertificadoDto certificadoDto = new CertificadoDto();
        certificadoDto.setIdCertificado(certificado.getIdCertificado());
        certificadoDto.setNumero_radicado(certificado.getNumeroRadicado().getNumeroRadicado());
        certificadoDto.setTipoCertificado(certificado.getTipoCertificado().getCodigoApp());
        certificadoDto.setObservaciones(certificado.getObservaciones());
        return certificadoDto;
    }

    private DetalleSolicitud mappingDetalleSolicitudDtoToDao(DetalleSolicitudDto detalleSolicitudDto, Solicitud solicitud){
        DetalleSolicitud detalleSolicitud = new DetalleSolicitud();
        TablaBasica tablaBasica = new TablaBasica();
        detalleSolicitud.setNumeroRadicado(solicitud);
        detalleSolicitud.setDescripcionMotivo(detalleSolicitud.getDescripcionMotivo());
        detalleSolicitud.setDiaSolicitado(detalleSolicitudDto.getDiaSolicitado());
        detalleSolicitud.setDiasSolicitados(detalleSolicitudDto.getDiasSolicitados());
        detalleSolicitud.setFechaFin(detalleSolicitudDto.getFechaFin());
        detalleSolicitud.setFechaInicio(detalleSolicitudDto.getFechaInicio());
        detalleSolicitud.setHoraFin(detalleSolicitudDto.getHoraFin());
        detalleSolicitud.setHoraInicio(detalleSolicitudDto.getHoraInicio());
        tablaBasica.setIdTablaBasica(
                solicitudRepository.findIdTablaBasica(detalleSolicitudDto.getMotivoSolicitud()));
        detalleSolicitud.setMotivoSolicitud(tablaBasica);
        return  detalleSolicitud;
    }

    private EmpleadoDto mappingEmpleadoDaoToDto(Empleado empleado){
        EmpleadoDto empleadoDto = new EmpleadoDto();
        empleadoDto.setCargo(empleado.getCargo().getCodigoApp());
        empleadoDto.setCorreoElectronico(empleado.getCorreoElectronico());
        empleadoDto.setEstado(empleado.getEstado());
        empleadoDto.setEstadoEmpleado(empleado.getEstadoEmpleado().getCodigoApp());
        empleadoDto.setFechaCreacion(empleado.getFechaCreacion());
        empleadoDto.setFechaIngreso(empleado.getFechaIngreso());
        empleadoDto.setFechaModificacion(empleado.getFechaModificacion());
        empleadoDto.setGenero(empleado.getGenero());
        empleadoDto.setIdEmpleado(empleado.getIdEmpleado());
        empleadoDto.setImagen(empleado.getImagen());
        empleadoDto.setInformacionVacacionesDto(mappingInformacionVacacionesDaoToDto(empleado.getInformacionVacaciones()));
       if(empleado.getJefeInmediato() != null){
        empleadoDto.setJefeInmediato(mappingEmpleadoDaoToDto(empleado.getJefeInmediato()));}
        empleadoDto.setNumeroDocumento(empleado.getNumeroDocumento());
        empleadoDto.setPrimerApellido(empleado.getPrimerApellido());
        empleadoDto.setSegundoApellido(empleado.getSegundoApellido());
        empleadoDto.setPrimerNombre(empleado.getPrimerNombre());
        empleadoDto.setSegundoNombre(empleado.getSegundoNombre());
        empleadoDto.setTelefono(empleado.getTelefono());
        empleadoDto.setTipoContrato(empleado.getTipoContrato().getCodigoApp());
        empleadoDto.setTipoDocumento(empleado.getTipoDocumento().getCodigoApp());
        return empleadoDto;
    }

    private InformacionVacacionesDto mappingInformacionVacacionesDaoToDto(InformacionVacaciones informacionVacaciones){
        InformacionVacacionesDto informacionVacacionesDto = new InformacionVacacionesDto();
        informacionVacacionesDto.setIdInformacionVacaciones(informacionVacaciones.getIdInformacionVacaciones());
        informacionVacacionesDto.setDiasDisponibles(informacionVacaciones.getDiasDisponibles());
        informacionVacacionesDto.setDiasSolicitados(informacionVacaciones.getDiasSolicitados());
        informacionVacacionesDto.setDiasAnticipados(informacionVacaciones.getDiasanticipados());
        return  informacionVacacionesDto;
    }

    private GestionSolicitudDto mappingGestionSolicitudDaoToDto(GestionSolicitud gestionSolicitud){
        GestionSolicitudDto gestionSolicitudDto = new GestionSolicitudDto();
        gestionSolicitudDto.setIdGestionSolicitud(gestionSolicitud.getIdGestionSolicitud());
        if(gestionSolicitud.getAprobGerente() != null){
            gestionSolicitudDto.setAprobGerente(
                    mappingGestionEntesSuperioresDaoDto(gestionSolicitud.getAprobGerente()));
        }
        if(gestionSolicitud.getAprobJefe() != null){
            gestionSolicitudDto.setAprobJefe(
                    mappingGestionEntesSuperioresDaoDto(gestionSolicitud.getAprobJefe()));
        }
        if(gestionSolicitud.getClasificaciones() != null){
            gestionSolicitudDto.setClasificacion(gestionSolicitud.getClasificaciones().getCodigoApp());

        }
        if(gestionSolicitud.getObservaciones() != null){
            gestionSolicitudDto.setObservaciones(gestionSolicitud.getObservaciones());
        }
        if(gestionSolicitud.getAprobRrhh() != null){
            gestionSolicitudDto.setAprobRrhh(gestionSolicitud.getAprobRrhh());
        }
        if(gestionSolicitud.getFechaAprobRrhh() != null){
            gestionSolicitudDto.setFechaAprobRrhh(gestionSolicitud.getFechaAprobRrhh());
        }
        return gestionSolicitudDto;
    }

    private GestionTramiteEntesSuperioresDto mappingGestionEntesSuperioresDaoDto(GestionTramiteEnteSuperior gestionTramiteEnteSuperior){
        GestionTramiteEntesSuperioresDto gestionEntesSuperioresDto = new GestionTramiteEntesSuperioresDto();
        gestionEntesSuperioresDto.setIdGestionTramite(gestionTramiteEnteSuperior.getIdGestionTramite());
        gestionEntesSuperioresDto.setEmpleadoReemplazo(gestionTramiteEnteSuperior.getEmpleadoReemplazo().getNumeroDocumento());
        gestionEntesSuperioresDto.setFechaAprobacion(gestionTramiteEnteSuperior.getFechaAprobacion());
        gestionEntesSuperioresDto.setReemplazo(gestionTramiteEnteSuperior.getReemplazo());
        gestionEntesSuperioresDto.setRemunerado(gestionTramiteEnteSuperior.getRemunerado());
        gestionEntesSuperioresDto.setVistoBueno(gestionTramiteEnteSuperior.getVistoBueno().getCodigoApp());
        gestionEntesSuperioresDto.setObservaciones(gestionTramiteEnteSuperior.getObservaciones());
        return gestionEntesSuperioresDto;
    }

}
