package co.com.soaint.srvradicarsolicitud.repository.radicarsolicitud.impl.solicitud;


import co.com.soaint.componentescomunes.entidades.Empleado;
import co.com.soaint.componentescomunes.entidades.Solicitud;

import co.com.soaint.srvradicarsolicitud.repository.radicarsolicitud.ISolicitudRepository;
import co.com.soaint.srvradicarsolicitud.repository.radicarsolicitud.impl.solicitud.ISolicitudRepositoryFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class SolicitudRepositoryImpl implements ISolicitudRepositoryFacade {

    private final ISolicitudRepository repository;

    @Autowired
    public SolicitudRepositoryImpl(ISolicitudRepository repository) {
        this.repository = repository;
    }

    @Override
    public Solicitud findByNumeroRadicado(String numeroSolicitud) {
        return repository.findByNumeroRadicado(numeroSolicitud);
    }

    @Override
    public boolean existenceSolicitud(String numeroRadicado) {
        return repository.existenceSolicitud(numeroRadicado);
    }

    @Override
    public Solicitud createSolicitud(Solicitud solicitud) {
        return repository.saveAndFlush(solicitud);
    }

    @Override
    public Integer findIdSolicitudByNumeroRadicado(String numeroRadicado) {
        return repository.findIdSolicitudByNumeroRadicado(numeroRadicado);
    }

    @Override
    public Integer findIdTablaBasica(String codigoApp) {
        Integer idTablaBasica =repository.findIdTablaBasicaByNumeroRadicado(codigoApp);
        return idTablaBasica;
    }
}
