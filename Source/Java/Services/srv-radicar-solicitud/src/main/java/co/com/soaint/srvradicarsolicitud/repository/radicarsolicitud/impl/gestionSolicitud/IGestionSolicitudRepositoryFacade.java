package co.com.soaint.srvradicarsolicitud.repository.radicarsolicitud.impl.gestionSolicitud;

import co.com.soaint.componentescomunes.entidades.GestionSolicitud;

public interface IGestionSolicitudRepositoryFacade {
    GestionSolicitud createGestionSolicitud(GestionSolicitud gestionSolicitud);
}
