package co.com.soaint.srvradicarsolicitud.repository.radicarsolicitud.impl.solicitudVacaciones;
import co.com.soaint.componentescomunes.entidades.Solicitud;
import co.com.soaint.componentescomunes.entidades.SolicitudVacaciones;
import co.com.soaint.srvradicarsolicitud.repository.radicarsolicitud.ISolicituVacacionesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SolicitudVacacinesRepositoryImpl implements ISolicitudVacacionesRepositoryFacade {
    private final ISolicituVacacionesRepository repository;

    @Autowired
    public SolicitudVacacinesRepositoryImpl(ISolicituVacacionesRepository repository) {
        this.repository = repository;
    }

    @Override
    public SolicitudVacaciones createSolicitudVacaciones(SolicitudVacaciones solicitudVacaciones) {
        return repository.save(solicitudVacaciones);
    }

    @Override
    public SolicitudVacaciones findSolicitudVacacionesById(Solicitud idSolicitudVacaciones) {
        return repository.findByNumeroRadicado(idSolicitudVacaciones);
    }
}
