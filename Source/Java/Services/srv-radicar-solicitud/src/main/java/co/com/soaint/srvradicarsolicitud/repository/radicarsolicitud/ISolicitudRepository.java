package co.com.soaint.srvradicarsolicitud.repository.radicarsolicitud;

import co.com.soaint.componentescomunes.entidades.Empleado;
import co.com.soaint.componentescomunes.entidades.Solicitud;
import co.com.soaint.componentescomunes.entidades.TablaBasica;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ISolicitudRepository extends JpaRepository<Solicitud, String> {

    Solicitud findByNumeroRadicado(String numeroRadicado);

    @Query("select (solicitud.idSolicitud) from Solicitud solicitud where (solicitud.numeroRadicado) like lower(:numeroRadicado)")
    Integer findIdSolicitudByNumeroRadicado(@Param("numeroRadicado") String numeroRadicado);

    @Query("select case when count(numero_radicado)> 0 then true else false end from Solicitud solicitud where lower(solicitud.numeroRadicado) like lower(:numeroRadicado)")
    boolean existenceSolicitud(@Param("numeroRadicado") String numeroRadicado);

    @Query("select (tablabasica.idTablaBasica) from TablaBasica tablabasica where (tablabasica.codigoApp) like (:codigoApp)")
    Integer findIdTablaBasicaByNumeroRadicado(@Param("codigoApp") String codigoApp);


}


