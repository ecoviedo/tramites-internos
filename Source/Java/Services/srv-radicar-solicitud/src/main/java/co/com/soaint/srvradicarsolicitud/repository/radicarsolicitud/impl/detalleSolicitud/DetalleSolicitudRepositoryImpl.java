package co.com.soaint.srvradicarsolicitud.repository.radicarsolicitud.impl.detalleSolicitud;

import co.com.soaint.componentescomunes.entidades.DetalleSolicitud;
import co.com.soaint.componentescomunes.entidades.Solicitud;
import co.com.soaint.srvradicarsolicitud.repository.radicarsolicitud.IDetalleSolicitudRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DetalleSolicitudRepositoryImpl implements IDetalleSolicitudRepositoryFacade{
    private final IDetalleSolicitudRepository repository;
    @Autowired
    public DetalleSolicitudRepositoryImpl(IDetalleSolicitudRepository repository) {
        this.repository = repository;
    }

    @Override
    public DetalleSolicitud createDetalleSolicitud(DetalleSolicitud detalleSolicitud) {
        return repository.save(detalleSolicitud);
    }

    @Override
    public DetalleSolicitud findDetalleSolciitudByNumeroRadicado(Solicitud idDetalleSolicitud) {
        return repository.findByNumeroRadicado(idDetalleSolicitud);
    }
}
