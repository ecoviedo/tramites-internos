package co.com.soaint.srvradicarsolicitud.adapter.specific.ApiClienteCertificado;

import co.com.soaint.componentescomunes.Dto.CertificadoDto;
import co.com.soaint.srvradicarsolicitud.adapter.domain.response.certificado.CertificadoDtoResponse;
import co.com.soaint.srvradicarsolicitud.commons.constants.api.radicarSolicitud.EndpointRadicarSolicitud;
import co.com.soaint.srvradicarsolicitud.commons.exception.webClient.WebClientException;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.google.gson.Gson;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;


@Component
@JsonAutoDetect(getterVisibility = JsonAutoDetect.Visibility.NON_PRIVATE)
public class ApiClienteCertificado implements IApiClienteCertificado {

    private RestTemplate template = new RestTemplate();

    @Override
    public CertificadoDtoResponse createCertificado(CertificadoDto startProcess) throws WebClientException {
        try {

            String url = EndpointRadicarSolicitud.SRV_CERTIFICADO;


            Gson gson = new Gson();
            HttpEntity<CertificadoDto> entity = new HttpEntity<>(startProcess);
            String responseService = template.postForObject(url, entity, String.class);
            CertificadoDtoResponse responseMethodt = gson.fromJson(responseService, CertificadoDtoResponse.class);
            return responseMethodt;

        } catch (RestClientException e) {

            throw new WebClientException(" Error Invocando servicio certificado: " + e.getMessage());
        }
    }
}