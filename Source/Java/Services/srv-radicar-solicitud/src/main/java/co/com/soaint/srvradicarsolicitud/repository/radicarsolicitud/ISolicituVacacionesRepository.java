package co.com.soaint.srvradicarsolicitud.repository.radicarsolicitud;
import co.com.soaint.componentescomunes.entidades.Solicitud;
import co.com.soaint.componentescomunes.entidades.SolicitudVacaciones;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ISolicituVacacionesRepository  extends JpaRepository<SolicitudVacaciones, Solicitud> {
    SolicitudVacaciones findByNumeroRadicado (Solicitud idSolicitudVacaciones);
}