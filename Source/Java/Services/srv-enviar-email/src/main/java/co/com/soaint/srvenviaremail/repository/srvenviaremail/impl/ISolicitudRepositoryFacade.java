package co.com.soaint.srvenviaremail.repository.srvenviaremail.impl;

import co.com.soaint.componentescomunes.entidades.Solicitud;

public interface ISolicitudRepositoryFacade {
    public Solicitud findByNumeroRadicado(String numeroRadicado);


}
