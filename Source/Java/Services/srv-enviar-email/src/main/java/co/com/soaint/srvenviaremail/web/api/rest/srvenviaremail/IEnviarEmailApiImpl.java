package co.com.soaint.srvenviaremail.web.api.rest.srvenviaremail;

import co.com.soaint.srvenviaremail.commons.dto.EmailDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

public interface IEnviarEmailApiImpl {


    @GetMapping("/")
    @RequestMapping(value = "/"+"numeroRadicado{numeroRadicado}"+"/estado{estado}")
    ResponseEntity<?> enviarEmail(@PathVariable("numeroRadicado") String numeroRadicado, @PathVariable("estado") boolean estado);
}
