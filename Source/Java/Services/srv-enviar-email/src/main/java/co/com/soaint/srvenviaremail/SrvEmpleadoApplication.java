package co.com.soaint.srvenviaremail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EntityScan(basePackages = {"co.com.soaint.componentescomunes.entidades"})
@EnableEurekaClient
@SpringBootApplication
public class SrvEmpleadoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SrvEmpleadoApplication.class, args);
    }

}
