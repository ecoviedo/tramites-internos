package co.com.soaint.srvenviaremail.repository.srvenviaremail.impl;

import co.com.soaint.componentescomunes.entidades.Solicitud;
import co.com.soaint.srvenviaremail.repository.srvenviaremail.ISolicituRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ISolicitudRepositoryImpl implements  ISolicitudRepositoryFacade{
    private final ISolicituRepository repository;
    @Autowired
    public ISolicitudRepositoryImpl(ISolicituRepository repository) {
        this.repository = repository;
    }

    @Override
    public Solicitud findByNumeroRadicado(String numeroRadicado) {
        return repository.findByNumeroRadicado(numeroRadicado);
    }

}
