package co.com.soaint.srvenviaremail.repository.srvenviaremail;

import co.com.soaint.componentescomunes.entidades.Solicitud;
import co.com.soaint.componentescomunes.entidades.SolicitudVacaciones;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ISolicitudVacacionesRepository extends JpaRepository<SolicitudVacaciones, Solicitud> {
    public SolicitudVacaciones findByNumeroRadicado(Solicitud solicitud);
}
