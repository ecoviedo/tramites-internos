package co.com.soaint.srvenviaremail.commons.dto;

import java.io.Serializable;
import java.util.Date;

public class EmailDto implements Serializable {
    public String emailTarget;
    public String estadoSolicitud;
    public String nombreEmpleado;
    public Date fechaSolicitud;
    public String numeroRadicado;

    public EmailDto(String emailTarget, String estadoSolicitud, String nombreEmpleado, Date fechaSolicitud, String numeroRadicado) {
        this.emailTarget = emailTarget;
        this.estadoSolicitud = estadoSolicitud;
        this.nombreEmpleado = nombreEmpleado;
        this.fechaSolicitud = fechaSolicitud;
        this.numeroRadicado = numeroRadicado;
    }

    public String getEmailTarget() {
        return emailTarget;
    }

    public void setEmailTarget(String emailTarget) {
        this.emailTarget = emailTarget;
    }

    public String getEstadoSolicitud() {
        return estadoSolicitud;
    }

    public void setEstadoSolicitud(String estadoSolicitud) {
        this.estadoSolicitud = estadoSolicitud;
    }

    public String getNombreEmpleado() {
        return nombreEmpleado;
    }

    public void setNombreEmpleado(String nombreEmpleado) {
        this.nombreEmpleado = nombreEmpleado;
    }

    public Date getFechaSolicitud() {
        return fechaSolicitud;
    }

    public void setFechaSolicitud(Date fechaSolicitud) {
        this.fechaSolicitud = fechaSolicitud;
    }

    public String getNumeroRadicado() {
        return numeroRadicado;
    }

    public void setNumeroRadicado(String numeroRadicado) {
        this.numeroRadicado = numeroRadicado;
    }
}
