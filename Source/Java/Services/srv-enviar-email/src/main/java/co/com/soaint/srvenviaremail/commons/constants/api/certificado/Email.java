package co.com.soaint.srvenviaremail.commons.constants.api.certificado;

public interface Email {
    String SOLICITUD_APROBADA_SUBJECT ="Solicitud Aprobada Numero Radicado: ";
    String SOLICITUD_RECHAZADA_SUBJECT ="Solicitud Rechazada Numero Radicado: ";
    String SEND_SUCCESS ="Envío Exitoso";
    String SOLICITUD_APROBADA ="<br><br><br><br><br>\n" +
            "<div style=\"text-align: center; background-color: #ededed\">\n" +
            "  <br>\n" +
            "  <img src=\"http://soaint.com/wp-content/uploads/2011/09/logo-soaint_b.png\" alt=\"\" style=\"margin-bottom: 5%\">\n" +
            "\n" +
            "  <div style=\"text-align: center;\n" +
            "              margin-left: 10%;\n" +
            "              margin-right: 10%;\n" +
            "              border: #c1bfea 1px solid;\n" +
            "              border-radius: 10px;\n" +
            "              background-color: #f2f2f2\">\n" +
            "    <br>\n" +
            "    <br>\n" +
            "    <br>\n" +
            "    <strong style=\"font-size: 270%\">Hola, NOMBRE_EMPLEADO¡</strong>\n" +
            "    <hr>\n" +
            "    <br>\n" +
            "    <h6 style=\"margin-right: 4%; font-size: 180%; margin-left: 4%\">\n" +
            "      <div>Tu Solicitud Realizada el Día FECHA_SOLICITUD con Numero de Radicado NUMERO_RADICADO fue:\n" +
            "      </div>\n" +
            "    </h6>\n" +
            "    <h6 style=\"margin-right: 4%;font-size: 250%; margin-left: 4%\">\n" +
            "      <div>ESTADO_SOLICITUD\n" +
            "      </div>\n" +
            "    </h6>\n" +
            "    <br>\n" +
            "  </div>\n" +
            "  <br>\n" +
            "  <img src=\"http://soaint.com/wp-content/uploads/2011/09/logo-soaint_b.png\" alt=\"\" style=\"margin-bottom: 5%; margin-top: 5%\">\n" +
            "\n" +
            "  <br><br><br>\n" +
            "\n" +
            "</div>\n" +
            "<br><br><br><br><br>";

}
