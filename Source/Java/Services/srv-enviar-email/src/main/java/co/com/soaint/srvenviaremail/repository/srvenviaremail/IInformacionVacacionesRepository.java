package co.com.soaint.srvenviaremail.repository.srvenviaremail;

import co.com.soaint.componentescomunes.entidades.InformacionVacaciones;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IInformacionVacacionesRepository extends JpaRepository<InformacionVacaciones, Integer> {
    public InformacionVacaciones findByIdInformacionVacaciones (Integer idInformacionVacaciones);
}
