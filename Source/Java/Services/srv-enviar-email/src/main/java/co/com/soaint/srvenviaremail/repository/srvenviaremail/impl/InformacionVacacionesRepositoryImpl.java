package co.com.soaint.srvenviaremail.repository.srvenviaremail.impl;

import co.com.soaint.componentescomunes.entidades.InformacionVacaciones;
import co.com.soaint.srvenviaremail.repository.srvenviaremail.IInformacionVacacionesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class InformacionVacacionesRepositoryImpl implements IInformacionVacacionesRepositoryFacade {
    private final IInformacionVacacionesRepository repository;
    @Autowired
    public InformacionVacacionesRepositoryImpl(IInformacionVacacionesRepository repository) {
        this.repository = repository;
    }

    @Override
    public InformacionVacaciones findByIdInformacionVacaciones(Integer idInformacionVacaciones) {
        return repository.findByIdInformacionVacaciones(idInformacionVacaciones);
    }

    @Override
    public void updateInformacionVacaciones(InformacionVacaciones informacionVacaciones) {
        repository.save(informacionVacaciones);
    }
}
