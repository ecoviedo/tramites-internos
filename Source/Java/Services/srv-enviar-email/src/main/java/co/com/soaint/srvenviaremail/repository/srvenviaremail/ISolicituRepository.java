package co.com.soaint.srvenviaremail.repository.srvenviaremail;

import co.com.soaint.componentescomunes.entidades.Solicitud;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ISolicituRepository  extends JpaRepository<Solicitud, String> {
    public Solicitud findByNumeroRadicado(String numeroRadicado);

}
