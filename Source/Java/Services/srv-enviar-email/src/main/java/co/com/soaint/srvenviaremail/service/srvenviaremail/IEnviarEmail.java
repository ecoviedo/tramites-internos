package co.com.soaint.srvenviaremail.service.srvenviaremail;

import co.com.soaint.componentescomunes.entidades.Solicitud;
import co.com.soaint.srvenviaremail.commons.dto.EmailDto;

public interface IEnviarEmail {
    void enviarEmail(String numeroRadicado, boolean aprobado) throws Exception;

    String replaceParamsBodyEmail(Solicitud solicitud, boolean aprobado);
}
