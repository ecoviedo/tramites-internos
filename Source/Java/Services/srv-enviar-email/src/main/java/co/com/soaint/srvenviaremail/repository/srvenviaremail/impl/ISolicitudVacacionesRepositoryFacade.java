package co.com.soaint.srvenviaremail.repository.srvenviaremail.impl;

import co.com.soaint.componentescomunes.entidades.Solicitud;
import co.com.soaint.componentescomunes.entidades.SolicitudVacaciones;

public interface ISolicitudVacacionesRepositoryFacade {
    public SolicitudVacaciones findByNumeroRadicado(Solicitud solicitud );


}
