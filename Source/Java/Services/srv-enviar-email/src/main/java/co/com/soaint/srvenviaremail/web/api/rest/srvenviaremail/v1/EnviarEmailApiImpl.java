package co.com.soaint.srvenviaremail.web.api.rest.srvenviaremail.v1;

import co.com.soaint.srvenviaremail.commons.constants.api.certificado.Email;
import co.com.soaint.srvenviaremail.commons.constants.api.certificado.EndpointEnviarEmail;
import co.com.soaint.srvenviaremail.commons.domains.response.builder.ResponseBuilder;
import co.com.soaint.srvenviaremail.commons.dto.EmailDto;
import co.com.soaint.srvenviaremail.service.srvenviaremail.IEnviarEmail;
import co.com.soaint.srvenviaremail.web.api.rest.srvenviaremail.IEnviarEmailApiImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value= EndpointEnviarEmail.ENVIAR_EMAIL_API_V1)
@CrossOrigin("*")
public class EnviarEmailApiImpl implements IEnviarEmailApiImpl {
    private final IEnviarEmail iEnviarEmail;
    @Autowired
    public EnviarEmailApiImpl(IEnviarEmail iEnviarEmail) {
        this.iEnviarEmail = iEnviarEmail;
    }

    @GetMapping("/")
    @RequestMapping(value = "/"+"numeroRadicado/{numeroRadicado}"+"/estado/{estado}")
    @Override
    public ResponseEntity<?> enviarEmail(@PathVariable("numeroRadicado") String numeroRadicado, @PathVariable("estado") boolean estado) {
        try{
            iEnviarEmail.enviarEmail(numeroRadicado,estado);
            return ResponseBuilder.newBuilder()
                    .withStatus(HttpStatus.OK)
                    .withMessage(Email.SEND_SUCCESS)
                    .withPath(EndpointEnviarEmail.ENVIAR_EMAIL_API_V1)
                    .buildResponse();
        }catch(Exception ex){
            return ResponseBuilder.newBuilder()
                    .withStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                    .withMessage(ex.getMessage())
                    .buildResponse();
        }

    }
}
