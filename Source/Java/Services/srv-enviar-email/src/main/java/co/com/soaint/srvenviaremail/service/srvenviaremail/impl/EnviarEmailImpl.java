package co.com.soaint.srvenviaremail.service.srvenviaremail.impl;

import co.com.soaint.componentescomunes.entidades.InformacionVacaciones;
import co.com.soaint.componentescomunes.entidades.Solicitud;
import co.com.soaint.componentescomunes.entidades.SolicitudVacaciones;
import co.com.soaint.srvenviaremail.commons.constants.api.certificado.Email;
import co.com.soaint.srvenviaremail.repository.srvenviaremail.impl.IInformacionVacacionesRepositoryFacade;
import co.com.soaint.srvenviaremail.repository.srvenviaremail.impl.ISolicitudRepositoryFacade;
import co.com.soaint.srvenviaremail.repository.srvenviaremail.impl.ISolicitudVacacionesRepositoryFacade;
import co.com.soaint.srvenviaremail.service.srvenviaremail.IEnviarEmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;

@Service
public class EnviarEmailImpl implements IEnviarEmail {
    private final ISolicitudRepositoryFacade iSolicitudRepositoryFacade;
    private final IInformacionVacacionesRepositoryFacade iInformacionVacacionesFacade;
    private final ISolicitudVacacionesRepositoryFacade iSolicitudVacacionesRepositoryFacade;
    @Autowired
    private JavaMailSender javaMailSender;
    @Autowired
    public EnviarEmailImpl(ISolicitudRepositoryFacade iSolicitudRepositoryFacade, IInformacionVacacionesRepositoryFacade iInformacionVacacionesFacade, ISolicitudVacacionesRepositoryFacade iSolicitudVacacionesRepositoryFacade) {
        this.iSolicitudRepositoryFacade = iSolicitudRepositoryFacade;
        this.iInformacionVacacionesFacade = iInformacionVacacionesFacade;
        this.iSolicitudVacacionesRepositoryFacade = iSolicitudVacacionesRepositoryFacade;
    }


    @Override
    public void enviarEmail(String numeroRadicado, boolean estado) throws Exception {
        Solicitud solicitud = iSolicitudRepositoryFacade.findByNumeroRadicado(numeroRadicado);
        if(solicitud.getTipoSolicitud().getCodigoApp().equals("@TS_VA") && estado){
            aplicarInformacionVacaciones(solicitud);
        }
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);
        helper.setTo(solicitud.getSolicitante().getCorreoElectronico());
        helper.setText(replaceParamsBodyEmail(solicitud,estado), true);
        if(estado) {
            helper.setSubject(Email.SOLICITUD_APROBADA_SUBJECT + solicitud.getNumeroRadicado());
        } else {
            helper.setSubject(Email.SOLICITUD_RECHAZADA_SUBJECT + solicitud.getNumeroRadicado());
        }
        javaMailSender.send(message);

    }

    public void aplicarInformacionVacaciones(Solicitud solicitud){
        SolicitudVacaciones solicitudVacaciones = iSolicitudVacacionesRepositoryFacade.findByNumeroRadicado(solicitud);
        InformacionVacaciones informacionVacaciones = iInformacionVacacionesFacade.findByIdInformacionVacaciones(
                solicitud.getSolicitante().getInformacionVacaciones().getIdInformacionVacaciones());
        informacionVacaciones.setDiasSolicitados(solicitudVacaciones.getDiasSolicitados());
        informacionVacaciones.setDiasDisponibles(solicitudVacaciones.getDiasDisponibles());
        informacionVacaciones.setDiasanticipados(solicitudVacaciones.getDiasAnticipados());
        iInformacionVacacionesFacade.updateInformacionVacaciones(informacionVacaciones);


    }

    @Override
    public String replaceParamsBodyEmail(Solicitud solicitud, boolean estado) {

        String body = Email.SOLICITUD_APROBADA;
        body = body.replace("NOMBRE_EMPLEADO",solicitud.getSolicitante().getPrimerNombre()+solicitud.getSolicitante().getPrimerApellido());
        body =body.replace("FECHA_SOLICITUD", solicitud.getFechaSolicitud().toString());
        body =body.replace("NUMERO_RADICADO", solicitud.getNumeroRadicado());
        if(estado){
            body = body.replace("ESTADO_SOLICITUD","APROBADA");
        } else {
            body = body.replace("ESTADO_SOLICITUD","RECHAZADA");
        }
        return body;
    }

}
