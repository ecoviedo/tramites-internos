package co.com.soaint.srvenviaremail.repository.srvenviaremail.impl;

import co.com.soaint.componentescomunes.entidades.InformacionVacaciones;

public interface IInformacionVacacionesRepositoryFacade {
    public InformacionVacaciones findByIdInformacionVacaciones (Integer idInformacionVacaciones);

    public void updateInformacionVacaciones(InformacionVacaciones informacionVacaciones);
}
