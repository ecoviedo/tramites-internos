package co.com.soaint.srvenviaremail.repository.srvenviaremail.impl;

import co.com.soaint.componentescomunes.entidades.Solicitud;
import co.com.soaint.componentescomunes.entidades.SolicitudVacaciones;
import co.com.soaint.srvenviaremail.repository.srvenviaremail.ISolicituRepository;
import co.com.soaint.srvenviaremail.repository.srvenviaremail.ISolicitudVacacionesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ISolicitudVacacionesVacacionesRepositoryImpl implements ISolicitudVacacionesRepositoryFacade {
    private final ISolicitudVacacionesRepository repository;
    @Autowired
    public ISolicitudVacacionesVacacionesRepositoryImpl(ISolicitudVacacionesRepository repository) {
        this.repository = repository;
    }


    @Override
    public SolicitudVacaciones findByNumeroRadicado(Solicitud idSolicitudVacaciones) {
        return repository.findByNumeroRadicado(idSolicitudVacaciones);
    }
}
