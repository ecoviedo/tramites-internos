package co.com.soaint.srvaprobgerente.web.api.rest.srvaprobgerente.v1;
import co.com.soaint.srvaprobgerente.commons.Dto.AprobacionGerenteDto;
import co.com.soaint.srvaprobgerente.commons.constants.api.radicarSolicitud.EndpointAprobJefeSolicitud;
import co.com.soaint.srvaprobgerente.commons.domains.response.builder.ResponseBuilder;
import co.com.soaint.srvaprobgerente.commons.enums.TransactionState;
import co.com.soaint.srvaprobgerente.service.srvaprobgerente.IAprobGerente;
import co.com.soaint.srvaprobgerente.web.api.rest.IAprobJefeInmediatoApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(EndpointAprobJefeSolicitud.APROB_GERENTE_API_V1)
@CrossOrigin("*")
public class AprobJefeInmediatoApiImp implements IAprobJefeInmediatoApi {

    private final IAprobGerente iAprobGerente;

    @Autowired
    public AprobJefeInmediatoApiImp(IAprobGerente iAprobGerente) {
        this.iAprobGerente = iAprobGerente;
    }

    @GetMapping("/")
    @RequestMapping(value = EndpointAprobJefeSolicitud.FIND_APROB_GERENTE + "{numeroRadicado}")
    @Override
    public ResponseEntity findSolicitudByNumeroRadicado(@PathVariable("numeroRadicado") String numeroRadicado) {
        try {
            return ResponseBuilder.newBuilder()
                    .withStatus(HttpStatus.OK)
                    .withTransactionState(TransactionState.OK)
                    .withResponse( iAprobGerente.findAprobGerente(numeroRadicado))
                    .withMessage("La Consulta Aprobacion gerente se ha realizado Con Exito")
                    .withPath(EndpointAprobJefeSolicitud.SET_APROB_GERENTE)
                    .buildResponse();
        } catch (Exception e){
            e.printStackTrace();
            return ResponseBuilder.newBuilder()
                    .withStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                    .withTransactionState(TransactionState.FAIL)
                    .withPath(EndpointAprobJefeSolicitud.SET_APROB_GERENTE)
                    .withMessage(e.getMessage())
                    .buildResponse();
        } }

    @PostMapping(EndpointAprobJefeSolicitud.SET_APROB_GERENTE)
    @Override
    public ResponseEntity setAprobJefeInmediato(@RequestBody AprobacionGerenteDto aprobJefeInmediato) {
        try {
            iAprobGerente.setAprobGerente(aprobJefeInmediato);
            return ResponseBuilder.newBuilder()
                    .withStatus(HttpStatus.OK)
                    .withTransactionState(TransactionState.OK)
                    .withMessage("La Aprobacion del Gerente se ha realizado Con Exito")
                    .withPath(EndpointAprobJefeSolicitud.SET_APROB_GERENTE)
                    .buildResponse();
        } catch (Exception e){
            e.printStackTrace();
            return ResponseBuilder.newBuilder()
                    .withStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                    .withTransactionState(TransactionState.FAIL)
                    .withPath(EndpointAprobJefeSolicitud.SET_APROB_GERENTE)
                    .withMessage(e.getMessage())
                    .buildResponse();
        }
    }

}
