package co.com.soaint.srvaprobgerente.service.srvaprobgerente;


import co.com.soaint.srvaprobgerente.commons.Dto.AprobacionGerenteDto;

public interface IAprobGerente {

    void setAprobGerente(AprobacionGerenteDto aprobJefeInmediato) throws Exception;

    AprobacionGerenteDto findAprobGerente(String numeroRadicado) throws Exception;

}
