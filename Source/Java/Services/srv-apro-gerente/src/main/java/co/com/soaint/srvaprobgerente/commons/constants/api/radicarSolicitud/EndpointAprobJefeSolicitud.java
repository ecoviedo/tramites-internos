package co.com.soaint.srvaprobgerente.commons.constants.api.radicarSolicitud;

public interface EndpointAprobJefeSolicitud {

    String APROB_GERENTE_API_V1 = "api/v1";
    String SET_APROB_GERENTE = "/aprobgerente/";
    String FIND_APROB_GERENTE = "/aprobgerente/";
}


