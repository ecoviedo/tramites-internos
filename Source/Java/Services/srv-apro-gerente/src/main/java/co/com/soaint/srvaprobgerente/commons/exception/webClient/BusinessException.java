package co.com.soaint.srvaprobgerente.commons.exception.webClient;

public class BusinessException extends Exception {

    public static Exception alreadyExistManagement(String enteSuperior){
     return new Exception("El "+enteSuperior+" ya realizo la gestion Correspondiente ");
    }
    public static Exception notFound(String tupla){
        return new Exception("El registro no existe en la tabla "+tupla);
    }
}
