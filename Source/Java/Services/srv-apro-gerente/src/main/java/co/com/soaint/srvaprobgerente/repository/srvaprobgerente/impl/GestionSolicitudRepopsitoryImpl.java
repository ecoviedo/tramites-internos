package co.com.soaint.srvaprobgerente.repository.srvaprobgerente.impl;

import co.com.soaint.componentescomunes.entidades.GestionSolicitud;
import co.com.soaint.srvaprobgerente.repository.srvaprobgerente.IGestionSolicitudRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GestionSolicitudRepopsitoryImpl implements IGestionSolicitudRepositoryFacade {
    private final IGestionSolicitudRepository repository;

    @Autowired
    public GestionSolicitudRepopsitoryImpl(IGestionSolicitudRepository repository) {
        this.repository = repository;
    }


    @Override
    public GestionSolicitud setAprobJefeInmediato(GestionSolicitud gestionSolicitud) {
        return repository.save(gestionSolicitud);
    }

    @Override
    public GestionSolicitud findAprobJefeInmediato(Integer numeroSolicitud) {
        return repository.findByIdGestionSolicitud(numeroSolicitud);
    }

    @Override
    public Integer findIdTablaBasicaByCodigoApp(String codigoApp) {
        return repository.findIdTablaBasicaByNumeroRadicado(codigoApp);
    }

}
