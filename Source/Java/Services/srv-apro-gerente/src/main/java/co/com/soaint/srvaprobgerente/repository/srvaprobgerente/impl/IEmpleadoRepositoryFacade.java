package co.com.soaint.srvaprobgerente.repository.srvaprobgerente.impl;

import co.com.soaint.componentescomunes.entidades.Empleado;

public interface IEmpleadoRepositoryFacade {
    Empleado findEmpleadoByNumeroDocumento(String numeroDocumento);
}
