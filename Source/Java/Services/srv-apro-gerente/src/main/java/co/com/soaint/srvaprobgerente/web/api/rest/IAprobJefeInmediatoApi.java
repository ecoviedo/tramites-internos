package co.com.soaint.srvaprobgerente.web.api.rest;

import co.com.soaint.srvaprobgerente.commons.Dto.AprobacionGerenteDto;
import org.springframework.http.ResponseEntity;
public interface IAprobJefeInmediatoApi {

    ResponseEntity setAprobJefeInmediato(AprobacionGerenteDto aprobJefeInmediato);
    ResponseEntity findSolicitudByNumeroRadicado(String numeroRadicado);
}
