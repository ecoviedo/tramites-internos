package co.com.soaint.srvaprobgerente.repository.srvaprobgerente.impl;

import co.com.soaint.componentescomunes.entidades.GestionSolicitud;

public interface IGestionSolicitudRepositoryFacade {
    GestionSolicitud setAprobJefeInmediato(GestionSolicitud gestionSolicitud);

    GestionSolicitud findAprobJefeInmediato(Integer numeroSolicitud);

    Integer findIdTablaBasicaByCodigoApp(String codigoApp);
}
