package co.com.soaint.srvaprobgerente.repository.srvaprobgerente.impl;

import co.com.soaint.componentescomunes.entidades.Solicitud;

public interface ISolicitudRepositoryFacade {
        Solicitud findIdSolicitudByNumeroRadicado(String numeroRadicado);
        boolean updateSolicitud(Solicitud solicitud);
}
