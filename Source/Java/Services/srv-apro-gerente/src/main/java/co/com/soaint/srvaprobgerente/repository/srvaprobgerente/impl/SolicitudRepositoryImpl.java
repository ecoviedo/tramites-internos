package co.com.soaint.srvaprobgerente.repository.srvaprobgerente.impl;

import co.com.soaint.componentescomunes.entidades.Solicitud;
import co.com.soaint.srvaprobgerente.repository.srvaprobgerente.ISolicitudRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SolicitudRepositoryImpl implements  ISolicitudRepositoryFacade{
    private final ISolicitudRepository repository;
    @Autowired
    public SolicitudRepositoryImpl(ISolicitudRepository repository) {
        this.repository = repository;
    }

    @Override
    public Solicitud findIdSolicitudByNumeroRadicado(String numeroRadicado) {
        return repository.findByNumeroRadicado(numeroRadicado);
    }

    @Override
    public boolean updateSolicitud(Solicitud solicitud) {
        if(repository.save(solicitud) == null){
            return false;
        }else {
            return true;
        }
    }

}
