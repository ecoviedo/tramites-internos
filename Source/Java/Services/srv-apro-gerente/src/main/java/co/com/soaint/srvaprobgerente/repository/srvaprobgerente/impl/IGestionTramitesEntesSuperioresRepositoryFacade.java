package co.com.soaint.srvaprobgerente.repository.srvaprobgerente.impl;


import co.com.soaint.componentescomunes.entidades.GestionTramiteEnteSuperior;

public interface IGestionTramitesEntesSuperioresRepositoryFacade {
    void createGestionEnteSuperior(GestionTramiteEnteSuperior gestionTramiteEnteSuperior);
}
