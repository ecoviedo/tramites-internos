package co.com.soaint.srvaprobgerente.repository.srvaprobgerente.impl;

import co.com.soaint.componentescomunes.entidades.Empleado;
import co.com.soaint.srvaprobgerente.repository.srvaprobgerente.IEmpleadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EmpleadoRepostiroryImpl implements  IEmpleadoRepositoryFacade{
private final IEmpleadoRepository empleadoRepository;
    @Autowired
    public EmpleadoRepostiroryImpl(IEmpleadoRepository empleadoRepository) {
        this.empleadoRepository = empleadoRepository;
    }

    @Override
    public Empleado findEmpleadoByNumeroDocumento(String numeroDocumento) {
        return empleadoRepository.findByNumeroDocumento(numeroDocumento);
    }
}
