package co.com.soaint.srvaprobgerente.service.srvaprobgerente.impl;
import co.com.soaint.srvaprobgerente.commons.exception.webClient.BusinessException;
import co.com.soaint.srvaprobgerente.commons.Dto.AprobacionGerenteDto;
import co.com.soaint.srvaprobgerente.repository.srvaprobgerente.impl.IEmpleadoRepositoryFacade;
import co.com.soaint.srvaprobgerente.repository.srvaprobgerente.impl.IGestionSolicitudRepositoryFacade;
import co.com.soaint.srvaprobgerente.repository.srvaprobgerente.impl.IGestionTramitesEntesSuperioresRepositoryFacade;
import co.com.soaint.srvaprobgerente.repository.srvaprobgerente.impl.ISolicitudRepositoryFacade;
import co.com.soaint.srvaprobgerente.service.srvaprobgerente.IAprobGerente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class AprobGerenteImpl implements IAprobGerente {
    private final IGestionSolicitudRepositoryFacade gestionSolicitudRepositoryFacade;
    private final IGestionTramitesEntesSuperioresRepositoryFacade gestionTramitesEntesSuperioresRepositoryFacade;
    private final ISolicitudRepositoryFacade solicitudRepositoryFacade;
    private final IEmpleadoRepositoryFacade empleadoRepositoryFacade;
    @Autowired
    public AprobGerenteImpl(IGestionSolicitudRepositoryFacade gestionSolicitudRepositoryFacade, IGestionTramitesEntesSuperioresRepositoryFacade gestionTramitesEntesSuperioresRepositoryFacade, ISolicitudRepositoryFacade solicitudRepositoryFacade, IEmpleadoRepositoryFacade empleadoRepositoryFacade) {
        this.gestionSolicitudRepositoryFacade = gestionSolicitudRepositoryFacade;
        this.gestionTramitesEntesSuperioresRepositoryFacade = gestionTramitesEntesSuperioresRepositoryFacade;
        this.solicitudRepositoryFacade = solicitudRepositoryFacade;
        this.empleadoRepositoryFacade = empleadoRepositoryFacade;
    }


    @Transactional
    @Override
    public void setAprobGerente(AprobacionGerenteDto aprobGerente) throws Exception {
        Solicitud solicitud = solicitudRepositoryFacade.findIdSolicitudByNumeroRadicado(aprobGerente.getNumeroRadicado());
        if(solicitud == null) {
            throw BusinessException.notFound("Solicitud");
        }
        GestionSolicitud gestionSolicitud = new GestionSolicitud();
        gestionSolicitud.setAprobGerente(createGestionTramiteGerente(aprobGerente));
        solicitud.setGestionSolicitud(gestionSolicitudRepositoryFacade.setAprobJefeInmediato(gestionSolicitud));
        solicitudRepositoryFacade.updateSolicitud(solicitud);

    }

    @Override
    public AprobacionGerenteDto findAprobGerente(String numeroRadicado) throws Exception {
       Solicitud solicitud = solicitudRepositoryFacade.findIdSolicitudByNumeroRadicado(numeroRadicado);
        if(solicitud == null) {
            throw BusinessException.notFound("Solicitud");
        }
       GestionSolicitud gestionSolicitud = solicitud.getGestionSolicitud();
       AprobacionGerenteDto aprobacionGerenteDto = new AprobacionGerenteDto();
       aprobacionGerenteDto.setNumeroRadicado(solicitud.getNumeroRadicado());
       aprobacionGerenteDto.setEmpleadoRemplazo(gestionSolicitud.getAprobGerente().getEmpleadoReemplazo().getNumeroDocumento());
       aprobacionGerenteDto.setFechaAprobacion(gestionSolicitud.getAprobGerente().getFechaAprobacion());
        aprobacionGerenteDto.setObservaciones(gestionSolicitud.getAprobGerente().getObservaciones());
        aprobacionGerenteDto.setReemplazo(gestionSolicitud.getAprobGerente().getReemplazo());
        aprobacionGerenteDto.setRemunerado(gestionSolicitud.getAprobGerente().getRemunerado());
        aprobacionGerenteDto.setVistoBueno(gestionSolicitud.getAprobGerente().getVistoBueno().getCodigoApp());
        return aprobacionGerenteDto;
    }

    private GestionTramiteEnteSuperior createGestionTramiteGerente(AprobacionGerenteDto aprobGerente){
        GestionTramiteEnteSuperior gestionTramiteEnteSuperior = new GestionTramiteEnteSuperior();
        TablaBasica tablaBasica = new TablaBasica();
        tablaBasica.setIdTablaBasica(gestionSolicitudRepositoryFacade.findIdTablaBasicaByCodigoApp(aprobGerente.getVistoBueno()));
        if(aprobGerente.getEmpleadoRemplazo() != null){
            Empleado empleado = empleadoRepositoryFacade.findEmpleadoByNumeroDocumento(aprobGerente.getEmpleadoRemplazo());
            gestionTramiteEnteSuperior.setEmpleadoReemplazo(empleado);
        }
        gestionTramiteEnteSuperior.setFechaAprobacion(aprobGerente.getFechaAprobacion());
        gestionTramiteEnteSuperior.setObservaciones(aprobGerente.getObservaciones());
        gestionTramiteEnteSuperior.setReemplazo(aprobGerente.isReemplazo());
        gestionTramiteEnteSuperior.setRemunerado(aprobGerente.isRemunerado());
        gestionTramiteEnteSuperior.setVistoBueno(tablaBasica);
        gestionTramitesEntesSuperioresRepositoryFacade.createGestionEnteSuperior(gestionTramiteEnteSuperior);
        return gestionTramiteEnteSuperior;
    }
}
