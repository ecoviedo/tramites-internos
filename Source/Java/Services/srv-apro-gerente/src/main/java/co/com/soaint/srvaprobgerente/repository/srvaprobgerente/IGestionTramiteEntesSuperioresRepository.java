package co.com.soaint.srvaprobgerente.repository.srvaprobgerente;
import co.com.soaint.componentescomunes.entidades.GestionTramiteEnteSuperior;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IGestionTramiteEntesSuperioresRepository extends JpaRepository<GestionTramiteEnteSuperior, Integer> {

}
