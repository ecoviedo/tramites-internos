package co.com.soaint.srvdocumentos.repository.documentos.impl;

import co.com.soaint.componentescomunes.entidades.Documento;
import co.com.soaint.srvdocumentos.repository.documentos.IDocumentosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DocumentoRepositoryImpl implements IDocumentoRepositoryFacade {
    private final IDocumentosRepository repository;
    @Autowired
    public DocumentoRepositoryImpl(IDocumentosRepository repository) {
        this.repository = repository;
    }

    @Override
    public void createDocument(Documento documento) {
        repository.save(documento);
    }
}
