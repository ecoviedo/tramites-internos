package co.com.soaint.srvdocumentos.adapter.specific.ApiTablaBasica;

import co.com.soaint.componentescomunes.entidades.TablaBasica;
import co.com.soaint.srvdocumentos.commons.constants.api.certificado.EndpointCertificado;
import co.com.soaint.srvdocumentos.commons.exception.webClient.WebClientException;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.google.gson.Gson;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

@Component
@JsonAutoDetect(getterVisibility = JsonAutoDetect.Visibility.NON_PRIVATE)
public class ClienteTablaBasica implements IClienteTablaBasica {

    private RestTemplate template = new RestTemplate();

    @Override
    public TablaBasica findTablaBasicaByCodigoApp(String codigoApp) throws WebClientException{
        try {

            String url = EndpointCertificado.SRV_TABLAS_BASICAS+codigoApp;


            Gson gson = new Gson();
            URI uri = new URI(url);
            String responseService = template.getForObject(uri, String.class);
            TablaBasica [] response = gson.fromJson(responseService, TablaBasica[].class);
            return response[0];

        } catch (RestClientException | URISyntaxException e) {

            throw new WebClientException(" Error Invocando servicio Tablas Basicas: " + e.getMessage());
        }
    }
}