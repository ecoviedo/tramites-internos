package co.com.soaint.srvdocumentos.adapter.specific.ApiSolicitud;

import co.com.soaint.srvdocumentos.commons.exception.webClient.WebClientException;

public interface IClienteSolicitud {
    Integer findIdSolicitudByNumeroRadicado(String NumeroRadicado) throws WebClientException;
}
