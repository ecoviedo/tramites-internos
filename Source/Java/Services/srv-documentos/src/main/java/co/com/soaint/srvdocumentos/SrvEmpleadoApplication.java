package co.com.soaint.srvdocumentos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EntityScan(basePackages = {"co.com.soaint.componentescomunes.entidades"})
@EnableEurekaClient
@EnableJpaRepositories
@SpringBootApplication
public class SrvEmpleadoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SrvEmpleadoApplication.class, args);
    }

}
