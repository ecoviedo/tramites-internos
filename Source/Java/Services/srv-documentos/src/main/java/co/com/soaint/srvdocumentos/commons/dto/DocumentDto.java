package co.com.soaint.srvdocumentos.commons.dto;

import java.io.Serializable;

public class DocumentDto implements Serializable {
    public Integer idTablaBasica;
    public Integer idSolicitud;
}
