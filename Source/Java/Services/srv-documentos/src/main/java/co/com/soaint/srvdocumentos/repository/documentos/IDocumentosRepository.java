package co.com.soaint.srvdocumentos.repository.documentos;

import co.com.soaint.componentescomunes.entidades.Documento;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IDocumentosRepository extends JpaRepository<Documento,  Integer> {

}
