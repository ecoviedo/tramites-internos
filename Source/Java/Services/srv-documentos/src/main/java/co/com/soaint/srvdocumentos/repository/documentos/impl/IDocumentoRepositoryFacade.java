package co.com.soaint.srvdocumentos.repository.documentos.impl;

import co.com.soaint.componentescomunes.entidades.Documento;

public interface IDocumentoRepositoryFacade {
    public void createDocument(Documento documento);
}
