package co.com.soaint.srvdocumentos.commons.constants.api.certificado;

public interface EndpointCertificado {

    String SOLICITUD_API_V1 = "api/v1";
    String FIND_CERTIFICADO = "/";
    String CREATE_CERTIFICADO = "/certificado/";
    String UPDATE_CERTIFICADO = "/certificado/";
    String SRV_TABLAS_BASICAS = "http://localhost:8002/"+SOLICITUD_API_V1+FIND_CERTIFICADO;
}
