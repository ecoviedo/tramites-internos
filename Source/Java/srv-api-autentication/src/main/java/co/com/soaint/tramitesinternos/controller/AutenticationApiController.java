package co.com.soaint.tramitesinternos.controller;

import co.com.soaint.tramitesinternos.services.IServicesAutentication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/user")
@Component
@CrossOrigin(origins = "*", methods = {RequestMethod.GET})
public class AutenticationApiController {

    private final IServicesAutentication servicesAutentication;

    public AutenticationApiController(IServicesAutentication servicesAutentication) {
        this.servicesAutentication = servicesAutentication;
    }

    @GetMapping("/authentication")
    public ResponseEntity<?> autentication(HttpServletRequest request) throws Exception {
        return new ResponseEntity<>(servicesAutentication.autentication(request), HttpStatus.OK);
    }

    @GetMapping("/mail/")
    public ResponseEntity<?> getMail(@RequestParam String usuario) throws NamingException {
        return new ResponseEntity<>(servicesAutentication.findMailByUser(usuario), HttpStatus.OK);
    }
}
