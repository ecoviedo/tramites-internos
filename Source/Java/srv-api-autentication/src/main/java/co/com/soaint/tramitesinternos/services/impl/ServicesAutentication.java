package co.com.soaint.tramitesinternos.services.impl;

import co.com.soaint.tramitesinternos.commons.constant.Properties;
import co.com.soaint.tramitesinternos.commons.domains.util.LdapAuth;
import co.com.soaint.tramitesinternos.commons.dto.LdapDTO;
import co.com.soaint.tramitesinternos.commons.dto.ResponseDTO;
import co.com.soaint.tramitesinternos.commons.dto.StatusDTO;
import co.com.soaint.tramitesinternos.services.IServicesAutentication;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.servlet.http.HttpServletRequest;
import java.util.Base64;

@Service
@Component
public class ServicesAutentication implements IServicesAutentication {

    @Override
    public ResponseDTO autentication(HttpServletRequest request) throws Exception {
        //primero se captura el basi autentication y se decodifica
        String headerAuthorization = request.getHeader("authorization");
        String headerAuthorizationList[] = headerAuthorization.split(" ");
        byte[] decodedBytes = Base64.getDecoder().decode(headerAuthorizationList[1]);
        String decodedString = new String(decodedBytes);
        String credentialsList[] = decodedString.split(":");
        String usuario = credentialsList[0];
        String password = credentialsList[1];//decrypt("92AE31A79FEEB2A3", "0123456789ABCDEF", credentialsList[1]);

        //instancio el objeto LdapDTO y Lo cargo
        LdapDTO ldapDTO = new LdapDTO();
        LdapAuth ldapAuth = new LdapAuth();
        ldapDTO = ldapAuth.loadConfiguration();

        //luego de cargar las configuraciones iniciamos la conexion
        DirContext context = ldapAuth.inicializarConexion(ldapDTO);
        //valido si la conexion fue exitosa
        boolean validador = false;
        ResponseDTO responseDTO = new ResponseDTO();
        StatusDTO statusDTO = new StatusDTO();
        if (ldapDTO.isAutenticado()){
            //busco el usuario que quiere autenticar y valido si exixte
            String searchFilter  = Properties.CN_FILTER+usuario;
            SearchControls searchControls = new SearchControls();
            searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
            NamingEnumeration<SearchResult> results = context.search(Properties.USER_OU, searchFilter, searchControls);
            if (results.hasMoreElements()){
                SearchResult searchResult = null;
                //asigno las propiedades para luego saber la ruta y poder autenticar
                while (results.hasMoreElements()) {
                    searchResult = results.nextElement();
                }
                //capturo el atributo correo del usuario
                Attribute atribute = searchResult.getAttributes().get("mail");
                String email = atribute.get().toString();
                atribute = searchResult.getAttributes().get("name");
                String name = atribute.get().toString();
               //se inicia conexion con el usuario a autenticar
                ldapDTO.setDn(searchResult.getName()+","+Properties.USER_OU);
                ldapDTO.setClave(password);
                DirContext contextUserAut = ldapAuth.inicializarConexion(ldapDTO);
                if (ldapDTO.isAutenticado()){
                    validador=true;
                    statusDTO = StatusDTO.builder()
                            .status(HttpStatus.OK.name())
                            .statusCode(String.valueOf(HttpStatus.OK.value()))
                            .msg("AUTENTICACION CORRETA").build();
                    responseDTO = ResponseDTO.builder()
                            .usuario(name)
                            .mail(email)
                            .build();
                }
            }
        }
        if (!validador){
            statusDTO = StatusDTO.builder()
                    .status(HttpStatus.UNAUTHORIZED.name())
                    .statusCode(String.valueOf(HttpStatus.UNAUTHORIZED.value()))
                    .msg("ERROR DE AUTENTICACION").build();
        }
        responseDTO.setResponse(statusDTO);
        return responseDTO;
    }

    public String findMailByUser(String usuario) throws NamingException {
        //instancio el objeto LdapDTO y Lo cargo
        LdapDTO ldapDTO = new LdapDTO();
        LdapAuth ldapAuth = new LdapAuth();
        ldapDTO = ldapAuth.loadConfiguration();

        //luego de cargar las configuraciones iniciamos la conexion
        DirContext context = ldapAuth.inicializarConexion(ldapDTO);
        if (ldapDTO.isAutenticado()) {
            //busco el usuario que quiere autenticar y valido si exixte
            String searchFilter = Properties.CN_FILTER + usuario;
            SearchControls searchControls = new SearchControls();
            searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
            NamingEnumeration<SearchResult> results = context.search(Properties.USER_OU, searchFilter, searchControls);
            if (results.hasMoreElements()) {
                SearchResult searchResult = null;
                //asigno las propiedades para luego saber la ruta y poder autenticar
                while (results.hasMoreElements()) {
                    searchResult = results.nextElement();
                }
                //capturo el atributo correo del usuario
                Attribute atribute = searchResult.getAttributes().get("mail");
                String email = atribute.get().toString();
                return email;
            }
        }
        return "Fallo";
    }

    private String decrypt(String key, String iv, String encrypted) throws Exception {
        // Definición del tipo de algoritmo a utilizar (AES, DES, RSA)
        String alg = "AES";
        // Definición del modo de cifrado a utilizar
        String cI = "AES/CBC/PKCS5Padding";

        Cipher cipher = Cipher.getInstance(cI);
        SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), alg);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(iv.getBytes());
        byte[] enc = Base64.getDecoder().decode(encrypted);
        cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivParameterSpec);
        byte[] decrypted = cipher.doFinal(enc);
        return new String(decrypted);
    }
}
