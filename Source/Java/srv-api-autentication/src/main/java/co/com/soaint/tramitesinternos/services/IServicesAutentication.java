package co.com.soaint.tramitesinternos.services;

import co.com.soaint.tramitesinternos.commons.dto.ResponseDTO;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;

public interface IServicesAutentication {
    ResponseDTO autentication(HttpServletRequest request) throws Exception;

    String findMailByUser(String usuario) throws NamingException;
}
