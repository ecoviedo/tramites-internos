package co.com.soaint.tramitesinternos.commons.domains.util;

import co.com.soaint.tramitesinternos.commons.constant.Properties;
import co.com.soaint.tramitesinternos.commons.dto.LdapDTO;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import java.util.Hashtable;

public class LdapAuth {

    public LdapAuth (){
    }

    public LdapDTO loadConfiguration() {
        LdapDTO ldapDTO = new LdapDTO();
        ldapDTO.setServidor(Properties.SERVIDOR_LDAP);
        ldapDTO.setDn(Properties.LDAP_DN); //modificar y agregar en el properties
        ldapDTO.setTipoAuth("simple");
        ldapDTO.setClave(Properties.CREDENCIALES_DN);
        return ldapDTO;
    }

    public DirContext inicializarConexion(LdapDTO ldapDTO) {
        Hashtable env = new Hashtable();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, ldapDTO.getServidor());
        env.put(Context.SECURITY_AUTHENTICATION, ldapDTO.getTipoAuth());
        env.put(Context.SECURITY_PRINCIPAL, ldapDTO.getDn());
        env.put(Context.SECURITY_CREDENTIALS, ldapDTO.getClave());
        try {
            DirContext dc = new InitialDirContext(env);
            ldapDTO.setAutenticado(true);
            return dc;
        } catch (NamingException ex) {
            System.out.println("Error Autenticando mediante LDAP, Error causado por : " + ex.toString());
            ldapDTO.setAutenticado(false);
            return null;
        }
    }

}
