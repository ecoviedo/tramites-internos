package co.com.soaint.tramitesinternos.commons.constant;

public interface Properties {
    String SERVIDOR_LDAP = "ldap://192.168.1.200:389";
    String LDAP_DN = "CN=Administrator,CN=users,DC=SOAIN,DC=LCL";
    String USER_OU = "OU=COLOMBIA,OU=SOAINT,DC=SOAIN,DC=LCL";
    String CREDENCIALES_DN = "QsePliS041nt2008";
    String CN_FILTER = "sAMAccountName=";
}
