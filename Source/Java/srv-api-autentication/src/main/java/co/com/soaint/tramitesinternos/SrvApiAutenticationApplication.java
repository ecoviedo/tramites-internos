package co.com.soaint.tramitesinternos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class SrvApiAutenticationApplication {

    public static void main(String[] args) {
        SpringApplication.run(SrvApiAutenticationApplication.class, args);
    }

}
