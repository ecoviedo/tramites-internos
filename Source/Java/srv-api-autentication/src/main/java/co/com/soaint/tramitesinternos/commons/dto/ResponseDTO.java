package co.com.soaint.tramitesinternos.commons.dto;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class ResponseDTO {
    private String usuario;
    private String mail;
    private StatusDTO response;
}
