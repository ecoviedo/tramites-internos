package co.com.soaint.tramitesinternos.commons.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class LdapDTO {

    private String usuario;
    private String clave;
    private String servidor;
    private String dn;
    private String tipoAuth;
    private boolean autenticado;

    public boolean isAutenticado() {
        return autenticado;
    }

}
