package co.com.soaint.process.tramitesinternos.commons.dto;


import java.util.List;


public class ResponseDTO {
    List<NvlAprobacionDTO> nvlAprobacion;

    public ResponseDTO() {
    }

    public ResponseDTO(List<NvlAprobacionDTO> nvlAprobacion) {
        this.nvlAprobacion = nvlAprobacion;
    }

    public List<NvlAprobacionDTO> getNvlAprobacion() {
        return nvlAprobacion;
    }

    public void setNvlAprobacion(List<NvlAprobacionDTO> nvlAprobacion) {
        this.nvlAprobacion = nvlAprobacion;
    }
}
