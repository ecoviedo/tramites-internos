package co.com.soaint.process.tramitesinternos.commons.dto;



public class NvlAprobacionDTO {
    private String rol;
    private String nivel;
    private String usuario;
    private String email;

    public NvlAprobacionDTO() {
    }

    public NvlAprobacionDTO(String rol, String nivel, String usuario, String email) {
        this.rol = rol;
        this.nivel = nivel;
        this.usuario = usuario;
        this.email = email;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
