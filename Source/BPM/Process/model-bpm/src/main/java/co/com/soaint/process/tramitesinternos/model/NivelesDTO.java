package co.com.soaint.process.tramitesinternos.model;

import java.io.Serializable;

public class NivelesDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String estado;
    private String nombre;
    private String requerido;
    private String usuario;

    public NivelesDTO() {
    }

    public NivelesDTO(String estado, String nombre, String requerido, String usuario) {
        this.estado = estado;
        this.nombre = nombre;
        this.requerido = requerido;
        this.usuario = usuario;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRequerido() {
        return requerido;
    }

    public void setRequerido(String requerido) {
        this.requerido = requerido;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
}
