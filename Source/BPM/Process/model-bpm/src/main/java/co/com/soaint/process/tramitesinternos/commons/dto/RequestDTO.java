package co.com.soaint.process.tramitesinternos.commons.dto;

import java.util.List;


public class RequestDTO {
    private String area;
    private List<NvlAprobacionDTO> nvlAprobacion;

    public RequestDTO() {
    }

    public RequestDTO(String area, List<NvlAprobacionDTO> nvlAprobacion) {
        this.area = area;
        this.nvlAprobacion = nvlAprobacion;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public List<NvlAprobacionDTO> getNvlAprobacion() {
        return nvlAprobacion;
    }

    public void setNvlAprobacion(List<NvlAprobacionDTO> nvlAprobacion) {
        this.nvlAprobacion = nvlAprobacion;
    }
}
