package co.com.soaint.tramitesinternos.controller;

import co.com.soaint.process.tramitesinternos.commons.dto.ResponseDTO;
import co.com.soaint.process.tramitesinternos.model.NivelesDTO;
import co.com.soaint.tramitesinternos.services.ServiceCustom;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemHandler;
import org.kie.api.runtime.process.WorkItemManager;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
public class CustomWorkItem implements WorkItemHandler {

    private final ServiceCustom serviceCustom = new ServiceCustom();

    public void executeWorkItem(WorkItem workItem, WorkItemManager workItemManager) {
        String operation = (String) workItem.getParameter("Operation");
        String response = "";
        switch (operation) {
            case "Usuarios":
                String area = (String) workItem.getParameter("Area");
                HashMap<String, NivelesDTO> request = (HashMap<String, NivelesDTO>) workItem.getParameter("Request");
                response = serviceCustom.findUser(request, area);
                break;
            case "notificar":
                String mail = (String) workItem.getParameter("Mail");
                String body = (String) workItem.getParameter("Body");
                List<String> docNames = (List<String>) workItem.getParameter("DocNames");
                response = serviceCustom.sendMail(mail,Optional.ofNullable(docNames), body);
                break;
            default:
                break;
        }
        //Gson gson = new Gson();
        //String response = gson.toJson(responseDTO);
        Map<String, Object> mapa = new HashMap<String, Object>();
        mapa.put("Result", response);

        workItemManager.completeWorkItem(workItem.getId(), mapa);
    }

    public void abortWorkItem(WorkItem workItem, WorkItemManager workItemManager) {
        Map<String, Object> mapa = new HashMap<String, Object>();
        mapa.put("Result", "Error");

        workItemManager.completeWorkItem(workItem.getId(), mapa);
    }
}
