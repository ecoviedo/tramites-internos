package co.com.soaint.tramitesinternos.services;

import co.com.soaint.process.tramitesinternos.commons.dto.NvlAprobacionDTO;
import co.com.soaint.process.tramitesinternos.commons.dto.RequestDTO;
import co.com.soaint.process.tramitesinternos.model.NivelesDTO;
import co.com.soaint.tramitesinternos.commons.constant.Endpoint;
import co.com.soaint.tramitesinternos.commons.constant.Path;
import com.google.gson.Gson;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.*;

@Component
public class ServiceCustom {


    public String findUser(HashMap<String, NivelesDTO> map, String area) {
        String url = Endpoint.ENDPOINT_BASE;
        List<NvlAprobacionDTO> nvlAprobacionDTOS = new ArrayList<>();
        RequestDTO requestDTO = new RequestDTO();
        requestDTO.setArea(area);
        for (Map.Entry<String, NivelesDTO> entry : map.entrySet()) {
            NvlAprobacionDTO aprobacionDTO = new NvlAprobacionDTO();
            NivelesDTO nivelesDTO = entry.getValue();
            aprobacionDTO.setNivel(entry.getKey());
            aprobacionDTO.setRol(nivelesDTO.getNombre());
            if (nivelesDTO.getNombre().equalsIgnoreCase("JefeInmediato")) {
                aprobacionDTO.setUsuario(nivelesDTO.getUsuario());
            }
            nvlAprobacionDTOS.add(aprobacionDTO);
        }
        Gson gson = new Gson();
        requestDTO.setNvlAprobacion(nvlAprobacionDTOS);
        RestTemplate clientConsumer = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.ALL_VALUE);
        headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        String body = gson.toJson(requestDTO);
        ResponseEntity<String> responseEntity = clientConsumer.exchange(url, HttpMethod.POST, new HttpEntity(body, headers), String.class);
        String response = responseEntity.getBody();
        //ResponseDTO responseDTO = gson.fromJson(responseEntity.getBody(), ResponseDTO.class);
        return response;
    }

    public String sendMail(String mail, Optional<List<String>> docName, String body){

        try {
            Send(docName, mail, body);
            return "OK";
        }catch (Exception e){
            System.out.println("las causas son: "+e.getCause());
            return "Fallo";
        }
    }

    private void Send(Optional<List<String>> docName, String mail, String body){
        try {
            Properties props = new Properties();
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.setProperty("mail.smtp.starttls.enable", "true");
            props.setProperty("mail.smtp.port", "587");
            props.setProperty("mail.smtp.user", "direccion@gmail.com");
            props.setProperty("mail.smtp.auth", "true");

            Session session = Session.getDefaultInstance(props, null);
            // session.setDebug(true);

            // Se compone la parte del texto
            BodyPart texto = new MimeBodyPart();
            texto.setText(body);

            // Una MultiParte para agrupar texto e imagen.
            MimeMultipart multiParte = new MimeMultipart();
            multiParte.addBodyPart(texto);
            if (docName.isPresent()) {
                List<BodyPart> bodyPartList = new ArrayList<>();
                // Se compone el adjunto por cada uno de los archivos  se carga en una lista
                for (String name : docName.get()) {
                    BodyPart adjunto = new MimeBodyPart();
                    adjunto.setDataHandler(
                            new DataHandler(new FileDataSource(Path.Path_Document.concat(name))));
                    adjunto.setFileName(name);
                    bodyPartList.add(adjunto);
                }
                for (BodyPart bodyPart: bodyPartList) {
                    multiParte.addBodyPart(bodyPart);
                }
            }

            // Se compone el correo, dando to, from, subject y el
            // contenido.
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress("pruebasdeconcepto2019@gmail.com"));
            message.addRecipient(
                    Message.RecipientType.TO,
                    new InternetAddress(mail));
            message.setSubject("Respuesta del trámite interno");
            message.setContent(multiParte);

            // Se envia el correo.
            Transport t = session.getTransport("smtp");
            t.connect("pruebasdeconcepto2019@gmail.com", "Pruebas_2019");
            t.sendMessage(message, message.getAllRecipients());
            t.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

}
